<?php
include_once __DIR__ . '/../../vendor/autoload.php';

define("COMMERCE_WSDL", "https://ws.hubsoft.com/services/CommerceService?wsdl");
define("INTEGRATION_WSDL", "https://ws.hubsoft.com/services/IntegrationServiceV3?wsdl");
define("SITE_WSDL", "https://ws.hubsoft.com/services/SiteService?wsdl");

function generateIntegrationService() {
    $generator = new \Wsdl2PhpGenerator\Generator();
    $generator->generate( new \Wsdl2PhpGenerator\Config([
        'inputFile' => INTEGRATION_WSDL,
        'outputDir' => __DIR__ . '/../hubsoft/integrationservice',
        'namespaceName' => 'hubsoft\api\integrationservice'
    ]));
}

function generateCommerceService() {
    $generator = new \Wsdl2PhpGenerator\Generator();
    $generator->generate( new \Wsdl2PhpGenerator\Config([
        'inputFile'     => COMMERCE_WSDL,
        'outputDir'     => __DIR__ . '/../hubsoft/commerce',
        "namespaceName" => 'hubsoft\api\commerce'
    ]));
}

function generateSiteService() {
    $generator = new \Wsdl2PhpGenerator\Generator();
    $generator->generate( new \Wsdl2PhpGenerator\Config([
        'inputFile'     => SITE_WSDL,
        'outputDir'     => __DIR__ . '/../hubsoft/site',
        "namespaceName" => 'hubsoft\api\site'
    ]));
}


if (!isset($argv[1])) {
    die("No option specified!\n");
}

switch ($argv[1]) {
    case "commerce" :
        generateCommerceService();
        break;
    case "integration":
        generateIntegrationService();
        break;
    case "site":
        generateSiteService();
        break;
    case "all":
        generateCommerceService();
        generateIntegrationService();
        generateSiteService();
        break;
    default:
        die("No command matched!");
}


