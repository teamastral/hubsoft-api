<?php

namespace hubsoft\api\commerce;

class ArrayOfAvailableContestDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AvailableContestDTO[] $AvailableContestDTO
     */
    protected $AvailableContestDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AvailableContestDTO[]
     */
    public function getAvailableContestDTO()
    {
      return $this->AvailableContestDTO;
    }

    /**
     * @param AvailableContestDTO[] $AvailableContestDTO
     * @return \hubsoft\api\commerce\ArrayOfAvailableContestDTO
     */
    public function setAvailableContestDTO(array $AvailableContestDTO = null)
    {
      $this->AvailableContestDTO = $AvailableContestDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AvailableContestDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AvailableContestDTO
     */
    public function offsetGet($offset)
    {
      return $this->AvailableContestDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AvailableContestDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AvailableContestDTO[] = $value;
      } else {
        $this->AvailableContestDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AvailableContestDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AvailableContestDTO Return the current element
     */
    public function current()
    {
      return current($this->AvailableContestDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AvailableContestDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AvailableContestDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AvailableContestDTO);
    }

    /**
     * Countable implementation
     *
     * @return AvailableContestDTO Return count of elements
     */
    public function count()
    {
      return count($this->AvailableContestDTO);
    }

}
