<?php

namespace hubsoft\api\commerce;

class ArrayOfAvailableSchoolDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AvailableSchoolDTO[] $AvailableSchoolDTO
     */
    protected $AvailableSchoolDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AvailableSchoolDTO[]
     */
    public function getAvailableSchoolDTO()
    {
      return $this->AvailableSchoolDTO;
    }

    /**
     * @param AvailableSchoolDTO[] $AvailableSchoolDTO
     * @return \hubsoft\api\commerce\ArrayOfAvailableSchoolDTO
     */
    public function setAvailableSchoolDTO(array $AvailableSchoolDTO = null)
    {
      $this->AvailableSchoolDTO = $AvailableSchoolDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AvailableSchoolDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AvailableSchoolDTO
     */
    public function offsetGet($offset)
    {
      return $this->AvailableSchoolDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AvailableSchoolDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AvailableSchoolDTO[] = $value;
      } else {
        $this->AvailableSchoolDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AvailableSchoolDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AvailableSchoolDTO Return the current element
     */
    public function current()
    {
      return current($this->AvailableSchoolDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AvailableSchoolDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AvailableSchoolDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AvailableSchoolDTO);
    }

    /**
     * Countable implementation
     *
     * @return AvailableSchoolDTO Return count of elements
     */
    public function count()
    {
      return count($this->AvailableSchoolDTO);
    }

}
