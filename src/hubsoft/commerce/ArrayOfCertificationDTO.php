<?php

namespace hubsoft\api\commerce;

class ArrayOfCertificationDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CertificationDTO[] $CertificationDTO
     */
    protected $CertificationDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CertificationDTO[]
     */
    public function getCertificationDTO()
    {
      return $this->CertificationDTO;
    }

    /**
     * @param CertificationDTO[] $CertificationDTO
     * @return \hubsoft\api\commerce\ArrayOfCertificationDTO
     */
    public function setCertificationDTO(array $CertificationDTO = null)
    {
      $this->CertificationDTO = $CertificationDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CertificationDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CertificationDTO
     */
    public function offsetGet($offset)
    {
      return $this->CertificationDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CertificationDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->CertificationDTO[] = $value;
      } else {
        $this->CertificationDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CertificationDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CertificationDTO Return the current element
     */
    public function current()
    {
      return current($this->CertificationDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CertificationDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CertificationDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CertificationDTO);
    }

    /**
     * Countable implementation
     *
     * @return CertificationDTO Return count of elements
     */
    public function count()
    {
      return count($this->CertificationDTO);
    }

}
