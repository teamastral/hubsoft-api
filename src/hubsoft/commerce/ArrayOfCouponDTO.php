<?php

namespace hubsoft\api\commerce;

class ArrayOfCouponDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CouponDTO[] $CouponDTO
     */
    protected $CouponDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CouponDTO[]
     */
    public function getCouponDTO()
    {
      return $this->CouponDTO;
    }

    /**
     * @param CouponDTO[] $CouponDTO
     * @return \hubsoft\api\commerce\ArrayOfCouponDTO
     */
    public function setCouponDTO(array $CouponDTO = null)
    {
      $this->CouponDTO = $CouponDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CouponDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CouponDTO
     */
    public function offsetGet($offset)
    {
      return $this->CouponDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CouponDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->CouponDTO[] = $value;
      } else {
        $this->CouponDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CouponDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CouponDTO Return the current element
     */
    public function current()
    {
      return current($this->CouponDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CouponDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CouponDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CouponDTO);
    }

    /**
     * Countable implementation
     *
     * @return CouponDTO Return count of elements
     */
    public function count()
    {
      return count($this->CouponDTO);
    }

}
