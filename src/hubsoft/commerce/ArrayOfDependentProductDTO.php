<?php

namespace hubsoft\api\commerce;

class ArrayOfDependentProductDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DependentProductDTO[] $DependentProductDTO
     */
    protected $DependentProductDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DependentProductDTO[]
     */
    public function getDependentProductDTO()
    {
      return $this->DependentProductDTO;
    }

    /**
     * @param DependentProductDTO[] $DependentProductDTO
     * @return \hubsoft\api\commerce\ArrayOfDependentProductDTO
     */
    public function setDependentProductDTO(array $DependentProductDTO = null)
    {
      $this->DependentProductDTO = $DependentProductDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DependentProductDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DependentProductDTO
     */
    public function offsetGet($offset)
    {
      return $this->DependentProductDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DependentProductDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DependentProductDTO[] = $value;
      } else {
        $this->DependentProductDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DependentProductDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DependentProductDTO Return the current element
     */
    public function current()
    {
      return current($this->DependentProductDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DependentProductDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DependentProductDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DependentProductDTO);
    }

    /**
     * Countable implementation
     *
     * @return DependentProductDTO Return count of elements
     */
    public function count()
    {
      return count($this->DependentProductDTO);
    }

}
