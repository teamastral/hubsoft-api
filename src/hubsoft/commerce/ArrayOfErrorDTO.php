<?php

namespace hubsoft\api\commerce;

class ArrayOfErrorDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ErrorDTO[] $ErrorDTO
     */
    protected $ErrorDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ErrorDTO[]
     */
    public function getErrorDTO()
    {
      return $this->ErrorDTO;
    }

    /**
     * @param ErrorDTO[] $ErrorDTO
     * @return \hubsoft\api\commerce\ArrayOfErrorDTO
     */
    public function setErrorDTO(array $ErrorDTO = null)
    {
      $this->ErrorDTO = $ErrorDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ErrorDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ErrorDTO
     */
    public function offsetGet($offset)
    {
      return $this->ErrorDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ErrorDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ErrorDTO[] = $value;
      } else {
        $this->ErrorDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ErrorDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ErrorDTO Return the current element
     */
    public function current()
    {
      return current($this->ErrorDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ErrorDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ErrorDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ErrorDTO);
    }

    /**
     * Countable implementation
     *
     * @return ErrorDTO Return count of elements
     */
    public function count()
    {
      return count($this->ErrorDTO);
    }

}
