<?php

namespace hubsoft\api\commerce;

class ArrayOfOrderSummaryDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var OrderSummaryDTO[] $OrderSummaryDTO
     */
    protected $OrderSummaryDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return OrderSummaryDTO[]
     */
    public function getOrderSummaryDTO()
    {
      return $this->OrderSummaryDTO;
    }

    /**
     * @param OrderSummaryDTO[] $OrderSummaryDTO
     * @return \hubsoft\api\commerce\ArrayOfOrderSummaryDTO
     */
    public function setOrderSummaryDTO(array $OrderSummaryDTO = null)
    {
      $this->OrderSummaryDTO = $OrderSummaryDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->OrderSummaryDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return OrderSummaryDTO
     */
    public function offsetGet($offset)
    {
      return $this->OrderSummaryDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param OrderSummaryDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->OrderSummaryDTO[] = $value;
      } else {
        $this->OrderSummaryDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->OrderSummaryDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return OrderSummaryDTO Return the current element
     */
    public function current()
    {
      return current($this->OrderSummaryDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->OrderSummaryDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->OrderSummaryDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->OrderSummaryDTO);
    }

    /**
     * Countable implementation
     *
     * @return OrderSummaryDTO Return count of elements
     */
    public function count()
    {
      return count($this->OrderSummaryDTO);
    }

}
