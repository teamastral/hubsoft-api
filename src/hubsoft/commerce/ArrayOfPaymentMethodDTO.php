<?php

namespace hubsoft\api\commerce;

class ArrayOfPaymentMethodDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var PaymentMethodDTO[] $PaymentMethodDTO
     */
    protected $PaymentMethodDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return PaymentMethodDTO[]
     */
    public function getPaymentMethodDTO()
    {
      return $this->PaymentMethodDTO;
    }

    /**
     * @param PaymentMethodDTO[] $PaymentMethodDTO
     * @return \hubsoft\api\commerce\ArrayOfPaymentMethodDTO
     */
    public function setPaymentMethodDTO(array $PaymentMethodDTO = null)
    {
      $this->PaymentMethodDTO = $PaymentMethodDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->PaymentMethodDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return PaymentMethodDTO
     */
    public function offsetGet($offset)
    {
      return $this->PaymentMethodDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param PaymentMethodDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->PaymentMethodDTO[] = $value;
      } else {
        $this->PaymentMethodDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->PaymentMethodDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return PaymentMethodDTO Return the current element
     */
    public function current()
    {
      return current($this->PaymentMethodDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->PaymentMethodDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->PaymentMethodDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->PaymentMethodDTO);
    }

    /**
     * Countable implementation
     *
     * @return PaymentMethodDTO Return count of elements
     */
    public function count()
    {
      return count($this->PaymentMethodDTO);
    }

}
