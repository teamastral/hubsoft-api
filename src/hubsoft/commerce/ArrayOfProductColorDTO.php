<?php

namespace hubsoft\api\commerce;

class ArrayOfProductColorDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ProductColorDTO[] $ProductColorDTO
     */
    protected $ProductColorDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ProductColorDTO[]
     */
    public function getProductColorDTO()
    {
      return $this->ProductColorDTO;
    }

    /**
     * @param ProductColorDTO[] $ProductColorDTO
     * @return \hubsoft\api\commerce\ArrayOfProductColorDTO
     */
    public function setProductColorDTO(array $ProductColorDTO = null)
    {
      $this->ProductColorDTO = $ProductColorDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ProductColorDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ProductColorDTO
     */
    public function offsetGet($offset)
    {
      return $this->ProductColorDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ProductColorDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ProductColorDTO[] = $value;
      } else {
        $this->ProductColorDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ProductColorDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ProductColorDTO Return the current element
     */
    public function current()
    {
      return current($this->ProductColorDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ProductColorDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ProductColorDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ProductColorDTO);
    }

    /**
     * Countable implementation
     *
     * @return ProductColorDTO Return count of elements
     */
    public function count()
    {
      return count($this->ProductColorDTO);
    }

}
