<?php

namespace hubsoft\api\commerce;

class ArrayOfRelatedProductNumber implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var RelatedProductNumber[] $RelatedProductNumber
     */
    protected $RelatedProductNumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RelatedProductNumber[]
     */
    public function getRelatedProductNumber()
    {
      return $this->RelatedProductNumber;
    }

    /**
     * @param RelatedProductNumber[] $RelatedProductNumber
     * @return \hubsoft\api\commerce\ArrayOfRelatedProductNumber
     */
    public function setRelatedProductNumber(array $RelatedProductNumber = null)
    {
      $this->RelatedProductNumber = $RelatedProductNumber;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->RelatedProductNumber[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return RelatedProductNumber
     */
    public function offsetGet($offset)
    {
      return $this->RelatedProductNumber[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param RelatedProductNumber $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->RelatedProductNumber[] = $value;
      } else {
        $this->RelatedProductNumber[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->RelatedProductNumber[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return RelatedProductNumber Return the current element
     */
    public function current()
    {
      return current($this->RelatedProductNumber);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->RelatedProductNumber);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->RelatedProductNumber);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->RelatedProductNumber);
    }

    /**
     * Countable implementation
     *
     * @return RelatedProductNumber Return count of elements
     */
    public function count()
    {
      return count($this->RelatedProductNumber);
    }

}
