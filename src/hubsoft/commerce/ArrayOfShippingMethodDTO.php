<?php

namespace hubsoft\api\commerce;

class ArrayOfShippingMethodDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ShippingMethodDTO[] $ShippingMethodDTO
     */
    protected $ShippingMethodDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ShippingMethodDTO[]
     */
    public function getShippingMethodDTO()
    {
      return $this->ShippingMethodDTO;
    }

    /**
     * @param ShippingMethodDTO[] $ShippingMethodDTO
     * @return \hubsoft\api\commerce\ArrayOfShippingMethodDTO
     */
    public function setShippingMethodDTO(array $ShippingMethodDTO = null)
    {
      $this->ShippingMethodDTO = $ShippingMethodDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ShippingMethodDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ShippingMethodDTO
     */
    public function offsetGet($offset)
    {
      return $this->ShippingMethodDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ShippingMethodDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ShippingMethodDTO[] = $value;
      } else {
        $this->ShippingMethodDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ShippingMethodDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ShippingMethodDTO Return the current element
     */
    public function current()
    {
      return current($this->ShippingMethodDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ShippingMethodDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ShippingMethodDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ShippingMethodDTO);
    }

    /**
     * Countable implementation
     *
     * @return ShippingMethodDTO Return count of elements
     */
    public function count()
    {
      return count($this->ShippingMethodDTO);
    }

}
