<?php

namespace hubsoft\api\commerce;

class ArrayOfTechnologyDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var TechnologyDTO[] $TechnologyDTO
     */
    protected $TechnologyDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return TechnologyDTO[]
     */
    public function getTechnologyDTO()
    {
      return $this->TechnologyDTO;
    }

    /**
     * @param TechnologyDTO[] $TechnologyDTO
     * @return \hubsoft\api\commerce\ArrayOfTechnologyDTO
     */
    public function setTechnologyDTO(array $TechnologyDTO = null)
    {
      $this->TechnologyDTO = $TechnologyDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->TechnologyDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return TechnologyDTO
     */
    public function offsetGet($offset)
    {
      return $this->TechnologyDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param TechnologyDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->TechnologyDTO[] = $value;
      } else {
        $this->TechnologyDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->TechnologyDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return TechnologyDTO Return the current element
     */
    public function current()
    {
      return current($this->TechnologyDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->TechnologyDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->TechnologyDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->TechnologyDTO);
    }

    /**
     * Countable implementation
     *
     * @return TechnologyDTO Return count of elements
     */
    public function count()
    {
      return count($this->TechnologyDTO);
    }

}
