<?php

namespace hubsoft\api\commerce;

class ArrayOfTrainingProgressDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var TrainingProgressDTO[] $TrainingProgressDTO
     */
    protected $TrainingProgressDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return TrainingProgressDTO[]
     */
    public function getTrainingProgressDTO()
    {
      return $this->TrainingProgressDTO;
    }

    /**
     * @param TrainingProgressDTO[] $TrainingProgressDTO
     * @return \hubsoft\api\commerce\ArrayOfTrainingProgressDTO
     */
    public function setTrainingProgressDTO(array $TrainingProgressDTO = null)
    {
      $this->TrainingProgressDTO = $TrainingProgressDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->TrainingProgressDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return TrainingProgressDTO
     */
    public function offsetGet($offset)
    {
      return $this->TrainingProgressDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param TrainingProgressDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->TrainingProgressDTO[] = $value;
      } else {
        $this->TrainingProgressDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->TrainingProgressDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return TrainingProgressDTO Return the current element
     */
    public function current()
    {
      return current($this->TrainingProgressDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->TrainingProgressDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->TrainingProgressDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->TrainingProgressDTO);
    }

    /**
     * Countable implementation
     *
     * @return TrainingProgressDTO Return count of elements
     */
    public function count()
    {
      return count($this->TrainingProgressDTO);
    }

}
