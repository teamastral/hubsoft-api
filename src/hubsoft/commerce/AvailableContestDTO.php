<?php

namespace hubsoft\api\commerce;

class AvailableContestDTO
{

    /**
     * @var int $contestId
     */
    protected $contestId = null;

    /**
     * @var int $totalPoints
     */
    protected $totalPoints = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getContestId()
    {
      return $this->contestId;
    }

    /**
     * @param int $contestId
     * @return \hubsoft\api\commerce\AvailableContestDTO
     */
    public function setContestId($contestId)
    {
      $this->contestId = $contestId;
      return $this;
    }

    /**
     * @return int
     */
    public function getTotalPoints()
    {
      return $this->totalPoints;
    }

    /**
     * @param int $totalPoints
     * @return \hubsoft\api\commerce\AvailableContestDTO
     */
    public function setTotalPoints($totalPoints)
    {
      $this->totalPoints = $totalPoints;
      return $this;
    }

}
