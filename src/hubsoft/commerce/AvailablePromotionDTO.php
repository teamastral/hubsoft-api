<?php

namespace hubsoft\api\commerce;

class AvailablePromotionDTO
{

    /**
     * @var string $expiryDate
     */
    protected $expiryDate = null;

    /**
     * @var string $promotionCode
     */
    protected $promotionCode = null;

    /**
     * @var string $promotionName
     */
    protected $promotionName = null;

    /**
     * @var string $promotionTypeCode
     */
    protected $promotionTypeCode = null;

    /**
     * @var float $remainingAmount
     */
    protected $remainingAmount = null;

    /**
     * @var int $remainingUnits
     */
    protected $remainingUnits = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getExpiryDate()
    {
      return $this->expiryDate;
    }

    /**
     * @param string $expiryDate
     * @return \hubsoft\api\commerce\AvailablePromotionDTO
     */
    public function setExpiryDate($expiryDate)
    {
      $this->expiryDate = $expiryDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->promotionCode;
    }

    /**
     * @param string $promotionCode
     * @return \hubsoft\api\commerce\AvailablePromotionDTO
     */
    public function setPromotionCode($promotionCode)
    {
      $this->promotionCode = $promotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionName()
    {
      return $this->promotionName;
    }

    /**
     * @param string $promotionName
     * @return \hubsoft\api\commerce\AvailablePromotionDTO
     */
    public function setPromotionName($promotionName)
    {
      $this->promotionName = $promotionName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionTypeCode()
    {
      return $this->promotionTypeCode;
    }

    /**
     * @param string $promotionTypeCode
     * @return \hubsoft\api\commerce\AvailablePromotionDTO
     */
    public function setPromotionTypeCode($promotionTypeCode)
    {
      $this->promotionTypeCode = $promotionTypeCode;
      return $this;
    }

    /**
     * @return float
     */
    public function getRemainingAmount()
    {
      return $this->remainingAmount;
    }

    /**
     * @param float $remainingAmount
     * @return \hubsoft\api\commerce\AvailablePromotionDTO
     */
    public function setRemainingAmount($remainingAmount)
    {
      $this->remainingAmount = $remainingAmount;
      return $this;
    }

    /**
     * @return int
     */
    public function getRemainingUnits()
    {
      return $this->remainingUnits;
    }

    /**
     * @param int $remainingUnits
     * @return \hubsoft\api\commerce\AvailablePromotionDTO
     */
    public function setRemainingUnits($remainingUnits)
    {
      $this->remainingUnits = $remainingUnits;
      return $this;
    }

}
