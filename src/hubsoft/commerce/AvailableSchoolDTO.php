<?php

namespace hubsoft\api\commerce;

class AvailableSchoolDTO
{

    /**
     * @var int $schoolUid
     */
    protected $schoolUid = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getSchoolUid()
    {
      return $this->schoolUid;
    }

    /**
     * @param int $schoolUid
     * @return \hubsoft\api\commerce\AvailableSchoolDTO
     */
    public function setSchoolUid($schoolUid)
    {
      $this->schoolUid = $schoolUid;
      return $this;
    }

}
