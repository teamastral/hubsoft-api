<?php

namespace hubsoft\api\commerce;

class BillingDTO
{

    /**
     * @var string $cardHolderFirstName
     */
    protected $cardHolderFirstName = null;

    /**
     * @var string $cardHolderLastName
     */
    protected $cardHolderLastName = null;

    /**
     * @var string $cardNumber
     */
    protected $cardNumber = null;

    /**
     * @var string $city
     */
    protected $city = null;

    /**
     * @var string $countryCode
     */
    protected $countryCode = null;

    /**
     * @var string $expiryMonth
     */
    protected $expiryMonth = null;

    /**
     * @var string $expiryYear
     */
    protected $expiryYear = null;

    /**
     * @var float $giftCardAmount
     */
    protected $giftCardAmount = null;

    /**
     * @var string $giftCardNumber
     */
    protected $giftCardNumber = null;

    /**
     * @var string $gitCardPaymentReferenceNum
     */
    protected $gitCardPaymentReferenceNum = null;

    /**
     * @var string $paymentTypeCode
     */
    protected $paymentTypeCode = null;

    /**
     * @var string $postalCode
     */
    protected $postalCode = null;

    /**
     * @var string $securityCode
     */
    protected $securityCode = null;

    /**
     * @var string $stateCode
     */
    protected $stateCode = null;

    /**
     * @var string $street
     */
    protected $street = null;

    /**
     * @var string $street2
     */
    protected $street2 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCardHolderFirstName()
    {
      return $this->cardHolderFirstName;
    }

    /**
     * @param string $cardHolderFirstName
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setCardHolderFirstName($cardHolderFirstName)
    {
      $this->cardHolderFirstName = $cardHolderFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardHolderLastName()
    {
      return $this->cardHolderLastName;
    }

    /**
     * @param string $cardHolderLastName
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setCardHolderLastName($cardHolderLastName)
    {
      $this->cardHolderLastName = $cardHolderLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardNumber()
    {
      return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setCardNumber($cardNumber)
    {
      $this->cardNumber = $cardNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
      return $this->city;
    }

    /**
     * @param string $city
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setCity($city)
    {
      $this->city = $city;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
      return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setCountryCode($countryCode)
    {
      $this->countryCode = $countryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getExpiryMonth()
    {
      return $this->expiryMonth;
    }

    /**
     * @param string $expiryMonth
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setExpiryMonth($expiryMonth)
    {
      $this->expiryMonth = $expiryMonth;
      return $this;
    }

    /**
     * @return string
     */
    public function getExpiryYear()
    {
      return $this->expiryYear;
    }

    /**
     * @param string $expiryYear
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setExpiryYear($expiryYear)
    {
      $this->expiryYear = $expiryYear;
      return $this;
    }

    /**
     * @return float
     */
    public function getGiftCardAmount()
    {
      return $this->giftCardAmount;
    }

    /**
     * @param float $giftCardAmount
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setGiftCardAmount($giftCardAmount)
    {
      $this->giftCardAmount = $giftCardAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getGiftCardNumber()
    {
      return $this->giftCardNumber;
    }

    /**
     * @param string $giftCardNumber
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setGiftCardNumber($giftCardNumber)
    {
      $this->giftCardNumber = $giftCardNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getGitCardPaymentReferenceNum()
    {
      return $this->gitCardPaymentReferenceNum;
    }

    /**
     * @param string $gitCardPaymentReferenceNum
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setGitCardPaymentReferenceNum($gitCardPaymentReferenceNum)
    {
      $this->gitCardPaymentReferenceNum = $gitCardPaymentReferenceNum;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentTypeCode()
    {
      return $this->paymentTypeCode;
    }

    /**
     * @param string $paymentTypeCode
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setPaymentTypeCode($paymentTypeCode)
    {
      $this->paymentTypeCode = $paymentTypeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
      return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setPostalCode($postalCode)
    {
      $this->postalCode = $postalCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSecurityCode()
    {
      return $this->securityCode;
    }

    /**
     * @param string $securityCode
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setSecurityCode($securityCode)
    {
      $this->securityCode = $securityCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStateCode()
    {
      return $this->stateCode;
    }

    /**
     * @param string $stateCode
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setStateCode($stateCode)
    {
      $this->stateCode = $stateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
      return $this->street;
    }

    /**
     * @param string $street
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setStreet($street)
    {
      $this->street = $street;
      return $this;
    }

    /**
     * @return string
     */
    public function getStreet2()
    {
      return $this->street2;
    }

    /**
     * @param string $street2
     * @return \hubsoft\api\commerce\BillingDTO
     */
    public function setStreet2($street2)
    {
      $this->street2 = $street2;
      return $this;
    }

}
