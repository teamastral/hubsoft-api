<?php

namespace hubsoft\api\commerce;

class CertificationDTO
{

    /**
     * @var string $certification
     */
    protected $certification = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCertification()
    {
      return $this->certification;
    }

    /**
     * @param string $certification
     * @return \hubsoft\api\commerce\CertificationDTO
     */
    public function setCertification($certification)
    {
      $this->certification = $certification;
      return $this;
    }

}
