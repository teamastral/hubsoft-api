<?php

namespace hubsoft\api\commerce;

class CheckPromotionLimitsResponse
{

    /**
     * @var ArrayOfErrorDTO $errors
     */
    protected $errors = null;

    /**
     * @var string $shippingMessage
     */
    protected $shippingMessage = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfErrorDTO
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param ArrayOfErrorDTO $errors
     * @return \hubsoft\api\commerce\CheckPromotionLimitsResponse
     */
    public function setErrors($errors)
    {
      $this->errors = $errors;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingMessage()
    {
      return $this->shippingMessage;
    }

    /**
     * @param string $shippingMessage
     * @return \hubsoft\api\commerce\CheckPromotionLimitsResponse
     */
    public function setShippingMessage($shippingMessage)
    {
      $this->shippingMessage = $shippingMessage;
      return $this;
    }

}
