<?php

namespace hubsoft\api\commerce;

class CommerceService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'getPromotionDetailByRepCode' => 'hubsoft\\api\\commerce\\getPromotionDetailByRepCode',
      'getPromotionDetailByRepCodeResponse' => 'hubsoft\\api\\commerce\\getPromotionDetailByRepCodeResponse',
      'processPayment' => 'hubsoft\\api\\commerce\\processPayment',
      'processPaymentResponse' => 'hubsoft\\api\\commerce\\processPaymentResponse',
      'validateAuthCode' => 'hubsoft\\api\\commerce\\validateAuthCode',
      'validateAuthCodeResponse' => 'hubsoft\\api\\commerce\\validateAuthCodeResponse',
      'resetPassword' => 'hubsoft\\api\\commerce\\resetPassword',
      'resetPasswordResponse' => 'hubsoft\\api\\commerce\\resetPasswordResponse',
      'processShipTax' => 'hubsoft\\api\\commerce\\processShipTax',
      'processShipTaxResponse' => 'hubsoft\\api\\commerce\\processShipTaxResponse',
      'getPromotionDetail' => 'hubsoft\\api\\commerce\\getPromotionDetail',
      'getPromotionDetailResponse' => 'hubsoft\\api\\commerce\\getPromotionDetailResponse',
      'processPayPalPayment' => 'hubsoft\\api\\commerce\\processPayPalPayment',
      'processPayPalPaymentResponse' => 'hubsoft\\api\\commerce\\processPayPalPaymentResponse',
      'checkPromotionLimits' => 'hubsoft\\api\\commerce\\checkPromotionLimits',
      'checkPromotionLimitsResponse' => 'hubsoft\\api\\commerce\\checkPromotionLimitsResponse',
      'isEmailAvailable' => 'hubsoft\\api\\commerce\\isEmailAvailable',
      'isEmailAvailableResponse' => 'hubsoft\\api\\commerce\\isEmailAvailableResponse',
      'getUserInfo' => 'hubsoft\\api\\commerce\\getUserInfo',
      'getUserInfoResponse' => 'hubsoft\\api\\commerce\\getUserInfoResponse',
      'updateCustomer' => 'hubsoft\\api\\commerce\\updateCustomer',
      'updateCustomerResponse' => 'hubsoft\\api\\commerce\\updateCustomerResponse',
      'getPromotionDetailByAccessCode' => 'hubsoft\\api\\commerce\\getPromotionDetailByAccessCode',
      'getPromotionDetailByAccessCodeResponse' => 'hubsoft\\api\\commerce\\getPromotionDetailByAccessCodeResponse',
      'adjustUsedPoints' => 'hubsoft\\api\\commerce\\adjustUsedPoints',
      'adjustUsedPointsResponse' => 'hubsoft\\api\\commerce\\adjustUsedPointsResponse',
      'authCustomerByEmployeeCode' => 'hubsoft\\api\\commerce\\authCustomerByEmployeeCode',
      'authCustomerByEmployeeCodeResponse' => 'hubsoft\\api\\commerce\\authCustomerByEmployeeCodeResponse',
      'authCustomerByLoginName' => 'hubsoft\\api\\commerce\\authCustomerByLoginName',
      'authCustomerByLoginNameResponse' => 'hubsoft\\api\\commerce\\authCustomerByLoginNameResponse',
      'getGiftCardBalance' => 'hubsoft\\api\\commerce\\getGiftCardBalance',
      'getGiftCardBalanceResponse' => 'hubsoft\\api\\commerce\\getGiftCardBalanceResponse',
      'createCustomer' => 'hubsoft\\api\\commerce\\createCustomer',
      'createCustomerResponse' => 'hubsoft\\api\\commerce\\createCustomerResponse',
      'getActivePromotions' => 'hubsoft\\api\\commerce\\getActivePromotions',
      'getActivePromotionsResponse' => 'hubsoft\\api\\commerce\\getActivePromotionsResponse',
      'payOnPoints' => 'hubsoft\\api\\commerce\\payOnPoints',
      'payOnPointsResponse' => 'hubsoft\\api\\commerce\\payOnPointsResponse',
      'getAuthCode' => 'hubsoft\\api\\commerce\\getAuthCode',
      'getAuthCodeResponse' => 'hubsoft\\api\\commerce\\getAuthCodeResponse',
      'updatePassword' => 'hubsoft\\api\\commerce\\updatePassword',
      'updatePasswordResponse' => 'hubsoft\\api\\commerce\\updatePasswordResponse',
      'authCustomer' => 'hubsoft\\api\\commerce\\authCustomer',
      'authCustomerResponse' => 'hubsoft\\api\\commerce\\authCustomerResponse',
      'PromotionDTO' => 'hubsoft\\api\\commerce\\PromotionDTO',
      'ArrayOfCouponDTO' => 'hubsoft\\api\\commerce\\ArrayOfCouponDTO',
      'CouponDTO' => 'hubsoft\\api\\commerce\\CouponDTO',
      'ArrayOfTechnologyDTO' => 'hubsoft\\api\\commerce\\ArrayOfTechnologyDTO',
      'TechnologyDTO' => 'hubsoft\\api\\commerce\\TechnologyDTO',
      'ArrayOfPaymentMethodDTO' => 'hubsoft\\api\\commerce\\ArrayOfPaymentMethodDTO',
      'PaymentMethodDTO' => 'hubsoft\\api\\commerce\\PaymentMethodDTO',
      'ArrayOfShippingMethodDTO' => 'hubsoft\\api\\commerce\\ArrayOfShippingMethodDTO',
      'ShippingMethodDTO' => 'hubsoft\\api\\commerce\\ShippingMethodDTO',
      'ArrayOfProductDTO' => 'hubsoft\\api\\commerce\\ArrayOfProductDTO',
      'ProductDTO' => 'hubsoft\\api\\commerce\\ProductDTO',
      'ArrayOfProductColorDTO' => 'hubsoft\\api\\commerce\\ArrayOfProductColorDTO',
      'ProductColorDTO' => 'hubsoft\\api\\commerce\\ProductColorDTO',
      'ArrayOfSizeDTO' => 'hubsoft\\api\\commerce\\ArrayOfSizeDTO',
      'SizeDTO' => 'hubsoft\\api\\commerce\\SizeDTO',
      'ArrayOfRelatedProductNumber' => 'hubsoft\\api\\commerce\\ArrayOfRelatedProductNumber',
      'RelatedProductNumber' => 'hubsoft\\api\\commerce\\RelatedProductNumber',
      'ArrayOfDependentProductDTO' => 'hubsoft\\api\\commerce\\ArrayOfDependentProductDTO',
      'DependentProductDTO' => 'hubsoft\\api\\commerce\\DependentProductDTO',
      'BillingDTO' => 'hubsoft\\api\\commerce\\BillingDTO',
      'PaymentResultDTO' => 'hubsoft\\api\\commerce\\PaymentResultDTO',
      'ArrayOfErrorDTO' => 'hubsoft\\api\\commerce\\ArrayOfErrorDTO',
      'ErrorDTO' => 'hubsoft\\api\\commerce\\ErrorDTO',
      'OrderDTO' => 'hubsoft\\api\\commerce\\OrderDTO',
      'ArrayOfOrderItemDTO' => 'hubsoft\\api\\commerce\\ArrayOfOrderItemDTO',
      'OrderItemDTO' => 'hubsoft\\api\\commerce\\OrderItemDTO',
      'ShippingDTO' => 'hubsoft\\api\\commerce\\ShippingDTO',
      'OrderResultDTO' => 'hubsoft\\api\\commerce\\OrderResultDTO',
      'PayPalBilling' => 'hubsoft\\api\\commerce\\PayPalBilling',
      'ArrayOfOrderSummaryDTO' => 'hubsoft\\api\\commerce\\ArrayOfOrderSummaryDTO',
      'OrderSummaryDTO' => 'hubsoft\\api\\commerce\\OrderSummaryDTO',
      'ArrayOfOrderItemSummaryDTO' => 'hubsoft\\api\\commerce\\ArrayOfOrderItemSummaryDTO',
      'OrderItemSummaryDTO' => 'hubsoft\\api\\commerce\\OrderItemSummaryDTO',
      'ServiceDisabledException' => 'hubsoft\\api\\commerce\\ServiceDisabledException',
      'AuthenticationFailureException' => 'hubsoft\\api\\commerce\\AuthenticationFailureException',
      'InvalidCodeException' => 'hubsoft\\api\\commerce\\InvalidCodeException',
      'ValidateAuthCodeResponse' => 'hubsoft\\api\\commerce\\ValidateAuthCodeResponse',
      'CheckPromotionLimitsResponse' => 'hubsoft\\api\\commerce\\CheckPromotionLimitsResponse',
      'PromoUserOutDTO' => 'hubsoft\\api\\commerce\\PromoUserOutDTO',
      'ArrayOfTrainingProgressDTO' => 'hubsoft\\api\\commerce\\ArrayOfTrainingProgressDTO',
      'TrainingProgressDTO' => 'hubsoft\\api\\commerce\\TrainingProgressDTO',
      'ArrayOfAvailablePromotionDTO' => 'hubsoft\\api\\commerce\\ArrayOfAvailablePromotionDTO',
      'AvailablePromotionDTO' => 'hubsoft\\api\\commerce\\AvailablePromotionDTO',
      'ArrayOfAvailableSchoolDTO' => 'hubsoft\\api\\commerce\\ArrayOfAvailableSchoolDTO',
      'AvailableSchoolDTO' => 'hubsoft\\api\\commerce\\AvailableSchoolDTO',
      'ArrayOfCertificationDTO' => 'hubsoft\\api\\commerce\\ArrayOfCertificationDTO',
      'CertificationDTO' => 'hubsoft\\api\\commerce\\CertificationDTO',
      'ArrayOfUserTrainingResultDTO' => 'hubsoft\\api\\commerce\\ArrayOfUserTrainingResultDTO',
      'UserTrainingResultDTO' => 'hubsoft\\api\\commerce\\UserTrainingResultDTO',
      'ArrayOfAvailableContestDTO' => 'hubsoft\\api\\commerce\\ArrayOfAvailableContestDTO',
      'AvailableContestDTO' => 'hubsoft\\api\\commerce\\AvailableContestDTO',
      'LoginFailureException' => 'hubsoft\\api\\commerce\\LoginFailureException',
      'PromoUserInDTO' => 'hubsoft\\api\\commerce\\PromoUserInDTO',
      'EmailAlreadyExistsException' => 'hubsoft\\api\\commerce\\EmailAlreadyExistsException',
      'GetAuthCodeResponse' => 'hubsoft\\api\\commerce\\GetAuthCodeResponse',
      'ArrayOfNameValueBean' => 'hubsoft\\api\\commerce\\ArrayOfNameValueBean',
      'NameValueBean' => 'hubsoft\\api\\commerce\\NameValueBean',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'https://ws.hubsoft.com/services/CommerceService?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param getPromotionDetailByRepCode $parameters
     * @return getPromotionDetailByRepCodeResponse
     */
    public function getPromotionDetailByRepCode(getPromotionDetailByRepCode $parameters)
    {
      return $this->__soapCall('getPromotionDetailByRepCode', array($parameters));
    }

    /**
     * @param processPayment $parameters
     * @return processPaymentResponse
     */
    public function processPayment(processPayment $parameters)
    {
      return $this->__soapCall('processPayment', array($parameters));
    }

    /**
     * @param validateAuthCode $parameters
     * @return validateAuthCodeResponse
     */
    public function validateAuthCode(validateAuthCode $parameters)
    {
      return $this->__soapCall('validateAuthCode', array($parameters));
    }

    /**
     * @param resetPassword $parameters
     * @return resetPasswordResponse
     */
    public function resetPassword(resetPassword $parameters)
    {
      return $this->__soapCall('resetPassword', array($parameters));
    }

    /**
     * @param processShipTax $parameters
     * @return processShipTaxResponse
     */
    public function processShipTax(processShipTax $parameters)
    {
      return $this->__soapCall('processShipTax', array($parameters));
    }

    /**
     * @param getPromotionDetail $parameters
     * @return getPromotionDetailResponse
     */
    public function getPromotionDetail(getPromotionDetail $parameters)
    {
      return $this->__soapCall('getPromotionDetail', array($parameters));
    }

    /**
     * @param processPayPalPayment $parameters
     * @return processPayPalPaymentResponse
     */
    public function processPayPalPayment(processPayPalPayment $parameters)
    {
      return $this->__soapCall('processPayPalPayment', array($parameters));
    }

    /**
     * @param checkPromotionLimits $parameters
     * @return checkPromotionLimitsResponse
     */
    public function checkPromotionLimits(checkPromotionLimits $parameters)
    {
      return $this->__soapCall('checkPromotionLimits', array($parameters));
    }

    /**
     * @param isEmailAvailable $parameters
     * @return isEmailAvailableResponse
     */
    public function isEmailAvailable(isEmailAvailable $parameters)
    {
      return $this->__soapCall('isEmailAvailable', array($parameters));
    }

    /**
     * @param getUserInfo $parameters
     * @return getUserInfoResponse
     */
    public function getUserInfo(getUserInfo $parameters)
    {
      return $this->__soapCall('getUserInfo', array($parameters));
    }

    /**
     * @param updateCustomer $parameters
     * @return updateCustomerResponse
     */
    public function updateCustomer(updateCustomer $parameters)
    {
      return $this->__soapCall('updateCustomer', array($parameters));
    }

    /**
     * @param getPromotionDetailByAccessCode $parameters
     * @return getPromotionDetailByAccessCodeResponse
     */
    public function getPromotionDetailByAccessCode(getPromotionDetailByAccessCode $parameters)
    {
      return $this->__soapCall('getPromotionDetailByAccessCode', array($parameters));
    }

    /**
     * @param adjustUsedPoints $parameters
     * @return adjustUsedPointsResponse
     */
    public function adjustUsedPoints(adjustUsedPoints $parameters)
    {
      return $this->__soapCall('adjustUsedPoints', array($parameters));
    }

    /**
     * @param authCustomerByEmployeeCode $parameters
     * @return authCustomerByEmployeeCodeResponse
     */
    public function authCustomerByEmployeeCode(authCustomerByEmployeeCode $parameters)
    {
      return $this->__soapCall('authCustomerByEmployeeCode', array($parameters));
    }

    /**
     * @param authCustomerByLoginName $parameters
     * @return authCustomerByLoginNameResponse
     */
    public function authCustomerByLoginName(authCustomerByLoginName $parameters)
    {
      return $this->__soapCall('authCustomerByLoginName', array($parameters));
    }

    /**
     * @param getGiftCardBalance $parameters
     * @return getGiftCardBalanceResponse
     */
    public function getGiftCardBalance(getGiftCardBalance $parameters)
    {
      return $this->__soapCall('getGiftCardBalance', array($parameters));
    }

    /**
     * @param createCustomer $parameters
     * @return createCustomerResponse
     */
    public function createCustomer(createCustomer $parameters)
    {
      return $this->__soapCall('createCustomer', array($parameters));
    }

    /**
     * @param getActivePromotions $parameters
     * @return getActivePromotionsResponse
     */
    public function getActivePromotions(getActivePromotions $parameters)
    {
      return $this->__soapCall('getActivePromotions', array($parameters));
    }

    /**
     * @param payOnPoints $parameters
     * @return payOnPointsResponse
     */
    public function payOnPoints(payOnPoints $parameters)
    {
      return $this->__soapCall('payOnPoints', array($parameters));
    }

    /**
     * @param getAuthCode $parameters
     * @return getAuthCodeResponse
     */
    public function getAuthCode(getAuthCode $parameters)
    {
      return $this->__soapCall('getAuthCode', array($parameters));
    }

    /**
     * @param updatePassword $parameters
     * @return updatePasswordResponse
     */
    public function updatePassword(updatePassword $parameters)
    {
      return $this->__soapCall('updatePassword', array($parameters));
    }

    /**
     * @param authCustomer $parameters
     * @return authCustomerResponse
     */
    public function authCustomer(authCustomer $parameters)
    {
      return $this->__soapCall('authCustomer', array($parameters));
    }

}
