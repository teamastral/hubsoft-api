<?php

namespace hubsoft\api\commerce;

class CouponDTO
{

    /**
     * @var string $couponCode
     */
    protected $couponCode = null;

    /**
     * @var string $couponExpire
     */
    protected $couponExpire = null;

    /**
     * @var float $discountPerc
     */
    protected $discountPerc = null;

    /**
     * @var float $minAmount
     */
    protected $minAmount = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCouponCode()
    {
      return $this->couponCode;
    }

    /**
     * @param string $couponCode
     * @return \hubsoft\api\commerce\CouponDTO
     */
    public function setCouponCode($couponCode)
    {
      $this->couponCode = $couponCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCouponExpire()
    {
      return $this->couponExpire;
    }

    /**
     * @param string $couponExpire
     * @return \hubsoft\api\commerce\CouponDTO
     */
    public function setCouponExpire($couponExpire)
    {
      $this->couponExpire = $couponExpire;
      return $this;
    }

    /**
     * @return float
     */
    public function getDiscountPerc()
    {
      return $this->discountPerc;
    }

    /**
     * @param float $discountPerc
     * @return \hubsoft\api\commerce\CouponDTO
     */
    public function setDiscountPerc($discountPerc)
    {
      $this->discountPerc = $discountPerc;
      return $this;
    }

    /**
     * @return float
     */
    public function getMinAmount()
    {
      return $this->minAmount;
    }

    /**
     * @param float $minAmount
     * @return \hubsoft\api\commerce\CouponDTO
     */
    public function setMinAmount($minAmount)
    {
      $this->minAmount = $minAmount;
      return $this;
    }

}
