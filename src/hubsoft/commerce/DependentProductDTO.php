<?php

namespace hubsoft\api\commerce;

class DependentProductDTO
{

    /**
     * @var int $minQty
     */
    protected $minQty = null;

    /**
     * @var float $msrpPrice
     */
    protected $msrpPrice = null;

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    /**
     * @var float $wholesalePrice
     */
    protected $wholesalePrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getMinQty()
    {
      return $this->minQty;
    }

    /**
     * @param int $minQty
     * @return \hubsoft\api\commerce\DependentProductDTO
     */
    public function setMinQty($minQty)
    {
      $this->minQty = $minQty;
      return $this;
    }

    /**
     * @return float
     */
    public function getMsrpPrice()
    {
      return $this->msrpPrice;
    }

    /**
     * @param float $msrpPrice
     * @return \hubsoft\api\commerce\DependentProductDTO
     */
    public function setMsrpPrice($msrpPrice)
    {
      $this->msrpPrice = $msrpPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \hubsoft\api\commerce\DependentProductDTO
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

    /**
     * @return float
     */
    public function getWholesalePrice()
    {
      return $this->wholesalePrice;
    }

    /**
     * @param float $wholesalePrice
     * @return \hubsoft\api\commerce\DependentProductDTO
     */
    public function setWholesalePrice($wholesalePrice)
    {
      $this->wholesalePrice = $wholesalePrice;
      return $this;
    }

}
