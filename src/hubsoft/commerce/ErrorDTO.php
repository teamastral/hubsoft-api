<?php

namespace hubsoft\api\commerce;

class ErrorDTO
{

    /**
     * @var string $code
     */
    protected $code = null;

    /**
     * @var string $mesg
     */
    protected $mesg = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->code;
    }

    /**
     * @param string $code
     * @return \hubsoft\api\commerce\ErrorDTO
     */
    public function setCode($code)
    {
      $this->code = $code;
      return $this;
    }

    /**
     * @return string
     */
    public function getMesg()
    {
      return $this->mesg;
    }

    /**
     * @param string $mesg
     * @return \hubsoft\api\commerce\ErrorDTO
     */
    public function setMesg($mesg)
    {
      $this->mesg = $mesg;
      return $this;
    }

}
