<?php

namespace hubsoft\api\commerce;

class GetAuthCodeResponse
{

    /**
     * @var string $authCode
     */
    protected $authCode = null;

    /**
     * @var ArrayOfErrorDTO $errors
     */
    protected $errors = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAuthCode()
    {
      return $this->authCode;
    }

    /**
     * @param string $authCode
     * @return \hubsoft\api\commerce\GetAuthCodeResponse
     */
    public function setAuthCode($authCode)
    {
      $this->authCode = $authCode;
      return $this;
    }

    /**
     * @return ArrayOfErrorDTO
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param ArrayOfErrorDTO $errors
     * @return \hubsoft\api\commerce\GetAuthCodeResponse
     */
    public function setErrors($errors)
    {
      $this->errors = $errors;
      return $this;
    }

}
