<?php

namespace hubsoft\api\commerce;

class NameValueBean
{

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var string $value
     */
    protected $value = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return \hubsoft\api\commerce\NameValueBean
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param string $value
     * @return \hubsoft\api\commerce\NameValueBean
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

}
