<?php

namespace hubsoft\api\commerce;

class OrderDTO
{

    /**
     * @var string $accessCode
     */
    protected $accessCode = null;

    /**
     * @var string $comments
     */
    protected $comments = null;

    /**
     * @var string $ipSource
     */
    protected $ipSource = null;

    /**
     * @var ArrayOfOrderItemDTO $items
     */
    protected $items = null;

    /**
     * @var ArrayOfNameValueBean $orderAttribs
     */
    protected $orderAttribs = null;

    /**
     * @var string $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var string $promotionCode
     */
    protected $promotionCode = null;

    /**
     * @var string $receivePromoEmails
     */
    protected $receivePromoEmails = null;

    /**
     * @var string $salesRepCode
     */
    protected $salesRepCode = null;

    /**
     * @var ShippingDTO $shippingInfo
     */
    protected $shippingInfo = null;

    /**
     * @var string $startDate
     */
    protected $startDate = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAccessCode()
    {
      return $this->accessCode;
    }

    /**
     * @param string $accessCode
     * @return \hubsoft\api\commerce\OrderDTO
     */
    public function setAccessCode($accessCode)
    {
      $this->accessCode = $accessCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getComments()
    {
      return $this->comments;
    }

    /**
     * @param string $comments
     * @return \hubsoft\api\commerce\OrderDTO
     */
    public function setComments($comments)
    {
      $this->comments = $comments;
      return $this;
    }

    /**
     * @return string
     */
    public function getIpSource()
    {
      return $this->ipSource;
    }

    /**
     * @param string $ipSource
     * @return \hubsoft\api\commerce\OrderDTO
     */
    public function setIpSource($ipSource)
    {
      $this->ipSource = $ipSource;
      return $this;
    }

    /**
     * @return ArrayOfOrderItemDTO
     */
    public function getItems()
    {
      return $this->items;
    }

    /**
     * @param ArrayOfOrderItemDTO $items
     * @return \hubsoft\api\commerce\OrderDTO
     */
    public function setItems($items)
    {
      $this->items = $items;
      return $this;
    }

    /**
     * @return ArrayOfNameValueBean
     */
    public function getOrderAttribs()
    {
      return $this->orderAttribs;
    }

    /**
     * @param ArrayOfNameValueBean $orderAttribs
     * @return \hubsoft\api\commerce\OrderDTO
     */
    public function setOrderAttribs($orderAttribs)
    {
      $this->orderAttribs = $orderAttribs;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
      return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return \hubsoft\api\commerce\OrderDTO
     */
    public function setOrderNumber($orderNumber)
    {
      $this->orderNumber = $orderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->promotionCode;
    }

    /**
     * @param string $promotionCode
     * @return \hubsoft\api\commerce\OrderDTO
     */
    public function setPromotionCode($promotionCode)
    {
      $this->promotionCode = $promotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getReceivePromoEmails()
    {
      return $this->receivePromoEmails;
    }

    /**
     * @param string $receivePromoEmails
     * @return \hubsoft\api\commerce\OrderDTO
     */
    public function setReceivePromoEmails($receivePromoEmails)
    {
      $this->receivePromoEmails = $receivePromoEmails;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesRepCode()
    {
      return $this->salesRepCode;
    }

    /**
     * @param string $salesRepCode
     * @return \hubsoft\api\commerce\OrderDTO
     */
    public function setSalesRepCode($salesRepCode)
    {
      $this->salesRepCode = $salesRepCode;
      return $this;
    }

    /**
     * @return ShippingDTO
     */
    public function getShippingInfo()
    {
      return $this->shippingInfo;
    }

    /**
     * @param ShippingDTO $shippingInfo
     * @return \hubsoft\api\commerce\OrderDTO
     */
    public function setShippingInfo($shippingInfo)
    {
      $this->shippingInfo = $shippingInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
      return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return \hubsoft\api\commerce\OrderDTO
     */
    public function setStartDate($startDate)
    {
      $this->startDate = $startDate;
      return $this;
    }

}
