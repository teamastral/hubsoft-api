<?php

namespace hubsoft\api\commerce;

class OrderItemDTO
{

    /**
     * @var int $orderedQty
     */
    protected $orderedQty = null;

    /**
     * @var string $sku
     */
    protected $sku = null;

    /**
     * @var float $unitPrice
     */
    protected $unitPrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getOrderedQty()
    {
      return $this->orderedQty;
    }

    /**
     * @param int $orderedQty
     * @return \hubsoft\api\commerce\OrderItemDTO
     */
    public function setOrderedQty($orderedQty)
    {
      $this->orderedQty = $orderedQty;
      return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
      return $this->sku;
    }

    /**
     * @param string $sku
     * @return \hubsoft\api\commerce\OrderItemDTO
     */
    public function setSku($sku)
    {
      $this->sku = $sku;
      return $this;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
      return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     * @return \hubsoft\api\commerce\OrderItemDTO
     */
    public function setUnitPrice($unitPrice)
    {
      $this->unitPrice = $unitPrice;
      return $this;
    }

}
