<?php

namespace hubsoft\api\commerce;

class OrderItemSummaryDTO
{

    /**
     * @var string $itemColor
     */
    protected $itemColor = null;

    /**
     * @var string $itemImage
     */
    protected $itemImage = null;

    /**
     * @var string $itemName
     */
    protected $itemName = null;

    /**
     * @var string $itemNumber
     */
    protected $itemNumber = null;

    /**
     * @var int $itemQuantity
     */
    protected $itemQuantity = null;

    /**
     * @var string $itemSize
     */
    protected $itemSize = null;

    /**
     * @var float $itemTotalPrice
     */
    protected $itemTotalPrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getItemColor()
    {
      return $this->itemColor;
    }

    /**
     * @param string $itemColor
     * @return \hubsoft\api\commerce\OrderItemSummaryDTO
     */
    public function setItemColor($itemColor)
    {
      $this->itemColor = $itemColor;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemImage()
    {
      return $this->itemImage;
    }

    /**
     * @param string $itemImage
     * @return \hubsoft\api\commerce\OrderItemSummaryDTO
     */
    public function setItemImage($itemImage)
    {
      $this->itemImage = $itemImage;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemName()
    {
      return $this->itemName;
    }

    /**
     * @param string $itemName
     * @return \hubsoft\api\commerce\OrderItemSummaryDTO
     */
    public function setItemName($itemName)
    {
      $this->itemName = $itemName;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemNumber()
    {
      return $this->itemNumber;
    }

    /**
     * @param string $itemNumber
     * @return \hubsoft\api\commerce\OrderItemSummaryDTO
     */
    public function setItemNumber($itemNumber)
    {
      $this->itemNumber = $itemNumber;
      return $this;
    }

    /**
     * @return int
     */
    public function getItemQuantity()
    {
      return $this->itemQuantity;
    }

    /**
     * @param int $itemQuantity
     * @return \hubsoft\api\commerce\OrderItemSummaryDTO
     */
    public function setItemQuantity($itemQuantity)
    {
      $this->itemQuantity = $itemQuantity;
      return $this;
    }

    /**
     * @return string
     */
    public function getItemSize()
    {
      return $this->itemSize;
    }

    /**
     * @param string $itemSize
     * @return \hubsoft\api\commerce\OrderItemSummaryDTO
     */
    public function setItemSize($itemSize)
    {
      $this->itemSize = $itemSize;
      return $this;
    }

    /**
     * @return float
     */
    public function getItemTotalPrice()
    {
      return $this->itemTotalPrice;
    }

    /**
     * @param float $itemTotalPrice
     * @return \hubsoft\api\commerce\OrderItemSummaryDTO
     */
    public function setItemTotalPrice($itemTotalPrice)
    {
      $this->itemTotalPrice = $itemTotalPrice;
      return $this;
    }

}
