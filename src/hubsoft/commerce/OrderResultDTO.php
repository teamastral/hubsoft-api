<?php

namespace hubsoft\api\commerce;

class OrderResultDTO
{

    /**
     * @var ArrayOfErrorDTO $errors
     */
    protected $errors = null;

    /**
     * @var string $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var string $promoShippingMesg
     */
    protected $promoShippingMesg = null;

    /**
     * @var float $shippingAmount
     */
    protected $shippingAmount = null;

    /**
     * @var ShippingDTO $shippingInfo
     */
    protected $shippingInfo = null;

    /**
     * @var boolean $success
     */
    protected $success = null;

    /**
     * @var float $taxAmount
     */
    protected $taxAmount = null;

    /**
     * @var boolean $validAddress
     */
    protected $validAddress = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfErrorDTO
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param ArrayOfErrorDTO $errors
     * @return \hubsoft\api\commerce\OrderResultDTO
     */
    public function setErrors($errors)
    {
      $this->errors = $errors;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
      return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return \hubsoft\api\commerce\OrderResultDTO
     */
    public function setOrderNumber($orderNumber)
    {
      $this->orderNumber = $orderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromoShippingMesg()
    {
      return $this->promoShippingMesg;
    }

    /**
     * @param string $promoShippingMesg
     * @return \hubsoft\api\commerce\OrderResultDTO
     */
    public function setPromoShippingMesg($promoShippingMesg)
    {
      $this->promoShippingMesg = $promoShippingMesg;
      return $this;
    }

    /**
     * @return float
     */
    public function getShippingAmount()
    {
      return $this->shippingAmount;
    }

    /**
     * @param float $shippingAmount
     * @return \hubsoft\api\commerce\OrderResultDTO
     */
    public function setShippingAmount($shippingAmount)
    {
      $this->shippingAmount = $shippingAmount;
      return $this;
    }

    /**
     * @return ShippingDTO
     */
    public function getShippingInfo()
    {
      return $this->shippingInfo;
    }

    /**
     * @param ShippingDTO $shippingInfo
     * @return \hubsoft\api\commerce\OrderResultDTO
     */
    public function setShippingInfo($shippingInfo)
    {
      $this->shippingInfo = $shippingInfo;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->success;
    }

    /**
     * @param boolean $success
     * @return \hubsoft\api\commerce\OrderResultDTO
     */
    public function setSuccess($success)
    {
      $this->success = $success;
      return $this;
    }

    /**
     * @return float
     */
    public function getTaxAmount()
    {
      return $this->taxAmount;
    }

    /**
     * @param float $taxAmount
     * @return \hubsoft\api\commerce\OrderResultDTO
     */
    public function setTaxAmount($taxAmount)
    {
      $this->taxAmount = $taxAmount;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getValidAddress()
    {
      return $this->validAddress;
    }

    /**
     * @param boolean $validAddress
     * @return \hubsoft\api\commerce\OrderResultDTO
     */
    public function setValidAddress($validAddress)
    {
      $this->validAddress = $validAddress;
      return $this;
    }

}
