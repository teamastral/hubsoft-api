<?php

namespace hubsoft\api\commerce;

class OrderSummaryDTO
{

    /**
     * @var string $billToZip
     */
    protected $billToZip = null;

    /**
     * @var string $ccBrand
     */
    protected $ccBrand = null;

    /**
     * @var string $expiryDate
     */
    protected $expiryDate = null;

    /**
     * @var float $giftCardAmount
     */
    protected $giftCardAmount = null;

    /**
     * @var ArrayOfOrderItemSummaryDTO $items
     */
    protected $items = null;

    /**
     * @var string $last4CreditCard
     */
    protected $last4CreditCard = null;

    /**
     * @var string $orderCurrencyCode
     */
    protected $orderCurrencyCode = null;

    /**
     * @var string $orderDate
     */
    protected $orderDate = null;

    /**
     * @var string $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var string $orderStatusDesc
     */
    protected $orderStatusDesc = null;

    /**
     * @var string $promoAccessCode
     */
    protected $promoAccessCode = null;

    /**
     * @var string $promotionCode
     */
    protected $promotionCode = null;

    /**
     * @var string $promotionName
     */
    protected $promotionName = null;

    /**
     * @var string $shipToAttention
     */
    protected $shipToAttention = null;

    /**
     * @var string $shipToCity
     */
    protected $shipToCity = null;

    /**
     * @var string $shipToCountry
     */
    protected $shipToCountry = null;

    /**
     * @var string $shipToState
     */
    protected $shipToState = null;

    /**
     * @var string $shipToStreet
     */
    protected $shipToStreet = null;

    /**
     * @var string $shipToZip
     */
    protected $shipToZip = null;

    /**
     * @var string $shippedDate
     */
    protected $shippedDate = null;

    /**
     * @var string $shippingCarrier
     */
    protected $shippingCarrier = null;

    /**
     * @var string $shippingMethodCode
     */
    protected $shippingMethodCode = null;

    /**
     * @var string $shippingMethodName
     */
    protected $shippingMethodName = null;

    /**
     * @var float $totalOrderedAmount
     */
    protected $totalOrderedAmount = null;

    /**
     * @var int $totalOrderedQty
     */
    protected $totalOrderedQty = null;

    /**
     * @var float $totalSalesTaxAmount
     */
    protected $totalSalesTaxAmount = null;

    /**
     * @var float $totalShippingAmount
     */
    protected $totalShippingAmount = null;

    /**
     * @var string $trackingNumber
     */
    protected $trackingNumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getBillToZip()
    {
      return $this->billToZip;
    }

    /**
     * @param string $billToZip
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setBillToZip($billToZip)
    {
      $this->billToZip = $billToZip;
      return $this;
    }

    /**
     * @return string
     */
    public function getCcBrand()
    {
      return $this->ccBrand;
    }

    /**
     * @param string $ccBrand
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setCcBrand($ccBrand)
    {
      $this->ccBrand = $ccBrand;
      return $this;
    }

    /**
     * @return string
     */
    public function getExpiryDate()
    {
      return $this->expiryDate;
    }

    /**
     * @param string $expiryDate
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setExpiryDate($expiryDate)
    {
      $this->expiryDate = $expiryDate;
      return $this;
    }

    /**
     * @return float
     */
    public function getGiftCardAmount()
    {
      return $this->giftCardAmount;
    }

    /**
     * @param float $giftCardAmount
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setGiftCardAmount($giftCardAmount)
    {
      $this->giftCardAmount = $giftCardAmount;
      return $this;
    }

    /**
     * @return ArrayOfOrderItemSummaryDTO
     */
    public function getItems()
    {
      return $this->items;
    }

    /**
     * @param ArrayOfOrderItemSummaryDTO $items
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setItems($items)
    {
      $this->items = $items;
      return $this;
    }

    /**
     * @return string
     */
    public function getLast4CreditCard()
    {
      return $this->last4CreditCard;
    }

    /**
     * @param string $last4CreditCard
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setLast4CreditCard($last4CreditCard)
    {
      $this->last4CreditCard = $last4CreditCard;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderCurrencyCode()
    {
      return $this->orderCurrencyCode;
    }

    /**
     * @param string $orderCurrencyCode
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setOrderCurrencyCode($orderCurrencyCode)
    {
      $this->orderCurrencyCode = $orderCurrencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderDate()
    {
      return $this->orderDate;
    }

    /**
     * @param string $orderDate
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setOrderDate($orderDate)
    {
      $this->orderDate = $orderDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
      return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setOrderNumber($orderNumber)
    {
      $this->orderNumber = $orderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderStatusDesc()
    {
      return $this->orderStatusDesc;
    }

    /**
     * @param string $orderStatusDesc
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setOrderStatusDesc($orderStatusDesc)
    {
      $this->orderStatusDesc = $orderStatusDesc;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromoAccessCode()
    {
      return $this->promoAccessCode;
    }

    /**
     * @param string $promoAccessCode
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setPromoAccessCode($promoAccessCode)
    {
      $this->promoAccessCode = $promoAccessCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->promotionCode;
    }

    /**
     * @param string $promotionCode
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setPromotionCode($promotionCode)
    {
      $this->promotionCode = $promotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionName()
    {
      return $this->promotionName;
    }

    /**
     * @param string $promotionName
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setPromotionName($promotionName)
    {
      $this->promotionName = $promotionName;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToAttention()
    {
      return $this->shipToAttention;
    }

    /**
     * @param string $shipToAttention
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setShipToAttention($shipToAttention)
    {
      $this->shipToAttention = $shipToAttention;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToCity()
    {
      return $this->shipToCity;
    }

    /**
     * @param string $shipToCity
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setShipToCity($shipToCity)
    {
      $this->shipToCity = $shipToCity;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToCountry()
    {
      return $this->shipToCountry;
    }

    /**
     * @param string $shipToCountry
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setShipToCountry($shipToCountry)
    {
      $this->shipToCountry = $shipToCountry;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToState()
    {
      return $this->shipToState;
    }

    /**
     * @param string $shipToState
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setShipToState($shipToState)
    {
      $this->shipToState = $shipToState;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToStreet()
    {
      return $this->shipToStreet;
    }

    /**
     * @param string $shipToStreet
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setShipToStreet($shipToStreet)
    {
      $this->shipToStreet = $shipToStreet;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToZip()
    {
      return $this->shipToZip;
    }

    /**
     * @param string $shipToZip
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setShipToZip($shipToZip)
    {
      $this->shipToZip = $shipToZip;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippedDate()
    {
      return $this->shippedDate;
    }

    /**
     * @param string $shippedDate
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setShippedDate($shippedDate)
    {
      $this->shippedDate = $shippedDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingCarrier()
    {
      return $this->shippingCarrier;
    }

    /**
     * @param string $shippingCarrier
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setShippingCarrier($shippingCarrier)
    {
      $this->shippingCarrier = $shippingCarrier;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingMethodCode()
    {
      return $this->shippingMethodCode;
    }

    /**
     * @param string $shippingMethodCode
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setShippingMethodCode($shippingMethodCode)
    {
      $this->shippingMethodCode = $shippingMethodCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingMethodName()
    {
      return $this->shippingMethodName;
    }

    /**
     * @param string $shippingMethodName
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setShippingMethodName($shippingMethodName)
    {
      $this->shippingMethodName = $shippingMethodName;
      return $this;
    }

    /**
     * @return float
     */
    public function getTotalOrderedAmount()
    {
      return $this->totalOrderedAmount;
    }

    /**
     * @param float $totalOrderedAmount
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setTotalOrderedAmount($totalOrderedAmount)
    {
      $this->totalOrderedAmount = $totalOrderedAmount;
      return $this;
    }

    /**
     * @return int
     */
    public function getTotalOrderedQty()
    {
      return $this->totalOrderedQty;
    }

    /**
     * @param int $totalOrderedQty
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setTotalOrderedQty($totalOrderedQty)
    {
      $this->totalOrderedQty = $totalOrderedQty;
      return $this;
    }

    /**
     * @return float
     */
    public function getTotalSalesTaxAmount()
    {
      return $this->totalSalesTaxAmount;
    }

    /**
     * @param float $totalSalesTaxAmount
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setTotalSalesTaxAmount($totalSalesTaxAmount)
    {
      $this->totalSalesTaxAmount = $totalSalesTaxAmount;
      return $this;
    }

    /**
     * @return float
     */
    public function getTotalShippingAmount()
    {
      return $this->totalShippingAmount;
    }

    /**
     * @param float $totalShippingAmount
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setTotalShippingAmount($totalShippingAmount)
    {
      $this->totalShippingAmount = $totalShippingAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getTrackingNumber()
    {
      return $this->trackingNumber;
    }

    /**
     * @param string $trackingNumber
     * @return \hubsoft\api\commerce\OrderSummaryDTO
     */
    public function setTrackingNumber($trackingNumber)
    {
      $this->trackingNumber = $trackingNumber;
      return $this;
    }

}
