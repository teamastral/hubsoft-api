<?php

namespace hubsoft\api\commerce;

class PayPalBilling
{

    /**
     * @var string $payPalFirstName
     */
    protected $payPalFirstName = null;

    /**
     * @var string $payPalLastName
     */
    protected $payPalLastName = null;

    /**
     * @var string $paymentCurrency
     */
    protected $paymentCurrency = null;

    /**
     * @var string $paymentType
     */
    protected $paymentType = null;

    /**
     * @var string $transactionID
     */
    protected $transactionID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getPayPalFirstName()
    {
      return $this->payPalFirstName;
    }

    /**
     * @param string $payPalFirstName
     * @return \hubsoft\api\commerce\PayPalBilling
     */
    public function setPayPalFirstName($payPalFirstName)
    {
      $this->payPalFirstName = $payPalFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPayPalLastName()
    {
      return $this->payPalLastName;
    }

    /**
     * @param string $payPalLastName
     * @return \hubsoft\api\commerce\PayPalBilling
     */
    public function setPayPalLastName($payPalLastName)
    {
      $this->payPalLastName = $payPalLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentCurrency()
    {
      return $this->paymentCurrency;
    }

    /**
     * @param string $paymentCurrency
     * @return \hubsoft\api\commerce\PayPalBilling
     */
    public function setPaymentCurrency($paymentCurrency)
    {
      $this->paymentCurrency = $paymentCurrency;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
      return $this->paymentType;
    }

    /**
     * @param string $paymentType
     * @return \hubsoft\api\commerce\PayPalBilling
     */
    public function setPaymentType($paymentType)
    {
      $this->paymentType = $paymentType;
      return $this;
    }

    /**
     * @return string
     */
    public function getTransactionID()
    {
      return $this->transactionID;
    }

    /**
     * @param string $transactionID
     * @return \hubsoft\api\commerce\PayPalBilling
     */
    public function setTransactionID($transactionID)
    {
      $this->transactionID = $transactionID;
      return $this;
    }

}
