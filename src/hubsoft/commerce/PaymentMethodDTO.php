<?php

namespace hubsoft\api\commerce;

class PaymentMethodDTO
{

    /**
     * @var string $paymentCode
     */
    protected $paymentCode = null;

    /**
     * @var string $paymentDesc
     */
    protected $paymentDesc = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getPaymentCode()
    {
      return $this->paymentCode;
    }

    /**
     * @param string $paymentCode
     * @return \hubsoft\api\commerce\PaymentMethodDTO
     */
    public function setPaymentCode($paymentCode)
    {
      $this->paymentCode = $paymentCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentDesc()
    {
      return $this->paymentDesc;
    }

    /**
     * @param string $paymentDesc
     * @return \hubsoft\api\commerce\PaymentMethodDTO
     */
    public function setPaymentDesc($paymentDesc)
    {
      $this->paymentDesc = $paymentDesc;
      return $this;
    }

}
