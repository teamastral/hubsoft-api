<?php

namespace hubsoft\api\commerce;

class PaymentResultDTO
{

    /**
     * @var string $approvalCode
     */
    protected $approvalCode = null;

    /**
     * @var string $approvalTimestamp
     */
    protected $approvalTimestamp = null;

    /**
     * @var ArrayOfErrorDTO $errors
     */
    protected $errors = null;

    /**
     * @var string $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var boolean $success
     */
    protected $success = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getApprovalCode()
    {
      return $this->approvalCode;
    }

    /**
     * @param string $approvalCode
     * @return \hubsoft\api\commerce\PaymentResultDTO
     */
    public function setApprovalCode($approvalCode)
    {
      $this->approvalCode = $approvalCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getApprovalTimestamp()
    {
      return $this->approvalTimestamp;
    }

    /**
     * @param string $approvalTimestamp
     * @return \hubsoft\api\commerce\PaymentResultDTO
     */
    public function setApprovalTimestamp($approvalTimestamp)
    {
      $this->approvalTimestamp = $approvalTimestamp;
      return $this;
    }

    /**
     * @return ArrayOfErrorDTO
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param ArrayOfErrorDTO $errors
     * @return \hubsoft\api\commerce\PaymentResultDTO
     */
    public function setErrors($errors)
    {
      $this->errors = $errors;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
      return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return \hubsoft\api\commerce\PaymentResultDTO
     */
    public function setOrderNumber($orderNumber)
    {
      $this->orderNumber = $orderNumber;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->success;
    }

    /**
     * @param boolean $success
     * @return \hubsoft\api\commerce\PaymentResultDTO
     */
    public function setSuccess($success)
    {
      $this->success = $success;
      return $this;
    }

}
