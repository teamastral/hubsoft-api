<?php

namespace hubsoft\api\commerce;

class ProductColorDTO
{

    /**
     * @var int $classificationUID
     */
    protected $classificationUID = null;

    /**
     * @var string $colorName
     */
    protected $colorName = null;

    /**
     * @var string $colorNumber
     */
    protected $colorNumber = null;

    /**
     * @var string $colorValue
     */
    protected $colorValue = null;

    /**
     * @var ArrayOfDependentProductDTO $dependentProducts
     */
    protected $dependentProducts = null;

    /**
     * @var string $dependentProductsPolicy
     */
    protected $dependentProductsPolicy = null;

    /**
     * @var string $description
     */
    protected $description = null;

    /**
     * @var string $description1
     */
    protected $description1 = null;

    /**
     * @var string $description2
     */
    protected $description2 = null;

    /**
     * @var string $description3
     */
    protected $description3 = null;

    /**
     * @var string $description4
     */
    protected $description4 = null;

    /**
     * @var string $description5
     */
    protected $description5 = null;

    /**
     * @var string $description6
     */
    protected $description6 = null;

    /**
     * @var string $imageLink1
     */
    protected $imageLink1 = null;

    /**
     * @var string $imageLink2
     */
    protected $imageLink2 = null;

    /**
     * @var string $imageLink3
     */
    protected $imageLink3 = null;

    /**
     * @var string $imageLink4
     */
    protected $imageLink4 = null;

    /**
     * @var string $imageLink5
     */
    protected $imageLink5 = null;

    /**
     * @var string $imageLink6
     */
    protected $imageLink6 = null;

    /**
     * @var string $imageLinkThumb
     */
    protected $imageLinkThumb = null;

    /**
     * @var string $imageSwatchUrl
     */
    protected $imageSwatchUrl = null;

    /**
     * @var string $metaDescription
     */
    protected $metaDescription = null;

    /**
     * @var string $metaKeyword
     */
    protected $metaKeyword = null;

    /**
     * @var float $msrp
     */
    protected $msrp = null;

    /**
     * @var string $pageTitle
     */
    protected $pageTitle = null;

    /**
     * @var string $productDesc
     */
    protected $productDesc = null;

    /**
     * @var string $productName
     */
    protected $productName = null;

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    /**
     * @var int $productUID
     */
    protected $productUID = null;

    /**
     * @var ArrayOfRelatedProductNumber $relatedProductNumbers
     */
    protected $relatedProductNumbers = null;

    /**
     * @var string $salesCategoryName
     */
    protected $salesCategoryName = null;

    /**
     * @var string $searchTerm
     */
    protected $searchTerm = null;

    /**
     * @var ArrayOfSizeDTO $sizes
     */
    protected $sizes = null;

    /**
     * @var int $sortOrder
     */
    protected $sortOrder = null;

    /**
     * @var string $techCodes
     */
    protected $techCodes = null;

    /**
     * @var string $topSeller
     */
    protected $topSeller = null;

    /**
     * @var float $wholesalePrice
     */
    protected $wholesalePrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getClassificationUID()
    {
      return $this->classificationUID;
    }

    /**
     * @param int $classificationUID
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setClassificationUID($classificationUID)
    {
      $this->classificationUID = $classificationUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getColorName()
    {
      return $this->colorName;
    }

    /**
     * @param string $colorName
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setColorName($colorName)
    {
      $this->colorName = $colorName;
      return $this;
    }

    /**
     * @return string
     */
    public function getColorNumber()
    {
      return $this->colorNumber;
    }

    /**
     * @param string $colorNumber
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setColorNumber($colorNumber)
    {
      $this->colorNumber = $colorNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getColorValue()
    {
      return $this->colorValue;
    }

    /**
     * @param string $colorValue
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setColorValue($colorValue)
    {
      $this->colorValue = $colorValue;
      return $this;
    }

    /**
     * @return ArrayOfDependentProductDTO
     */
    public function getDependentProducts()
    {
      return $this->dependentProducts;
    }

    /**
     * @param ArrayOfDependentProductDTO $dependentProducts
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setDependentProducts($dependentProducts)
    {
      $this->dependentProducts = $dependentProducts;
      return $this;
    }

    /**
     * @return string
     */
    public function getDependentProductsPolicy()
    {
      return $this->dependentProductsPolicy;
    }

    /**
     * @param string $dependentProductsPolicy
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setDependentProductsPolicy($dependentProductsPolicy)
    {
      $this->dependentProductsPolicy = $dependentProductsPolicy;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->description;
    }

    /**
     * @param string $description
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setDescription($description)
    {
      $this->description = $description;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription1()
    {
      return $this->description1;
    }

    /**
     * @param string $description1
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setDescription1($description1)
    {
      $this->description1 = $description1;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription2()
    {
      return $this->description2;
    }

    /**
     * @param string $description2
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setDescription2($description2)
    {
      $this->description2 = $description2;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription3()
    {
      return $this->description3;
    }

    /**
     * @param string $description3
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setDescription3($description3)
    {
      $this->description3 = $description3;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription4()
    {
      return $this->description4;
    }

    /**
     * @param string $description4
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setDescription4($description4)
    {
      $this->description4 = $description4;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription5()
    {
      return $this->description5;
    }

    /**
     * @param string $description5
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setDescription5($description5)
    {
      $this->description5 = $description5;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription6()
    {
      return $this->description6;
    }

    /**
     * @param string $description6
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setDescription6($description6)
    {
      $this->description6 = $description6;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink1()
    {
      return $this->imageLink1;
    }

    /**
     * @param string $imageLink1
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setImageLink1($imageLink1)
    {
      $this->imageLink1 = $imageLink1;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink2()
    {
      return $this->imageLink2;
    }

    /**
     * @param string $imageLink2
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setImageLink2($imageLink2)
    {
      $this->imageLink2 = $imageLink2;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink3()
    {
      return $this->imageLink3;
    }

    /**
     * @param string $imageLink3
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setImageLink3($imageLink3)
    {
      $this->imageLink3 = $imageLink3;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink4()
    {
      return $this->imageLink4;
    }

    /**
     * @param string $imageLink4
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setImageLink4($imageLink4)
    {
      $this->imageLink4 = $imageLink4;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink5()
    {
      return $this->imageLink5;
    }

    /**
     * @param string $imageLink5
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setImageLink5($imageLink5)
    {
      $this->imageLink5 = $imageLink5;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink6()
    {
      return $this->imageLink6;
    }

    /**
     * @param string $imageLink6
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setImageLink6($imageLink6)
    {
      $this->imageLink6 = $imageLink6;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLinkThumb()
    {
      return $this->imageLinkThumb;
    }

    /**
     * @param string $imageLinkThumb
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setImageLinkThumb($imageLinkThumb)
    {
      $this->imageLinkThumb = $imageLinkThumb;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageSwatchUrl()
    {
      return $this->imageSwatchUrl;
    }

    /**
     * @param string $imageSwatchUrl
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setImageSwatchUrl($imageSwatchUrl)
    {
      $this->imageSwatchUrl = $imageSwatchUrl;
      return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
      return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setMetaDescription($metaDescription)
    {
      $this->metaDescription = $metaDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeyword()
    {
      return $this->metaKeyword;
    }

    /**
     * @param string $metaKeyword
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setMetaKeyword($metaKeyword)
    {
      $this->metaKeyword = $metaKeyword;
      return $this;
    }

    /**
     * @return float
     */
    public function getMsrp()
    {
      return $this->msrp;
    }

    /**
     * @param float $msrp
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setMsrp($msrp)
    {
      $this->msrp = $msrp;
      return $this;
    }

    /**
     * @return string
     */
    public function getPageTitle()
    {
      return $this->pageTitle;
    }

    /**
     * @param string $pageTitle
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setPageTitle($pageTitle)
    {
      $this->pageTitle = $pageTitle;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductDesc()
    {
      return $this->productDesc;
    }

    /**
     * @param string $productDesc
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setProductDesc($productDesc)
    {
      $this->productDesc = $productDesc;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
      return $this->productName;
    }

    /**
     * @param string $productName
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setProductName($productName)
    {
      $this->productName = $productName;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

    /**
     * @return int
     */
    public function getProductUID()
    {
      return $this->productUID;
    }

    /**
     * @param int $productUID
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setProductUID($productUID)
    {
      $this->productUID = $productUID;
      return $this;
    }

    /**
     * @return ArrayOfRelatedProductNumber
     */
    public function getRelatedProductNumbers()
    {
      return $this->relatedProductNumbers;
    }

    /**
     * @param ArrayOfRelatedProductNumber $relatedProductNumbers
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setRelatedProductNumbers($relatedProductNumbers)
    {
      $this->relatedProductNumbers = $relatedProductNumbers;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesCategoryName()
    {
      return $this->salesCategoryName;
    }

    /**
     * @param string $salesCategoryName
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setSalesCategoryName($salesCategoryName)
    {
      $this->salesCategoryName = $salesCategoryName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSearchTerm()
    {
      return $this->searchTerm;
    }

    /**
     * @param string $searchTerm
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setSearchTerm($searchTerm)
    {
      $this->searchTerm = $searchTerm;
      return $this;
    }

    /**
     * @return ArrayOfSizeDTO
     */
    public function getSizes()
    {
      return $this->sizes;
    }

    /**
     * @param ArrayOfSizeDTO $sizes
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setSizes($sizes)
    {
      $this->sizes = $sizes;
      return $this;
    }

    /**
     * @return int
     */
    public function getSortOrder()
    {
      return $this->sortOrder;
    }

    /**
     * @param int $sortOrder
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setSortOrder($sortOrder)
    {
      $this->sortOrder = $sortOrder;
      return $this;
    }

    /**
     * @return string
     */
    public function getTechCodes()
    {
      return $this->techCodes;
    }

    /**
     * @param string $techCodes
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setTechCodes($techCodes)
    {
      $this->techCodes = $techCodes;
      return $this;
    }

    /**
     * @return string
     */
    public function getTopSeller()
    {
      return $this->topSeller;
    }

    /**
     * @param string $topSeller
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setTopSeller($topSeller)
    {
      $this->topSeller = $topSeller;
      return $this;
    }

    /**
     * @return float
     */
    public function getWholesalePrice()
    {
      return $this->wholesalePrice;
    }

    /**
     * @param float $wholesalePrice
     * @return \hubsoft\api\commerce\ProductColorDTO
     */
    public function setWholesalePrice($wholesalePrice)
    {
      $this->wholesalePrice = $wholesalePrice;
      return $this;
    }

}
