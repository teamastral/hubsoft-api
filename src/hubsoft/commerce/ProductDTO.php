<?php

namespace hubsoft\api\commerce;

class ProductDTO
{

    /**
     * @var int $classificationUID
     */
    protected $classificationUID = null;

    /**
     * @var string $colorName
     */
    protected $colorName = null;

    /**
     * @var string $colorNumber
     */
    protected $colorNumber = null;

    /**
     * @var string $colorValue
     */
    protected $colorValue = null;

    /**
     * @var string $imageLink1
     */
    protected $imageLink1 = null;

    /**
     * @var string $imageLink2
     */
    protected $imageLink2 = null;

    /**
     * @var string $imageLink3
     */
    protected $imageLink3 = null;

    /**
     * @var string $imageLink4
     */
    protected $imageLink4 = null;

    /**
     * @var string $imageLink5
     */
    protected $imageLink5 = null;

    /**
     * @var string $imageLink6
     */
    protected $imageLink6 = null;

    /**
     * @var string $imageLinkThumb
     */
    protected $imageLinkThumb = null;

    /**
     * @var string $imageSwatchUrl
     */
    protected $imageSwatchUrl = null;

    /**
     * @var string $metaDescription
     */
    protected $metaDescription = null;

    /**
     * @var string $metaKeyword
     */
    protected $metaKeyword = null;

    /**
     * @var float $msrp
     */
    protected $msrp = null;

    /**
     * @var string $pageTitle
     */
    protected $pageTitle = null;

    /**
     * @var int $productColorSortOrder
     */
    protected $productColorSortOrder = null;

    /**
     * @var string $productDesc
     */
    protected $productDesc = null;

    /**
     * @var string $productName
     */
    protected $productName = null;

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    /**
     * @var int $productUID
     */
    protected $productUID = null;

    /**
     * @var int $qtyAvailable
     */
    protected $qtyAvailable = null;

    /**
     * @var string $searchTerm
     */
    protected $searchTerm = null;

    /**
     * @var string $sizeName
     */
    protected $sizeName = null;

    /**
     * @var int $sizeSortOrder
     */
    protected $sizeSortOrder = null;

    /**
     * @var string $sku
     */
    protected $sku = null;

    /**
     * @var string $topSeller
     */
    protected $topSeller = null;

    /**
     * @var float $unitPrice
     */
    protected $unitPrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getClassificationUID()
    {
      return $this->classificationUID;
    }

    /**
     * @param int $classificationUID
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setClassificationUID($classificationUID)
    {
      $this->classificationUID = $classificationUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getColorName()
    {
      return $this->colorName;
    }

    /**
     * @param string $colorName
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setColorName($colorName)
    {
      $this->colorName = $colorName;
      return $this;
    }

    /**
     * @return string
     */
    public function getColorNumber()
    {
      return $this->colorNumber;
    }

    /**
     * @param string $colorNumber
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setColorNumber($colorNumber)
    {
      $this->colorNumber = $colorNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getColorValue()
    {
      return $this->colorValue;
    }

    /**
     * @param string $colorValue
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setColorValue($colorValue)
    {
      $this->colorValue = $colorValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink1()
    {
      return $this->imageLink1;
    }

    /**
     * @param string $imageLink1
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setImageLink1($imageLink1)
    {
      $this->imageLink1 = $imageLink1;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink2()
    {
      return $this->imageLink2;
    }

    /**
     * @param string $imageLink2
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setImageLink2($imageLink2)
    {
      $this->imageLink2 = $imageLink2;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink3()
    {
      return $this->imageLink3;
    }

    /**
     * @param string $imageLink3
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setImageLink3($imageLink3)
    {
      $this->imageLink3 = $imageLink3;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink4()
    {
      return $this->imageLink4;
    }

    /**
     * @param string $imageLink4
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setImageLink4($imageLink4)
    {
      $this->imageLink4 = $imageLink4;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink5()
    {
      return $this->imageLink5;
    }

    /**
     * @param string $imageLink5
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setImageLink5($imageLink5)
    {
      $this->imageLink5 = $imageLink5;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLink6()
    {
      return $this->imageLink6;
    }

    /**
     * @param string $imageLink6
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setImageLink6($imageLink6)
    {
      $this->imageLink6 = $imageLink6;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageLinkThumb()
    {
      return $this->imageLinkThumb;
    }

    /**
     * @param string $imageLinkThumb
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setImageLinkThumb($imageLinkThumb)
    {
      $this->imageLinkThumb = $imageLinkThumb;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageSwatchUrl()
    {
      return $this->imageSwatchUrl;
    }

    /**
     * @param string $imageSwatchUrl
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setImageSwatchUrl($imageSwatchUrl)
    {
      $this->imageSwatchUrl = $imageSwatchUrl;
      return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
      return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setMetaDescription($metaDescription)
    {
      $this->metaDescription = $metaDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeyword()
    {
      return $this->metaKeyword;
    }

    /**
     * @param string $metaKeyword
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setMetaKeyword($metaKeyword)
    {
      $this->metaKeyword = $metaKeyword;
      return $this;
    }

    /**
     * @return float
     */
    public function getMsrp()
    {
      return $this->msrp;
    }

    /**
     * @param float $msrp
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setMsrp($msrp)
    {
      $this->msrp = $msrp;
      return $this;
    }

    /**
     * @return string
     */
    public function getPageTitle()
    {
      return $this->pageTitle;
    }

    /**
     * @param string $pageTitle
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setPageTitle($pageTitle)
    {
      $this->pageTitle = $pageTitle;
      return $this;
    }

    /**
     * @return int
     */
    public function getProductColorSortOrder()
    {
      return $this->productColorSortOrder;
    }

    /**
     * @param int $productColorSortOrder
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setProductColorSortOrder($productColorSortOrder)
    {
      $this->productColorSortOrder = $productColorSortOrder;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductDesc()
    {
      return $this->productDesc;
    }

    /**
     * @param string $productDesc
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setProductDesc($productDesc)
    {
      $this->productDesc = $productDesc;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
      return $this->productName;
    }

    /**
     * @param string $productName
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setProductName($productName)
    {
      $this->productName = $productName;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

    /**
     * @return int
     */
    public function getProductUID()
    {
      return $this->productUID;
    }

    /**
     * @param int $productUID
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setProductUID($productUID)
    {
      $this->productUID = $productUID;
      return $this;
    }

    /**
     * @return int
     */
    public function getQtyAvailable()
    {
      return $this->qtyAvailable;
    }

    /**
     * @param int $qtyAvailable
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setQtyAvailable($qtyAvailable)
    {
      $this->qtyAvailable = $qtyAvailable;
      return $this;
    }

    /**
     * @return string
     */
    public function getSearchTerm()
    {
      return $this->searchTerm;
    }

    /**
     * @param string $searchTerm
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setSearchTerm($searchTerm)
    {
      $this->searchTerm = $searchTerm;
      return $this;
    }

    /**
     * @return string
     */
    public function getSizeName()
    {
      return $this->sizeName;
    }

    /**
     * @param string $sizeName
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setSizeName($sizeName)
    {
      $this->sizeName = $sizeName;
      return $this;
    }

    /**
     * @return int
     */
    public function getSizeSortOrder()
    {
      return $this->sizeSortOrder;
    }

    /**
     * @param int $sizeSortOrder
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setSizeSortOrder($sizeSortOrder)
    {
      $this->sizeSortOrder = $sizeSortOrder;
      return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
      return $this->sku;
    }

    /**
     * @param string $sku
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setSku($sku)
    {
      $this->sku = $sku;
      return $this;
    }

    /**
     * @return string
     */
    public function getTopSeller()
    {
      return $this->topSeller;
    }

    /**
     * @param string $topSeller
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setTopSeller($topSeller)
    {
      $this->topSeller = $topSeller;
      return $this;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
      return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     * @return \hubsoft\api\commerce\ProductDTO
     */
    public function setUnitPrice($unitPrice)
    {
      $this->unitPrice = $unitPrice;
      return $this;
    }

}
