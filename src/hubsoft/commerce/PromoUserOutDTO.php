<?php

namespace hubsoft\api\commerce;

class PromoUserOutDTO
{

    /**
     * @var ArrayOfAvailableContestDTO $availableContests
     */
    protected $availableContests = null;

    /**
     * @var ArrayOfAvailablePromotionDTO $availablePromotions
     */
    protected $availablePromotions = null;

    /**
     * @var ArrayOfAvailableSchoolDTO $availableSchools
     */
    protected $availableSchools = null;

    /**
     * @var string $billtoCity
     */
    protected $billtoCity = null;

    /**
     * @var string $billtoCountryCode
     */
    protected $billtoCountryCode = null;

    /**
     * @var string $billtoPostalCode
     */
    protected $billtoPostalCode = null;

    /**
     * @var string $billtoStateCode
     */
    protected $billtoStateCode = null;

    /**
     * @var string $billtoStreet
     */
    protected $billtoStreet = null;

    /**
     * @var string $billtoStreet2
     */
    protected $billtoStreet2 = null;

    /**
     * @var ArrayOfCertificationDTO $certifications
     */
    protected $certifications = null;

    /**
     * @var ArrayOfNameValueBean $customerInfo
     */
    protected $customerInfo = null;

    /**
     * @var string $email
     */
    protected $email = null;

    /**
     * @var ArrayOfNameValueBean $employeePurchaseInfo
     */
    protected $employeePurchaseInfo = null;

    /**
     * @var string $firstName
     */
    protected $firstName = null;

    /**
     * @var string $ipSource
     */
    protected $ipSource = null;

    /**
     * @var string $lastName
     */
    protected $lastName = null;

    /**
     * @var ArrayOfOrderSummaryDTO $myOrders
     */
    protected $myOrders = null;

    /**
     * @var string $phone
     */
    protected $phone = null;

    /**
     * @var string $receiveNewsletter
     */
    protected $receiveNewsletter = null;

    /**
     * @var string $receivePromoEmails
     */
    protected $receivePromoEmails = null;

    /**
     * @var int $remainingPurchasePoints
     */
    protected $remainingPurchasePoints = null;

    /**
     * @var string $salesRepCode
     */
    protected $salesRepCode = null;

    /**
     * @var string $salesRepName
     */
    protected $salesRepName = null;

    /**
     * @var string $shiptoCity
     */
    protected $shiptoCity = null;

    /**
     * @var string $shiptoCountryCode
     */
    protected $shiptoCountryCode = null;

    /**
     * @var string $shiptoPostalCode
     */
    protected $shiptoPostalCode = null;

    /**
     * @var string $shiptoStateCode
     */
    protected $shiptoStateCode = null;

    /**
     * @var string $shiptoStreet
     */
    protected $shiptoStreet = null;

    /**
     * @var string $shiptoStreet2
     */
    protected $shiptoStreet2 = null;

    /**
     * @var int $totalContestPoints
     */
    protected $totalContestPoints = null;

    /**
     * @var int $totalTrainingPoints
     */
    protected $totalTrainingPoints = null;

    /**
     * @var ArrayOfTrainingProgressDTO $trainingProgress
     */
    protected $trainingProgress = null;

    /**
     * @var ArrayOfUserTrainingResultDTO $trainingResults
     */
    protected $trainingResults = null;

    /**
     * @var string $userStatus
     */
    protected $userStatus = null;

    /**
     * @var string $userType
     */
    protected $userType = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAvailableContestDTO
     */
    public function getAvailableContests()
    {
      return $this->availableContests;
    }

    /**
     * @param ArrayOfAvailableContestDTO $availableContests
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setAvailableContests($availableContests)
    {
      $this->availableContests = $availableContests;
      return $this;
    }

    /**
     * @return ArrayOfAvailablePromotionDTO
     */
    public function getAvailablePromotions()
    {
      return $this->availablePromotions;
    }

    /**
     * @param ArrayOfAvailablePromotionDTO $availablePromotions
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setAvailablePromotions($availablePromotions)
    {
      $this->availablePromotions = $availablePromotions;
      return $this;
    }

    /**
     * @return ArrayOfAvailableSchoolDTO
     */
    public function getAvailableSchools()
    {
      return $this->availableSchools;
    }

    /**
     * @param ArrayOfAvailableSchoolDTO $availableSchools
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setAvailableSchools($availableSchools)
    {
      $this->availableSchools = $availableSchools;
      return $this;
    }

    /**
     * @return string
     */
    public function getBilltoCity()
    {
      return $this->billtoCity;
    }

    /**
     * @param string $billtoCity
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setBilltoCity($billtoCity)
    {
      $this->billtoCity = $billtoCity;
      return $this;
    }

    /**
     * @return string
     */
    public function getBilltoCountryCode()
    {
      return $this->billtoCountryCode;
    }

    /**
     * @param string $billtoCountryCode
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setBilltoCountryCode($billtoCountryCode)
    {
      $this->billtoCountryCode = $billtoCountryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBilltoPostalCode()
    {
      return $this->billtoPostalCode;
    }

    /**
     * @param string $billtoPostalCode
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setBilltoPostalCode($billtoPostalCode)
    {
      $this->billtoPostalCode = $billtoPostalCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBilltoStateCode()
    {
      return $this->billtoStateCode;
    }

    /**
     * @param string $billtoStateCode
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setBilltoStateCode($billtoStateCode)
    {
      $this->billtoStateCode = $billtoStateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBilltoStreet()
    {
      return $this->billtoStreet;
    }

    /**
     * @param string $billtoStreet
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setBilltoStreet($billtoStreet)
    {
      $this->billtoStreet = $billtoStreet;
      return $this;
    }

    /**
     * @return string
     */
    public function getBilltoStreet2()
    {
      return $this->billtoStreet2;
    }

    /**
     * @param string $billtoStreet2
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setBilltoStreet2($billtoStreet2)
    {
      $this->billtoStreet2 = $billtoStreet2;
      return $this;
    }

    /**
     * @return ArrayOfCertificationDTO
     */
    public function getCertifications()
    {
      return $this->certifications;
    }

    /**
     * @param ArrayOfCertificationDTO $certifications
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setCertifications($certifications)
    {
      $this->certifications = $certifications;
      return $this;
    }

    /**
     * @return ArrayOfNameValueBean
     */
    public function getCustomerInfo()
    {
      return $this->customerInfo;
    }

    /**
     * @param ArrayOfNameValueBean $customerInfo
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setCustomerInfo($customerInfo)
    {
      $this->customerInfo = $customerInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
      return $this->email;
    }

    /**
     * @param string $email
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

    /**
     * @return ArrayOfNameValueBean
     */
    public function getEmployeePurchaseInfo()
    {
      return $this->employeePurchaseInfo;
    }

    /**
     * @param ArrayOfNameValueBean $employeePurchaseInfo
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setEmployeePurchaseInfo($employeePurchaseInfo)
    {
      $this->employeePurchaseInfo = $employeePurchaseInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setFirstName($firstName)
    {
      $this->firstName = $firstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getIpSource()
    {
      return $this->ipSource;
    }

    /**
     * @param string $ipSource
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setIpSource($ipSource)
    {
      $this->ipSource = $ipSource;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setLastName($lastName)
    {
      $this->lastName = $lastName;
      return $this;
    }

    /**
     * @return ArrayOfOrderSummaryDTO
     */
    public function getMyOrders()
    {
      return $this->myOrders;
    }

    /**
     * @param ArrayOfOrderSummaryDTO $myOrders
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setMyOrders($myOrders)
    {
      $this->myOrders = $myOrders;
      return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
      return $this->phone;
    }

    /**
     * @param string $phone
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setPhone($phone)
    {
      $this->phone = $phone;
      return $this;
    }

    /**
     * @return string
     */
    public function getReceiveNewsletter()
    {
      return $this->receiveNewsletter;
    }

    /**
     * @param string $receiveNewsletter
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setReceiveNewsletter($receiveNewsletter)
    {
      $this->receiveNewsletter = $receiveNewsletter;
      return $this;
    }

    /**
     * @return string
     */
    public function getReceivePromoEmails()
    {
      return $this->receivePromoEmails;
    }

    /**
     * @param string $receivePromoEmails
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setReceivePromoEmails($receivePromoEmails)
    {
      $this->receivePromoEmails = $receivePromoEmails;
      return $this;
    }

    /**
     * @return int
     */
    public function getRemainingPurchasePoints()
    {
      return $this->remainingPurchasePoints;
    }

    /**
     * @param int $remainingPurchasePoints
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setRemainingPurchasePoints($remainingPurchasePoints)
    {
      $this->remainingPurchasePoints = $remainingPurchasePoints;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesRepCode()
    {
      return $this->salesRepCode;
    }

    /**
     * @param string $salesRepCode
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setSalesRepCode($salesRepCode)
    {
      $this->salesRepCode = $salesRepCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesRepName()
    {
      return $this->salesRepName;
    }

    /**
     * @param string $salesRepName
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setSalesRepName($salesRepName)
    {
      $this->salesRepName = $salesRepName;
      return $this;
    }

    /**
     * @return string
     */
    public function getShiptoCity()
    {
      return $this->shiptoCity;
    }

    /**
     * @param string $shiptoCity
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setShiptoCity($shiptoCity)
    {
      $this->shiptoCity = $shiptoCity;
      return $this;
    }

    /**
     * @return string
     */
    public function getShiptoCountryCode()
    {
      return $this->shiptoCountryCode;
    }

    /**
     * @param string $shiptoCountryCode
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setShiptoCountryCode($shiptoCountryCode)
    {
      $this->shiptoCountryCode = $shiptoCountryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getShiptoPostalCode()
    {
      return $this->shiptoPostalCode;
    }

    /**
     * @param string $shiptoPostalCode
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setShiptoPostalCode($shiptoPostalCode)
    {
      $this->shiptoPostalCode = $shiptoPostalCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getShiptoStateCode()
    {
      return $this->shiptoStateCode;
    }

    /**
     * @param string $shiptoStateCode
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setShiptoStateCode($shiptoStateCode)
    {
      $this->shiptoStateCode = $shiptoStateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getShiptoStreet()
    {
      return $this->shiptoStreet;
    }

    /**
     * @param string $shiptoStreet
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setShiptoStreet($shiptoStreet)
    {
      $this->shiptoStreet = $shiptoStreet;
      return $this;
    }

    /**
     * @return string
     */
    public function getShiptoStreet2()
    {
      return $this->shiptoStreet2;
    }

    /**
     * @param string $shiptoStreet2
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setShiptoStreet2($shiptoStreet2)
    {
      $this->shiptoStreet2 = $shiptoStreet2;
      return $this;
    }

    /**
     * @return int
     */
    public function getTotalContestPoints()
    {
      return $this->totalContestPoints;
    }

    /**
     * @param int $totalContestPoints
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setTotalContestPoints($totalContestPoints)
    {
      $this->totalContestPoints = $totalContestPoints;
      return $this;
    }

    /**
     * @return int
     */
    public function getTotalTrainingPoints()
    {
      return $this->totalTrainingPoints;
    }

    /**
     * @param int $totalTrainingPoints
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setTotalTrainingPoints($totalTrainingPoints)
    {
      $this->totalTrainingPoints = $totalTrainingPoints;
      return $this;
    }

    /**
     * @return ArrayOfTrainingProgressDTO
     */
    public function getTrainingProgress()
    {
      return $this->trainingProgress;
    }

    /**
     * @param ArrayOfTrainingProgressDTO $trainingProgress
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setTrainingProgress($trainingProgress)
    {
      $this->trainingProgress = $trainingProgress;
      return $this;
    }

    /**
     * @return ArrayOfUserTrainingResultDTO
     */
    public function getTrainingResults()
    {
      return $this->trainingResults;
    }

    /**
     * @param ArrayOfUserTrainingResultDTO $trainingResults
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setTrainingResults($trainingResults)
    {
      $this->trainingResults = $trainingResults;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserStatus()
    {
      return $this->userStatus;
    }

    /**
     * @param string $userStatus
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setUserStatus($userStatus)
    {
      $this->userStatus = $userStatus;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserType()
    {
      return $this->userType;
    }

    /**
     * @param string $userType
     * @return \hubsoft\api\commerce\PromoUserOutDTO
     */
    public function setUserType($userType)
    {
      $this->userType = $userType;
      return $this;
    }

}
