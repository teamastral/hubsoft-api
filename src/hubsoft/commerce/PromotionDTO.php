<?php

namespace hubsoft\api\commerce;

class PromotionDTO
{

    /**
     * @var string $allowGiftCardPayments
     */
    protected $allowGiftCardPayments = null;

    /**
     * @var string $allowInternationalShipping
     */
    protected $allowInternationalShipping = null;

    /**
     * @var string $collectEmailOptIn
     */
    protected $collectEmailOptIn = null;

    /**
     * @var ArrayOfCouponDTO $couponList
     */
    protected $couponList = null;

    /**
     * @var int $maxTotalOrderedQty
     */
    protected $maxTotalOrderedQty = null;

    /**
     * @var ArrayOfPaymentMethodDTO $paymentOptions
     */
    protected $paymentOptions = null;

    /**
     * @var ArrayOfProductColorDTO $productColors
     */
    protected $productColors = null;

    /**
     * @var ArrayOfProductDTO $products
     */
    protected $products = null;

    /**
     * @var string $promotionCode
     */
    protected $promotionCode = null;

    /**
     * @var string $promotionName
     */
    protected $promotionName = null;

    /**
     * @var string $promotionTypeCode
     */
    protected $promotionTypeCode = null;

    /**
     * @var string $returnPolicy
     */
    protected $returnPolicy = null;

    /**
     * @var string $salesRepCode
     */
    protected $salesRepCode = null;

    /**
     * @var ArrayOfShippingMethodDTO $shippingOptions
     */
    protected $shippingOptions = null;

    /**
     * @var ArrayOfTechnologyDTO $technologies
     */
    protected $technologies = null;

    /**
     * @var string $welcomeMessage
     */
    protected $welcomeMessage = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAllowGiftCardPayments()
    {
      return $this->allowGiftCardPayments;
    }

    /**
     * @param string $allowGiftCardPayments
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setAllowGiftCardPayments($allowGiftCardPayments)
    {
      $this->allowGiftCardPayments = $allowGiftCardPayments;
      return $this;
    }

    /**
     * @return string
     */
    public function getAllowInternationalShipping()
    {
      return $this->allowInternationalShipping;
    }

    /**
     * @param string $allowInternationalShipping
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setAllowInternationalShipping($allowInternationalShipping)
    {
      $this->allowInternationalShipping = $allowInternationalShipping;
      return $this;
    }

    /**
     * @return string
     */
    public function getCollectEmailOptIn()
    {
      return $this->collectEmailOptIn;
    }

    /**
     * @param string $collectEmailOptIn
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setCollectEmailOptIn($collectEmailOptIn)
    {
      $this->collectEmailOptIn = $collectEmailOptIn;
      return $this;
    }

    /**
     * @return ArrayOfCouponDTO
     */
    public function getCouponList()
    {
      return $this->couponList;
    }

    /**
     * @param ArrayOfCouponDTO $couponList
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setCouponList($couponList)
    {
      $this->couponList = $couponList;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxTotalOrderedQty()
    {
      return $this->maxTotalOrderedQty;
    }

    /**
     * @param int $maxTotalOrderedQty
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setMaxTotalOrderedQty($maxTotalOrderedQty)
    {
      $this->maxTotalOrderedQty = $maxTotalOrderedQty;
      return $this;
    }

    /**
     * @return ArrayOfPaymentMethodDTO
     */
    public function getPaymentOptions()
    {
      return $this->paymentOptions;
    }

    /**
     * @param ArrayOfPaymentMethodDTO $paymentOptions
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setPaymentOptions($paymentOptions)
    {
      $this->paymentOptions = $paymentOptions;
      return $this;
    }

    /**
     * @return ArrayOfProductColorDTO
     */
    public function getProductColors()
    {
      return $this->productColors;
    }

    /**
     * @param ArrayOfProductColorDTO $productColors
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setProductColors($productColors)
    {
      $this->productColors = $productColors;
      return $this;
    }

    /**
     * @return ArrayOfProductDTO
     */
    public function getProducts()
    {
      return $this->products;
    }

    /**
     * @param ArrayOfProductDTO $products
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setProducts($products)
    {
      $this->products = $products;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->promotionCode;
    }

    /**
     * @param string $promotionCode
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setPromotionCode($promotionCode)
    {
      $this->promotionCode = $promotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionName()
    {
      return $this->promotionName;
    }

    /**
     * @param string $promotionName
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setPromotionName($promotionName)
    {
      $this->promotionName = $promotionName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionTypeCode()
    {
      return $this->promotionTypeCode;
    }

    /**
     * @param string $promotionTypeCode
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setPromotionTypeCode($promotionTypeCode)
    {
      $this->promotionTypeCode = $promotionTypeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getReturnPolicy()
    {
      return $this->returnPolicy;
    }

    /**
     * @param string $returnPolicy
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setReturnPolicy($returnPolicy)
    {
      $this->returnPolicy = $returnPolicy;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesRepCode()
    {
      return $this->salesRepCode;
    }

    /**
     * @param string $salesRepCode
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setSalesRepCode($salesRepCode)
    {
      $this->salesRepCode = $salesRepCode;
      return $this;
    }

    /**
     * @return ArrayOfShippingMethodDTO
     */
    public function getShippingOptions()
    {
      return $this->shippingOptions;
    }

    /**
     * @param ArrayOfShippingMethodDTO $shippingOptions
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setShippingOptions($shippingOptions)
    {
      $this->shippingOptions = $shippingOptions;
      return $this;
    }

    /**
     * @return ArrayOfTechnologyDTO
     */
    public function getTechnologies()
    {
      return $this->technologies;
    }

    /**
     * @param ArrayOfTechnologyDTO $technologies
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setTechnologies($technologies)
    {
      $this->technologies = $technologies;
      return $this;
    }

    /**
     * @return string
     */
    public function getWelcomeMessage()
    {
      return $this->welcomeMessage;
    }

    /**
     * @param string $welcomeMessage
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function setWelcomeMessage($welcomeMessage)
    {
      $this->welcomeMessage = $welcomeMessage;
      return $this;
    }

}
