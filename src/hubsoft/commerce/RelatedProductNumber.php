<?php

namespace hubsoft\api\commerce;

class RelatedProductNumber
{

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \hubsoft\api\commerce\RelatedProductNumber
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

}
