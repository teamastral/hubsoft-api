<?php

namespace hubsoft\api\commerce;

class ShippingMethodDTO
{

    /**
     * @var string $displayFor
     */
    protected $displayFor = null;

    /**
     * @var string $shippingCode
     */
    protected $shippingCode = null;

    /**
     * @var string $shippingDesc
     */
    protected $shippingDesc = null;

    /**
     * @var int $sortOrder
     */
    protected $sortOrder = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDisplayFor()
    {
      return $this->displayFor;
    }

    /**
     * @param string $displayFor
     * @return \hubsoft\api\commerce\ShippingMethodDTO
     */
    public function setDisplayFor($displayFor)
    {
      $this->displayFor = $displayFor;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingCode()
    {
      return $this->shippingCode;
    }

    /**
     * @param string $shippingCode
     * @return \hubsoft\api\commerce\ShippingMethodDTO
     */
    public function setShippingCode($shippingCode)
    {
      $this->shippingCode = $shippingCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingDesc()
    {
      return $this->shippingDesc;
    }

    /**
     * @param string $shippingDesc
     * @return \hubsoft\api\commerce\ShippingMethodDTO
     */
    public function setShippingDesc($shippingDesc)
    {
      $this->shippingDesc = $shippingDesc;
      return $this;
    }

    /**
     * @return int
     */
    public function getSortOrder()
    {
      return $this->sortOrder;
    }

    /**
     * @param int $sortOrder
     * @return \hubsoft\api\commerce\ShippingMethodDTO
     */
    public function setSortOrder($sortOrder)
    {
      $this->sortOrder = $sortOrder;
      return $this;
    }

}
