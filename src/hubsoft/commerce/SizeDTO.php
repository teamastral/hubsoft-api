<?php

namespace hubsoft\api\commerce;

class SizeDTO
{

    /**
     * @var float $msrp
     */
    protected $msrp = null;

    /**
     * @var string $nextAvailableDate
     */
    protected $nextAvailableDate = null;

    /**
     * @var int $nextAvailableQty
     */
    protected $nextAvailableQty = null;

    /**
     * @var int $qtyAvailable
     */
    protected $qtyAvailable = null;

    /**
     * @var string $sizeName
     */
    protected $sizeName = null;

    /**
     * @var string $sku
     */
    protected $sku = null;

    /**
     * @var int $sortOrder
     */
    protected $sortOrder = null;

    /**
     * @var float $unitPrice
     */
    protected $unitPrice = null;

    /**
     * @var string $upc
     */
    protected $upc = null;

    /**
     * @var float $wholesalePrice
     */
    protected $wholesalePrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getMsrp()
    {
      return $this->msrp;
    }

    /**
     * @param float $msrp
     * @return \hubsoft\api\commerce\SizeDTO
     */
    public function setMsrp($msrp)
    {
      $this->msrp = $msrp;
      return $this;
    }

    /**
     * @return string
     */
    public function getNextAvailableDate()
    {
      return $this->nextAvailableDate;
    }

    /**
     * @param string $nextAvailableDate
     * @return \hubsoft\api\commerce\SizeDTO
     */
    public function setNextAvailableDate($nextAvailableDate)
    {
      $this->nextAvailableDate = $nextAvailableDate;
      return $this;
    }

    /**
     * @return int
     */
    public function getNextAvailableQty()
    {
      return $this->nextAvailableQty;
    }

    /**
     * @param int $nextAvailableQty
     * @return \hubsoft\api\commerce\SizeDTO
     */
    public function setNextAvailableQty($nextAvailableQty)
    {
      $this->nextAvailableQty = $nextAvailableQty;
      return $this;
    }

    /**
     * @return int
     */
    public function getQtyAvailable()
    {
      return $this->qtyAvailable;
    }

    /**
     * @param int $qtyAvailable
     * @return \hubsoft\api\commerce\SizeDTO
     */
    public function setQtyAvailable($qtyAvailable)
    {
      $this->qtyAvailable = $qtyAvailable;
      return $this;
    }

    /**
     * @return string
     */
    public function getSizeName()
    {
      return $this->sizeName;
    }

    /**
     * @param string $sizeName
     * @return \hubsoft\api\commerce\SizeDTO
     */
    public function setSizeName($sizeName)
    {
      $this->sizeName = $sizeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
      return $this->sku;
    }

    /**
     * @param string $sku
     * @return \hubsoft\api\commerce\SizeDTO
     */
    public function setSku($sku)
    {
      $this->sku = $sku;
      return $this;
    }

    /**
     * @return int
     */
    public function getSortOrder()
    {
      return $this->sortOrder;
    }

    /**
     * @param int $sortOrder
     * @return \hubsoft\api\commerce\SizeDTO
     */
    public function setSortOrder($sortOrder)
    {
      $this->sortOrder = $sortOrder;
      return $this;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
      return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     * @return \hubsoft\api\commerce\SizeDTO
     */
    public function setUnitPrice($unitPrice)
    {
      $this->unitPrice = $unitPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getUpc()
    {
      return $this->upc;
    }

    /**
     * @param string $upc
     * @return \hubsoft\api\commerce\SizeDTO
     */
    public function setUpc($upc)
    {
      $this->upc = $upc;
      return $this;
    }

    /**
     * @return float
     */
    public function getWholesalePrice()
    {
      return $this->wholesalePrice;
    }

    /**
     * @param float $wholesalePrice
     * @return \hubsoft\api\commerce\SizeDTO
     */
    public function setWholesalePrice($wholesalePrice)
    {
      $this->wholesalePrice = $wholesalePrice;
      return $this;
    }

}
