<?php

namespace hubsoft\api\commerce;

class TechnologyDTO
{

    /**
     * @var string $code
     */
    protected $code = null;

    /**
     * @var string $description
     */
    protected $description = null;

    /**
     * @var string $display
     */
    protected $display = null;

    /**
     * @var string $iconUrl
     */
    protected $iconUrl = null;

    /**
     * @var string $name
     */
    protected $name = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->code;
    }

    /**
     * @param string $code
     * @return \hubsoft\api\commerce\TechnologyDTO
     */
    public function setCode($code)
    {
      $this->code = $code;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->description;
    }

    /**
     * @param string $description
     * @return \hubsoft\api\commerce\TechnologyDTO
     */
    public function setDescription($description)
    {
      $this->description = $description;
      return $this;
    }

    /**
     * @return string
     */
    public function getDisplay()
    {
      return $this->display;
    }

    /**
     * @param string $display
     * @return \hubsoft\api\commerce\TechnologyDTO
     */
    public function setDisplay($display)
    {
      $this->display = $display;
      return $this;
    }

    /**
     * @return string
     */
    public function getIconUrl()
    {
      return $this->iconUrl;
    }

    /**
     * @param string $iconUrl
     * @return \hubsoft\api\commerce\TechnologyDTO
     */
    public function setIconUrl($iconUrl)
    {
      $this->iconUrl = $iconUrl;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return \hubsoft\api\commerce\TechnologyDTO
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

}
