<?php

namespace hubsoft\api\commerce;

class TrainingProgressDTO
{

    /**
     * @var int $availablePoints
     */
    protected $availablePoints = null;

    /**
     * @var string $description
     */
    protected $description = null;

    /**
     * @var int $pointsEarned
     */
    protected $pointsEarned = null;

    /**
     * @var string $progressBarName
     */
    protected $progressBarName = null;

    /**
     * @var int $rulefromPoints
     */
    protected $rulefromPoints = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getAvailablePoints()
    {
      return $this->availablePoints;
    }

    /**
     * @param int $availablePoints
     * @return \hubsoft\api\commerce\TrainingProgressDTO
     */
    public function setAvailablePoints($availablePoints)
    {
      $this->availablePoints = $availablePoints;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->description;
    }

    /**
     * @param string $description
     * @return \hubsoft\api\commerce\TrainingProgressDTO
     */
    public function setDescription($description)
    {
      $this->description = $description;
      return $this;
    }

    /**
     * @return int
     */
    public function getPointsEarned()
    {
      return $this->pointsEarned;
    }

    /**
     * @param int $pointsEarned
     * @return \hubsoft\api\commerce\TrainingProgressDTO
     */
    public function setPointsEarned($pointsEarned)
    {
      $this->pointsEarned = $pointsEarned;
      return $this;
    }

    /**
     * @return string
     */
    public function getProgressBarName()
    {
      return $this->progressBarName;
    }

    /**
     * @param string $progressBarName
     * @return \hubsoft\api\commerce\TrainingProgressDTO
     */
    public function setProgressBarName($progressBarName)
    {
      $this->progressBarName = $progressBarName;
      return $this;
    }

    /**
     * @return int
     */
    public function getRulefromPoints()
    {
      return $this->rulefromPoints;
    }

    /**
     * @param int $rulefromPoints
     * @return \hubsoft\api\commerce\TrainingProgressDTO
     */
    public function setRulefromPoints($rulefromPoints)
    {
      $this->rulefromPoints = $rulefromPoints;
      return $this;
    }

}
