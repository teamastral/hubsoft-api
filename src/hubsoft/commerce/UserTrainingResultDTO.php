<?php

namespace hubsoft\api\commerce;

class UserTrainingResultDTO
{

    /**
     * @var string $dateCompleted
     */
    protected $dateCompleted = null;

    /**
     * @var int $moduleUid
     */
    protected $moduleUid = null;

    /**
     * @var string $passNotPass
     */
    protected $passNotPass = null;

    /**
     * @var string $points
     */
    protected $points = null;

    /**
     * @var int $schoolUid
     */
    protected $schoolUid = null;

    /**
     * @var float $score
     */
    protected $score = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDateCompleted()
    {
      return $this->dateCompleted;
    }

    /**
     * @param string $dateCompleted
     * @return \hubsoft\api\commerce\UserTrainingResultDTO
     */
    public function setDateCompleted($dateCompleted)
    {
      $this->dateCompleted = $dateCompleted;
      return $this;
    }

    /**
     * @return int
     */
    public function getModuleUid()
    {
      return $this->moduleUid;
    }

    /**
     * @param int $moduleUid
     * @return \hubsoft\api\commerce\UserTrainingResultDTO
     */
    public function setModuleUid($moduleUid)
    {
      $this->moduleUid = $moduleUid;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassNotPass()
    {
      return $this->passNotPass;
    }

    /**
     * @param string $passNotPass
     * @return \hubsoft\api\commerce\UserTrainingResultDTO
     */
    public function setPassNotPass($passNotPass)
    {
      $this->passNotPass = $passNotPass;
      return $this;
    }

    /**
     * @return string
     */
    public function getPoints()
    {
      return $this->points;
    }

    /**
     * @param string $points
     * @return \hubsoft\api\commerce\UserTrainingResultDTO
     */
    public function setPoints($points)
    {
      $this->points = $points;
      return $this;
    }

    /**
     * @return int
     */
    public function getSchoolUid()
    {
      return $this->schoolUid;
    }

    /**
     * @param int $schoolUid
     * @return \hubsoft\api\commerce\UserTrainingResultDTO
     */
    public function setSchoolUid($schoolUid)
    {
      $this->schoolUid = $schoolUid;
      return $this;
    }

    /**
     * @return float
     */
    public function getScore()
    {
      return $this->score;
    }

    /**
     * @param float $score
     * @return \hubsoft\api\commerce\UserTrainingResultDTO
     */
    public function setScore($score)
    {
      $this->score = $score;
      return $this;
    }

}
