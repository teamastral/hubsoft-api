<?php

namespace hubsoft\api\commerce;

class ValidateAuthCodeResponse
{

    /**
     * @var ArrayOfErrorDTO $errors
     */
    protected $errors = null;

    /**
     * @var string $promoProgramCode
     */
    protected $promoProgramCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfErrorDTO
     */
    public function getErrors()
    {
      return $this->errors;
    }

    /**
     * @param ArrayOfErrorDTO $errors
     * @return \hubsoft\api\commerce\ValidateAuthCodeResponse
     */
    public function setErrors($errors)
    {
      $this->errors = $errors;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromoProgramCode()
    {
      return $this->promoProgramCode;
    }

    /**
     * @param string $promoProgramCode
     * @return \hubsoft\api\commerce\ValidateAuthCodeResponse
     */
    public function setPromoProgramCode($promoProgramCode)
    {
      $this->promoProgramCode = $promoProgramCode;
      return $this;
    }

}
