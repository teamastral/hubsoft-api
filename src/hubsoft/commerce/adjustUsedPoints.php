<?php

namespace hubsoft\api\commerce;

class adjustUsedPoints
{

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @var string $sessionToken
     */
    protected $sessionToken = null;

    /**
     * @var int $usedPoints
     */
    protected $usedPoints = null;

    /**
     * @param string $accessKey
     * @param string $sessionToken
     * @param int $usedPoints
     */
    public function __construct($accessKey, $sessionToken, $usedPoints)
    {
      $this->accessKey = $accessKey;
      $this->sessionToken = $sessionToken;
      $this->usedPoints = $usedPoints;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\adjustUsedPoints
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSessionToken()
    {
      return $this->sessionToken;
    }

    /**
     * @param string $sessionToken
     * @return \hubsoft\api\commerce\adjustUsedPoints
     */
    public function setSessionToken($sessionToken)
    {
      $this->sessionToken = $sessionToken;
      return $this;
    }

    /**
     * @return int
     */
    public function getUsedPoints()
    {
      return $this->usedPoints;
    }

    /**
     * @param int $usedPoints
     * @return \hubsoft\api\commerce\adjustUsedPoints
     */
    public function setUsedPoints($usedPoints)
    {
      $this->usedPoints = $usedPoints;
      return $this;
    }

}
