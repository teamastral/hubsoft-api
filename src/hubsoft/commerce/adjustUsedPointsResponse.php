<?php

namespace hubsoft\api\commerce;

class adjustUsedPointsResponse
{

    /**
     * @var int $out
     */
    protected $out = null;

    /**
     * @param int $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return int
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param int $out
     * @return \hubsoft\api\commerce\adjustUsedPointsResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
