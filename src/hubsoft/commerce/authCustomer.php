<?php

namespace hubsoft\api\commerce;

class authCustomer
{

    /**
     * @var string $email
     */
    protected $email = null;

    /**
     * @var string $password
     */
    protected $password = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $email
     * @param string $password
     * @param string $accessKey
     */
    public function __construct($email, $password, $accessKey)
    {
      $this->email = $email;
      $this->password = $password;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
      return $this->email;
    }

    /**
     * @param string $email
     * @return \hubsoft\api\commerce\authCustomer
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
      return $this->password;
    }

    /**
     * @param string $password
     * @return \hubsoft\api\commerce\authCustomer
     */
    public function setPassword($password)
    {
      $this->password = $password;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\authCustomer
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
