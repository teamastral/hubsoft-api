<?php

namespace hubsoft\api\commerce;

class authCustomerByEmployeeCode
{

    /**
     * @var string $employeeCode
     */
    protected $employeeCode = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $employeeCode
     * @param string $accessKey
     */
    public function __construct($employeeCode, $accessKey)
    {
      $this->employeeCode = $employeeCode;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getEmployeeCode()
    {
      return $this->employeeCode;
    }

    /**
     * @param string $employeeCode
     * @return \hubsoft\api\commerce\authCustomerByEmployeeCode
     */
    public function setEmployeeCode($employeeCode)
    {
      $this->employeeCode = $employeeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\authCustomerByEmployeeCode
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
