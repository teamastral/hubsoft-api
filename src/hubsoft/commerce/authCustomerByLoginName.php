<?php

namespace hubsoft\api\commerce;

class authCustomerByLoginName
{

    /**
     * @var string $loginName
     */
    protected $loginName = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $loginName
     * @param string $accessKey
     */
    public function __construct($loginName, $accessKey)
    {
      $this->loginName = $loginName;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getLoginName()
    {
      return $this->loginName;
    }

    /**
     * @param string $loginName
     * @return \hubsoft\api\commerce\authCustomerByLoginName
     */
    public function setLoginName($loginName)
    {
      $this->loginName = $loginName;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\authCustomerByLoginName
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
