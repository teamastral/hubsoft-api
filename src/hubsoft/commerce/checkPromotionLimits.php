<?php

namespace hubsoft\api\commerce;

class checkPromotionLimits
{

    /**
     * @var OrderDTO $order
     */
    protected $order = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param OrderDTO $order
     * @param string $accessKey
     */
    public function __construct($order, $accessKey)
    {
      $this->order = $order;
      $this->accessKey = $accessKey;
    }

    /**
     * @return OrderDTO
     */
    public function getOrder()
    {
      return $this->order;
    }

    /**
     * @param OrderDTO $order
     * @return \hubsoft\api\commerce\checkPromotionLimits
     */
    public function setOrder($order)
    {
      $this->order = $order;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\checkPromotionLimits
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
