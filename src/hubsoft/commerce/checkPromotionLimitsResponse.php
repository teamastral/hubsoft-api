<?php

namespace hubsoft\api\commerce;

class checkPromotionLimitsResponse
{

    /**
     * @var CheckPromotionLimitsResponse $out
     */
    protected $out = null;

    /**
     * @param CheckPromotionLimitsResponse $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return CheckPromotionLimitsResponse
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param CheckPromotionLimitsResponse $out
     * @return \hubsoft\api\commerce\checkPromotionLimitsResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
