<?php

namespace hubsoft\api\commerce;

class createCustomer
{

    /**
     * @var PromoUserInDTO $userDTO
     */
    protected $userDTO = null;

    /**
     * @var string $promotionCode
     */
    protected $promotionCode = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param PromoUserInDTO $userDTO
     * @param string $promotionCode
     * @param string $accessKey
     */
    public function __construct($userDTO, $promotionCode, $accessKey)
    {
      $this->userDTO = $userDTO;
      $this->promotionCode = $promotionCode;
      $this->accessKey = $accessKey;
    }

    /**
     * @return PromoUserInDTO
     */
    public function getUserDTO()
    {
      return $this->userDTO;
    }

    /**
     * @param PromoUserInDTO $userDTO
     * @return \hubsoft\api\commerce\createCustomer
     */
    public function setUserDTO($userDTO)
    {
      $this->userDTO = $userDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->promotionCode;
    }

    /**
     * @param string $promotionCode
     * @return \hubsoft\api\commerce\createCustomer
     */
    public function setPromotionCode($promotionCode)
    {
      $this->promotionCode = $promotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\createCustomer
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
