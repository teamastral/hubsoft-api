<?php

namespace hubsoft\api\commerce;

class getActivePromotions
{

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $accessKey
     */
    public function __construct($accessKey)
    {
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\getActivePromotions
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
