<?php

namespace hubsoft\api\commerce;

class getActivePromotionsResponse
{

    /**
     * @var ArrayOfNameValueBean $out
     */
    protected $out = null;

    /**
     * @param ArrayOfNameValueBean $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfNameValueBean
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfNameValueBean $out
     * @return \hubsoft\api\commerce\getActivePromotionsResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
