<?php

namespace hubsoft\api\commerce;

class getAuthCode
{

    /**
     * @var string $promotionCode
     */
    protected $promotionCode = null;

    /**
     * @var string $expireDays
     */
    protected $expireDays = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $promotionCode
     * @param string $expireDays
     * @param string $accessKey
     */
    public function __construct($promotionCode, $expireDays, $accessKey)
    {
      $this->promotionCode = $promotionCode;
      $this->expireDays = $expireDays;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->promotionCode;
    }

    /**
     * @param string $promotionCode
     * @return \hubsoft\api\commerce\getAuthCode
     */
    public function setPromotionCode($promotionCode)
    {
      $this->promotionCode = $promotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getExpireDays()
    {
      return $this->expireDays;
    }

    /**
     * @param string $expireDays
     * @return \hubsoft\api\commerce\getAuthCode
     */
    public function setExpireDays($expireDays)
    {
      $this->expireDays = $expireDays;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\getAuthCode
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
