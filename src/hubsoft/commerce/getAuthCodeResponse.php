<?php

namespace hubsoft\api\commerce;

class getAuthCodeResponse
{

    /**
     * @var GetAuthCodeResponse $out
     */
    protected $out = null;

    /**
     * @param GetAuthCodeResponse $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return GetAuthCodeResponse
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param GetAuthCodeResponse $out
     * @return \hubsoft\api\commerce\getAuthCodeResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
