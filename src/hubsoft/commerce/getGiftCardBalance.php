<?php

namespace hubsoft\api\commerce;

class getGiftCardBalance
{

    /**
     * @var string $giftCardNumber
     */
    protected $giftCardNumber = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $giftCardNumber
     * @param string $accessKey
     */
    public function __construct($giftCardNumber, $accessKey)
    {
      $this->giftCardNumber = $giftCardNumber;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getGiftCardNumber()
    {
      return $this->giftCardNumber;
    }

    /**
     * @param string $giftCardNumber
     * @return \hubsoft\api\commerce\getGiftCardBalance
     */
    public function setGiftCardNumber($giftCardNumber)
    {
      $this->giftCardNumber = $giftCardNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\getGiftCardBalance
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
