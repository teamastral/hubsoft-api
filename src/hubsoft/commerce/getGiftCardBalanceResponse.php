<?php

namespace hubsoft\api\commerce;

class getGiftCardBalanceResponse
{

    /**
     * @var float $out
     */
    protected $out = null;

    /**
     * @param float $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return float
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param float $out
     * @return \hubsoft\api\commerce\getGiftCardBalanceResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
