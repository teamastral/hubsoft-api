<?php

namespace hubsoft\api\commerce;

class getPromotionDetail
{

    /**
     * @var string $promotionCode
     */
    protected $promotionCode = null;

    /**
     * @var int $classificationUID
     */
    protected $classificationUID = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $promotionCode
     * @param int $classificationUID
     * @param string $accessKey
     */
    public function __construct($promotionCode, $classificationUID, $accessKey)
    {
      $this->promotionCode = $promotionCode;
      $this->classificationUID = $classificationUID;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->promotionCode;
    }

    /**
     * @param string $promotionCode
     * @return \hubsoft\api\commerce\getPromotionDetail
     */
    public function setPromotionCode($promotionCode)
    {
      $this->promotionCode = $promotionCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getClassificationUID()
    {
      return $this->classificationUID;
    }

    /**
     * @param int $classificationUID
     * @return \hubsoft\api\commerce\getPromotionDetail
     */
    public function setClassificationUID($classificationUID)
    {
      $this->classificationUID = $classificationUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\getPromotionDetail
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
