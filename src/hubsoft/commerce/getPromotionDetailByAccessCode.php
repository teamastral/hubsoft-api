<?php

namespace hubsoft\api\commerce;

class getPromotionDetailByAccessCode
{

    /**
     * @var string $authorizationCode
     */
    protected $authorizationCode = null;

    /**
     * @var int $classificationUID
     */
    protected $classificationUID = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $authorizationCode
     * @param int $classificationUID
     * @param string $accessKey
     */
    public function __construct($authorizationCode, $classificationUID, $accessKey)
    {
      $this->authorizationCode = $authorizationCode;
      $this->classificationUID = $classificationUID;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getAuthorizationCode()
    {
      return $this->authorizationCode;
    }

    /**
     * @param string $authorizationCode
     * @return \hubsoft\api\commerce\getPromotionDetailByAccessCode
     */
    public function setAuthorizationCode($authorizationCode)
    {
      $this->authorizationCode = $authorizationCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getClassificationUID()
    {
      return $this->classificationUID;
    }

    /**
     * @param int $classificationUID
     * @return \hubsoft\api\commerce\getPromotionDetailByAccessCode
     */
    public function setClassificationUID($classificationUID)
    {
      $this->classificationUID = $classificationUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\getPromotionDetailByAccessCode
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
