<?php

namespace hubsoft\api\commerce;

class getPromotionDetailByAccessCodeResponse
{

    /**
     * @var PromotionDTO $out
     */
    protected $out = null;

    /**
     * @param PromotionDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return PromotionDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param PromotionDTO $out
     * @return \hubsoft\api\commerce\getPromotionDetailByAccessCodeResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
