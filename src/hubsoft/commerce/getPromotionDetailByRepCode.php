<?php

namespace hubsoft\api\commerce;

class getPromotionDetailByRepCode
{

    /**
     * @var string $repCode
     */
    protected $repCode = null;

    /**
     * @var string $promotionCode
     */
    protected $promotionCode = null;

    /**
     * @var int $classificationUID
     */
    protected $classificationUID = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $repCode
     * @param string $promotionCode
     * @param int $classificationUID
     * @param string $accessKey
     */
    public function __construct($repCode, $promotionCode, $classificationUID, $accessKey)
    {
      $this->repCode = $repCode;
      $this->promotionCode = $promotionCode;
      $this->classificationUID = $classificationUID;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getRepCode()
    {
      return $this->repCode;
    }

    /**
     * @param string $repCode
     * @return \hubsoft\api\commerce\getPromotionDetailByRepCode
     */
    public function setRepCode($repCode)
    {
      $this->repCode = $repCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->promotionCode;
    }

    /**
     * @param string $promotionCode
     * @return \hubsoft\api\commerce\getPromotionDetailByRepCode
     */
    public function setPromotionCode($promotionCode)
    {
      $this->promotionCode = $promotionCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getClassificationUID()
    {
      return $this->classificationUID;
    }

    /**
     * @param int $classificationUID
     * @return \hubsoft\api\commerce\getPromotionDetailByRepCode
     */
    public function setClassificationUID($classificationUID)
    {
      $this->classificationUID = $classificationUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\getPromotionDetailByRepCode
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
