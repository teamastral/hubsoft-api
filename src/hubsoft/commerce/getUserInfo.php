<?php

namespace hubsoft\api\commerce;

class getUserInfo
{

    /**
     * @var string $sessionToken
     */
    protected $sessionToken = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $sessionToken
     * @param string $accessKey
     */
    public function __construct($sessionToken, $accessKey)
    {
      $this->sessionToken = $sessionToken;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getSessionToken()
    {
      return $this->sessionToken;
    }

    /**
     * @param string $sessionToken
     * @return \hubsoft\api\commerce\getUserInfo
     */
    public function setSessionToken($sessionToken)
    {
      $this->sessionToken = $sessionToken;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\getUserInfo
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
