<?php

namespace hubsoft\api\commerce;

class getUserInfoResponse
{

    /**
     * @var PromoUserOutDTO $out
     */
    protected $out = null;

    /**
     * @param PromoUserOutDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return PromoUserOutDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param PromoUserOutDTO $out
     * @return \hubsoft\api\commerce\getUserInfoResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
