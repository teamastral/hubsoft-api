<?php

namespace hubsoft\api\commerce;

class payOnPoints
{

    /**
     * @var string $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @var string $sessionToken
     */
    protected $sessionToken = null;

    /**
     * @param string $orderNumber
     * @param string $accessKey
     * @param string $sessionToken
     */
    public function __construct($orderNumber, $accessKey, $sessionToken)
    {
      $this->orderNumber = $orderNumber;
      $this->accessKey = $accessKey;
      $this->sessionToken = $sessionToken;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
      return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return \hubsoft\api\commerce\payOnPoints
     */
    public function setOrderNumber($orderNumber)
    {
      $this->orderNumber = $orderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\payOnPoints
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSessionToken()
    {
      return $this->sessionToken;
    }

    /**
     * @param string $sessionToken
     * @return \hubsoft\api\commerce\payOnPoints
     */
    public function setSessionToken($sessionToken)
    {
      $this->sessionToken = $sessionToken;
      return $this;
    }

}
