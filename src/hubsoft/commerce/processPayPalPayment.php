<?php

namespace hubsoft\api\commerce;

class processPayPalPayment
{

    /**
     * @var string $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var PayPalBilling $billingInfo
     */
    protected $billingInfo = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $orderNumber
     * @param PayPalBilling $billingInfo
     * @param string $accessKey
     */
    public function __construct($orderNumber, $billingInfo, $accessKey)
    {
      $this->orderNumber = $orderNumber;
      $this->billingInfo = $billingInfo;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
      return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return \hubsoft\api\commerce\processPayPalPayment
     */
    public function setOrderNumber($orderNumber)
    {
      $this->orderNumber = $orderNumber;
      return $this;
    }

    /**
     * @return PayPalBilling
     */
    public function getBillingInfo()
    {
      return $this->billingInfo;
    }

    /**
     * @param PayPalBilling $billingInfo
     * @return \hubsoft\api\commerce\processPayPalPayment
     */
    public function setBillingInfo($billingInfo)
    {
      $this->billingInfo = $billingInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\processPayPalPayment
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
