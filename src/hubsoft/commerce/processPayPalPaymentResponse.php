<?php

namespace hubsoft\api\commerce;

class processPayPalPaymentResponse
{

    /**
     * @var PaymentResultDTO $out
     */
    protected $out = null;

    /**
     * @param PaymentResultDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return PaymentResultDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param PaymentResultDTO $out
     * @return \hubsoft\api\commerce\processPayPalPaymentResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
