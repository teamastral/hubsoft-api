<?php

namespace hubsoft\api\commerce;

class processPayment
{

    /**
     * @var string $orderNumber
     */
    protected $orderNumber = null;

    /**
     * @var BillingDTO $billingInfo
     */
    protected $billingInfo = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $orderNumber
     * @param BillingDTO $billingInfo
     * @param string $accessKey
     */
    public function __construct($orderNumber, $billingInfo, $accessKey)
    {
      $this->orderNumber = $orderNumber;
      $this->billingInfo = $billingInfo;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
      return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return \hubsoft\api\commerce\processPayment
     */
    public function setOrderNumber($orderNumber)
    {
      $this->orderNumber = $orderNumber;
      return $this;
    }

    /**
     * @return BillingDTO
     */
    public function getBillingInfo()
    {
      return $this->billingInfo;
    }

    /**
     * @param BillingDTO $billingInfo
     * @return \hubsoft\api\commerce\processPayment
     */
    public function setBillingInfo($billingInfo)
    {
      $this->billingInfo = $billingInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\processPayment
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
