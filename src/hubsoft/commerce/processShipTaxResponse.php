<?php

namespace hubsoft\api\commerce;

class processShipTaxResponse
{

    /**
     * @var OrderResultDTO $out
     */
    protected $out = null;

    /**
     * @param OrderResultDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return OrderResultDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param OrderResultDTO $out
     * @return \hubsoft\api\commerce\processShipTaxResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
