<?php

namespace hubsoft\api\commerce;

class resetPassword
{

    /**
     * @var string $email
     */
    protected $email = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $email
     * @param string $accessKey
     */
    public function __construct($email, $accessKey)
    {
      $this->email = $email;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
      return $this->email;
    }

    /**
     * @param string $email
     * @return \hubsoft\api\commerce\resetPassword
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\resetPassword
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
