<?php

namespace hubsoft\api\commerce;

class updateCustomer
{

    /**
     * @var PromoUserInDTO $userDTO
     */
    protected $userDTO = null;

    /**
     * @var string $promotionCode
     */
    protected $promotionCode = null;

    /**
     * @var string $sessionToken
     */
    protected $sessionToken = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param PromoUserInDTO $userDTO
     * @param string $promotionCode
     * @param string $sessionToken
     * @param string $accessKey
     */
    public function __construct($userDTO, $promotionCode, $sessionToken, $accessKey)
    {
      $this->userDTO = $userDTO;
      $this->promotionCode = $promotionCode;
      $this->sessionToken = $sessionToken;
      $this->accessKey = $accessKey;
    }

    /**
     * @return PromoUserInDTO
     */
    public function getUserDTO()
    {
      return $this->userDTO;
    }

    /**
     * @param PromoUserInDTO $userDTO
     * @return \hubsoft\api\commerce\updateCustomer
     */
    public function setUserDTO($userDTO)
    {
      $this->userDTO = $userDTO;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->promotionCode;
    }

    /**
     * @param string $promotionCode
     * @return \hubsoft\api\commerce\updateCustomer
     */
    public function setPromotionCode($promotionCode)
    {
      $this->promotionCode = $promotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSessionToken()
    {
      return $this->sessionToken;
    }

    /**
     * @param string $sessionToken
     * @return \hubsoft\api\commerce\updateCustomer
     */
    public function setSessionToken($sessionToken)
    {
      $this->sessionToken = $sessionToken;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\updateCustomer
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
