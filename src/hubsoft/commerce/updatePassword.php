<?php

namespace hubsoft\api\commerce;

class updatePassword
{

    /**
     * @var string $sessionToken
     */
    protected $sessionToken = null;

    /**
     * @var string $oldPassword
     */
    protected $oldPassword = null;

    /**
     * @var string $newPassword
     */
    protected $newPassword = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $sessionToken
     * @param string $oldPassword
     * @param string $newPassword
     * @param string $accessKey
     */
    public function __construct($sessionToken, $oldPassword, $newPassword, $accessKey)
    {
      $this->sessionToken = $sessionToken;
      $this->oldPassword = $oldPassword;
      $this->newPassword = $newPassword;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getSessionToken()
    {
      return $this->sessionToken;
    }

    /**
     * @param string $sessionToken
     * @return \hubsoft\api\commerce\updatePassword
     */
    public function setSessionToken($sessionToken)
    {
      $this->sessionToken = $sessionToken;
      return $this;
    }

    /**
     * @return string
     */
    public function getOldPassword()
    {
      return $this->oldPassword;
    }

    /**
     * @param string $oldPassword
     * @return \hubsoft\api\commerce\updatePassword
     */
    public function setOldPassword($oldPassword)
    {
      $this->oldPassword = $oldPassword;
      return $this;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
      return $this->newPassword;
    }

    /**
     * @param string $newPassword
     * @return \hubsoft\api\commerce\updatePassword
     */
    public function setNewPassword($newPassword)
    {
      $this->newPassword = $newPassword;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\updatePassword
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
