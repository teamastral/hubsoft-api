<?php

namespace hubsoft\api\commerce;

class validateAuthCode
{

    /**
     * @var string $authorizationCode
     */
    protected $authorizationCode = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $authorizationCode
     * @param string $accessKey
     */
    public function __construct($authorizationCode, $accessKey)
    {
      $this->authorizationCode = $authorizationCode;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getAuthorizationCode()
    {
      return $this->authorizationCode;
    }

    /**
     * @param string $authorizationCode
     * @return \hubsoft\api\commerce\validateAuthCode
     */
    public function setAuthorizationCode($authorizationCode)
    {
      $this->authorizationCode = $authorizationCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\commerce\validateAuthCode
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
