<?php

namespace hubsoft\api\commerce;

class validateAuthCodeResponse
{

    /**
     * @var ValidateAuthCodeResponse $out
     */
    protected $out = null;

    /**
     * @param ValidateAuthCodeResponse $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ValidateAuthCodeResponse
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ValidateAuthCodeResponse $out
     * @return \hubsoft\api\commerce\validateAuthCodeResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
