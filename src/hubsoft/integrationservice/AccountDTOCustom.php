<?php

namespace mattanger\hubsoft\integrationservice;

class AccountDTOCustom
{

    /**
     * @var AddressDTO $address
     */
    protected $address = null;

    /**
     * @var ArrayOfAddtlAddressDTO $addtlBillToList
     */
    protected $addtlBillToList = null;

    /**
     * @var string $billingTermCode
     */
    protected $billingTermCode = null;

    /**
     * @var ArrayOfBuyerDTO $buyerList
     */
    protected $buyerList = null;

    /**
     * @var string $code
     */
    protected $code = null;

    /**
     * @var ContactDTO $contact
     */
    protected $contact = null;

    /**
     * @var string $copyNotesToOrder
     */
    protected $copyNotesToOrder = null;

    /**
     * @var string $createdDate
     */
    protected $createdDate = null;

    /**
     * @var float $creditLimitAmount
     */
    protected $creditLimitAmount = null;

    /**
     * @var string $dealerSince
     */
    protected $dealerSince = null;

    /**
     * @var string $defaultIncentive
     */
    protected $defaultIncentive = null;

    /**
     * @var string $defaultPriceBook
     */
    protected $defaultPriceBook = null;

    /**
     * @var string $federalTaxId
     */
    protected $federalTaxId = null;

    /**
     * @var string $legalName
     */
    protected $legalName = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var string $notes
     */
    protected $notes = null;

    /**
     * @var string $priorityCode
     */
    protected $priorityCode = null;

    /**
     * @var boolean $shipPurchasesToStore
     */
    protected $shipPurchasesToStore = null;

    /**
     * @var string $statusCode
     */
    protected $statusCode = null;

    /**
     * @var ArrayOfStoreDTO $storeList
     */
    protected $storeList = null;

    /**
     * @var string $taxExemptionId
     */
    protected $taxExemptionId = null;

    /**
     * @var string $typeCode
     */
    protected $typeCode = null;

    /**
     * @param string $createdDate
     * @param boolean $shipPurchasesToStore
     */
    public function __construct($createdDate, $shipPurchasesToStore)
    {
      $this->createdDate = $createdDate;
      $this->shipPurchasesToStore = $shipPurchasesToStore;
    }

    /**
     * @return AddressDTO
     */
    public function getAddress()
    {
      return $this->address;
    }

    /**
     * @param AddressDTO $address
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setAddress($address)
    {
      $this->address = $address;
      return $this;
    }

    /**
     * @return ArrayOfAddtlAddressDTO
     */
    public function getAddtlBillToList()
    {
      return $this->addtlBillToList;
    }

    /**
     * @param ArrayOfAddtlAddressDTO $addtlBillToList
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setAddtlBillToList($addtlBillToList)
    {
      $this->addtlBillToList = $addtlBillToList;
      return $this;
    }

    /**
     * @return string
     */
    public function getBillingTermCode()
    {
      return $this->billingTermCode;
    }

    /**
     * @param string $billingTermCode
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setBillingTermCode($billingTermCode)
    {
      $this->billingTermCode = $billingTermCode;
      return $this;
    }

    /**
     * @return ArrayOfBuyerDTO
     */
    public function getBuyerList()
    {
      return $this->buyerList;
    }

    /**
     * @param ArrayOfBuyerDTO $buyerList
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setBuyerList($buyerList)
    {
      $this->buyerList = $buyerList;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->code;
    }

    /**
     * @param string $code
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setCode($code)
    {
      $this->code = $code;
      return $this;
    }

    /**
     * @return ContactDTO
     */
    public function getContact()
    {
      return $this->contact;
    }

    /**
     * @param ContactDTO $contact
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setContact($contact)
    {
      $this->contact = $contact;
      return $this;
    }

    /**
     * @return string
     */
    public function getCopyNotesToOrder()
    {
      return $this->copyNotesToOrder;
    }

    /**
     * @param string $copyNotesToOrder
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setCopyNotesToOrder($copyNotesToOrder)
    {
      $this->copyNotesToOrder = $copyNotesToOrder;
      return $this;
    }

    /**
     * @return string
     */
    public function getCreatedDate()
    {
      return $this->createdDate;
    }

    /**
     * @param string $createdDate
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setCreatedDate($createdDate)
    {
      $this->createdDate = $createdDate;
      return $this;
    }

    /**
     * @return float
     */
    public function getCreditLimitAmount()
    {
      return $this->creditLimitAmount;
    }

    /**
     * @param float $creditLimitAmount
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setCreditLimitAmount($creditLimitAmount)
    {
      $this->creditLimitAmount = $creditLimitAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getDealerSince()
    {
      return $this->dealerSince;
    }

    /**
     * @param string $dealerSince
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setDealerSince($dealerSince)
    {
      $this->dealerSince = $dealerSince;
      return $this;
    }

    /**
     * @return string
     */
    public function getDefaultIncentive()
    {
      return $this->defaultIncentive;
    }

    /**
     * @param string $defaultIncentive
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setDefaultIncentive($defaultIncentive)
    {
      $this->defaultIncentive = $defaultIncentive;
      return $this;
    }

    /**
     * @return string
     */
    public function getDefaultPriceBook()
    {
      return $this->defaultPriceBook;
    }

    /**
     * @param string $defaultPriceBook
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setDefaultPriceBook($defaultPriceBook)
    {
      $this->defaultPriceBook = $defaultPriceBook;
      return $this;
    }

    /**
     * @return string
     */
    public function getFederalTaxId()
    {
      return $this->federalTaxId;
    }

    /**
     * @param string $federalTaxId
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setFederalTaxId($federalTaxId)
    {
      $this->federalTaxId = $federalTaxId;
      return $this;
    }

    /**
     * @return string
     */
    public function getLegalName()
    {
      return $this->legalName;
    }

    /**
     * @param string $legalName
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setLegalName($legalName)
    {
      $this->legalName = $legalName;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
      return $this->notes;
    }

    /**
     * @param string $notes
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setNotes($notes)
    {
      $this->notes = $notes;
      return $this;
    }

    /**
     * @return string
     */
    public function getPriorityCode()
    {
      return $this->priorityCode;
    }

    /**
     * @param string $priorityCode
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setPriorityCode($priorityCode)
    {
      $this->priorityCode = $priorityCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getShipPurchasesToStore()
    {
      return $this->shipPurchasesToStore;
    }

    /**
     * @param boolean $shipPurchasesToStore
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setShipPurchasesToStore($shipPurchasesToStore)
    {
      $this->shipPurchasesToStore = $shipPurchasesToStore;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
      return $this->statusCode;
    }

    /**
     * @param string $statusCode
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setStatusCode($statusCode)
    {
      $this->statusCode = $statusCode;
      return $this;
    }

    /**
     * @return ArrayOfStoreDTO
     */
    public function getStoreList()
    {
      return $this->storeList;
    }

    /**
     * @param ArrayOfStoreDTO $storeList
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setStoreList($storeList)
    {
      $this->storeList = $storeList;
      return $this;
    }

    /**
     * @return string
     */
    public function getTaxExemptionId()
    {
      return $this->taxExemptionId;
    }

    /**
     * @param string $taxExemptionId
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setTaxExemptionId($taxExemptionId)
    {
      $this->taxExemptionId = $taxExemptionId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTypeCode()
    {
      return $this->typeCode;
    }

    /**
     * @param string $typeCode
     * @return \mattanger\hubsoft\integrationservice\AccountDTO
     */
    public function setTypeCode($typeCode)
    {
      $this->typeCode = $typeCode;
      return $this;
    }

}
