<?php

namespace mattanger\hubsoft\integrationservice;

class AccountLoginDTOCustom
{

    /**
     * @var string $loginName
     */
    protected $loginName = null;

    /**
     * @var string $loginPassword
     */
    protected $loginPassword = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getLoginName()
    {
      return $this->loginName;
    }

    /**
     * @param string $loginName
     * @return \mattanger\hubsoft\integrationservice\AccountLoginDTO
     */
    public function setLoginName($loginName)
    {
      $this->loginName = $loginName;
      return $this;
    }

    /**
     * @return string
     */
    public function getLoginPassword()
    {
      return $this->loginPassword;
    }

    /**
     * @param string $loginPassword
     * @return \mattanger\hubsoft\integrationservice\AccountLoginDTO
     */
    public function setLoginPassword($loginPassword)
    {
      $this->loginPassword = $loginPassword;
      return $this;
    }

}
