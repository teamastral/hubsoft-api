<?php

namespace mattanger\hubsoft\integrationservice;

class AccountProspectDTOCustom2
{

    /**
     * @var string $accountCode
     */
    protected $accountCode = null;

    /**
     * @var AccountLoginDTO $accountLogin
     */
    protected $accountLogin = null;

    /**
     * @var string $accountName
     */
    protected $accountName = null;

    /**
     * @var AddressDTO $address
     */
    protected $address = null;

    /**
     * @var string $assignAllStores
     */
    protected $assignAllStores = null;

    /**
     * @var ContactDTO $contact
     */
    protected $contact = null;

    /**
     * @var string $repCode
     */
    protected $repCode = null;

    /**
     * @var string $statusCode
     */
    protected $statusCode = null;

    /**
     * @var string $storeCode
     */
    protected $storeCode = null;

    /**
     * @var string $storeName
     */
    protected $storeName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAccountCode()
    {
      return $this->accountCode;
    }

    /**
     * @param string $accountCode
     * @return \mattanger\hubsoft\integrationservice\AccountProspectDTO
     */
    public function setAccountCode($accountCode)
    {
      $this->accountCode = $accountCode;
      return $this;
    }

    /**
     * @return AccountLoginDTO
     */
    public function getAccountLogin()
    {
      return $this->accountLogin;
    }

    /**
     * @param AccountLoginDTO $accountLogin
     * @return \mattanger\hubsoft\integrationservice\AccountProspectDTO
     */
    public function setAccountLogin($accountLogin)
    {
      $this->accountLogin = $accountLogin;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccountName()
    {
      return $this->accountName;
    }

    /**
     * @param string $accountName
     * @return \mattanger\hubsoft\integrationservice\AccountProspectDTO
     */
    public function setAccountName($accountName)
    {
      $this->accountName = $accountName;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getAddress()
    {
      return $this->address;
    }

    /**
     * @param AddressDTO $address
     * @return \mattanger\hubsoft\integrationservice\AccountProspectDTO
     */
    public function setAddress($address)
    {
      $this->address = $address;
      return $this;
    }

    /**
     * @return string
     */
    public function getAssignAllStores()
    {
      return $this->assignAllStores;
    }

    /**
     * @param string $assignAllStores
     * @return \mattanger\hubsoft\integrationservice\AccountProspectDTO
     */
    public function setAssignAllStores($assignAllStores)
    {
      $this->assignAllStores = $assignAllStores;
      return $this;
    }

    /**
     * @return ContactDTO
     */
    public function getContact()
    {
      return $this->contact;
    }

    /**
     * @param ContactDTO $contact
     * @return \mattanger\hubsoft\integrationservice\AccountProspectDTO
     */
    public function setContact($contact)
    {
      $this->contact = $contact;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCode()
    {
      return $this->repCode;
    }

    /**
     * @param string $repCode
     * @return \mattanger\hubsoft\integrationservice\AccountProspectDTO
     */
    public function setRepCode($repCode)
    {
      $this->repCode = $repCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
      return $this->statusCode;
    }

    /**
     * @param string $statusCode
     * @return \mattanger\hubsoft\integrationservice\AccountProspectDTO
     */
    public function setStatusCode($statusCode)
    {
      $this->statusCode = $statusCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
      return $this->storeCode;
    }

    /**
     * @param string $storeCode
     * @return \mattanger\hubsoft\integrationservice\AccountProspectDTO
     */
    public function setStoreCode($storeCode)
    {
      $this->storeCode = $storeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreName()
    {
      return $this->storeName;
    }

    /**
     * @param string $storeName
     * @return \mattanger\hubsoft\integrationservice\AccountProspectDTO
     */
    public function setStoreName($storeName)
    {
      $this->storeName = $storeName;
      return $this;
    }

}
