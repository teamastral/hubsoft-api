<?php

namespace mattanger\hubsoft\integrationservice;

class AccountStoreDTOCustom2
{

    /**
     * @var string $accountCode
     */
    protected $accountCode = null;

    /**
     * @var string $assignStoreToActiveBuyers
     */
    protected $assignStoreToActiveBuyers = null;

    /**
     * @var AddressDTO $billingAddress
     */
    protected $billingAddress = null;

    /**
     * @var ContactDTO $contact
     */
    protected $contact = null;

    /**
     * @var string $defaultStore
     */
    protected $defaultStore = null;

    /**
     * @var string $exposeToLocator
     */
    protected $exposeToLocator = null;

    /**
     * @var string $freightTermCode
     */
    protected $freightTermCode = null;

    /**
     * @var AddressDTO $locatorAddress
     */
    protected $locatorAddress = null;

    /**
     * @var string $locatorNotes
     */
    protected $locatorNotes = null;

    /**
     * @var string $locatorPhone
     */
    protected $locatorPhone = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var string $repCode
     */
    protected $repCode = null;

    /**
     * @var string $shipperAccountNumber
     */
    protected $shipperAccountNumber = null;

    /**
     * @var AddressDTO $shippingAddress
     */
    protected $shippingAddress = null;

    /**
     * @var string $shippingMethodCode
     */
    protected $shippingMethodCode = null;

    /**
     * @var string $storeCode
     */
    protected $storeCode = null;

    /**
     * @var string $storeTypeCode
     */
    protected $storeTypeCode = null;

    /**
     * @var string $territoryCode
     */
    protected $territoryCode = null;

    /**
     * @var string $url
     */
    protected $url = null;

    /**
     * @var string $warehouseCode
     */
    protected $warehouseCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAccountCode()
    {
      return $this->accountCode;
    }

    /**
     * @param string $accountCode
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setAccountCode($accountCode)
    {
      $this->accountCode = $accountCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getAssignStoreToActiveBuyers()
    {
      return $this->assignStoreToActiveBuyers;
    }

    /**
     * @param string $assignStoreToActiveBuyers
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setAssignStoreToActiveBuyers($assignStoreToActiveBuyers)
    {
      $this->assignStoreToActiveBuyers = $assignStoreToActiveBuyers;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getBillingAddress()
    {
      return $this->billingAddress;
    }

    /**
     * @param AddressDTO $billingAddress
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setBillingAddress($billingAddress)
    {
      $this->billingAddress = $billingAddress;
      return $this;
    }

    /**
     * @return ContactDTO
     */
    public function getContact()
    {
      return $this->contact;
    }

    /**
     * @param ContactDTO $contact
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setContact($contact)
    {
      $this->contact = $contact;
      return $this;
    }

    /**
     * @return string
     */
    public function getDefaultStore()
    {
      return $this->defaultStore;
    }

    /**
     * @param string $defaultStore
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setDefaultStore($defaultStore)
    {
      $this->defaultStore = $defaultStore;
      return $this;
    }

    /**
     * @return string
     */
    public function getExposeToLocator()
    {
      return $this->exposeToLocator;
    }

    /**
     * @param string $exposeToLocator
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setExposeToLocator($exposeToLocator)
    {
      $this->exposeToLocator = $exposeToLocator;
      return $this;
    }

    /**
     * @return string
     */
    public function getFreightTermCode()
    {
      return $this->freightTermCode;
    }

    /**
     * @param string $freightTermCode
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setFreightTermCode($freightTermCode)
    {
      $this->freightTermCode = $freightTermCode;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getLocatorAddress()
    {
      return $this->locatorAddress;
    }

    /**
     * @param AddressDTO $locatorAddress
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setLocatorAddress($locatorAddress)
    {
      $this->locatorAddress = $locatorAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getLocatorNotes()
    {
      return $this->locatorNotes;
    }

    /**
     * @param string $locatorNotes
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setLocatorNotes($locatorNotes)
    {
      $this->locatorNotes = $locatorNotes;
      return $this;
    }

    /**
     * @return string
     */
    public function getLocatorPhone()
    {
      return $this->locatorPhone;
    }

    /**
     * @param string $locatorPhone
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setLocatorPhone($locatorPhone)
    {
      $this->locatorPhone = $locatorPhone;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCode()
    {
      return $this->repCode;
    }

    /**
     * @param string $repCode
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setRepCode($repCode)
    {
      $this->repCode = $repCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipperAccountNumber()
    {
      return $this->shipperAccountNumber;
    }

    /**
     * @param string $shipperAccountNumber
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setShipperAccountNumber($shipperAccountNumber)
    {
      $this->shipperAccountNumber = $shipperAccountNumber;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getShippingAddress()
    {
      return $this->shippingAddress;
    }

    /**
     * @param AddressDTO $shippingAddress
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setShippingAddress($shippingAddress)
    {
      $this->shippingAddress = $shippingAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingMethodCode()
    {
      return $this->shippingMethodCode;
    }

    /**
     * @param string $shippingMethodCode
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setShippingMethodCode($shippingMethodCode)
    {
      $this->shippingMethodCode = $shippingMethodCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
      return $this->storeCode;
    }

    /**
     * @param string $storeCode
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setStoreCode($storeCode)
    {
      $this->storeCode = $storeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreTypeCode()
    {
      return $this->storeTypeCode;
    }

    /**
     * @param string $storeTypeCode
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setStoreTypeCode($storeTypeCode)
    {
      $this->storeTypeCode = $storeTypeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getTerritoryCode()
    {
      return $this->territoryCode;
    }

    /**
     * @param string $territoryCode
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setTerritoryCode($territoryCode)
    {
      $this->territoryCode = $territoryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
      return $this->url;
    }

    /**
     * @param string $url
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setUrl($url)
    {
      $this->url = $url;
      return $this;
    }

    /**
     * @return string
     */
    public function getWarehouseCode()
    {
      return $this->warehouseCode;
    }

    /**
     * @param string $warehouseCode
     * @return \mattanger\hubsoft\integrationservice\AccountStoreDTO
     */
    public function setWarehouseCode($warehouseCode)
    {
      $this->warehouseCode = $warehouseCode;
      return $this;
    }

}
