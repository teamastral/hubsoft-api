<?php

namespace mattanger\hubsoft\integrationservice;

class AddressDTOCustom
{

    /**
     * @var string $city
     */
    protected $city = null;

    /**
     * @var string $countryCode
     */
    protected $countryCode = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var string $postalCode
     */
    protected $postalCode = null;

    /**
     * @var string $stateCode
     */
    protected $stateCode = null;

    /**
     * @var string $street1
     */
    protected $street1 = null;

    /**
     * @var string $street2
     */
    protected $street2 = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCity()
    {
      return $this->city;
    }

    /**
     * @param string $city
     * @return \mattanger\hubsoft\integrationservice\AddressDTO
     */
    public function setCity($city)
    {
      $this->city = $city;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
      return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return \mattanger\hubsoft\integrationservice\AddressDTO
     */
    public function setCountryCode($countryCode)
    {
      $this->countryCode = $countryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return \mattanger\hubsoft\integrationservice\AddressDTO
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
      return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return \mattanger\hubsoft\integrationservice\AddressDTO
     */
    public function setPostalCode($postalCode)
    {
      $this->postalCode = $postalCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStateCode()
    {
      return $this->stateCode;
    }

    /**
     * @param string $stateCode
     * @return \mattanger\hubsoft\integrationservice\AddressDTO
     */
    public function setStateCode($stateCode)
    {
      $this->stateCode = $stateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStreet1()
    {
      return $this->street1;
    }

    /**
     * @param string $street1
     * @return \mattanger\hubsoft\integrationservice\AddressDTO
     */
    public function setStreet1($street1)
    {
      $this->street1 = $street1;
      return $this;
    }

    /**
     * @return string
     */
    public function getStreet2()
    {
      return $this->street2;
    }

    /**
     * @param string $street2
     * @return \mattanger\hubsoft\integrationservice\AddressDTO
     */
    public function setStreet2($street2)
    {
      $this->street2 = $street2;
      return $this;
    }

}
