<?php

namespace hubsoft\api\integrationservice;

class ArrayOfAccountDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AccountDTO[] $AccountDTO
     */
    protected $AccountDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AccountDTO[]
     */
    public function getAccountDTO()
    {
      return $this->AccountDTO;
    }

    /**
     * @param AccountDTO[] $AccountDTO
     * @return \hubsoft\api\integrationservice\ArrayOfAccountDTO
     */
    public function setAccountDTO(array $AccountDTO = null)
    {
      $this->AccountDTO = $AccountDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AccountDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AccountDTO
     */
    public function offsetGet($offset)
    {
      return $this->AccountDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AccountDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AccountDTO[] = $value;
      } else {
        $this->AccountDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AccountDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AccountDTO Return the current element
     */
    public function current()
    {
      return current($this->AccountDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AccountDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AccountDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AccountDTO);
    }

    /**
     * Countable implementation
     *
     * @return AccountDTO Return count of elements
     */
    public function count()
    {
      return count($this->AccountDTO);
    }

}
