<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfAccountProspectDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AccountProspectDTO[] $AccountProspectDTO
     */
    protected $AccountProspectDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AccountProspectDTO[]
     */
    public function getAccountProspectDTO()
    {
      return $this->AccountProspectDTO;
    }

    /**
     * @param AccountProspectDTO[] $AccountProspectDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfAccountProspectDTO
     */
    public function setAccountProspectDTO(array $AccountProspectDTO = null)
    {
      $this->AccountProspectDTO = $AccountProspectDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AccountProspectDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AccountProspectDTO
     */
    public function offsetGet($offset)
    {
      return $this->AccountProspectDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AccountProspectDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AccountProspectDTO[] = $value;
      } else {
        $this->AccountProspectDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AccountProspectDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AccountProspectDTO Return the current element
     */
    public function current()
    {
      return current($this->AccountProspectDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AccountProspectDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AccountProspectDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AccountProspectDTO);
    }

    /**
     * Countable implementation
     *
     * @return AccountProspectDTO Return count of elements
     */
    public function count()
    {
      return count($this->AccountProspectDTO);
    }

}
