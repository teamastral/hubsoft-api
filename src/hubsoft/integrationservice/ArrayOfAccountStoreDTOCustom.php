<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfAccountStoreDTOCustom implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AccountStoreDTO[] $AccountStoreDTO
     */
    protected $AccountStoreDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AccountStoreDTO[]
     */
    public function getAccountStoreDTO()
    {
      return $this->AccountStoreDTO;
    }

    /**
     * @param AccountStoreDTO[] $AccountStoreDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfAccountStoreDTO
     */
    public function setAccountStoreDTO(array $AccountStoreDTO = null)
    {
      $this->AccountStoreDTO = $AccountStoreDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AccountStoreDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AccountStoreDTO
     */
    public function offsetGet($offset)
    {
      return $this->AccountStoreDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AccountStoreDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AccountStoreDTO[] = $value;
      } else {
        $this->AccountStoreDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AccountStoreDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AccountStoreDTO Return the current element
     */
    public function current()
    {
      return current($this->AccountStoreDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AccountStoreDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AccountStoreDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AccountStoreDTO);
    }

    /**
     * Countable implementation
     *
     * @return AccountStoreDTO Return count of elements
     */
    public function count()
    {
      return count($this->AccountStoreDTO);
    }

}
