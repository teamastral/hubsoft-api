<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfAddtlAddressDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AddtlAddressDTO[] $AddtlAddressDTO
     */
    protected $AddtlAddressDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AddtlAddressDTO[]
     */
    public function getAddtlAddressDTO()
    {
      return $this->AddtlAddressDTO;
    }

    /**
     * @param AddtlAddressDTO[] $AddtlAddressDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfAddtlAddressDTO
     */
    public function setAddtlAddressDTO(array $AddtlAddressDTO = null)
    {
      $this->AddtlAddressDTO = $AddtlAddressDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AddtlAddressDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AddtlAddressDTO
     */
    public function offsetGet($offset)
    {
      return $this->AddtlAddressDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AddtlAddressDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AddtlAddressDTO[] = $value;
      } else {
        $this->AddtlAddressDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AddtlAddressDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AddtlAddressDTO Return the current element
     */
    public function current()
    {
      return current($this->AddtlAddressDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AddtlAddressDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AddtlAddressDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AddtlAddressDTO);
    }

    /**
     * Countable implementation
     *
     * @return AddtlAddressDTO Return count of elements
     */
    public function count()
    {
      return count($this->AddtlAddressDTO);
    }

}
