<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfAttrDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AttrDTO[] $AttrDTO
     */
    protected $AttrDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AttrDTO[]
     */
    public function getAttrDTO()
    {
      return $this->AttrDTO;
    }

    /**
     * @param AttrDTO[] $AttrDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfAttrDTO
     */
    public function setAttrDTO(array $AttrDTO = null)
    {
      $this->AttrDTO = $AttrDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AttrDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AttrDTO
     */
    public function offsetGet($offset)
    {
      return $this->AttrDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AttrDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AttrDTO[] = $value;
      } else {
        $this->AttrDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AttrDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AttrDTO Return the current element
     */
    public function current()
    {
      return current($this->AttrDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AttrDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AttrDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AttrDTO);
    }

    /**
     * Countable implementation
     *
     * @return AttrDTO Return count of elements
     */
    public function count()
    {
      return count($this->AttrDTO);
    }

}
