<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfBulkItemDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BulkItemDTO[] $BulkItemDTO
     */
    protected $BulkItemDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return BulkItemDTO[]
     */
    public function getBulkItemDTO()
    {
      return $this->BulkItemDTO;
    }

    /**
     * @param BulkItemDTO[] $BulkItemDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfBulkItemDTO
     */
    public function setBulkItemDTO(array $BulkItemDTO = null)
    {
      $this->BulkItemDTO = $BulkItemDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BulkItemDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BulkItemDTO
     */
    public function offsetGet($offset)
    {
      return $this->BulkItemDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BulkItemDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->BulkItemDTO[] = $value;
      } else {
        $this->BulkItemDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BulkItemDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BulkItemDTO Return the current element
     */
    public function current()
    {
      return current($this->BulkItemDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BulkItemDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BulkItemDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BulkItemDTO);
    }

    /**
     * Countable implementation
     *
     * @return BulkItemDTO Return count of elements
     */
    public function count()
    {
      return count($this->BulkItemDTO);
    }

}
