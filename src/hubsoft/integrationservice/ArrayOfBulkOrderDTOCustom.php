<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfBulkOrderDTOCustom implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BulkOrderDTO[] $BulkOrderDTO
     */
    protected $BulkOrderDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return BulkOrderDTO[]
     */
    public function getBulkOrderDTO()
    {
      return $this->BulkOrderDTO;
    }

    /**
     * @param BulkOrderDTO[] $BulkOrderDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfBulkOrderDTO
     */
    public function setBulkOrderDTO(array $BulkOrderDTO = null)
    {
      $this->BulkOrderDTO = $BulkOrderDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BulkOrderDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BulkOrderDTO
     */
    public function offsetGet($offset)
    {
      return $this->BulkOrderDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BulkOrderDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->BulkOrderDTO[] = $value;
      } else {
        $this->BulkOrderDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BulkOrderDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BulkOrderDTO Return the current element
     */
    public function current()
    {
      return current($this->BulkOrderDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BulkOrderDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BulkOrderDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BulkOrderDTO);
    }

    /**
     * Countable implementation
     *
     * @return BulkOrderDTO Return count of elements
     */
    public function count()
    {
      return count($this->BulkOrderDTO);
    }

}
