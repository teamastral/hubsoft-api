<?php

namespace hubsoft\api\integrationservice;

class ArrayOfBuyerDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BuyerDTO[] $BuyerDTO
     */
    protected $BuyerDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return BuyerDTO[]
     */
    public function getBuyerDTO()
    {
      return $this->BuyerDTO;
    }

    /**
     * @param BuyerDTO[] $BuyerDTO
     * @return \hubsoft\api\integrationservice\ArrayOfBuyerDTO
     */
    public function setBuyerDTO(array $BuyerDTO = null)
    {
      $this->BuyerDTO = $BuyerDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BuyerDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BuyerDTO
     */
    public function offsetGet($offset)
    {
      return $this->BuyerDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BuyerDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->BuyerDTO[] = $value;
      } else {
        $this->BuyerDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BuyerDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BuyerDTO Return the current element
     */
    public function current()
    {
      return current($this->BuyerDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BuyerDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BuyerDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BuyerDTO);
    }

    /**
     * Countable implementation
     *
     * @return BuyerDTO Return count of elements
     */
    public function count()
    {
      return count($this->BuyerDTO);
    }

}
