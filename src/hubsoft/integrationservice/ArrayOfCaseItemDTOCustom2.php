<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfCaseItemDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CaseItemDTO[] $CaseItemDTO
     */
    protected $CaseItemDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CaseItemDTO[]
     */
    public function getCaseItemDTO()
    {
      return $this->CaseItemDTO;
    }

    /**
     * @param CaseItemDTO[] $CaseItemDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfCaseItemDTO
     */
    public function setCaseItemDTO(array $CaseItemDTO = null)
    {
      $this->CaseItemDTO = $CaseItemDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CaseItemDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CaseItemDTO
     */
    public function offsetGet($offset)
    {
      return $this->CaseItemDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CaseItemDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->CaseItemDTO[] = $value;
      } else {
        $this->CaseItemDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CaseItemDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CaseItemDTO Return the current element
     */
    public function current()
    {
      return current($this->CaseItemDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CaseItemDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CaseItemDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CaseItemDTO);
    }

    /**
     * Countable implementation
     *
     * @return CaseItemDTO Return count of elements
     */
    public function count()
    {
      return count($this->CaseItemDTO);
    }

}
