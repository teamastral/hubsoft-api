<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfDealerOrderDeletedDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DealerOrderDeletedDTO[] $DealerOrderDeletedDTO
     */
    protected $DealerOrderDeletedDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DealerOrderDeletedDTO[]
     */
    public function getDealerOrderDeletedDTO()
    {
      return $this->DealerOrderDeletedDTO;
    }

    /**
     * @param DealerOrderDeletedDTO[] $DealerOrderDeletedDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfDealerOrderDeletedDTO
     */
    public function setDealerOrderDeletedDTO(array $DealerOrderDeletedDTO = null)
    {
      $this->DealerOrderDeletedDTO = $DealerOrderDeletedDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DealerOrderDeletedDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DealerOrderDeletedDTO
     */
    public function offsetGet($offset)
    {
      return $this->DealerOrderDeletedDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DealerOrderDeletedDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DealerOrderDeletedDTO[] = $value;
      } else {
        $this->DealerOrderDeletedDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DealerOrderDeletedDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DealerOrderDeletedDTO Return the current element
     */
    public function current()
    {
      return current($this->DealerOrderDeletedDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DealerOrderDeletedDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DealerOrderDeletedDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DealerOrderDeletedDTO);
    }

    /**
     * Countable implementation
     *
     * @return DealerOrderDeletedDTO Return count of elements
     */
    public function count()
    {
      return count($this->DealerOrderDeletedDTO);
    }

}
