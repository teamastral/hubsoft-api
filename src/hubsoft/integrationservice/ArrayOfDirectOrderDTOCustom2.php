<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfDirectOrderDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DirectOrderDTO[] $DirectOrderDTO
     */
    protected $DirectOrderDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DirectOrderDTO[]
     */
    public function getDirectOrderDTO()
    {
      return $this->DirectOrderDTO;
    }

    /**
     * @param DirectOrderDTO[] $DirectOrderDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfDirectOrderDTO
     */
    public function setDirectOrderDTO(array $DirectOrderDTO = null)
    {
      $this->DirectOrderDTO = $DirectOrderDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DirectOrderDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DirectOrderDTO
     */
    public function offsetGet($offset)
    {
      return $this->DirectOrderDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DirectOrderDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DirectOrderDTO[] = $value;
      } else {
        $this->DirectOrderDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DirectOrderDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DirectOrderDTO Return the current element
     */
    public function current()
    {
      return current($this->DirectOrderDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DirectOrderDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DirectOrderDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DirectOrderDTO);
    }

    /**
     * Countable implementation
     *
     * @return DirectOrderDTO Return count of elements
     */
    public function count()
    {
      return count($this->DirectOrderDTO);
    }

}
