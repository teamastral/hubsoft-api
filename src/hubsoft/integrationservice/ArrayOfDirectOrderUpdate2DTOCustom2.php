<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfDirectOrderUpdate2DTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DirectOrderUpdate2DTO[] $DirectOrderUpdate2DTO
     */
    protected $DirectOrderUpdate2DTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DirectOrderUpdate2DTO[]
     */
    public function getDirectOrderUpdate2DTO()
    {
      return $this->DirectOrderUpdate2DTO;
    }

    /**
     * @param DirectOrderUpdate2DTO[] $DirectOrderUpdate2DTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfDirectOrderUpdate2DTO
     */
    public function setDirectOrderUpdate2DTO(array $DirectOrderUpdate2DTO = null)
    {
      $this->DirectOrderUpdate2DTO = $DirectOrderUpdate2DTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DirectOrderUpdate2DTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DirectOrderUpdate2DTO
     */
    public function offsetGet($offset)
    {
      return $this->DirectOrderUpdate2DTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DirectOrderUpdate2DTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DirectOrderUpdate2DTO[] = $value;
      } else {
        $this->DirectOrderUpdate2DTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DirectOrderUpdate2DTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DirectOrderUpdate2DTO Return the current element
     */
    public function current()
    {
      return current($this->DirectOrderUpdate2DTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DirectOrderUpdate2DTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DirectOrderUpdate2DTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DirectOrderUpdate2DTO);
    }

    /**
     * Countable implementation
     *
     * @return DirectOrderUpdate2DTO Return count of elements
     */
    public function count()
    {
      return count($this->DirectOrderUpdate2DTO);
    }

}
