<?php

namespace hubsoft\api\integrationservice;

class ArrayOfDirectOrderUpdateDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DirectOrderUpdateDTO[] $DirectOrderUpdateDTO
     */
    protected $DirectOrderUpdateDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DirectOrderUpdateDTO[]
     */
    public function getDirectOrderUpdateDTO()
    {
      return $this->DirectOrderUpdateDTO;
    }

    /**
     * @param DirectOrderUpdateDTO[] $DirectOrderUpdateDTO
     * @return \hubsoft\api\integrationservice\ArrayOfDirectOrderUpdateDTO
     */
    public function setDirectOrderUpdateDTO(array $DirectOrderUpdateDTO = null)
    {
      $this->DirectOrderUpdateDTO = $DirectOrderUpdateDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DirectOrderUpdateDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DirectOrderUpdateDTO
     */
    public function offsetGet($offset)
    {
      return $this->DirectOrderUpdateDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DirectOrderUpdateDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DirectOrderUpdateDTO[] = $value;
      } else {
        $this->DirectOrderUpdateDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DirectOrderUpdateDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DirectOrderUpdateDTO Return the current element
     */
    public function current()
    {
      return current($this->DirectOrderUpdateDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DirectOrderUpdateDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DirectOrderUpdateDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DirectOrderUpdateDTO);
    }

    /**
     * Countable implementation
     *
     * @return DirectOrderUpdateDTO Return count of elements
     */
    public function count()
    {
      return count($this->DirectOrderUpdateDTO);
    }

}
