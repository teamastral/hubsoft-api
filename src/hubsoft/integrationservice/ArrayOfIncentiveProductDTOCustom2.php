<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfIncentiveProductDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var IncentiveProductDTO[] $IncentiveProductDTO
     */
    protected $IncentiveProductDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return IncentiveProductDTO[]
     */
    public function getIncentiveProductDTO()
    {
      return $this->IncentiveProductDTO;
    }

    /**
     * @param IncentiveProductDTO[] $IncentiveProductDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfIncentiveProductDTO
     */
    public function setIncentiveProductDTO(array $IncentiveProductDTO = null)
    {
      $this->IncentiveProductDTO = $IncentiveProductDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->IncentiveProductDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return IncentiveProductDTO
     */
    public function offsetGet($offset)
    {
      return $this->IncentiveProductDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param IncentiveProductDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->IncentiveProductDTO[] = $value;
      } else {
        $this->IncentiveProductDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->IncentiveProductDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return IncentiveProductDTO Return the current element
     */
    public function current()
    {
      return current($this->IncentiveProductDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->IncentiveProductDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->IncentiveProductDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->IncentiveProductDTO);
    }

    /**
     * Countable implementation
     *
     * @return IncentiveProductDTO Return count of elements
     */
    public function count()
    {
      return count($this->IncentiveProductDTO);
    }

}
