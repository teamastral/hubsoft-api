<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfInventoryDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var InventoryDTO[] $InventoryDTO
     */
    protected $InventoryDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return InventoryDTO[]
     */
    public function getInventoryDTO()
    {
      return $this->InventoryDTO;
    }

    /**
     * @param InventoryDTO[] $InventoryDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfInventoryDTO
     */
    public function setInventoryDTO(array $InventoryDTO = null)
    {
      $this->InventoryDTO = $InventoryDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->InventoryDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return InventoryDTO
     */
    public function offsetGet($offset)
    {
      return $this->InventoryDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param InventoryDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->InventoryDTO[] = $value;
      } else {
        $this->InventoryDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->InventoryDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return InventoryDTO Return the current element
     */
    public function current()
    {
      return current($this->InventoryDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->InventoryDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->InventoryDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->InventoryDTO);
    }

    /**
     * Countable implementation
     *
     * @return InventoryDTO Return count of elements
     */
    public function count()
    {
      return count($this->InventoryDTO);
    }

}
