<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfInvoiceCustom implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Invoice[] $Invoice
     */
    protected $Invoice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Invoice[]
     */
    public function getInvoice()
    {
      return $this->Invoice;
    }

    /**
     * @param Invoice[] $Invoice
     * @return \mattanger\hubsoft\integrationservice\ArrayOfInvoice
     */
    public function setInvoice(array $Invoice = null)
    {
      $this->Invoice = $Invoice;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Invoice[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Invoice
     */
    public function offsetGet($offset)
    {
      return $this->Invoice[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Invoice $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->Invoice[] = $value;
      } else {
        $this->Invoice[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Invoice[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Invoice Return the current element
     */
    public function current()
    {
      return current($this->Invoice);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Invoice);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Invoice);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Invoice);
    }

    /**
     * Countable implementation
     *
     * @return Invoice Return count of elements
     */
    public function count()
    {
      return count($this->Invoice);
    }

}
