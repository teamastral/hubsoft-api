<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfKeyObjectCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var KeyObject[] $KeyObject
     */
    protected $KeyObject = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return KeyObject[]
     */
    public function getKeyObject()
    {
      return $this->KeyObject;
    }

    /**
     * @param KeyObject[] $KeyObject
     * @return \mattanger\hubsoft\integrationservice\ArrayOfKeyObject
     */
    public function setKeyObject(array $KeyObject = null)
    {
      $this->KeyObject = $KeyObject;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->KeyObject[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return KeyObject
     */
    public function offsetGet($offset)
    {
      return $this->KeyObject[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param KeyObject $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->KeyObject[] = $value;
      } else {
        $this->KeyObject[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->KeyObject[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return KeyObject Return the current element
     */
    public function current()
    {
      return current($this->KeyObject);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->KeyObject);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->KeyObject);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->KeyObject);
    }

    /**
     * Countable implementation
     *
     * @return KeyObject Return count of elements
     */
    public function count()
    {
      return count($this->KeyObject);
    }

}
