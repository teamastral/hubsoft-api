<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfOrderItemDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var OrderItemDTO[] $OrderItemDTO
     */
    protected $OrderItemDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return OrderItemDTO[]
     */
    public function getOrderItemDTO()
    {
      return $this->OrderItemDTO;
    }

    /**
     * @param OrderItemDTO[] $OrderItemDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfOrderItemDTO
     */
    public function setOrderItemDTO(array $OrderItemDTO = null)
    {
      $this->OrderItemDTO = $OrderItemDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->OrderItemDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return OrderItemDTO
     */
    public function offsetGet($offset)
    {
      return $this->OrderItemDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param OrderItemDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->OrderItemDTO[] = $value;
      } else {
        $this->OrderItemDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->OrderItemDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return OrderItemDTO Return the current element
     */
    public function current()
    {
      return current($this->OrderItemDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->OrderItemDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->OrderItemDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->OrderItemDTO);
    }

    /**
     * Countable implementation
     *
     * @return OrderItemDTO Return count of elements
     */
    public function count()
    {
      return count($this->OrderItemDTO);
    }

}
