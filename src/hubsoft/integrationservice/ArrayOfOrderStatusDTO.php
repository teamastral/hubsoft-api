<?php

namespace hubsoft\api\integrationservice;

class ArrayOfOrderStatusDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var OrderStatusDTO[] $OrderStatusDTO
     */
    protected $OrderStatusDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return OrderStatusDTO[]
     */
    public function getOrderStatusDTO()
    {
      return $this->OrderStatusDTO;
    }

    /**
     * @param OrderStatusDTO[] $OrderStatusDTO
     * @return \hubsoft\api\integrationservice\ArrayOfOrderStatusDTO
     */
    public function setOrderStatusDTO(array $OrderStatusDTO = null)
    {
      $this->OrderStatusDTO = $OrderStatusDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->OrderStatusDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return OrderStatusDTO
     */
    public function offsetGet($offset)
    {
      return $this->OrderStatusDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param OrderStatusDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->OrderStatusDTO[] = $value;
      } else {
        $this->OrderStatusDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->OrderStatusDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return OrderStatusDTO Return the current element
     */
    public function current()
    {
      return current($this->OrderStatusDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->OrderStatusDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->OrderStatusDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->OrderStatusDTO);
    }

    /**
     * Countable implementation
     *
     * @return OrderStatusDTO Return count of elements
     */
    public function count()
    {
      return count($this->OrderStatusDTO);
    }

}
