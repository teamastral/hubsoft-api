<?php

namespace hubsoft\api\integrationservice;

class ArrayOfProductDescDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ProductDescDTO[] $ProductDescDTO
     */
    protected $ProductDescDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ProductDescDTO[]
     */
    public function getProductDescDTO()
    {
      return $this->ProductDescDTO;
    }

    /**
     * @param ProductDescDTO[] $ProductDescDTO
     * @return \hubsoft\api\integrationservice\ArrayOfProductDescDTO
     */
    public function setProductDescDTO(array $ProductDescDTO = null)
    {
      $this->ProductDescDTO = $ProductDescDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ProductDescDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ProductDescDTO
     */
    public function offsetGet($offset)
    {
      return $this->ProductDescDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ProductDescDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ProductDescDTO[] = $value;
      } else {
        $this->ProductDescDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ProductDescDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ProductDescDTO Return the current element
     */
    public function current()
    {
      return current($this->ProductDescDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ProductDescDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ProductDescDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ProductDescDTO);
    }

    /**
     * Countable implementation
     *
     * @return ProductDescDTO Return count of elements
     */
    public function count()
    {
      return count($this->ProductDescDTO);
    }

}
