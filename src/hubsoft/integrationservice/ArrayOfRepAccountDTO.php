<?php

namespace hubsoft\api\integrationservice;

class ArrayOfRepAccountDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var RepAccountDTO[] $RepAccountDTO
     */
    protected $RepAccountDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RepAccountDTO[]
     */
    public function getRepAccountDTO()
    {
      return $this->RepAccountDTO;
    }

    /**
     * @param RepAccountDTO[] $RepAccountDTO
     * @return \hubsoft\api\integrationservice\ArrayOfRepAccountDTO
     */
    public function setRepAccountDTO(array $RepAccountDTO = null)
    {
      $this->RepAccountDTO = $RepAccountDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->RepAccountDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return RepAccountDTO
     */
    public function offsetGet($offset)
    {
      return $this->RepAccountDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param RepAccountDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->RepAccountDTO[] = $value;
      } else {
        $this->RepAccountDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->RepAccountDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return RepAccountDTO Return the current element
     */
    public function current()
    {
      return current($this->RepAccountDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->RepAccountDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->RepAccountDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->RepAccountDTO);
    }

    /**
     * Countable implementation
     *
     * @return RepAccountDTO Return count of elements
     */
    public function count()
    {
      return count($this->RepAccountDTO);
    }

}
