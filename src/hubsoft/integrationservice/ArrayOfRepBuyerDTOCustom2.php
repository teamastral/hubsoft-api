<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfRepBuyerDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var RepBuyerDTO[] $RepBuyerDTO
     */
    protected $RepBuyerDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RepBuyerDTO[]
     */
    public function getRepBuyerDTO()
    {
      return $this->RepBuyerDTO;
    }

    /**
     * @param RepBuyerDTO[] $RepBuyerDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfRepBuyerDTO
     */
    public function setRepBuyerDTO(array $RepBuyerDTO = null)
    {
      $this->RepBuyerDTO = $RepBuyerDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->RepBuyerDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return RepBuyerDTO
     */
    public function offsetGet($offset)
    {
      return $this->RepBuyerDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param RepBuyerDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->RepBuyerDTO[] = $value;
      } else {
        $this->RepBuyerDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->RepBuyerDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return RepBuyerDTO Return the current element
     */
    public function current()
    {
      return current($this->RepBuyerDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->RepBuyerDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->RepBuyerDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->RepBuyerDTO);
    }

    /**
     * Countable implementation
     *
     * @return RepBuyerDTO Return count of elements
     */
    public function count()
    {
      return count($this->RepBuyerDTO);
    }

}
