<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfRepBuyerStoreDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var RepBuyerStoreDTO[] $RepBuyerStoreDTO
     */
    protected $RepBuyerStoreDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RepBuyerStoreDTO[]
     */
    public function getRepBuyerStoreDTO()
    {
      return $this->RepBuyerStoreDTO;
    }

    /**
     * @param RepBuyerStoreDTO[] $RepBuyerStoreDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfRepBuyerStoreDTO
     */
    public function setRepBuyerStoreDTO(array $RepBuyerStoreDTO = null)
    {
      $this->RepBuyerStoreDTO = $RepBuyerStoreDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->RepBuyerStoreDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return RepBuyerStoreDTO
     */
    public function offsetGet($offset)
    {
      return $this->RepBuyerStoreDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param RepBuyerStoreDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->RepBuyerStoreDTO[] = $value;
      } else {
        $this->RepBuyerStoreDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->RepBuyerStoreDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return RepBuyerStoreDTO Return the current element
     */
    public function current()
    {
      return current($this->RepBuyerStoreDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->RepBuyerStoreDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->RepBuyerStoreDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->RepBuyerStoreDTO);
    }

    /**
     * Countable implementation
     *
     * @return RepBuyerStoreDTO Return count of elements
     */
    public function count()
    {
      return count($this->RepBuyerStoreDTO);
    }

}
