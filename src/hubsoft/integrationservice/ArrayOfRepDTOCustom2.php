<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfRepDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var RepDTO[] $RepDTO
     */
    protected $RepDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RepDTO[]
     */
    public function getRepDTO()
    {
      return $this->RepDTO;
    }

    /**
     * @param RepDTO[] $RepDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfRepDTO
     */
    public function setRepDTO(array $RepDTO = null)
    {
      $this->RepDTO = $RepDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->RepDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return RepDTO
     */
    public function offsetGet($offset)
    {
      return $this->RepDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param RepDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->RepDTO[] = $value;
      } else {
        $this->RepDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->RepDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return RepDTO Return the current element
     */
    public function current()
    {
      return current($this->RepDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->RepDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->RepDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->RepDTO);
    }

    /**
     * Countable implementation
     *
     * @return RepDTO Return count of elements
     */
    public function count()
    {
      return count($this->RepDTO);
    }

}
