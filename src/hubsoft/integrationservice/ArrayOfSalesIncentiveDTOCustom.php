<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfSalesIncentiveDTOCustom implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SalesIncentiveDTO[] $SalesIncentiveDTO
     */
    protected $SalesIncentiveDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SalesIncentiveDTO[]
     */
    public function getSalesIncentiveDTO()
    {
      return $this->SalesIncentiveDTO;
    }

    /**
     * @param SalesIncentiveDTO[] $SalesIncentiveDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfSalesIncentiveDTO
     */
    public function setSalesIncentiveDTO(array $SalesIncentiveDTO = null)
    {
      $this->SalesIncentiveDTO = $SalesIncentiveDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SalesIncentiveDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SalesIncentiveDTO
     */
    public function offsetGet($offset)
    {
      return $this->SalesIncentiveDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SalesIncentiveDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->SalesIncentiveDTO[] = $value;
      } else {
        $this->SalesIncentiveDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SalesIncentiveDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SalesIncentiveDTO Return the current element
     */
    public function current()
    {
      return current($this->SalesIncentiveDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SalesIncentiveDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SalesIncentiveDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SalesIncentiveDTO);
    }

    /**
     * Countable implementation
     *
     * @return SalesIncentiveDTO Return count of elements
     */
    public function count()
    {
      return count($this->SalesIncentiveDTO);
    }

}
