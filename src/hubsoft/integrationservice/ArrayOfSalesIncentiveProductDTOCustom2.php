<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfSalesIncentiveProductDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SalesIncentiveProductDTO[] $SalesIncentiveProductDTO
     */
    protected $SalesIncentiveProductDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SalesIncentiveProductDTO[]
     */
    public function getSalesIncentiveProductDTO()
    {
      return $this->SalesIncentiveProductDTO;
    }

    /**
     * @param SalesIncentiveProductDTO[] $SalesIncentiveProductDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfSalesIncentiveProductDTO
     */
    public function setSalesIncentiveProductDTO(array $SalesIncentiveProductDTO = null)
    {
      $this->SalesIncentiveProductDTO = $SalesIncentiveProductDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SalesIncentiveProductDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SalesIncentiveProductDTO
     */
    public function offsetGet($offset)
    {
      return $this->SalesIncentiveProductDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SalesIncentiveProductDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->SalesIncentiveProductDTO[] = $value;
      } else {
        $this->SalesIncentiveProductDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SalesIncentiveProductDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SalesIncentiveProductDTO Return the current element
     */
    public function current()
    {
      return current($this->SalesIncentiveProductDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SalesIncentiveProductDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SalesIncentiveProductDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SalesIncentiveProductDTO);
    }

    /**
     * Countable implementation
     *
     * @return SalesIncentiveProductDTO Return count of elements
     */
    public function count()
    {
      return count($this->SalesIncentiveProductDTO);
    }

}
