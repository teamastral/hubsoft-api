<?php

namespace hubsoft\api\integrationservice;

class ArrayOfSalesTerritoryDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SalesTerritoryDTO[] $SalesTerritoryDTO
     */
    protected $SalesTerritoryDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SalesTerritoryDTO[]
     */
    public function getSalesTerritoryDTO()
    {
      return $this->SalesTerritoryDTO;
    }

    /**
     * @param SalesTerritoryDTO[] $SalesTerritoryDTO
     * @return \hubsoft\api\integrationservice\ArrayOfSalesTerritoryDTO
     */
    public function setSalesTerritoryDTO(array $SalesTerritoryDTO = null)
    {
      $this->SalesTerritoryDTO = $SalesTerritoryDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SalesTerritoryDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SalesTerritoryDTO
     */
    public function offsetGet($offset)
    {
      return $this->SalesTerritoryDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SalesTerritoryDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->SalesTerritoryDTO[] = $value;
      } else {
        $this->SalesTerritoryDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SalesTerritoryDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SalesTerritoryDTO Return the current element
     */
    public function current()
    {
      return current($this->SalesTerritoryDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SalesTerritoryDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SalesTerritoryDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SalesTerritoryDTO);
    }

    /**
     * Countable implementation
     *
     * @return SalesTerritoryDTO Return count of elements
     */
    public function count()
    {
      return count($this->SalesTerritoryDTO);
    }

}
