<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfShipmentInfoCustom implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ShipmentInfo[] $ShipmentInfo
     */
    protected $ShipmentInfo = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ShipmentInfo[]
     */
    public function getShipmentInfo()
    {
      return $this->ShipmentInfo;
    }

    /**
     * @param ShipmentInfo[] $ShipmentInfo
     * @return \mattanger\hubsoft\integrationservice\ArrayOfShipmentInfo
     */
    public function setShipmentInfo(array $ShipmentInfo = null)
    {
      $this->ShipmentInfo = $ShipmentInfo;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ShipmentInfo[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ShipmentInfo
     */
    public function offsetGet($offset)
    {
      return $this->ShipmentInfo[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ShipmentInfo $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ShipmentInfo[] = $value;
      } else {
        $this->ShipmentInfo[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ShipmentInfo[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ShipmentInfo Return the current element
     */
    public function current()
    {
      return current($this->ShipmentInfo);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ShipmentInfo);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ShipmentInfo);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ShipmentInfo);
    }

    /**
     * Countable implementation
     *
     * @return ShipmentInfo Return count of elements
     */
    public function count()
    {
      return count($this->ShipmentInfo);
    }

}
