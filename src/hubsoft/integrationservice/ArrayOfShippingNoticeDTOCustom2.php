<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfShippingNoticeDTOCustom2 implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ShippingNoticeDTO[] $ShippingNoticeDTO
     */
    protected $ShippingNoticeDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ShippingNoticeDTO[]
     */
    public function getShippingNoticeDTO()
    {
      return $this->ShippingNoticeDTO;
    }

    /**
     * @param ShippingNoticeDTO[] $ShippingNoticeDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfShippingNoticeDTO
     */
    public function setShippingNoticeDTO(array $ShippingNoticeDTO = null)
    {
      $this->ShippingNoticeDTO = $ShippingNoticeDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ShippingNoticeDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ShippingNoticeDTO
     */
    public function offsetGet($offset)
    {
      return $this->ShippingNoticeDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ShippingNoticeDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ShippingNoticeDTO[] = $value;
      } else {
        $this->ShippingNoticeDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ShippingNoticeDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ShippingNoticeDTO Return the current element
     */
    public function current()
    {
      return current($this->ShippingNoticeDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ShippingNoticeDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ShippingNoticeDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ShippingNoticeDTO);
    }

    /**
     * Countable implementation
     *
     * @return ShippingNoticeDTO Return count of elements
     */
    public function count()
    {
      return count($this->ShippingNoticeDTO);
    }

}
