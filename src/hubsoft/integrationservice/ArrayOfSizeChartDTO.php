<?php

namespace hubsoft\api\integrationservice;

class ArrayOfSizeChartDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SizeChartDTO[] $SizeChartDTO
     */
    protected $SizeChartDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SizeChartDTO[]
     */
    public function getSizeChartDTO()
    {
      return $this->SizeChartDTO;
    }

    /**
     * @param SizeChartDTO[] $SizeChartDTO
     * @return \hubsoft\api\integrationservice\ArrayOfSizeChartDTO
     */
    public function setSizeChartDTO(array $SizeChartDTO = null)
    {
      $this->SizeChartDTO = $SizeChartDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SizeChartDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SizeChartDTO
     */
    public function offsetGet($offset)
    {
      return $this->SizeChartDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SizeChartDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->SizeChartDTO[] = $value;
      } else {
        $this->SizeChartDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SizeChartDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SizeChartDTO Return the current element
     */
    public function current()
    {
      return current($this->SizeChartDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SizeChartDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SizeChartDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SizeChartDTO);
    }

    /**
     * Countable implementation
     *
     * @return SizeChartDTO Return count of elements
     */
    public function count()
    {
      return count($this->SizeChartDTO);
    }

}
