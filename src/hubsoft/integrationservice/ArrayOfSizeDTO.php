<?php

namespace hubsoft\api\integrationservice;

class ArrayOfSizeDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SizeDTO[] $SizeDTO
     */
    protected $SizeDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SizeDTO[]
     */
    public function getSizeDTO()
    {
      return $this->SizeDTO;
    }

    /**
     * @param SizeDTO[] $SizeDTO
     * @return \hubsoft\api\integrationservice\ArrayOfSizeDTO
     */
    public function setSizeDTO(array $SizeDTO = null)
    {
      $this->SizeDTO = $SizeDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SizeDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SizeDTO
     */
    public function offsetGet($offset)
    {
      return $this->SizeDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SizeDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->SizeDTO[] = $value;
      } else {
        $this->SizeDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SizeDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SizeDTO Return the current element
     */
    public function current()
    {
      return current($this->SizeDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SizeDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SizeDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SizeDTO);
    }

    /**
     * Countable implementation
     *
     * @return SizeDTO Return count of elements
     */
    public function count()
    {
      return count($this->SizeDTO);
    }

}
