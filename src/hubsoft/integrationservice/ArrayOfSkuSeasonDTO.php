<?php

namespace hubsoft\api\integrationservice;

class ArrayOfSkuSeasonDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SkuSeasonDTO[] $SkuSeasonDTO
     */
    protected $SkuSeasonDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SkuSeasonDTO[]
     */
    public function getSkuSeasonDTO()
    {
      return $this->SkuSeasonDTO;
    }

    /**
     * @param SkuSeasonDTO[] $SkuSeasonDTO
     * @return \hubsoft\api\integrationservice\ArrayOfSkuSeasonDTO
     */
    public function setSkuSeasonDTO(array $SkuSeasonDTO = null)
    {
      $this->SkuSeasonDTO = $SkuSeasonDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SkuSeasonDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SkuSeasonDTO
     */
    public function offsetGet($offset)
    {
      return $this->SkuSeasonDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SkuSeasonDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->SkuSeasonDTO[] = $value;
      } else {
        $this->SkuSeasonDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SkuSeasonDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SkuSeasonDTO Return the current element
     */
    public function current()
    {
      return current($this->SkuSeasonDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SkuSeasonDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SkuSeasonDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SkuSeasonDTO);
    }

    /**
     * Countable implementation
     *
     * @return SkuSeasonDTO Return count of elements
     */
    public function count()
    {
      return count($this->SkuSeasonDTO);
    }

}
