<?php

namespace mattanger\hubsoft\integrationservice;

class ArrayOfStoreDTOCustom implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var StoreDTO[] $StoreDTO
     */
    protected $StoreDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return StoreDTO[]
     */
    public function getStoreDTO()
    {
      return $this->StoreDTO;
    }

    /**
     * @param StoreDTO[] $StoreDTO
     * @return \mattanger\hubsoft\integrationservice\ArrayOfStoreDTO
     */
    public function setStoreDTO(array $StoreDTO = null)
    {
      $this->StoreDTO = $StoreDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->StoreDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return StoreDTO
     */
    public function offsetGet($offset)
    {
      return $this->StoreDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param StoreDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->StoreDTO[] = $value;
      } else {
        $this->StoreDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->StoreDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return StoreDTO Return the current element
     */
    public function current()
    {
      return current($this->StoreDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->StoreDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->StoreDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->StoreDTO);
    }

    /**
     * Countable implementation
     *
     * @return StoreDTO Return count of elements
     */
    public function count()
    {
      return count($this->StoreDTO);
    }

}
