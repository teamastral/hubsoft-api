<?php

namespace mattanger\hubsoft\integrationservice;

class BulkItemDTOCustom
{

    /**
     * @var int $appliedQty
     */
    protected $appliedQty = null;

    /**
     * @var string $requestedDate
     */
    protected $requestedDate = null;

    /**
     * @var int $reservedQty
     */
    protected $reservedQty = null;

    /**
     * @var string $scheduleDate
     */
    protected $scheduleDate = null;

    /**
     * @var string $sku
     */
    protected $sku = null;

    /**
     * @var string $unitPrice
     */
    protected $unitPrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getAppliedQty()
    {
      return $this->appliedQty;
    }

    /**
     * @param int $appliedQty
     * @return \mattanger\hubsoft\integrationservice\BulkItemDTO
     */
    public function setAppliedQty($appliedQty)
    {
      $this->appliedQty = $appliedQty;
      return $this;
    }

    /**
     * @return string
     */
    public function getRequestedDate()
    {
      return $this->requestedDate;
    }

    /**
     * @param string $requestedDate
     * @return \mattanger\hubsoft\integrationservice\BulkItemDTO
     */
    public function setRequestedDate($requestedDate)
    {
      $this->requestedDate = $requestedDate;
      return $this;
    }

    /**
     * @return int
     */
    public function getReservedQty()
    {
      return $this->reservedQty;
    }

    /**
     * @param int $reservedQty
     * @return \mattanger\hubsoft\integrationservice\BulkItemDTO
     */
    public function setReservedQty($reservedQty)
    {
      $this->reservedQty = $reservedQty;
      return $this;
    }

    /**
     * @return string
     */
    public function getScheduleDate()
    {
      return $this->scheduleDate;
    }

    /**
     * @param string $scheduleDate
     * @return \mattanger\hubsoft\integrationservice\BulkItemDTO
     */
    public function setScheduleDate($scheduleDate)
    {
      $this->scheduleDate = $scheduleDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
      return $this->sku;
    }

    /**
     * @param string $sku
     * @return \mattanger\hubsoft\integrationservice\BulkItemDTO
     */
    public function setSku($sku)
    {
      $this->sku = $sku;
      return $this;
    }

    /**
     * @return string
     */
    public function getUnitPrice()
    {
      return $this->unitPrice;
    }

    /**
     * @param string $unitPrice
     * @return \mattanger\hubsoft\integrationservice\BulkItemDTO
     */
    public function setUnitPrice($unitPrice)
    {
      $this->unitPrice = $unitPrice;
      return $this;
    }

}
