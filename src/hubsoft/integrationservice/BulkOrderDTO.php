<?php

namespace hubsoft\api\integrationservice;

class BulkOrderDTO
{

    /**
     * @var string $accountCode
     */
    protected $accountCode = null;

    /**
     * @var string $active
     */
    protected $active = null;

    /**
     * @var string $bulkOrderCode
     */
    protected $bulkOrderCode = null;

    /**
     * @var string $bulkOrderName
     */
    protected $bulkOrderName = null;

    /**
     * @var string $endDate
     */
    protected $endDate = null;

    /**
     * @var ArrayOfBulkItemDTO $itemList
     */
    protected $itemList = null;

    /**
     * @var string $repCode
     */
    protected $repCode = null;

    /**
     * @var string $startDate
     */
    protected $startDate = null;

    /**
     * @var string $warehouseCode
     */
    protected $warehouseCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAccountCode()
    {
      return $this->accountCode;
    }

    /**
     * @param string $accountCode
     * @return \hubsoft\api\integrationservice\BulkOrderDTO
     */
    public function setAccountCode($accountCode)
    {
      $this->accountCode = $accountCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getActive()
    {
      return $this->active;
    }

    /**
     * @param string $active
     * @return \hubsoft\api\integrationservice\BulkOrderDTO
     */
    public function setActive($active)
    {
      $this->active = $active;
      return $this;
    }

    /**
     * @return string
     */
    public function getBulkOrderCode()
    {
      return $this->bulkOrderCode;
    }

    /**
     * @param string $bulkOrderCode
     * @return \hubsoft\api\integrationservice\BulkOrderDTO
     */
    public function setBulkOrderCode($bulkOrderCode)
    {
      $this->bulkOrderCode = $bulkOrderCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBulkOrderName()
    {
      return $this->bulkOrderName;
    }

    /**
     * @param string $bulkOrderName
     * @return \hubsoft\api\integrationservice\BulkOrderDTO
     */
    public function setBulkOrderName($bulkOrderName)
    {
      $this->bulkOrderName = $bulkOrderName;
      return $this;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
      return $this->endDate;
    }

    /**
     * @param string $endDate
     * @return \hubsoft\api\integrationservice\BulkOrderDTO
     */
    public function setEndDate($endDate)
    {
      $this->endDate = $endDate;
      return $this;
    }

    /**
     * @return ArrayOfBulkItemDTO
     */
    public function getItemList()
    {
      return $this->itemList;
    }

    /**
     * @param ArrayOfBulkItemDTO $itemList
     * @return \hubsoft\api\integrationservice\BulkOrderDTO
     */
    public function setItemList($itemList)
    {
      $this->itemList = $itemList;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCode()
    {
      return $this->repCode;
    }

    /**
     * @param string $repCode
     * @return \hubsoft\api\integrationservice\BulkOrderDTO
     */
    public function setRepCode($repCode)
    {
      $this->repCode = $repCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
      return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return \hubsoft\api\integrationservice\BulkOrderDTO
     */
    public function setStartDate($startDate)
    {
      $this->startDate = $startDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getWarehouseCode()
    {
      return $this->warehouseCode;
    }

    /**
     * @param string $warehouseCode
     * @return \hubsoft\api\integrationservice\BulkOrderDTO
     */
    public function setWarehouseCode($warehouseCode)
    {
      $this->warehouseCode = $warehouseCode;
      return $this;
    }

}
