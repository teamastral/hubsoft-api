<?php

namespace mattanger\hubsoft\integrationservice;

class BuyerDTOCustom2
{

    /**
     * @var ContactDTO $contact
     */
    protected $contact = null;

    /**
     * @var string $divisionRestrictionCode
     */
    protected $divisionRestrictionCode = null;

    /**
     * @var string $repCode
     */
    protected $repCode = null;

    /**
     * @var ArrayOfString $storeList
     */
    protected $storeList = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ContactDTO
     */
    public function getContact()
    {
      return $this->contact;
    }

    /**
     * @param ContactDTO $contact
     * @return \mattanger\hubsoft\integrationservice\BuyerDTO
     */
    public function setContact($contact)
    {
      $this->contact = $contact;
      return $this;
    }

    /**
     * @return string
     */
    public function getDivisionRestrictionCode()
    {
      return $this->divisionRestrictionCode;
    }

    /**
     * @param string $divisionRestrictionCode
     * @return \mattanger\hubsoft\integrationservice\BuyerDTO
     */
    public function setDivisionRestrictionCode($divisionRestrictionCode)
    {
      $this->divisionRestrictionCode = $divisionRestrictionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCode()
    {
      return $this->repCode;
    }

    /**
     * @param string $repCode
     * @return \mattanger\hubsoft\integrationservice\BuyerDTO
     */
    public function setRepCode($repCode)
    {
      $this->repCode = $repCode;
      return $this;
    }

    /**
     * @return ArrayOfString
     */
    public function getStoreList()
    {
      return $this->storeList;
    }

    /**
     * @param ArrayOfString $storeList
     * @return \mattanger\hubsoft\integrationservice\BuyerDTO
     */
    public function setStoreList($storeList)
    {
      $this->storeList = $storeList;
      return $this;
    }

}
