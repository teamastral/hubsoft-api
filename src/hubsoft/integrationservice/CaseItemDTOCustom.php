<?php

namespace mattanger\hubsoft\integrationservice;

class CaseItemDTOCustom
{

    /**
     * @var float $msrp
     */
    protected $msrp = null;

    /**
     * @var int $orderedQty
     */
    protected $orderedQty = null;

    /**
     * @var string $sku
     */
    protected $sku = null;

    /**
     * @var float $unitPrice
     */
    protected $unitPrice = null;

    /**
     * @var string $upc
     */
    protected $upc = null;

    /**
     * @var float $wholesalePrice
     */
    protected $wholesalePrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getMsrp()
    {
      return $this->msrp;
    }

    /**
     * @param float $msrp
     * @return \mattanger\hubsoft\integrationservice\CaseItemDTO
     */
    public function setMsrp($msrp)
    {
      $this->msrp = $msrp;
      return $this;
    }

    /**
     * @return int
     */
    public function getOrderedQty()
    {
      return $this->orderedQty;
    }

    /**
     * @param int $orderedQty
     * @return \mattanger\hubsoft\integrationservice\CaseItemDTO
     */
    public function setOrderedQty($orderedQty)
    {
      $this->orderedQty = $orderedQty;
      return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
      return $this->sku;
    }

    /**
     * @param string $sku
     * @return \mattanger\hubsoft\integrationservice\CaseItemDTO
     */
    public function setSku($sku)
    {
      $this->sku = $sku;
      return $this;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
      return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     * @return \mattanger\hubsoft\integrationservice\CaseItemDTO
     */
    public function setUnitPrice($unitPrice)
    {
      $this->unitPrice = $unitPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getUpc()
    {
      return $this->upc;
    }

    /**
     * @param string $upc
     * @return \mattanger\hubsoft\integrationservice\CaseItemDTO
     */
    public function setUpc($upc)
    {
      $this->upc = $upc;
      return $this;
    }

    /**
     * @return float
     */
    public function getWholesalePrice()
    {
      return $this->wholesalePrice;
    }

    /**
     * @param float $wholesalePrice
     * @return \mattanger\hubsoft\integrationservice\CaseItemDTO
     */
    public function setWholesalePrice($wholesalePrice)
    {
      $this->wholesalePrice = $wholesalePrice;
      return $this;
    }

}
