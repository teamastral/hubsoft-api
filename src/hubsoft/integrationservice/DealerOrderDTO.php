<?php

namespace hubsoft\api\integrationservice;

class DealerOrderDTO
{

    /**
     * @var string $accountCode
     */
    protected $accountCode = null;

    /**
     * @var string $accountName
     */
    protected $accountName = null;

    /**
     * @var string $allowShipEarly
     */
    protected $allowShipEarly = null;

    /**
     * @var string $billingTermCode
     */
    protected $billingTermCode = null;

    /**
     * @var string $buyerEmail
     */
    protected $buyerEmail = null;

    /**
     * @var string $buyerFirstName
     */
    protected $buyerFirstName = null;

    /**
     * @var string $buyerLastName
     */
    protected $buyerLastName = null;

    /**
     * @var string $buyerPhone
     */
    protected $buyerPhone = null;

    /**
     * @var string $cancelDate
     */
    protected $cancelDate = null;

    /**
     * @var string $cardLast4Digit
     */
    protected $cardLast4Digit = null;

    /**
     * @var string $createdByFirstName
     */
    protected $createdByFirstName = null;

    /**
     * @var string $createdByLastName
     */
    protected $createdByLastName = null;

    /**
     * @var string $creditStatusCode
     */
    protected $creditStatusCode = null;

    /**
     * @var string $currencyCode
     */
    protected $currencyCode = null;

    /**
     * @var string $hubsoftOrderNumber
     */
    protected $hubsoftOrderNumber = null;

    /**
     * @var string $incentiveCode
     */
    protected $incentiveCode = null;

    /**
     * @var string $incentiveName
     */
    protected $incentiveName = null;

    /**
     * @var string $invoiceDate
     */
    protected $invoiceDate = null;

    /**
     * @var string $invoiceNumber
     */
    protected $invoiceNumber = null;

    /**
     * @var string $modifiedDateTime
     */
    protected $modifiedDateTime = null;

    /**
     * @var string $moq
     */
    protected $moq = null;

    /**
     * @var string $mustShipComplete
     */
    protected $mustShipComplete = null;

    /**
     * @var MustShipTogether $mustShipTogether
     */
    protected $mustShipTogether = null;

    /**
     * @var string $notes
     */
    protected $notes = null;

    /**
     * @var string $orderDateTime
     */
    protected $orderDateTime = null;

    /**
     * @var string $orderStatusCode
     */
    protected $orderStatusCode = null;

    /**
     * @var ArrayOfOrderStoreDTO $orderStoreList
     */
    protected $orderStoreList = null;

    /**
     * @var string $orderTypeCode
     */
    protected $orderTypeCode = null;

    /**
     * @var string $paymentApprovalCode
     */
    protected $paymentApprovalCode = null;

    /**
     * @var string $paymentDate
     */
    protected $paymentDate = null;

    /**
     * @var string $paymentMethod
     */
    protected $paymentMethod = null;

    /**
     * @var string $paymentReference
     */
    protected $paymentReference = null;

    /**
     * @var string $planToShipDate
     */
    protected $planToShipDate = null;

    /**
     * @var string $priceBookCode
     */
    protected $priceBookCode = null;

    /**
     * @var string $purchaseOrderNumber
     */
    protected $purchaseOrderNumber = null;

    /**
     * @var string $remoteLastModifiedDt
     */
    protected $remoteLastModifiedDt = null;

    /**
     * @var string $remoteOrderNumber
     */
    protected $remoteOrderNumber = null;

    /**
     * @var string $repCode
     */
    protected $repCode = null;

    /**
     * @var string $seasonCode
     */
    protected $seasonCode = null;

    /**
     * @var string $shipDate
     */
    protected $shipDate = null;

    /**
     * @var string $sourceCode
     */
    protected $sourceCode = null;

    /**
     * @var string $startDate
     */
    protected $startDate = null;

    /**
     * @param string $allowShipEarly
     * @param string $cardLast4Digit
     * @param string $createdByFirstName
     * @param string $createdByLastName
     * @param string $incentiveName
     * @param string $moq
     * @param string $mustShipComplete
     * @param MustShipTogether $mustShipTogether
     * @param string $paymentApprovalCode
     * @param string $paymentDate
     * @param string $paymentMethod
     * @param string $paymentReference
     */
    public function __construct($allowShipEarly, $cardLast4Digit, $createdByFirstName, $createdByLastName, $incentiveName, $moq, $mustShipComplete, $mustShipTogether, $paymentApprovalCode, $paymentDate, $paymentMethod, $paymentReference)
    {
      $this->allowShipEarly = $allowShipEarly;
      $this->cardLast4Digit = $cardLast4Digit;
      $this->createdByFirstName = $createdByFirstName;
      $this->createdByLastName = $createdByLastName;
      $this->incentiveName = $incentiveName;
      $this->moq = $moq;
      $this->mustShipComplete = $mustShipComplete;
      $this->mustShipTogether = $mustShipTogether;
      $this->paymentApprovalCode = $paymentApprovalCode;
      $this->paymentDate = $paymentDate;
      $this->paymentMethod = $paymentMethod;
      $this->paymentReference = $paymentReference;
    }

    /**
     * @return string
     */
    public function getAccountCode()
    {
      return $this->accountCode;
    }

    /**
     * @param string $accountCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setAccountCode($accountCode)
    {
      $this->accountCode = $accountCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccountName()
    {
      return $this->accountName;
    }

    /**
     * @param string $accountName
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setAccountName($accountName)
    {
      $this->accountName = $accountName;
      return $this;
    }

    /**
     * @return string
     */
    public function getAllowShipEarly()
    {
      return $this->allowShipEarly;
    }

    /**
     * @param string $allowShipEarly
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setAllowShipEarly($allowShipEarly)
    {
      $this->allowShipEarly = $allowShipEarly;
      return $this;
    }

    /**
     * @return string
     */
    public function getBillingTermCode()
    {
      return $this->billingTermCode;
    }

    /**
     * @param string $billingTermCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setBillingTermCode($billingTermCode)
    {
      $this->billingTermCode = $billingTermCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBuyerEmail()
    {
      return $this->buyerEmail;
    }

    /**
     * @param string $buyerEmail
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setBuyerEmail($buyerEmail)
    {
      $this->buyerEmail = $buyerEmail;
      return $this;
    }

    /**
     * @return string
     */
    public function getBuyerFirstName()
    {
      return $this->buyerFirstName;
    }

    /**
     * @param string $buyerFirstName
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setBuyerFirstName($buyerFirstName)
    {
      $this->buyerFirstName = $buyerFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getBuyerLastName()
    {
      return $this->buyerLastName;
    }

    /**
     * @param string $buyerLastName
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setBuyerLastName($buyerLastName)
    {
      $this->buyerLastName = $buyerLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getBuyerPhone()
    {
      return $this->buyerPhone;
    }

    /**
     * @param string $buyerPhone
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setBuyerPhone($buyerPhone)
    {
      $this->buyerPhone = $buyerPhone;
      return $this;
    }

    /**
     * @return string
     */
    public function getCancelDate()
    {
      return $this->cancelDate;
    }

    /**
     * @param string $cancelDate
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setCancelDate($cancelDate)
    {
      $this->cancelDate = $cancelDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardLast4Digit()
    {
      return $this->cardLast4Digit;
    }

    /**
     * @param string $cardLast4Digit
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setCardLast4Digit($cardLast4Digit)
    {
      $this->cardLast4Digit = $cardLast4Digit;
      return $this;
    }

    /**
     * @return string
     */
    public function getCreatedByFirstName()
    {
      return $this->createdByFirstName;
    }

    /**
     * @param string $createdByFirstName
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setCreatedByFirstName($createdByFirstName)
    {
      $this->createdByFirstName = $createdByFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCreatedByLastName()
    {
      return $this->createdByLastName;
    }

    /**
     * @param string $createdByLastName
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setCreatedByLastName($createdByLastName)
    {
      $this->createdByLastName = $createdByLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCreditStatusCode()
    {
      return $this->creditStatusCode;
    }

    /**
     * @param string $creditStatusCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setCreditStatusCode($creditStatusCode)
    {
      $this->creditStatusCode = $creditStatusCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
      return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setCurrencyCode($currencyCode)
    {
      $this->currencyCode = $currencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getHubsoftOrderNumber()
    {
      return $this->hubsoftOrderNumber;
    }

    /**
     * @param string $hubsoftOrderNumber
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setHubsoftOrderNumber($hubsoftOrderNumber)
    {
      $this->hubsoftOrderNumber = $hubsoftOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getIncentiveCode()
    {
      return $this->incentiveCode;
    }

    /**
     * @param string $incentiveCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setIncentiveCode($incentiveCode)
    {
      $this->incentiveCode = $incentiveCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getIncentiveName()
    {
      return $this->incentiveName;
    }

    /**
     * @param string $incentiveName
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setIncentiveName($incentiveName)
    {
      $this->incentiveName = $incentiveName;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceDate()
    {
      return $this->invoiceDate;
    }

    /**
     * @param string $invoiceDate
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setInvoiceDate($invoiceDate)
    {
      $this->invoiceDate = $invoiceDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNumber()
    {
      return $this->invoiceNumber;
    }

    /**
     * @param string $invoiceNumber
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setInvoiceNumber($invoiceNumber)
    {
      $this->invoiceNumber = $invoiceNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getModifiedDateTime()
    {
      return $this->modifiedDateTime;
    }

    /**
     * @param string $modifiedDateTime
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setModifiedDateTime($modifiedDateTime)
    {
      $this->modifiedDateTime = $modifiedDateTime;
      return $this;
    }

    /**
     * @return string
     */
    public function getMoq()
    {
      return $this->moq;
    }

    /**
     * @param string $moq
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setMoq($moq)
    {
      $this->moq = $moq;
      return $this;
    }

    /**
     * @return string
     */
    public function getMustShipComplete()
    {
      return $this->mustShipComplete;
    }

    /**
     * @param string $mustShipComplete
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setMustShipComplete($mustShipComplete)
    {
      $this->mustShipComplete = $mustShipComplete;
      return $this;
    }

    /**
     * @return MustShipTogether
     */
    public function getMustShipTogether()
    {
      return $this->mustShipTogether;
    }

    /**
     * @param MustShipTogether $mustShipTogether
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setMustShipTogether($mustShipTogether)
    {
      $this->mustShipTogether = $mustShipTogether;
      return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
      return $this->notes;
    }

    /**
     * @param string $notes
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setNotes($notes)
    {
      $this->notes = $notes;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderDateTime()
    {
      return $this->orderDateTime;
    }

    /**
     * @param string $orderDateTime
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setOrderDateTime($orderDateTime)
    {
      $this->orderDateTime = $orderDateTime;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderStatusCode()
    {
      return $this->orderStatusCode;
    }

    /**
     * @param string $orderStatusCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setOrderStatusCode($orderStatusCode)
    {
      $this->orderStatusCode = $orderStatusCode;
      return $this;
    }

    /**
     * @return ArrayOfOrderStoreDTO
     */
    public function getOrderStoreList()
    {
      return $this->orderStoreList;
    }

    /**
     * @param ArrayOfOrderStoreDTO $orderStoreList
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setOrderStoreList($orderStoreList)
    {
      $this->orderStoreList = $orderStoreList;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderTypeCode()
    {
      return $this->orderTypeCode;
    }

    /**
     * @param string $orderTypeCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setOrderTypeCode($orderTypeCode)
    {
      $this->orderTypeCode = $orderTypeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentApprovalCode()
    {
      return $this->paymentApprovalCode;
    }

    /**
     * @param string $paymentApprovalCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setPaymentApprovalCode($paymentApprovalCode)
    {
      $this->paymentApprovalCode = $paymentApprovalCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentDate()
    {
      return $this->paymentDate;
    }

    /**
     * @param string $paymentDate
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setPaymentDate($paymentDate)
    {
      $this->paymentDate = $paymentDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
      return $this->paymentMethod;
    }

    /**
     * @param string $paymentMethod
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setPaymentMethod($paymentMethod)
    {
      $this->paymentMethod = $paymentMethod;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentReference()
    {
      return $this->paymentReference;
    }

    /**
     * @param string $paymentReference
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setPaymentReference($paymentReference)
    {
      $this->paymentReference = $paymentReference;
      return $this;
    }

    /**
     * @return string
     */
    public function getPlanToShipDate()
    {
      return $this->planToShipDate;
    }

    /**
     * @param string $planToShipDate
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setPlanToShipDate($planToShipDate)
    {
      $this->planToShipDate = $planToShipDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getPriceBookCode()
    {
      return $this->priceBookCode;
    }

    /**
     * @param string $priceBookCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setPriceBookCode($priceBookCode)
    {
      $this->priceBookCode = $priceBookCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPurchaseOrderNumber()
    {
      return $this->purchaseOrderNumber;
    }

    /**
     * @param string $purchaseOrderNumber
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setPurchaseOrderNumber($purchaseOrderNumber)
    {
      $this->purchaseOrderNumber = $purchaseOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getRemoteLastModifiedDt()
    {
      return $this->remoteLastModifiedDt;
    }

    /**
     * @param string $remoteLastModifiedDt
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setRemoteLastModifiedDt($remoteLastModifiedDt)
    {
      $this->remoteLastModifiedDt = $remoteLastModifiedDt;
      return $this;
    }

    /**
     * @return string
     */
    public function getRemoteOrderNumber()
    {
      return $this->remoteOrderNumber;
    }

    /**
     * @param string $remoteOrderNumber
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setRemoteOrderNumber($remoteOrderNumber)
    {
      $this->remoteOrderNumber = $remoteOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCode()
    {
      return $this->repCode;
    }

    /**
     * @param string $repCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setRepCode($repCode)
    {
      $this->repCode = $repCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSeasonCode()
    {
      return $this->seasonCode;
    }

    /**
     * @param string $seasonCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setSeasonCode($seasonCode)
    {
      $this->seasonCode = $seasonCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipDate()
    {
      return $this->shipDate;
    }

    /**
     * @param string $shipDate
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setShipDate($shipDate)
    {
      $this->shipDate = $shipDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getSourceCode()
    {
      return $this->sourceCode;
    }

    /**
     * @param string $sourceCode
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setSourceCode($sourceCode)
    {
      $this->sourceCode = $sourceCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
      return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return \hubsoft\api\integrationservice\DealerOrderDTO
     */
    public function setStartDate($startDate)
    {
      $this->startDate = $startDate;
      return $this;
    }

}
