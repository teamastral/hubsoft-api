<?php

namespace mattanger\hubsoft\integrationservice;

class DealerOrderDeletedDTOCustom
{

    /**
     * @var ArrayOfString $deleteSkuList
     */
    protected $deleteSkuList = null;

    /**
     * @var string $hubsoftOrderNumber
     */
    protected $hubsoftOrderNumber = null;

    /**
     * @var string $remoteOrderNumber
     */
    protected $remoteOrderNumber = null;

    /**
     * @var string $status
     */
    protected $status = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfString
     */
    public function getDeleteSkuList()
    {
      return $this->deleteSkuList;
    }

    /**
     * @param ArrayOfString $deleteSkuList
     * @return \mattanger\hubsoft\integrationservice\DealerOrderDeletedDTO
     */
    public function setDeleteSkuList($deleteSkuList)
    {
      $this->deleteSkuList = $deleteSkuList;
      return $this;
    }

    /**
     * @return string
     */
    public function getHubsoftOrderNumber()
    {
      return $this->hubsoftOrderNumber;
    }

    /**
     * @param string $hubsoftOrderNumber
     * @return \mattanger\hubsoft\integrationservice\DealerOrderDeletedDTO
     */
    public function setHubsoftOrderNumber($hubsoftOrderNumber)
    {
      $this->hubsoftOrderNumber = $hubsoftOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getRemoteOrderNumber()
    {
      return $this->remoteOrderNumber;
    }

    /**
     * @param string $remoteOrderNumber
     * @return \mattanger\hubsoft\integrationservice\DealerOrderDeletedDTO
     */
    public function setRemoteOrderNumber($remoteOrderNumber)
    {
      $this->remoteOrderNumber = $remoteOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
      return $this->status;
    }

    /**
     * @param string $status
     * @return \mattanger\hubsoft\integrationservice\DealerOrderDeletedDTO
     */
    public function setStatus($status)
    {
      $this->status = $status;
      return $this;
    }

}
