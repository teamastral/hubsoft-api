<?php

namespace hubsoft\api\integrationservice;

class Detail
{

    /**
     * @var string $code
     */
    protected $code = null;

    /**
     * @var string $keyObjectType
     */
    protected $keyObjectType = null;

    /**
     * @var string $keyObjectValue
     */
    protected $keyObjectValue = null;

    /**
     * @var string $message
     */
    protected $message = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->code;
    }

    /**
     * @param string $code
     * @return \hubsoft\api\integrationservice\Detail
     */
    public function setCode($code)
    {
      $this->code = $code;
      return $this;
    }

    /**
     * @return string
     */
    public function getKeyObjectType()
    {
      return $this->keyObjectType;
    }

    /**
     * @param string $keyObjectType
     * @return \hubsoft\api\integrationservice\Detail
     */
    public function setKeyObjectType($keyObjectType)
    {
      $this->keyObjectType = $keyObjectType;
      return $this;
    }

    /**
     * @return string
     */
    public function getKeyObjectValue()
    {
      return $this->keyObjectValue;
    }

    /**
     * @param string $keyObjectValue
     * @return \hubsoft\api\integrationservice\Detail
     */
    public function setKeyObjectValue($keyObjectValue)
    {
      $this->keyObjectValue = $keyObjectValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
      return $this->message;
    }

    /**
     * @param string $message
     * @return \hubsoft\api\integrationservice\Detail
     */
    public function setMessage($message)
    {
      $this->message = $message;
      return $this;
    }

}
