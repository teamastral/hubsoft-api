<?php

namespace mattanger\hubsoft\integrationservice;

class DirectOrderDTOCustom2
{

    /**
     * @var string $accountCode
     */
    protected $accountCode = null;

    /**
     * @var string $accountName
     */
    protected $accountName = null;

    /**
     * @var AddressDTO $billingAddress
     */
    protected $billingAddress = null;

    /**
     * @var string $cancelDate
     */
    protected $cancelDate = null;

    /**
     * @var string $cardExpiryMMYYYY
     */
    protected $cardExpiryMMYYYY = null;

    /**
     * @var string $cardExpiryMonth
     */
    protected $cardExpiryMonth = null;

    /**
     * @var string $cardExpiryYear
     */
    protected $cardExpiryYear = null;

    /**
     * @var string $cardHolderFirstName
     */
    protected $cardHolderFirstName = null;

    /**
     * @var string $cardHolderLastName
     */
    protected $cardHolderLastName = null;

    /**
     * @var string $cardNumber
     */
    protected $cardNumber = null;

    /**
     * @var string $customerNumber
     */
    protected $customerNumber = null;

    /**
     * @var string $customerType
     */
    protected $customerType = null;

    /**
     * @var float $freightTax
     */
    protected $freightTax = null;

    /**
     * @var float $giftCardAmount
     */
    protected $giftCardAmount = null;

    /**
     * @var string $giftCardNumber
     */
    protected $giftCardNumber = null;

    /**
     * @var string $giftCardPaymentApprovalCode
     */
    protected $giftCardPaymentApprovalCode = null;

    /**
     * @var string $giftCardPaymentProcessDate
     */
    protected $giftCardPaymentProcessDate = null;

    /**
     * @var string $giftCardPaymentProcessResult
     */
    protected $giftCardPaymentProcessResult = null;

    /**
     * @var string $giftCardPaymentReference
     */
    protected $giftCardPaymentReference = null;

    /**
     * @var string $hubsoftOrderNumber
     */
    protected $hubsoftOrderNumber = null;

    /**
     * @var string $modifiedDateTime
     */
    protected $modifiedDateTime = null;

    /**
     * @var string $notes
     */
    protected $notes = null;

    /**
     * @var string $optInEmail
     */
    protected $optInEmail = null;

    /**
     * @var ArrayOfAttrDTO $orderAttrList
     */
    protected $orderAttrList = null;

    /**
     * @var string $orderDateTime
     */
    protected $orderDateTime = null;

    /**
     * @var ArrayOfOrderItemDTO $orderItemList
     */
    protected $orderItemList = null;

    /**
     * @var string $orderStatusCode
     */
    protected $orderStatusCode = null;

    /**
     * @var string $orderTypeCode
     */
    protected $orderTypeCode = null;

    /**
     * @var string $paymentApprovalCode
     */
    protected $paymentApprovalCode = null;

    /**
     * @var string $paymentMethodCode
     */
    protected $paymentMethodCode = null;

    /**
     * @var string $paymentProcessDate
     */
    protected $paymentProcessDate = null;

    /**
     * @var string $paymentReference
     */
    protected $paymentReference = null;

    /**
     * @var string $programName
     */
    protected $programName = null;

    /**
     * @var string $promotionCode
     */
    protected $promotionCode = null;

    /**
     * @var string $promotionTypeCode
     */
    protected $promotionTypeCode = null;

    /**
     * @var int $purchasePointsUsed
     */
    protected $purchasePointsUsed = null;

    /**
     * @var string $remoteOrderNumber
     */
    protected $remoteOrderNumber = null;

    /**
     * @var string $repCode
     */
    protected $repCode = null;

    /**
     * @var float $salesTax
     */
    protected $salesTax = null;

    /**
     * @var float $shipAmount
     */
    protected $shipAmount = null;

    /**
     * @var string $shipDate
     */
    protected $shipDate = null;

    /**
     * @var string $shipToAttention
     */
    protected $shipToAttention = null;

    /**
     * @var string $shipToEmail
     */
    protected $shipToEmail = null;

    /**
     * @var string $shipToPhone
     */
    protected $shipToPhone = null;

    /**
     * @var ArrayOfShipmentInfo $shipmentList
     */
    protected $shipmentList = null;

    /**
     * @var AddressDTO $shippingAddress
     */
    protected $shippingAddress = null;

    /**
     * @var string $startDate
     */
    protected $startDate = null;

    /**
     * @var string $storeCode
     */
    protected $storeCode = null;

    /**
     * @var int $totalOrderedQty
     */
    protected $totalOrderedQty = null;

    /**
     * @var string $warehouseCode
     */
    protected $warehouseCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAccountCode()
    {
      return $this->accountCode;
    }

    /**
     * @param string $accountCode
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setAccountCode($accountCode)
    {
      $this->accountCode = $accountCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccountName()
    {
      return $this->accountName;
    }

    /**
     * @param string $accountName
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setAccountName($accountName)
    {
      $this->accountName = $accountName;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getBillingAddress()
    {
      return $this->billingAddress;
    }

    /**
     * @param AddressDTO $billingAddress
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setBillingAddress($billingAddress)
    {
      $this->billingAddress = $billingAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getCancelDate()
    {
      return $this->cancelDate;
    }

    /**
     * @param string $cancelDate
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setCancelDate($cancelDate)
    {
      $this->cancelDate = $cancelDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardExpiryMMYYYY()
    {
      return $this->cardExpiryMMYYYY;
    }

    /**
     * @param string $cardExpiryMMYYYY
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setCardExpiryMMYYYY($cardExpiryMMYYYY)
    {
      $this->cardExpiryMMYYYY = $cardExpiryMMYYYY;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardExpiryMonth()
    {
      return $this->cardExpiryMonth;
    }

    /**
     * @param string $cardExpiryMonth
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setCardExpiryMonth($cardExpiryMonth)
    {
      $this->cardExpiryMonth = $cardExpiryMonth;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardExpiryYear()
    {
      return $this->cardExpiryYear;
    }

    /**
     * @param string $cardExpiryYear
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setCardExpiryYear($cardExpiryYear)
    {
      $this->cardExpiryYear = $cardExpiryYear;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardHolderFirstName()
    {
      return $this->cardHolderFirstName;
    }

    /**
     * @param string $cardHolderFirstName
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setCardHolderFirstName($cardHolderFirstName)
    {
      $this->cardHolderFirstName = $cardHolderFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardHolderLastName()
    {
      return $this->cardHolderLastName;
    }

    /**
     * @param string $cardHolderLastName
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setCardHolderLastName($cardHolderLastName)
    {
      $this->cardHolderLastName = $cardHolderLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCardNumber()
    {
      return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setCardNumber($cardNumber)
    {
      $this->cardNumber = $cardNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerNumber()
    {
      return $this->customerNumber;
    }

    /**
     * @param string $customerNumber
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setCustomerNumber($customerNumber)
    {
      $this->customerNumber = $customerNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerType()
    {
      return $this->customerType;
    }

    /**
     * @param string $customerType
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setCustomerType($customerType)
    {
      $this->customerType = $customerType;
      return $this;
    }

    /**
     * @return float
     */
    public function getFreightTax()
    {
      return $this->freightTax;
    }

    /**
     * @param float $freightTax
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setFreightTax($freightTax)
    {
      $this->freightTax = $freightTax;
      return $this;
    }

    /**
     * @return float
     */
    public function getGiftCardAmount()
    {
      return $this->giftCardAmount;
    }

    /**
     * @param float $giftCardAmount
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setGiftCardAmount($giftCardAmount)
    {
      $this->giftCardAmount = $giftCardAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getGiftCardNumber()
    {
      return $this->giftCardNumber;
    }

    /**
     * @param string $giftCardNumber
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setGiftCardNumber($giftCardNumber)
    {
      $this->giftCardNumber = $giftCardNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getGiftCardPaymentApprovalCode()
    {
      return $this->giftCardPaymentApprovalCode;
    }

    /**
     * @param string $giftCardPaymentApprovalCode
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setGiftCardPaymentApprovalCode($giftCardPaymentApprovalCode)
    {
      $this->giftCardPaymentApprovalCode = $giftCardPaymentApprovalCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getGiftCardPaymentProcessDate()
    {
      return $this->giftCardPaymentProcessDate;
    }

    /**
     * @param string $giftCardPaymentProcessDate
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setGiftCardPaymentProcessDate($giftCardPaymentProcessDate)
    {
      $this->giftCardPaymentProcessDate = $giftCardPaymentProcessDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getGiftCardPaymentProcessResult()
    {
      return $this->giftCardPaymentProcessResult;
    }

    /**
     * @param string $giftCardPaymentProcessResult
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setGiftCardPaymentProcessResult($giftCardPaymentProcessResult)
    {
      $this->giftCardPaymentProcessResult = $giftCardPaymentProcessResult;
      return $this;
    }

    /**
     * @return string
     */
    public function getGiftCardPaymentReference()
    {
      return $this->giftCardPaymentReference;
    }

    /**
     * @param string $giftCardPaymentReference
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setGiftCardPaymentReference($giftCardPaymentReference)
    {
      $this->giftCardPaymentReference = $giftCardPaymentReference;
      return $this;
    }

    /**
     * @return string
     */
    public function getHubsoftOrderNumber()
    {
      return $this->hubsoftOrderNumber;
    }

    /**
     * @param string $hubsoftOrderNumber
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setHubsoftOrderNumber($hubsoftOrderNumber)
    {
      $this->hubsoftOrderNumber = $hubsoftOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getModifiedDateTime()
    {
      return $this->modifiedDateTime;
    }

    /**
     * @param string $modifiedDateTime
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setModifiedDateTime($modifiedDateTime)
    {
      $this->modifiedDateTime = $modifiedDateTime;
      return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
      return $this->notes;
    }

    /**
     * @param string $notes
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setNotes($notes)
    {
      $this->notes = $notes;
      return $this;
    }

    /**
     * @return string
     */
    public function getOptInEmail()
    {
      return $this->optInEmail;
    }

    /**
     * @param string $optInEmail
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setOptInEmail($optInEmail)
    {
      $this->optInEmail = $optInEmail;
      return $this;
    }

    /**
     * @return ArrayOfAttrDTO
     */
    public function getOrderAttrList()
    {
      return $this->orderAttrList;
    }

    /**
     * @param ArrayOfAttrDTO $orderAttrList
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setOrderAttrList($orderAttrList)
    {
      $this->orderAttrList = $orderAttrList;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderDateTime()
    {
      return $this->orderDateTime;
    }

    /**
     * @param string $orderDateTime
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setOrderDateTime($orderDateTime)
    {
      $this->orderDateTime = $orderDateTime;
      return $this;
    }

    /**
     * @return ArrayOfOrderItemDTO
     */
    public function getOrderItemList()
    {
      return $this->orderItemList;
    }

    /**
     * @param ArrayOfOrderItemDTO $orderItemList
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setOrderItemList($orderItemList)
    {
      $this->orderItemList = $orderItemList;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderStatusCode()
    {
      return $this->orderStatusCode;
    }

    /**
     * @param string $orderStatusCode
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setOrderStatusCode($orderStatusCode)
    {
      $this->orderStatusCode = $orderStatusCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderTypeCode()
    {
      return $this->orderTypeCode;
    }

    /**
     * @param string $orderTypeCode
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setOrderTypeCode($orderTypeCode)
    {
      $this->orderTypeCode = $orderTypeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentApprovalCode()
    {
      return $this->paymentApprovalCode;
    }

    /**
     * @param string $paymentApprovalCode
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setPaymentApprovalCode($paymentApprovalCode)
    {
      $this->paymentApprovalCode = $paymentApprovalCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentMethodCode()
    {
      return $this->paymentMethodCode;
    }

    /**
     * @param string $paymentMethodCode
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setPaymentMethodCode($paymentMethodCode)
    {
      $this->paymentMethodCode = $paymentMethodCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentProcessDate()
    {
      return $this->paymentProcessDate;
    }

    /**
     * @param string $paymentProcessDate
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setPaymentProcessDate($paymentProcessDate)
    {
      $this->paymentProcessDate = $paymentProcessDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentReference()
    {
      return $this->paymentReference;
    }

    /**
     * @param string $paymentReference
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setPaymentReference($paymentReference)
    {
      $this->paymentReference = $paymentReference;
      return $this;
    }

    /**
     * @return string
     */
    public function getProgramName()
    {
      return $this->programName;
    }

    /**
     * @param string $programName
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setProgramName($programName)
    {
      $this->programName = $programName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionCode()
    {
      return $this->promotionCode;
    }

    /**
     * @param string $promotionCode
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setPromotionCode($promotionCode)
    {
      $this->promotionCode = $promotionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPromotionTypeCode()
    {
      return $this->promotionTypeCode;
    }

    /**
     * @param string $promotionTypeCode
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setPromotionTypeCode($promotionTypeCode)
    {
      $this->promotionTypeCode = $promotionTypeCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getPurchasePointsUsed()
    {
      return $this->purchasePointsUsed;
    }

    /**
     * @param int $purchasePointsUsed
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setPurchasePointsUsed($purchasePointsUsed)
    {
      $this->purchasePointsUsed = $purchasePointsUsed;
      return $this;
    }

    /**
     * @return string
     */
    public function getRemoteOrderNumber()
    {
      return $this->remoteOrderNumber;
    }

    /**
     * @param string $remoteOrderNumber
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setRemoteOrderNumber($remoteOrderNumber)
    {
      $this->remoteOrderNumber = $remoteOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCode()
    {
      return $this->repCode;
    }

    /**
     * @param string $repCode
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setRepCode($repCode)
    {
      $this->repCode = $repCode;
      return $this;
    }

    /**
     * @return float
     */
    public function getSalesTax()
    {
      return $this->salesTax;
    }

    /**
     * @param float $salesTax
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setSalesTax($salesTax)
    {
      $this->salesTax = $salesTax;
      return $this;
    }

    /**
     * @return float
     */
    public function getShipAmount()
    {
      return $this->shipAmount;
    }

    /**
     * @param float $shipAmount
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setShipAmount($shipAmount)
    {
      $this->shipAmount = $shipAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipDate()
    {
      return $this->shipDate;
    }

    /**
     * @param string $shipDate
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setShipDate($shipDate)
    {
      $this->shipDate = $shipDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToAttention()
    {
      return $this->shipToAttention;
    }

    /**
     * @param string $shipToAttention
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setShipToAttention($shipToAttention)
    {
      $this->shipToAttention = $shipToAttention;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToEmail()
    {
      return $this->shipToEmail;
    }

    /**
     * @param string $shipToEmail
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setShipToEmail($shipToEmail)
    {
      $this->shipToEmail = $shipToEmail;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToPhone()
    {
      return $this->shipToPhone;
    }

    /**
     * @param string $shipToPhone
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setShipToPhone($shipToPhone)
    {
      $this->shipToPhone = $shipToPhone;
      return $this;
    }

    /**
     * @return ArrayOfShipmentInfo
     */
    public function getShipmentList()
    {
      return $this->shipmentList;
    }

    /**
     * @param ArrayOfShipmentInfo $shipmentList
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setShipmentList($shipmentList)
    {
      $this->shipmentList = $shipmentList;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getShippingAddress()
    {
      return $this->shippingAddress;
    }

    /**
     * @param AddressDTO $shippingAddress
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setShippingAddress($shippingAddress)
    {
      $this->shippingAddress = $shippingAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
      return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setStartDate($startDate)
    {
      $this->startDate = $startDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
      return $this->storeCode;
    }

    /**
     * @param string $storeCode
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setStoreCode($storeCode)
    {
      $this->storeCode = $storeCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getTotalOrderedQty()
    {
      return $this->totalOrderedQty;
    }

    /**
     * @param int $totalOrderedQty
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setTotalOrderedQty($totalOrderedQty)
    {
      $this->totalOrderedQty = $totalOrderedQty;
      return $this;
    }

    /**
     * @return string
     */
    public function getWarehouseCode()
    {
      return $this->warehouseCode;
    }

    /**
     * @param string $warehouseCode
     * @return \mattanger\hubsoft\integrationservice\DirectOrderDTO
     */
    public function setWarehouseCode($warehouseCode)
    {
      $this->warehouseCode = $warehouseCode;
      return $this;
    }

}
