<?php

namespace hubsoft\api\integrationservice;

class DirectOrderUpdate2DTO
{

    /**
     * @var string $hubsoftOrderNumber
     */
    protected $hubsoftOrderNumber = null;

    /**
     * @var ArrayOfInvoice $invoiceList
     */
    protected $invoiceList = null;

    /**
     * @var string $notes
     */
    protected $notes = null;

    /**
     * @var string $optInEmail
     */
    protected $optInEmail = null;

    /**
     * @var string $orderStatus
     */
    protected $orderStatus = null;

    /**
     * @var string $remoteOrderNumber
     */
    protected $remoteOrderNumber = null;

    /**
     * @var string $salesRep
     */
    protected $salesRep = null;

    /**
     * @var string $shipDate
     */
    protected $shipDate = null;

    /**
     * @var string $shipToAttention
     */
    protected $shipToAttention = null;

    /**
     * @var string $shipToEmail
     */
    protected $shipToEmail = null;

    /**
     * @var string $shipToPhone
     */
    protected $shipToPhone = null;

    /**
     * @var AddressDTO $shippingAddress
     */
    protected $shippingAddress = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getHubsoftOrderNumber()
    {
      return $this->hubsoftOrderNumber;
    }

    /**
     * @param string $hubsoftOrderNumber
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setHubsoftOrderNumber($hubsoftOrderNumber)
    {
      $this->hubsoftOrderNumber = $hubsoftOrderNumber;
      return $this;
    }

    /**
     * @return ArrayOfInvoice
     */
    public function getInvoiceList()
    {
      return $this->invoiceList;
    }

    /**
     * @param ArrayOfInvoice $invoiceList
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setInvoiceList($invoiceList)
    {
      $this->invoiceList = $invoiceList;
      return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
      return $this->notes;
    }

    /**
     * @param string $notes
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setNotes($notes)
    {
      $this->notes = $notes;
      return $this;
    }

    /**
     * @return string
     */
    public function getOptInEmail()
    {
      return $this->optInEmail;
    }

    /**
     * @param string $optInEmail
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setOptInEmail($optInEmail)
    {
      $this->optInEmail = $optInEmail;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderStatus()
    {
      return $this->orderStatus;
    }

    /**
     * @param string $orderStatus
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setOrderStatus($orderStatus)
    {
      $this->orderStatus = $orderStatus;
      return $this;
    }

    /**
     * @return string
     */
    public function getRemoteOrderNumber()
    {
      return $this->remoteOrderNumber;
    }

    /**
     * @param string $remoteOrderNumber
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setRemoteOrderNumber($remoteOrderNumber)
    {
      $this->remoteOrderNumber = $remoteOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesRep()
    {
      return $this->salesRep;
    }

    /**
     * @param string $salesRep
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setSalesRep($salesRep)
    {
      $this->salesRep = $salesRep;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipDate()
    {
      return $this->shipDate;
    }

    /**
     * @param string $shipDate
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setShipDate($shipDate)
    {
      $this->shipDate = $shipDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToAttention()
    {
      return $this->shipToAttention;
    }

    /**
     * @param string $shipToAttention
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setShipToAttention($shipToAttention)
    {
      $this->shipToAttention = $shipToAttention;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToEmail()
    {
      return $this->shipToEmail;
    }

    /**
     * @param string $shipToEmail
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setShipToEmail($shipToEmail)
    {
      $this->shipToEmail = $shipToEmail;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToPhone()
    {
      return $this->shipToPhone;
    }

    /**
     * @param string $shipToPhone
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setShipToPhone($shipToPhone)
    {
      $this->shipToPhone = $shipToPhone;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getShippingAddress()
    {
      return $this->shippingAddress;
    }

    /**
     * @param AddressDTO $shippingAddress
     * @return \hubsoft\api\integrationservice\DirectOrderUpdate2DTO
     */
    public function setShippingAddress($shippingAddress)
    {
      $this->shippingAddress = $shippingAddress;
      return $this;
    }

}
