<?php

namespace hubsoft\api\integrationservice;

class DirectOrderUpdateDTO
{

    /**
     * @var string $hubsoftOrderNumber
     */
    protected $hubsoftOrderNumber = null;

    /**
     * @var string $notes
     */
    protected $notes = null;

    /**
     * @var string $optInEmail
     */
    protected $optInEmail = null;

    /**
     * @var string $orderStatusCode
     */
    protected $orderStatusCode = null;

    /**
     * @var string $remoteOrderNumber
     */
    protected $remoteOrderNumber = null;

    /**
     * @var string $salesTaxPerc
     */
    protected $salesTaxPerc = null;

    /**
     * @var string $shipDate
     */
    protected $shipDate = null;

    /**
     * @var string $shipToAttention
     */
    protected $shipToAttention = null;

    /**
     * @var string $shipToEmail
     */
    protected $shipToEmail = null;

    /**
     * @var string $shipToPhone
     */
    protected $shipToPhone = null;

    /**
     * @var ArrayOfShipmentInfo $shipmentList
     */
    protected $shipmentList = null;

    /**
     * @var AddressDTO $shippingAddress
     */
    protected $shippingAddress = null;

    /**
     * @var string $shippingMethodCode
     */
    protected $shippingMethodCode = null;

    /**
     * @var string $warehouseCode
     */
    protected $warehouseCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getHubsoftOrderNumber()
    {
      return $this->hubsoftOrderNumber;
    }

    /**
     * @param string $hubsoftOrderNumber
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setHubsoftOrderNumber($hubsoftOrderNumber)
    {
      $this->hubsoftOrderNumber = $hubsoftOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
      return $this->notes;
    }

    /**
     * @param string $notes
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setNotes($notes)
    {
      $this->notes = $notes;
      return $this;
    }

    /**
     * @return string
     */
    public function getOptInEmail()
    {
      return $this->optInEmail;
    }

    /**
     * @param string $optInEmail
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setOptInEmail($optInEmail)
    {
      $this->optInEmail = $optInEmail;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderStatusCode()
    {
      return $this->orderStatusCode;
    }

    /**
     * @param string $orderStatusCode
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setOrderStatusCode($orderStatusCode)
    {
      $this->orderStatusCode = $orderStatusCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRemoteOrderNumber()
    {
      return $this->remoteOrderNumber;
    }

    /**
     * @param string $remoteOrderNumber
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setRemoteOrderNumber($remoteOrderNumber)
    {
      $this->remoteOrderNumber = $remoteOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesTaxPerc()
    {
      return $this->salesTaxPerc;
    }

    /**
     * @param string $salesTaxPerc
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setSalesTaxPerc($salesTaxPerc)
    {
      $this->salesTaxPerc = $salesTaxPerc;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipDate()
    {
      return $this->shipDate;
    }

    /**
     * @param string $shipDate
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setShipDate($shipDate)
    {
      $this->shipDate = $shipDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToAttention()
    {
      return $this->shipToAttention;
    }

    /**
     * @param string $shipToAttention
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setShipToAttention($shipToAttention)
    {
      $this->shipToAttention = $shipToAttention;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToEmail()
    {
      return $this->shipToEmail;
    }

    /**
     * @param string $shipToEmail
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setShipToEmail($shipToEmail)
    {
      $this->shipToEmail = $shipToEmail;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToPhone()
    {
      return $this->shipToPhone;
    }

    /**
     * @param string $shipToPhone
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setShipToPhone($shipToPhone)
    {
      $this->shipToPhone = $shipToPhone;
      return $this;
    }

    /**
     * @return ArrayOfShipmentInfo
     */
    public function getShipmentList()
    {
      return $this->shipmentList;
    }

    /**
     * @param ArrayOfShipmentInfo $shipmentList
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setShipmentList($shipmentList)
    {
      $this->shipmentList = $shipmentList;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getShippingAddress()
    {
      return $this->shippingAddress;
    }

    /**
     * @param AddressDTO $shippingAddress
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setShippingAddress($shippingAddress)
    {
      $this->shippingAddress = $shippingAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingMethodCode()
    {
      return $this->shippingMethodCode;
    }

    /**
     * @param string $shippingMethodCode
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setShippingMethodCode($shippingMethodCode)
    {
      $this->shippingMethodCode = $shippingMethodCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getWarehouseCode()
    {
      return $this->warehouseCode;
    }

    /**
     * @param string $warehouseCode
     * @return \hubsoft\api\integrationservice\DirectOrderUpdateDTO
     */
    public function setWarehouseCode($warehouseCode)
    {
      $this->warehouseCode = $warehouseCode;
      return $this;
    }

}
