<?php

namespace mattanger\hubsoft\integrationservice;

class EntryCustom
{

    /**
     * @var Error $error
     */
    protected $error = null;

    /**
     * @var int $index
     */
    protected $index = null;

    /**
     * @var anyType $result
     */
    protected $result = null;

    /**
     * @var boolean $success
     */
    protected $success = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Error
     */
    public function getError()
    {
      return $this->error;
    }

    /**
     * @param Error $error
     * @return \mattanger\hubsoft\integrationservice\Entry
     */
    public function setError($error)
    {
      $this->error = $error;
      return $this;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
      return $this->index;
    }

    /**
     * @param int $index
     * @return \mattanger\hubsoft\integrationservice\Entry
     */
    public function setIndex($index)
    {
      $this->index = $index;
      return $this;
    }

    /**
     * @return anyType
     */
    public function getResult()
    {
      return $this->result;
    }

    /**
     * @param anyType $result
     * @return \mattanger\hubsoft\integrationservice\Entry
     */
    public function setResult($result)
    {
      $this->result = $result;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSuccess()
    {
      return $this->success;
    }

    /**
     * @param boolean $success
     * @return \mattanger\hubsoft\integrationservice\Entry
     */
    public function setSuccess($success)
    {
      $this->success = $success;
      return $this;
    }

}
