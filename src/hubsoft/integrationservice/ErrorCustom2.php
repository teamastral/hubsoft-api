<?php

namespace mattanger\hubsoft\integrationservice;

class ErrorCustom2
{

    /**
     * @var string $code
     */
    protected $code = null;

    /**
     * @var ArrayOfKeyObject $keyObjects
     */
    protected $keyObjects = null;

    /**
     * @var string $message
     */
    protected $message = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->code;
    }

    /**
     * @param string $code
     * @return \mattanger\hubsoft\integrationservice\Error
     */
    public function setCode($code)
    {
      $this->code = $code;
      return $this;
    }

    /**
     * @return ArrayOfKeyObject
     */
    public function getKeyObjects()
    {
      return $this->keyObjects;
    }

    /**
     * @param ArrayOfKeyObject $keyObjects
     * @return \mattanger\hubsoft\integrationservice\Error
     */
    public function setKeyObjects($keyObjects)
    {
      $this->keyObjects = $keyObjects;
      return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
      return $this->message;
    }

    /**
     * @param string $message
     * @return \mattanger\hubsoft\integrationservice\Error
     */
    public function setMessage($message)
    {
      $this->message = $message;
      return $this;
    }

}
