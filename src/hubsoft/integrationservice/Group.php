<?php

namespace hubsoft\api\integrationservice;

class Group
{

    /**
     * @var ArrayOfProduct $productList
     */
    protected $productList = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfProduct
     */
    public function getProductList()
    {
      return $this->productList;
    }

    /**
     * @param ArrayOfProduct $productList
     * @return \hubsoft\api\integrationservice\Group
     */
    public function setProductList($productList)
    {
      $this->productList = $productList;
      return $this;
    }

}
