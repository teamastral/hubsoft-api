<?php

namespace mattanger\hubsoft\integrationservice;

class GroupCustom
{

    /**
     * @var ArrayOfProduct $productList
     */
    protected $productList = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfProduct
     */
    public function getProductList()
    {
      return $this->productList;
    }

    /**
     * @param ArrayOfProduct $productList
     * @return \mattanger\hubsoft\integrationservice\Group
     */
    public function setProductList($productList)
    {
      $this->productList = $productList;
      return $this;
    }

}
