<?php

namespace mattanger\hubsoft\integrationservice;

class ImportResponseCustom
{

    /**
     * @var ArrayOfEntry $detail
     */
    protected $detail = null;

    /**
     * @var int $numError
     */
    protected $numError = null;

    /**
     * @var int $numRecords
     */
    protected $numRecords = null;

    /**
     * @var int $numSuccess
     */
    protected $numSuccess = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfEntry
     */
    public function getDetail()
    {
      return $this->detail;
    }

    /**
     * @param ArrayOfEntry $detail
     * @return \mattanger\hubsoft\integrationservice\ImportResponse
     */
    public function setDetail($detail)
    {
      $this->detail = $detail;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumError()
    {
      return $this->numError;
    }

    /**
     * @param int $numError
     * @return \mattanger\hubsoft\integrationservice\ImportResponse
     */
    public function setNumError($numError)
    {
      $this->numError = $numError;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumRecords()
    {
      return $this->numRecords;
    }

    /**
     * @param int $numRecords
     * @return \mattanger\hubsoft\integrationservice\ImportResponse
     */
    public function setNumRecords($numRecords)
    {
      $this->numRecords = $numRecords;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumSuccess()
    {
      return $this->numSuccess;
    }

    /**
     * @param int $numSuccess
     * @return \mattanger\hubsoft\integrationservice\ImportResponse
     */
    public function setNumSuccess($numSuccess)
    {
      $this->numSuccess = $numSuccess;
      return $this;
    }

}
