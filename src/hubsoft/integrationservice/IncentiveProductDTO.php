<?php

namespace hubsoft\api\integrationservice;

class IncentiveProductDTO
{

    /**
     * @var string $discountPct
     */
    protected $discountPct = null;

    /**
     * @var string $incentiveCode
     */
    protected $incentiveCode = null;

    /**
     * @var string $repCommission
     */
    protected $repCommission = null;

    /**
     * @var string $sku
     */
    protected $sku = null;

    /**
     * @var string $specifiedPrice
     */
    protected $specifiedPrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDiscountPct()
    {
      return $this->discountPct;
    }

    /**
     * @param string $discountPct
     * @return \hubsoft\api\integrationservice\IncentiveProductDTO
     */
    public function setDiscountPct($discountPct)
    {
      $this->discountPct = $discountPct;
      return $this;
    }

    /**
     * @return string
     */
    public function getIncentiveCode()
    {
      return $this->incentiveCode;
    }

    /**
     * @param string $incentiveCode
     * @return \hubsoft\api\integrationservice\IncentiveProductDTO
     */
    public function setIncentiveCode($incentiveCode)
    {
      $this->incentiveCode = $incentiveCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCommission()
    {
      return $this->repCommission;
    }

    /**
     * @param string $repCommission
     * @return \hubsoft\api\integrationservice\IncentiveProductDTO
     */
    public function setRepCommission($repCommission)
    {
      $this->repCommission = $repCommission;
      return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
      return $this->sku;
    }

    /**
     * @param string $sku
     * @return \hubsoft\api\integrationservice\IncentiveProductDTO
     */
    public function setSku($sku)
    {
      $this->sku = $sku;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecifiedPrice()
    {
      return $this->specifiedPrice;
    }

    /**
     * @param string $specifiedPrice
     * @return \hubsoft\api\integrationservice\IncentiveProductDTO
     */
    public function setSpecifiedPrice($specifiedPrice)
    {
      $this->specifiedPrice = $specifiedPrice;
      return $this;
    }

}
