<?php

namespace hubsoft\api\integrationservice;

class IntegrationServiceV3 extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'ArrayOfProductDescDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfProductDescDTO',
      'ProductDescDTO' => 'hubsoft\\api\\integrationservice\\ProductDescDTO',
      'ArrayOfAccountDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfAccountDTO',
      'AccountDTO' => 'hubsoft\\api\\integrationservice\\AccountDTO',
      'ArrayOfBuyerDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfBuyerDTO',
      'BuyerDTO' => 'hubsoft\\api\\integrationservice\\BuyerDTO',
      'ContactDTO' => 'hubsoft\\api\\integrationservice\\ContactDTO',
      'ArrayOfAddtlAddressDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfAddtlAddressDTO',
      'AddtlAddressDTO' => 'hubsoft\\api\\integrationservice\\AddtlAddressDTO',
      'ArrayOfStoreDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfStoreDTO',
      'StoreDTO' => 'hubsoft\\api\\integrationservice\\StoreDTO',
      'AddressDTO' => 'hubsoft\\api\\integrationservice\\AddressDTO',
      'ArrayOfDealerOrderDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfDealerOrderDTO',
      'DealerOrderDTO' => 'hubsoft\\api\\integrationservice\\DealerOrderDTO',
      'ArrayOfOrderStoreDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfOrderStoreDTO',
      'OrderStoreDTO' => 'hubsoft\\api\\integrationservice\\OrderStoreDTO',
      'ArrayOfShipmentInfo' => 'hubsoft\\api\\integrationservice\\ArrayOfShipmentInfo',
      'ShipmentInfo' => 'hubsoft\\api\\integrationservice\\ShipmentInfo',
      'ArrayOfOrderItemDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfOrderItemDTO',
      'OrderItemDTO' => 'hubsoft\\api\\integrationservice\\OrderItemDTO',
      'ArrayOfProductDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfProductDTO',
      'ProductDTO' => 'hubsoft\\api\\integrationservice\\ProductDTO',
      'ArrayOfProductColorDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfProductColorDTO',
      'ProductColorDTO' => 'hubsoft\\api\\integrationservice\\ProductColorDTO',
      'ArrayOfSizeDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfSizeDTO',
      'SizeDTO' => 'hubsoft\\api\\integrationservice\\SizeDTO',
      'ArrayOfSkuSeasonDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfSkuSeasonDTO',
      'SkuSeasonDTO' => 'hubsoft\\api\\integrationservice\\SkuSeasonDTO',
      'ArrayOfDealerOrderDeletedDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfDealerOrderDeletedDTO',
      'DealerOrderDeletedDTO' => 'hubsoft\\api\\integrationservice\\DealerOrderDeletedDTO',
      'ArrayOfAccountProspectDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfAccountProspectDTO',
      'AccountProspectDTO' => 'hubsoft\\api\\integrationservice\\AccountProspectDTO',
      'AccountLoginDTO' => 'hubsoft\\api\\integrationservice\\AccountLoginDTO',
      'ArrayOfShippingNoticeDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfShippingNoticeDTO',
      'ShippingNoticeDTO' => 'hubsoft\\api\\integrationservice\\ShippingNoticeDTO',
      'ArrayOfIncentiveProductDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfIncentiveProductDTO',
      'IncentiveProductDTO' => 'hubsoft\\api\\integrationservice\\IncentiveProductDTO',
      'ArrayOfInventoryDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfInventoryDTO',
      'InventoryDTO' => 'hubsoft\\api\\integrationservice\\InventoryDTO',
      'ArrayOfSalesIncentiveDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfSalesIncentiveDTO',
      'SalesIncentiveDTO' => 'hubsoft\\api\\integrationservice\\SalesIncentiveDTO',
      'ArrayOfSalesIncentiveProductDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfSalesIncentiveProductDTO',
      'SalesIncentiveProductDTO' => 'hubsoft\\api\\integrationservice\\SalesIncentiveProductDTO',
      'ArrayOfDirectOrderUpdateDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfDirectOrderUpdateDTO',
      'DirectOrderUpdateDTO' => 'hubsoft\\api\\integrationservice\\DirectOrderUpdateDTO',
      'ArrayOfOrderStatusDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfOrderStatusDTO',
      'OrderStatusDTO' => 'hubsoft\\api\\integrationservice\\OrderStatusDTO',
      'PriceBookDTO' => 'hubsoft\\api\\integrationservice\\PriceBookDTO',
      'ArrayOfPriceBookProductDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfPriceBookProductDTO',
      'PriceBookProductDTO' => 'hubsoft\\api\\integrationservice\\PriceBookProductDTO',
      'ArrayOfRepDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfRepDTO',
      'RepDTO' => 'hubsoft\\api\\integrationservice\\RepDTO',
      'ArrayOfRepAccountDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfRepAccountDTO',
      'RepAccountDTO' => 'hubsoft\\api\\integrationservice\\RepAccountDTO',
      'ArrayOfRepBuyerDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfRepBuyerDTO',
      'RepBuyerDTO' => 'hubsoft\\api\\integrationservice\\RepBuyerDTO',
      'ArrayOfRepBuyerStoreDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfRepBuyerStoreDTO',
      'RepBuyerStoreDTO' => 'hubsoft\\api\\integrationservice\\RepBuyerStoreDTO',
      'ArrayOfSizeChartDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfSizeChartDTO',
      'SizeChartDTO' => 'hubsoft\\api\\integrationservice\\SizeChartDTO',
      'ArrayOfSizeChartSizeDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfSizeChartSizeDTO',
      'SizeChartSizeDTO' => 'hubsoft\\api\\integrationservice\\SizeChartSizeDTO',
      'ArrayOfBulkOrderDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfBulkOrderDTO',
      'BulkOrderDTO' => 'hubsoft\\api\\integrationservice\\BulkOrderDTO',
      'ArrayOfBulkItemDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfBulkItemDTO',
      'BulkItemDTO' => 'hubsoft\\api\\integrationservice\\BulkItemDTO',
      'ArrayOfDirectOrderUpdate2DTO' => 'hubsoft\\api\\integrationservice\\ArrayOfDirectOrderUpdate2DTO',
      'DirectOrderUpdate2DTO' => 'hubsoft\\api\\integrationservice\\DirectOrderUpdate2DTO',
      'ArrayOfInvoice' => 'hubsoft\\api\\integrationservice\\ArrayOfInvoice',
      'Invoice' => 'hubsoft\\api\\integrationservice\\Invoice',
      'ArrayOfShipment' => 'hubsoft\\api\\integrationservice\\ArrayOfShipment',
      'Shipment' => 'hubsoft\\api\\integrationservice\\Shipment',
      'ArrayOfOrderItem' => 'hubsoft\\api\\integrationservice\\ArrayOfOrderItem',
      'OrderItem' => 'hubsoft\\api\\integrationservice\\OrderItem',
      'ArrayOfSalesTerritoryDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfSalesTerritoryDTO',
      'SalesTerritoryDTO' => 'hubsoft\\api\\integrationservice\\SalesTerritoryDTO',
      'ArrayOfAccountStoreDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfAccountStoreDTO',
      'AccountStoreDTO' => 'hubsoft\\api\\integrationservice\\AccountStoreDTO',
      'ArrayOfSourceDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfSourceDTO',
      'SourceDTO' => 'hubsoft\\api\\integrationservice\\SourceDTO',
      'ArrayOfAttrDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfAttrDTO',
      'AttrDTO' => 'hubsoft\\api\\integrationservice\\AttrDTO',
      'string2stringMap' => 'hubsoft\\api\\integrationservice\\string2stringMap',
      'entry' => 'hubsoft\\api\\integrationservice\\entry',
      'importProductDescription' => 'hubsoft\\api\\integrationservice\\importProductDescription',
      'ImportResponse' => 'hubsoft\\api\\integrationservice\\ImportResponse',
      'ArrayOfEntry' => 'hubsoft\\api\\integrationservice\\ArrayOfEntry',
      'Entry' => 'hubsoft\\api\\integrationservice\\Entry',
      'Error' => 'hubsoft\\api\\integrationservice\\Error',
      'ArrayOfKeyObject' => 'hubsoft\\api\\integrationservice\\ArrayOfKeyObject',
      'KeyObject' => 'hubsoft\\api\\integrationservice\\KeyObject',
      'importProductDescriptionResponse' => 'hubsoft\\api\\integrationservice\\importProductDescriptionResponse',
      'Detail' => 'hubsoft\\api\\integrationservice\\Detail',
      'ArrayOfString' => 'hubsoft\\api\\integrationservice\\ArrayOfString',
      'importAccount' => 'hubsoft\\api\\integrationservice\\importAccount',
      'importAccountResponse' => 'hubsoft\\api\\integrationservice\\importAccountResponse',
      'exportAccount' => 'hubsoft\\api\\integrationservice\\exportAccount',
      'exportAccountResponse' => 'hubsoft\\api\\integrationservice\\exportAccountResponse',
      'exportBulkOrders' => 'hubsoft\\api\\integrationservice\\exportBulkOrders',
      'exportBulkOrdersResponse' => 'hubsoft\\api\\integrationservice\\exportBulkOrdersResponse',
      'importDealerOrders' => 'hubsoft\\api\\integrationservice\\importDealerOrders',
      'importDealerOrdersResponse' => 'hubsoft\\api\\integrationservice\\importDealerOrdersResponse',
      'importProducts' => 'hubsoft\\api\\integrationservice\\importProducts',
      'importProductsResponse' => 'hubsoft\\api\\integrationservice\\importProductsResponse',
      'importSeasonAssignment' => 'hubsoft\\api\\integrationservice\\importSeasonAssignment',
      'importSeasonAssignmentResponse' => 'hubsoft\\api\\integrationservice\\importSeasonAssignmentResponse',
      'removeDealerOrders' => 'hubsoft\\api\\integrationservice\\removeDealerOrders',
      'removeDealerOrdersResponse' => 'hubsoft\\api\\integrationservice\\removeDealerOrdersResponse',
      'importAccountProspect' => 'hubsoft\\api\\integrationservice\\importAccountProspect',
      'importAccountProspectResponse' => 'hubsoft\\api\\integrationservice\\importAccountProspectResponse',
      'importShippingUpdate' => 'hubsoft\\api\\integrationservice\\importShippingUpdate',
      'importShippingUpdateResponse' => 'hubsoft\\api\\integrationservice\\importShippingUpdateResponse',
      'exportProducts' => 'hubsoft\\api\\integrationservice\\exportProducts',
      'exportProductsResponse' => 'hubsoft\\api\\integrationservice\\exportProductsResponse',
      'importIncentiveProducts' => 'hubsoft\\api\\integrationservice\\importIncentiveProducts',
      'importIncentiveProductsResponse' => 'hubsoft\\api\\integrationservice\\importIncentiveProductsResponse',
      'importInventory' => 'hubsoft\\api\\integrationservice\\importInventory',
      'importInventoryResponse' => 'hubsoft\\api\\integrationservice\\importInventoryResponse',
      'importSalesIncentives' => 'hubsoft\\api\\integrationservice\\importSalesIncentives',
      'importSalesIncentivesResponse' => 'hubsoft\\api\\integrationservice\\importSalesIncentivesResponse',
      'importDirectOrders' => 'hubsoft\\api\\integrationservice\\importDirectOrders',
      'importDirectOrdersResponse' => 'hubsoft\\api\\integrationservice\\importDirectOrdersResponse',
      'importOrderStatus' => 'hubsoft\\api\\integrationservice\\importOrderStatus',
      'importOrderStatusResponse' => 'hubsoft\\api\\integrationservice\\importOrderStatusResponse',
      'importPriceBooks' => 'hubsoft\\api\\integrationservice\\importPriceBooks',
      'importPriceBooksResponse' => 'hubsoft\\api\\integrationservice\\importPriceBooksResponse',
      'importBuyer' => 'hubsoft\\api\\integrationservice\\importBuyer',
      'importBuyerResponse' => 'hubsoft\\api\\integrationservice\\importBuyerResponse',
      'importSizeChart' => 'hubsoft\\api\\integrationservice\\importSizeChart',
      'importSizeChartResponse' => 'hubsoft\\api\\integrationservice\\importSizeChartResponse',
      'exportDealerOrders' => 'hubsoft\\api\\integrationservice\\exportDealerOrders',
      'ArrayOfCaseItemDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfCaseItemDTO',
      'CaseItemDTO' => 'hubsoft\\api\\integrationservice\\CaseItemDTO',
      'MustShipTogether' => 'hubsoft\\api\\integrationservice\\MustShipTogether',
      'ArrayOfGroup' => 'hubsoft\\api\\integrationservice\\ArrayOfGroup',
      'Group' => 'hubsoft\\api\\integrationservice\\Group',
      'ArrayOfProduct' => 'hubsoft\\api\\integrationservice\\ArrayOfProduct',
      'Product' => 'hubsoft\\api\\integrationservice\\Product',
      'exportDealerOrdersResponse' => 'hubsoft\\api\\integrationservice\\exportDealerOrdersResponse',
      'importBulkOrders' => 'hubsoft\\api\\integrationservice\\importBulkOrders',
      'importBulkOrdersResponse' => 'hubsoft\\api\\integrationservice\\importBulkOrdersResponse',
      'importDirectOrders2' => 'hubsoft\\api\\integrationservice\\importDirectOrders2',
      'importDirectOrders2Response' => 'hubsoft\\api\\integrationservice\\importDirectOrders2Response',
      'exportDirectOrders' => 'hubsoft\\api\\integrationservice\\exportDirectOrders',
      'ArrayOfDirectOrderDTO' => 'hubsoft\\api\\integrationservice\\ArrayOfDirectOrderDTO',
      'DirectOrderDTO' => 'hubsoft\\api\\integrationservice\\DirectOrderDTO',
      'exportDirectOrdersResponse' => 'hubsoft\\api\\integrationservice\\exportDirectOrdersResponse',
      'importSalesTerritory' => 'hubsoft\\api\\integrationservice\\importSalesTerritory',
      'importSalesTerritoryResponse' => 'hubsoft\\api\\integrationservice\\importSalesTerritoryResponse',
      'exportInventory' => 'hubsoft\\api\\integrationservice\\exportInventory',
      'exportInventoryResponse' => 'hubsoft\\api\\integrationservice\\exportInventoryResponse',
      'importStore' => 'hubsoft\\api\\integrationservice\\importStore',
      'importStoreResponse' => 'hubsoft\\api\\integrationservice\\importStoreResponse',
      'importSource' => 'hubsoft\\api\\integrationservice\\importSource',
      'importSourceResponse' => 'hubsoft\\api\\integrationservice\\importSourceResponse',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'https://ws.hubsoft.com/services/IntegrationServiceV3?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param importProductDescription $parameters
     * @return importProductDescriptionResponse
     */
    public function importProductDescription(importProductDescription $parameters)
    {
      return $this->__soapCall('importProductDescription', array($parameters));
    }

    /**
     * @param importAccount $parameters
     * @return importAccountResponse
     */
    public function importAccount(importAccount $parameters)
    {
      return $this->__soapCall('importAccount', array($parameters));
    }

    /**
     * @param exportAccount $parameters
     * @return exportAccountResponse
     */
    public function exportAccount(exportAccount $parameters)
    {
      return $this->__soapCall('exportAccount', array($parameters));
    }

    /**
     * @param exportBulkOrders $parameters
     * @return exportBulkOrdersResponse
     */
    public function exportBulkOrders(exportBulkOrders $parameters)
    {
      return $this->__soapCall('exportBulkOrders', array($parameters));
    }

    /**
     * @param importDealerOrders $parameters
     * @return importDealerOrdersResponse
     */
    public function importDealerOrders(importDealerOrders $parameters)
    {
      return $this->__soapCall('importDealerOrders', array($parameters));
    }

    /**
     * @param importProducts $parameters
     * @return importProductsResponse
     */
    public function importProducts(importProducts $parameters)
    {
      return $this->__soapCall('importProducts', array($parameters));
    }

    /**
     * @param importSeasonAssignment $parameters
     * @return importSeasonAssignmentResponse
     */
    public function importSeasonAssignment(importSeasonAssignment $parameters)
    {
      return $this->__soapCall('importSeasonAssignment', array($parameters));
    }

    /**
     * @param removeDealerOrders $parameters
     * @return removeDealerOrdersResponse
     */
    public function removeDealerOrders(removeDealerOrders $parameters)
    {
      return $this->__soapCall('removeDealerOrders', array($parameters));
    }

    /**
     * @param importAccountProspect $parameters
     * @return importAccountProspectResponse
     */
    public function importAccountProspect(importAccountProspect $parameters)
    {
      return $this->__soapCall('importAccountProspect', array($parameters));
    }

    /**
     * @param importShippingUpdate $parameters
     * @return importShippingUpdateResponse
     */
    public function importShippingUpdate(importShippingUpdate $parameters)
    {
      return $this->__soapCall('importShippingUpdate', array($parameters));
    }

    /**
     * @param exportProducts $parameters
     * @return exportProductsResponse
     */
    public function exportProducts(exportProducts $parameters)
    {
      return $this->__soapCall('exportProducts', array($parameters));
    }

    /**
     * @param importIncentiveProducts $parameters
     * @return importIncentiveProductsResponse
     */
    public function importIncentiveProducts(importIncentiveProducts $parameters)
    {
      return $this->__soapCall('importIncentiveProducts', array($parameters));
    }

    /**
     * @param importInventory $parameters
     * @return importInventoryResponse
     */
    public function importInventory(importInventory $parameters)
    {
      return $this->__soapCall('importInventory', array($parameters));
    }

    /**
     * @param importSalesIncentives $parameters
     * @return importSalesIncentivesResponse
     */
    public function importSalesIncentives(importSalesIncentives $parameters)
    {
      return $this->__soapCall('importSalesIncentives', array($parameters));
    }

    /**
     * @param importDirectOrders $parameters
     * @return importDirectOrdersResponse
     */
    public function importDirectOrders(importDirectOrders $parameters)
    {
      return $this->__soapCall('importDirectOrders', array($parameters));
    }

    /**
     * @param importOrderStatus $parameters
     * @return importOrderStatusResponse
     */
    public function importOrderStatus(importOrderStatus $parameters)
    {
      return $this->__soapCall('importOrderStatus', array($parameters));
    }

    /**
     * @param importPriceBooks $parameters
     * @return importPriceBooksResponse
     */
    public function importPriceBooks(importPriceBooks $parameters)
    {
      return $this->__soapCall('importPriceBooks', array($parameters));
    }

    /**
     * @param importBuyer $parameters
     * @return importBuyerResponse
     */
    public function importBuyer(importBuyer $parameters)
    {
      return $this->__soapCall('importBuyer', array($parameters));
    }

    /**
     * @param importSizeChart $parameters
     * @return importSizeChartResponse
     */
    public function importSizeChart(importSizeChart $parameters)
    {
      return $this->__soapCall('importSizeChart', array($parameters));
    }

    /**
     * @param exportDealerOrders $parameters
     * @return exportDealerOrdersResponse
     */
    public function exportDealerOrders(exportDealerOrders $parameters)
    {
      return $this->__soapCall('exportDealerOrders', array($parameters));
    }

    /**
     * @param importBulkOrders $parameters
     * @return importBulkOrdersResponse
     */
    public function importBulkOrders(importBulkOrders $parameters)
    {
      return $this->__soapCall('importBulkOrders', array($parameters));
    }

    /**
     * @param importDirectOrders2 $parameters
     * @return importDirectOrders2Response
     */
    public function importDirectOrders2(importDirectOrders2 $parameters)
    {
      return $this->__soapCall('importDirectOrders2', array($parameters));
    }

    /**
     * @param exportDirectOrders $parameters
     * @return exportDirectOrdersResponse
     */
    public function exportDirectOrders(exportDirectOrders $parameters)
    {
      return $this->__soapCall('exportDirectOrders', array($parameters));
    }

    /**
     * @param importSalesTerritory $parameters
     * @return importSalesTerritoryResponse
     */
    public function importSalesTerritory(importSalesTerritory $parameters)
    {
      return $this->__soapCall('importSalesTerritory', array($parameters));
    }

    /**
     * @param exportInventory $parameters
     * @return exportInventoryResponse
     */
    public function exportInventory(exportInventory $parameters)
    {
      return $this->__soapCall('exportInventory', array($parameters));
    }

    /**
     * @param importStore $parameters
     * @return importStoreResponse
     */
    public function importStore(importStore $parameters)
    {
      return $this->__soapCall('importStore', array($parameters));
    }

    /**
     * @param importSource $parameters
     * @return importSourceResponse
     */
    public function importSource(importSource $parameters)
    {
      return $this->__soapCall('importSource', array($parameters));
    }

}
