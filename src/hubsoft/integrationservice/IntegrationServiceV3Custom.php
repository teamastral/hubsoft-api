<?php

namespace mattanger\hubsoft\integrationservice;

class IntegrationServiceV3Custom extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'ArrayOfProductDescDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfProductDescDTOCustom',
      'ProductDescDTO' => 'mattanger\\hubsoft\\integrationservice\\ProductDescDTOCustom',
      'ArrayOfAccountDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfAccountDTOCustom',
      'AccountDTO' => 'mattanger\\hubsoft\\integrationservice\\AccountDTOCustom',
      'ArrayOfStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfStoreDTOCustom',
      'StoreDTO' => 'mattanger\\hubsoft\\integrationservice\\StoreDTOCustom',
      'AddressDTO' => 'mattanger\\hubsoft\\integrationservice\\AddressDTOCustom',
      'ContactDTO' => 'mattanger\\hubsoft\\integrationservice\\ContactDTOCustom',
      'ArrayOfAddtlAddressDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfAddtlAddressDTOCustom',
      'AddtlAddressDTO' => 'mattanger\\hubsoft\\integrationservice\\AddtlAddressDTOCustom',
      'ArrayOfBuyerDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfBuyerDTOCustom',
      'BuyerDTO' => 'mattanger\\hubsoft\\integrationservice\\BuyerDTOCustom',
      'ArrayOfDealerOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfDealerOrderDTOCustom',
      'DealerOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\DealerOrderDTOCustom',
      'ArrayOfOrderStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfOrderStoreDTOCustom',
      'OrderStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\OrderStoreDTOCustom',
      'ArrayOfShipmentInfo' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfShipmentInfoCustom',
      'ShipmentInfo' => 'mattanger\\hubsoft\\integrationservice\\ShipmentInfoCustom',
      'ArrayOfOrderItemDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfOrderItemDTOCustom',
      'OrderItemDTO' => 'mattanger\\hubsoft\\integrationservice\\OrderItemDTOCustom',
      'ArrayOfProductDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfProductDTOCustom',
      'ProductDTO' => 'mattanger\\hubsoft\\integrationservice\\ProductDTOCustom',
      'ArrayOfProductColorDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfProductColorDTOCustom',
      'ProductColorDTO' => 'mattanger\\hubsoft\\integrationservice\\ProductColorDTOCustom',
      'ArrayOfSizeDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSizeDTOCustom',
      'SizeDTO' => 'mattanger\\hubsoft\\integrationservice\\SizeDTOCustom',
      'ArrayOfSkuSeasonDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSkuSeasonDTOCustom',
      'SkuSeasonDTO' => 'mattanger\\hubsoft\\integrationservice\\SkuSeasonDTOCustom',
      'ArrayOfDealerOrderDeletedDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfDealerOrderDeletedDTOCustom',
      'DealerOrderDeletedDTO' => 'mattanger\\hubsoft\\integrationservice\\DealerOrderDeletedDTOCustom',
      'ArrayOfAccountProspectDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfAccountProspectDTOCustom',
      'AccountProspectDTO' => 'mattanger\\hubsoft\\integrationservice\\AccountProspectDTOCustom',
      'AccountLoginDTO' => 'mattanger\\hubsoft\\integrationservice\\AccountLoginDTOCustom',
      'ArrayOfShippingNoticeDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfShippingNoticeDTOCustom',
      'ShippingNoticeDTO' => 'mattanger\\hubsoft\\integrationservice\\ShippingNoticeDTOCustom',
      'ArrayOfIncentiveProductDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfIncentiveProductDTOCustom',
      'IncentiveProductDTO' => 'mattanger\\hubsoft\\integrationservice\\IncentiveProductDTOCustom',
      'ArrayOfInventoryDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfInventoryDTOCustom',
      'InventoryDTO' => 'mattanger\\hubsoft\\integrationservice\\InventoryDTOCustom',
      'ArrayOfSalesIncentiveDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSalesIncentiveDTOCustom',
      'SalesIncentiveDTO' => 'mattanger\\hubsoft\\integrationservice\\SalesIncentiveDTOCustom',
      'ArrayOfSalesIncentiveProductDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSalesIncentiveProductDTOCustom',
      'SalesIncentiveProductDTO' => 'mattanger\\hubsoft\\integrationservice\\SalesIncentiveProductDTOCustom',
      'ArrayOfDirectOrderUpdateDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfDirectOrderUpdateDTOCustom',
      'DirectOrderUpdateDTO' => 'mattanger\\hubsoft\\integrationservice\\DirectOrderUpdateDTOCustom',
      'ArrayOfOrderStatusDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfOrderStatusDTOCustom',
      'OrderStatusDTO' => 'mattanger\\hubsoft\\integrationservice\\OrderStatusDTOCustom',
      'PriceBookDTO' => 'mattanger\\hubsoft\\integrationservice\\PriceBookDTOCustom',
      'ArrayOfPriceBookProductDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfPriceBookProductDTOCustom',
      'PriceBookProductDTO' => 'mattanger\\hubsoft\\integrationservice\\PriceBookProductDTOCustom',
      'ArrayOfRepDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfRepDTOCustom',
      'RepDTO' => 'mattanger\\hubsoft\\integrationservice\\RepDTOCustom',
      'ArrayOfRepAccountDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfRepAccountDTOCustom',
      'RepAccountDTO' => 'mattanger\\hubsoft\\integrationservice\\RepAccountDTOCustom',
      'ArrayOfRepBuyerDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfRepBuyerDTOCustom',
      'RepBuyerDTO' => 'mattanger\\hubsoft\\integrationservice\\RepBuyerDTOCustom',
      'ArrayOfRepBuyerStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfRepBuyerStoreDTOCustom',
      'RepBuyerStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\RepBuyerStoreDTOCustom',
      'ArrayOfSizeChartDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSizeChartDTOCustom',
      'SizeChartDTO' => 'mattanger\\hubsoft\\integrationservice\\SizeChartDTOCustom',
      'ArrayOfSizeChartSizeDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSizeChartSizeDTOCustom',
      'SizeChartSizeDTO' => 'mattanger\\hubsoft\\integrationservice\\SizeChartSizeDTOCustom',
      'ArrayOfBulkOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfBulkOrderDTOCustom',
      'BulkOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\BulkOrderDTOCustom',
      'ArrayOfBulkItemDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfBulkItemDTOCustom',
      'BulkItemDTO' => 'mattanger\\hubsoft\\integrationservice\\BulkItemDTOCustom',
      'ArrayOfDirectOrderUpdate2DTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfDirectOrderUpdate2DTOCustom',
      'DirectOrderUpdate2DTO' => 'mattanger\\hubsoft\\integrationservice\\DirectOrderUpdate2DTOCustom',
      'ArrayOfInvoice' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfInvoiceCustom',
      'Invoice' => 'mattanger\\hubsoft\\integrationservice\\InvoiceCustom',
      'ArrayOfShipment' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfShipmentCustom',
      'Shipment' => 'mattanger\\hubsoft\\integrationservice\\ShipmentCustom',
      'ArrayOfOrderItem' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfOrderItemCustom',
      'OrderItem' => 'mattanger\\hubsoft\\integrationservice\\OrderItemCustom',
      'ArrayOfSalesTerritoryDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSalesTerritoryDTOCustom',
      'SalesTerritoryDTO' => 'mattanger\\hubsoft\\integrationservice\\SalesTerritoryDTOCustom',
      'ArrayOfAccountStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfAccountStoreDTOCustom',
      'AccountStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\AccountStoreDTOCustom',
      'ArrayOfSourceDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSourceDTOCustom',
      'SourceDTO' => 'mattanger\\hubsoft\\integrationservice\\SourceDTOCustom',
      'ArrayOfAttrDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfAttrDTOCustom',
      'AttrDTO' => 'mattanger\\hubsoft\\integrationservice\\AttrDTOCustom',
      'string2stringMap' => 'mattanger\\hubsoft\\integrationservice\\string2stringMapCustom',
      'entry' => 'mattanger\\hubsoft\\integrationservice\\entryCustom',
      'importProductDescription' => 'mattanger\\hubsoft\\integrationservice\\importProductDescriptionCustom',
      'ImportResponse' => 'mattanger\\hubsoft\\integrationservice\\ImportResponseCustom',
      'ArrayOfEntry' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfEntryCustom',
      'Entry' => 'mattanger\\hubsoft\\integrationservice\\EntryCustom',
      'Error' => 'mattanger\\hubsoft\\integrationservice\\ErrorCustom',
      'ArrayOfKeyObject' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfKeyObjectCustom',
      'KeyObject' => 'mattanger\\hubsoft\\integrationservice\\KeyObjectCustom',
      'importProductDescriptionResponse' => 'mattanger\\hubsoft\\integrationservice\\importProductDescriptionResponseCustom',
      'Detail' => 'mattanger\\hubsoft\\integrationservice\\DetailCustom',
      'ArrayOfString' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfStringCustom',
      'importAccount' => 'mattanger\\hubsoft\\integrationservice\\importAccountCustom',
      'importAccountResponse' => 'mattanger\\hubsoft\\integrationservice\\importAccountResponseCustom',
      'exportAccount' => 'mattanger\\hubsoft\\integrationservice\\exportAccountCustom',
      'exportAccountResponse' => 'mattanger\\hubsoft\\integrationservice\\exportAccountResponseCustom',
      'exportBulkOrders' => 'mattanger\\hubsoft\\integrationservice\\exportBulkOrdersCustom',
      'exportBulkOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\exportBulkOrdersResponseCustom',
      'importDealerOrders' => 'mattanger\\hubsoft\\integrationservice\\importDealerOrdersCustom',
      'importDealerOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\importDealerOrdersResponseCustom',
      'importProducts' => 'mattanger\\hubsoft\\integrationservice\\importProductsCustom',
      'importProductsResponse' => 'mattanger\\hubsoft\\integrationservice\\importProductsResponseCustom',
      'importSeasonAssignment' => 'mattanger\\hubsoft\\integrationservice\\importSeasonAssignmentCustom',
      'importSeasonAssignmentResponse' => 'mattanger\\hubsoft\\integrationservice\\importSeasonAssignmentResponseCustom',
      'removeDealerOrders' => 'mattanger\\hubsoft\\integrationservice\\removeDealerOrdersCustom',
      'removeDealerOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\removeDealerOrdersResponseCustom',
      'importAccountProspect' => 'mattanger\\hubsoft\\integrationservice\\importAccountProspectCustom',
      'importAccountProspectResponse' => 'mattanger\\hubsoft\\integrationservice\\importAccountProspectResponseCustom',
      'importShippingUpdate' => 'mattanger\\hubsoft\\integrationservice\\importShippingUpdateCustom',
      'importShippingUpdateResponse' => 'mattanger\\hubsoft\\integrationservice\\importShippingUpdateResponseCustom',
      'exportProducts' => 'mattanger\\hubsoft\\integrationservice\\exportProductsCustom',
      'exportProductsResponse' => 'mattanger\\hubsoft\\integrationservice\\exportProductsResponseCustom',
      'importIncentiveProducts' => 'mattanger\\hubsoft\\integrationservice\\importIncentiveProductsCustom',
      'importIncentiveProductsResponse' => 'mattanger\\hubsoft\\integrationservice\\importIncentiveProductsResponseCustom',
      'importInventory' => 'mattanger\\hubsoft\\integrationservice\\importInventoryCustom',
      'importInventoryResponse' => 'mattanger\\hubsoft\\integrationservice\\importInventoryResponseCustom',
      'importSalesIncentives' => 'mattanger\\hubsoft\\integrationservice\\importSalesIncentivesCustom',
      'importSalesIncentivesResponse' => 'mattanger\\hubsoft\\integrationservice\\importSalesIncentivesResponseCustom',
      'importDirectOrders' => 'mattanger\\hubsoft\\integrationservice\\importDirectOrdersCustom',
      'importDirectOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\importDirectOrdersResponseCustom',
      'importOrderStatus' => 'mattanger\\hubsoft\\integrationservice\\importOrderStatusCustom',
      'importOrderStatusResponse' => 'mattanger\\hubsoft\\integrationservice\\importOrderStatusResponseCustom',
      'importPriceBooks' => 'mattanger\\hubsoft\\integrationservice\\importPriceBooksCustom',
      'importPriceBooksResponse' => 'mattanger\\hubsoft\\integrationservice\\importPriceBooksResponseCustom',
      'importBuyer' => 'mattanger\\hubsoft\\integrationservice\\importBuyerCustom',
      'importBuyerResponse' => 'mattanger\\hubsoft\\integrationservice\\importBuyerResponseCustom',
      'importSizeChart' => 'mattanger\\hubsoft\\integrationservice\\importSizeChartCustom',
      'importSizeChartResponse' => 'mattanger\\hubsoft\\integrationservice\\importSizeChartResponseCustom',
      'exportDealerOrders' => 'mattanger\\hubsoft\\integrationservice\\exportDealerOrdersCustom',
      'ArrayOfCaseItemDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfCaseItemDTOCustom',
      'CaseItemDTO' => 'mattanger\\hubsoft\\integrationservice\\CaseItemDTOCustom',
      'MustShipTogether' => 'mattanger\\hubsoft\\integrationservice\\MustShipTogetherCustom',
      'ArrayOfGroup' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfGroupCustom',
      'Group' => 'mattanger\\hubsoft\\integrationservice\\GroupCustom',
      'ArrayOfProduct' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfProductCustom',
      'Product' => 'mattanger\\hubsoft\\integrationservice\\ProductCustom',
      'exportDealerOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\exportDealerOrdersResponseCustom',
      'importBulkOrders' => 'mattanger\\hubsoft\\integrationservice\\importBulkOrdersCustom',
      'importBulkOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\importBulkOrdersResponseCustom',
      'importDirectOrders2' => 'mattanger\\hubsoft\\integrationservice\\importDirectOrders2Custom',
      'importDirectOrders2Response' => 'mattanger\\hubsoft\\integrationservice\\importDirectOrders2ResponseCustom',
      'exportDirectOrders' => 'mattanger\\hubsoft\\integrationservice\\exportDirectOrdersCustom',
      'ArrayOfDirectOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfDirectOrderDTOCustom',
      'DirectOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\DirectOrderDTOCustom',
      'exportDirectOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\exportDirectOrdersResponseCustom',
      'importSalesTerritory' => 'mattanger\\hubsoft\\integrationservice\\importSalesTerritoryCustom',
      'importSalesTerritoryResponse' => 'mattanger\\hubsoft\\integrationservice\\importSalesTerritoryResponseCustom',
      'exportInventory' => 'mattanger\\hubsoft\\integrationservice\\exportInventoryCustom',
      'exportInventoryResponse' => 'mattanger\\hubsoft\\integrationservice\\exportInventoryResponseCustom',
      'importStore' => 'mattanger\\hubsoft\\integrationservice\\importStoreCustom',
      'importStoreResponse' => 'mattanger\\hubsoft\\integrationservice\\importStoreResponseCustom',
      'importSource' => 'mattanger\\hubsoft\\integrationservice\\importSourceCustom',
      'importSourceResponse' => 'mattanger\\hubsoft\\integrationservice\\importSourceResponseCustom',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'https://uat.hubsoft.com/services/IntegrationServiceV3?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param importProductDescriptionCustom $parameters
     * @return importProductDescriptionResponse
     */
    public function importProductDescription($parameters)
    {
      return $this->__soapCall('importProductDescription', array($parameters));
    }

    /**
     * @param importAccountCustom $parameters
     * @return importAccountResponse
     */
    public function importAccount($parameters)
    {
      return $this->__soapCall('importAccount', array($parameters));
    }

    /**
     * @param exportAccountCustom $parameters
     * @return exportAccountResponse
     */
    public function exportAccount($parameters)
    {
      return $this->__soapCall('exportAccount', array($parameters));
    }

    /**
     * @param exportBulkOrdersCustom $parameters
     * @return exportBulkOrdersResponse
     */
    public function exportBulkOrders($parameters)
    {
      return $this->__soapCall('exportBulkOrders', array($parameters));
    }

    /**
     * @param importDealerOrdersCustom $parameters
     * @return importDealerOrdersResponse
     */
    public function importDealerOrders($parameters)
    {
      return $this->__soapCall('importDealerOrders', array($parameters));
    }

    /**
     * @param importProductsCustom $parameters
     * @return importProductsResponse
     */
    public function importProducts($parameters)
    {
      return $this->__soapCall('importProducts', array($parameters));
    }

    /**
     * @param importSeasonAssignmentCustom $parameters
     * @return importSeasonAssignmentResponse
     */
    public function importSeasonAssignment($parameters)
    {
      return $this->__soapCall('importSeasonAssignment', array($parameters));
    }

    /**
     * @param removeDealerOrdersCustom $parameters
     * @return removeDealerOrdersResponse
     */
    public function removeDealerOrders($parameters)
    {
      return $this->__soapCall('removeDealerOrders', array($parameters));
    }

    /**
     * @param importAccountProspectCustom $parameters
     * @return importAccountProspectResponse
     */
    public function importAccountProspect($parameters)
    {
      return $this->__soapCall('importAccountProspect', array($parameters));
    }

    /**
     * @param importShippingUpdateCustom $parameters
     * @return importShippingUpdateResponse
     */
    public function importShippingUpdate($parameters)
    {
      return $this->__soapCall('importShippingUpdate', array($parameters));
    }

    /**
     * @param exportProductsCustom $parameters
     * @return exportProductsResponse
     */
    public function exportProducts($parameters)
    {
      return $this->__soapCall('exportProducts', array($parameters));
    }

    /**
     * @param importIncentiveProductsCustom $parameters
     * @return importIncentiveProductsResponse
     */
    public function importIncentiveProducts($parameters)
    {
      return $this->__soapCall('importIncentiveProducts', array($parameters));
    }

    /**
     * @param importInventoryCustom $parameters
     * @return importInventoryResponse
     */
    public function importInventory($parameters)
    {
      return $this->__soapCall('importInventory', array($parameters));
    }

    /**
     * @param importSalesIncentivesCustom $parameters
     * @return importSalesIncentivesResponse
     */
    public function importSalesIncentives($parameters)
    {
      return $this->__soapCall('importSalesIncentives', array($parameters));
    }

    /**
     * @param importDirectOrdersCustom $parameters
     * @return importDirectOrdersResponse
     */
    public function importDirectOrders($parameters)
    {
      return $this->__soapCall('importDirectOrders', array($parameters));
    }

    /**
     * @param importOrderStatusCustom $parameters
     * @return importOrderStatusResponse
     */
    public function importOrderStatus($parameters)
    {
      return $this->__soapCall('importOrderStatus', array($parameters));
    }

    /**
     * @param importPriceBooksCustom $parameters
     * @return importPriceBooksResponse
     */
    public function importPriceBooks($parameters)
    {
      return $this->__soapCall('importPriceBooks', array($parameters));
    }

    /**
     * @param importBuyerCustom $parameters
     * @return importBuyerResponse
     */
    public function importBuyer($parameters)
    {
      return $this->__soapCall('importBuyer', array($parameters));
    }

    /**
     * @param importSizeChartCustom $parameters
     * @return importSizeChartResponse
     */
    public function importSizeChart($parameters)
    {
      return $this->__soapCall('importSizeChart', array($parameters));
    }

    /**
     * @param exportDealerOrdersCustom $parameters
     * @return exportDealerOrdersResponse
     */
    public function exportDealerOrders($parameters)
    {
      return $this->__soapCall('exportDealerOrders', array($parameters));
    }

    /**
     * @param importBulkOrdersCustom $parameters
     * @return importBulkOrdersResponse
     */
    public function importBulkOrders($parameters)
    {
      return $this->__soapCall('importBulkOrders', array($parameters));
    }

    /**
     * @param importDirectOrders2Custom $parameters
     * @return importDirectOrders2Response
     */
    public function importDirectOrders2($parameters)
    {
      return $this->__soapCall('importDirectOrders2', array($parameters));
    }

    /**
     * @param exportDirectOrdersCustom $parameters
     * @return exportDirectOrdersResponse
     */
    public function exportDirectOrders($parameters)
    {
      return $this->__soapCall('exportDirectOrders', array($parameters));
    }

    /**
     * @param importSalesTerritoryCustom $parameters
     * @return importSalesTerritoryResponse
     */
    public function importSalesTerritory($parameters)
    {
      return $this->__soapCall('importSalesTerritory', array($parameters));
    }

    /**
     * @param exportInventoryCustom $parameters
     * @return exportInventoryResponse
     */
    public function exportInventory($parameters)
    {
      return $this->__soapCall('exportInventory', array($parameters));
    }

    /**
     * @param importStoreCustom $parameters
     * @return importStoreResponse
     */
    public function importStore($parameters)
    {
      return $this->__soapCall('importStore', array($parameters));
    }

    /**
     * @param importSourceCustom $parameters
     * @return importSourceResponse
     */
    public function importSource($parameters)
    {
      return $this->__soapCall('importSource', array($parameters));
    }

}
