<?php

namespace mattanger\hubsoft\integrationservice;

class IntegrationServiceV3Custom2 extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'ArrayOfProductDescDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfProductDescDTOCustom2',
      'ProductDescDTO' => 'mattanger\\hubsoft\\integrationservice\\ProductDescDTOCustom2',
      'ArrayOfAccountDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfAccountDTOCustom2',
      'AccountDTO' => 'mattanger\\hubsoft\\integrationservice\\AccountDTOCustom2',
      'AddressDTO' => 'mattanger\\hubsoft\\integrationservice\\AddressDTOCustom2',
      'ContactDTO' => 'mattanger\\hubsoft\\integrationservice\\ContactDTOCustom2',
      'ArrayOfBuyerDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfBuyerDTOCustom2',
      'BuyerDTO' => 'mattanger\\hubsoft\\integrationservice\\BuyerDTOCustom2',
      'ArrayOfStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfStoreDTOCustom2',
      'StoreDTO' => 'mattanger\\hubsoft\\integrationservice\\StoreDTOCustom2',
      'ArrayOfAddtlAddressDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfAddtlAddressDTOCustom2',
      'AddtlAddressDTO' => 'mattanger\\hubsoft\\integrationservice\\AddtlAddressDTOCustom2',
      'ArrayOfDealerOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfDealerOrderDTOCustom2',
      'DealerOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\DealerOrderDTOCustom2',
      'ArrayOfOrderStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfOrderStoreDTOCustom2',
      'OrderStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\OrderStoreDTOCustom2',
      'ArrayOfOrderItemDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfOrderItemDTOCustom2',
      'OrderItemDTO' => 'mattanger\\hubsoft\\integrationservice\\OrderItemDTOCustom2',
      'ArrayOfShipmentInfo' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfShipmentInfoCustom2',
      'ShipmentInfo' => 'mattanger\\hubsoft\\integrationservice\\ShipmentInfoCustom2',
      'ArrayOfProductDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfProductDTOCustom2',
      'ProductDTO' => 'mattanger\\hubsoft\\integrationservice\\ProductDTOCustom2',
      'ArrayOfProductColorDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfProductColorDTOCustom2',
      'ProductColorDTO' => 'mattanger\\hubsoft\\integrationservice\\ProductColorDTOCustom2',
      'ArrayOfSizeDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSizeDTOCustom2',
      'SizeDTO' => 'mattanger\\hubsoft\\integrationservice\\SizeDTOCustom2',
      'ArrayOfSkuSeasonDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSkuSeasonDTOCustom2',
      'SkuSeasonDTO' => 'mattanger\\hubsoft\\integrationservice\\SkuSeasonDTOCustom2',
      'ArrayOfDealerOrderDeletedDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfDealerOrderDeletedDTOCustom2',
      'DealerOrderDeletedDTO' => 'mattanger\\hubsoft\\integrationservice\\DealerOrderDeletedDTOCustom2',
      'ArrayOfAccountProspectDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfAccountProspectDTOCustom2',
      'AccountProspectDTO' => 'mattanger\\hubsoft\\integrationservice\\AccountProspectDTOCustom2',
      'AccountLoginDTO' => 'mattanger\\hubsoft\\integrationservice\\AccountLoginDTOCustom2',
      'ArrayOfShippingNoticeDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfShippingNoticeDTOCustom2',
      'ShippingNoticeDTO' => 'mattanger\\hubsoft\\integrationservice\\ShippingNoticeDTOCustom2',
      'ArrayOfIncentiveProductDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfIncentiveProductDTOCustom2',
      'IncentiveProductDTO' => 'mattanger\\hubsoft\\integrationservice\\IncentiveProductDTOCustom2',
      'ArrayOfInventoryDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfInventoryDTOCustom2',
      'InventoryDTO' => 'mattanger\\hubsoft\\integrationservice\\InventoryDTOCustom2',
      'ArrayOfSalesIncentiveDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSalesIncentiveDTOCustom2',
      'SalesIncentiveDTO' => 'mattanger\\hubsoft\\integrationservice\\SalesIncentiveDTOCustom2',
      'ArrayOfSalesIncentiveProductDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSalesIncentiveProductDTOCustom2',
      'SalesIncentiveProductDTO' => 'mattanger\\hubsoft\\integrationservice\\SalesIncentiveProductDTOCustom2',
      'ArrayOfDirectOrderUpdateDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfDirectOrderUpdateDTOCustom2',
      'DirectOrderUpdateDTO' => 'mattanger\\hubsoft\\integrationservice\\DirectOrderUpdateDTOCustom2',
      'ArrayOfOrderStatusDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfOrderStatusDTOCustom2',
      'OrderStatusDTO' => 'mattanger\\hubsoft\\integrationservice\\OrderStatusDTOCustom2',
      'PriceBookDTO' => 'mattanger\\hubsoft\\integrationservice\\PriceBookDTOCustom2',
      'ArrayOfPriceBookProductDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfPriceBookProductDTOCustom2',
      'PriceBookProductDTO' => 'mattanger\\hubsoft\\integrationservice\\PriceBookProductDTOCustom2',
      'ArrayOfRepDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfRepDTOCustom2',
      'RepDTO' => 'mattanger\\hubsoft\\integrationservice\\RepDTOCustom2',
      'ArrayOfRepAccountDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfRepAccountDTOCustom2',
      'RepAccountDTO' => 'mattanger\\hubsoft\\integrationservice\\RepAccountDTOCustom2',
      'ArrayOfRepBuyerDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfRepBuyerDTOCustom2',
      'RepBuyerDTO' => 'mattanger\\hubsoft\\integrationservice\\RepBuyerDTOCustom2',
      'ArrayOfRepBuyerStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfRepBuyerStoreDTOCustom2',
      'RepBuyerStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\RepBuyerStoreDTOCustom2',
      'ArrayOfSizeChartDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSizeChartDTOCustom2',
      'SizeChartDTO' => 'mattanger\\hubsoft\\integrationservice\\SizeChartDTOCustom2',
      'ArrayOfSizeChartSizeDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSizeChartSizeDTOCustom2',
      'SizeChartSizeDTO' => 'mattanger\\hubsoft\\integrationservice\\SizeChartSizeDTOCustom2',
      'ArrayOfBulkOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfBulkOrderDTOCustom2',
      'BulkOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\BulkOrderDTOCustom2',
      'ArrayOfBulkItemDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfBulkItemDTOCustom2',
      'BulkItemDTO' => 'mattanger\\hubsoft\\integrationservice\\BulkItemDTOCustom2',
      'ArrayOfDirectOrderUpdate2DTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfDirectOrderUpdate2DTOCustom2',
      'DirectOrderUpdate2DTO' => 'mattanger\\hubsoft\\integrationservice\\DirectOrderUpdate2DTOCustom2',
      'ArrayOfInvoice' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfInvoiceCustom2',
      'Invoice' => 'mattanger\\hubsoft\\integrationservice\\InvoiceCustom2',
      'ArrayOfOrderItem' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfOrderItemCustom2',
      'OrderItem' => 'mattanger\\hubsoft\\integrationservice\\OrderItemCustom2',
      'ArrayOfShipment' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfShipmentCustom2',
      'Shipment' => 'mattanger\\hubsoft\\integrationservice\\ShipmentCustom2',
      'ArrayOfSalesTerritoryDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSalesTerritoryDTOCustom2',
      'SalesTerritoryDTO' => 'mattanger\\hubsoft\\integrationservice\\SalesTerritoryDTOCustom2',
      'ArrayOfAccountStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfAccountStoreDTOCustom2',
      'AccountStoreDTO' => 'mattanger\\hubsoft\\integrationservice\\AccountStoreDTOCustom2',
      'ArrayOfSourceDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfSourceDTOCustom2',
      'SourceDTO' => 'mattanger\\hubsoft\\integrationservice\\SourceDTOCustom2',
      'ArrayOfAttrDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfAttrDTOCustom2',
      'AttrDTO' => 'mattanger\\hubsoft\\integrationservice\\AttrDTOCustom2',
      'string2stringMap' => 'mattanger\\hubsoft\\integrationservice\\string2stringMapCustom2',
      'entry' => 'mattanger\\hubsoft\\integrationservice\\entryCustom2',
      'importProductDescription' => 'mattanger\\hubsoft\\integrationservice\\importProductDescriptionCustom2',
      'ImportResponse' => 'mattanger\\hubsoft\\integrationservice\\ImportResponseCustom2',
      'ArrayOfEntry' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfEntryCustom2',
      'Entry' => 'mattanger\\hubsoft\\integrationservice\\EntryCustom2',
      'Error' => 'mattanger\\hubsoft\\integrationservice\\ErrorCustom2',
      'ArrayOfKeyObject' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfKeyObjectCustom2',
      'KeyObject' => 'mattanger\\hubsoft\\integrationservice\\KeyObjectCustom2',
      'importProductDescriptionResponse' => 'mattanger\\hubsoft\\integrationservice\\importProductDescriptionResponseCustom2',
      'Detail' => 'mattanger\\hubsoft\\integrationservice\\DetailCustom2',
      'ArrayOfString' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfStringCustom2',
      'importAccount' => 'mattanger\\hubsoft\\integrationservice\\importAccountCustom2',
      'importAccountResponse' => 'mattanger\\hubsoft\\integrationservice\\importAccountResponseCustom2',
      'exportAccount' => 'mattanger\\hubsoft\\integrationservice\\exportAccountCustom2',
      'exportAccountResponse' => 'mattanger\\hubsoft\\integrationservice\\exportAccountResponseCustom2',
      'exportBulkOrders' => 'mattanger\\hubsoft\\integrationservice\\exportBulkOrdersCustom2',
      'exportBulkOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\exportBulkOrdersResponseCustom2',
      'importDealerOrders' => 'mattanger\\hubsoft\\integrationservice\\importDealerOrdersCustom2',
      'importDealerOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\importDealerOrdersResponseCustom2',
      'importProducts' => 'mattanger\\hubsoft\\integrationservice\\importProductsCustom2',
      'importProductsResponse' => 'mattanger\\hubsoft\\integrationservice\\importProductsResponseCustom2',
      'importSeasonAssignment' => 'mattanger\\hubsoft\\integrationservice\\importSeasonAssignmentCustom2',
      'importSeasonAssignmentResponse' => 'mattanger\\hubsoft\\integrationservice\\importSeasonAssignmentResponseCustom2',
      'removeDealerOrders' => 'mattanger\\hubsoft\\integrationservice\\removeDealerOrdersCustom2',
      'removeDealerOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\removeDealerOrdersResponseCustom2',
      'importAccountProspect' => 'mattanger\\hubsoft\\integrationservice\\importAccountProspectCustom2',
      'importAccountProspectResponse' => 'mattanger\\hubsoft\\integrationservice\\importAccountProspectResponseCustom2',
      'importShippingUpdate' => 'mattanger\\hubsoft\\integrationservice\\importShippingUpdateCustom2',
      'importShippingUpdateResponse' => 'mattanger\\hubsoft\\integrationservice\\importShippingUpdateResponseCustom2',
      'exportProducts' => 'mattanger\\hubsoft\\integrationservice\\exportProductsCustom2',
      'exportProductsResponse' => 'mattanger\\hubsoft\\integrationservice\\exportProductsResponseCustom2',
      'importIncentiveProducts' => 'mattanger\\hubsoft\\integrationservice\\importIncentiveProductsCustom2',
      'importIncentiveProductsResponse' => 'mattanger\\hubsoft\\integrationservice\\importIncentiveProductsResponseCustom2',
      'importInventory' => 'mattanger\\hubsoft\\integrationservice\\importInventoryCustom2',
      'importInventoryResponse' => 'mattanger\\hubsoft\\integrationservice\\importInventoryResponseCustom2',
      'importSalesIncentives' => 'mattanger\\hubsoft\\integrationservice\\importSalesIncentivesCustom2',
      'importSalesIncentivesResponse' => 'mattanger\\hubsoft\\integrationservice\\importSalesIncentivesResponseCustom2',
      'importDirectOrders' => 'mattanger\\hubsoft\\integrationservice\\importDirectOrdersCustom2',
      'importDirectOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\importDirectOrdersResponseCustom2',
      'importOrderStatus' => 'mattanger\\hubsoft\\integrationservice\\importOrderStatusCustom2',
      'importOrderStatusResponse' => 'mattanger\\hubsoft\\integrationservice\\importOrderStatusResponseCustom2',
      'importPriceBooks' => 'mattanger\\hubsoft\\integrationservice\\importPriceBooksCustom2',
      'importPriceBooksResponse' => 'mattanger\\hubsoft\\integrationservice\\importPriceBooksResponseCustom2',
      'importBuyer' => 'mattanger\\hubsoft\\integrationservice\\importBuyerCustom2',
      'importBuyerResponse' => 'mattanger\\hubsoft\\integrationservice\\importBuyerResponseCustom2',
      'importSizeChart' => 'mattanger\\hubsoft\\integrationservice\\importSizeChartCustom2',
      'importSizeChartResponse' => 'mattanger\\hubsoft\\integrationservice\\importSizeChartResponseCustom2',
      'exportDealerOrders' => 'mattanger\\hubsoft\\integrationservice\\exportDealerOrdersCustom2',
      'MustShipTogether' => 'mattanger\\hubsoft\\integrationservice\\MustShipTogetherCustom2',
      'ArrayOfGroup' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfGroupCustom2',
      'Group' => 'mattanger\\hubsoft\\integrationservice\\GroupCustom2',
      'ArrayOfProduct' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfProductCustom2',
      'Product' => 'mattanger\\hubsoft\\integrationservice\\ProductCustom2',
      'ArrayOfCaseItemDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfCaseItemDTOCustom2',
      'CaseItemDTO' => 'mattanger\\hubsoft\\integrationservice\\CaseItemDTOCustom2',
      'exportDealerOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\exportDealerOrdersResponseCustom2',
      'importBulkOrders' => 'mattanger\\hubsoft\\integrationservice\\importBulkOrdersCustom2',
      'importBulkOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\importBulkOrdersResponseCustom2',
      'importDirectOrders2' => 'mattanger\\hubsoft\\integrationservice\\importDirectOrders2Custom2',
      'importDirectOrders2Response' => 'mattanger\\hubsoft\\integrationservice\\importDirectOrders2ResponseCustom2',
      'exportDirectOrders' => 'mattanger\\hubsoft\\integrationservice\\exportDirectOrdersCustom2',
      'ArrayOfDirectOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\ArrayOfDirectOrderDTOCustom2',
      'DirectOrderDTO' => 'mattanger\\hubsoft\\integrationservice\\DirectOrderDTOCustom2',
      'exportDirectOrdersResponse' => 'mattanger\\hubsoft\\integrationservice\\exportDirectOrdersResponseCustom2',
      'importSalesTerritory' => 'mattanger\\hubsoft\\integrationservice\\importSalesTerritoryCustom2',
      'importSalesTerritoryResponse' => 'mattanger\\hubsoft\\integrationservice\\importSalesTerritoryResponseCustom2',
      'exportInventory' => 'mattanger\\hubsoft\\integrationservice\\exportInventoryCustom2',
      'exportInventoryResponse' => 'mattanger\\hubsoft\\integrationservice\\exportInventoryResponseCustom2',
      'importStore' => 'mattanger\\hubsoft\\integrationservice\\importStoreCustom2',
      'importStoreResponse' => 'mattanger\\hubsoft\\integrationservice\\importStoreResponseCustom2',
      'importSource' => 'mattanger\\hubsoft\\integrationservice\\importSourceCustom2',
      'importSourceResponse' => 'mattanger\\hubsoft\\integrationservice\\importSourceResponseCustom2',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'https://ws.hubsoft.com/services/IntegrationServiceV3?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param importProductDescriptionCustom2 $parameters
     * @return importProductDescriptionResponse
     */
    public function importProductDescription($parameters)
    {
      return $this->__soapCall('importProductDescription', array($parameters));
    }

    /**
     * @param importAccountCustom2 $parameters
     * @return importAccountResponse
     */
    public function importAccount($parameters)
    {
      return $this->__soapCall('importAccount', array($parameters));
    }

    /**
     * @param exportAccountCustom2 $parameters
     * @return exportAccountResponse
     */
    public function exportAccount($parameters)
    {
      return $this->__soapCall('exportAccount', array($parameters));
    }

    /**
     * @param exportBulkOrdersCustom2 $parameters
     * @return exportBulkOrdersResponse
     */
    public function exportBulkOrders($parameters)
    {
      return $this->__soapCall('exportBulkOrders', array($parameters));
    }

    /**
     * @param importDealerOrdersCustom2 $parameters
     * @return importDealerOrdersResponse
     */
    public function importDealerOrders($parameters)
    {
      return $this->__soapCall('importDealerOrders', array($parameters));
    }

    /**
     * @param importProductsCustom2 $parameters
     * @return importProductsResponse
     */
    public function importProducts($parameters)
    {
      return $this->__soapCall('importProducts', array($parameters));
    }

    /**
     * @param importSeasonAssignmentCustom2 $parameters
     * @return importSeasonAssignmentResponse
     */
    public function importSeasonAssignment($parameters)
    {
      return $this->__soapCall('importSeasonAssignment', array($parameters));
    }

    /**
     * @param removeDealerOrdersCustom2 $parameters
     * @return removeDealerOrdersResponse
     */
    public function removeDealerOrders($parameters)
    {
      return $this->__soapCall('removeDealerOrders', array($parameters));
    }

    /**
     * @param importAccountProspectCustom2 $parameters
     * @return importAccountProspectResponse
     */
    public function importAccountProspect($parameters)
    {
      return $this->__soapCall('importAccountProspect', array($parameters));
    }

    /**
     * @param importShippingUpdateCustom2 $parameters
     * @return importShippingUpdateResponse
     */
    public function importShippingUpdate($parameters)
    {
      return $this->__soapCall('importShippingUpdate', array($parameters));
    }

    /**
     * @param exportProductsCustom2 $parameters
     * @return exportProductsResponse
     */
    public function exportProducts($parameters)
    {
      return $this->__soapCall('exportProducts', array($parameters));
    }

    /**
     * @param importIncentiveProductsCustom2 $parameters
     * @return importIncentiveProductsResponse
     */
    public function importIncentiveProducts($parameters)
    {
      return $this->__soapCall('importIncentiveProducts', array($parameters));
    }

    /**
     * @param importInventoryCustom2 $parameters
     * @return importInventoryResponse
     */
    public function importInventory($parameters)
    {
      return $this->__soapCall('importInventory', array($parameters));
    }

    /**
     * @param importSalesIncentivesCustom2 $parameters
     * @return importSalesIncentivesResponse
     */
    public function importSalesIncentives($parameters)
    {
      return $this->__soapCall('importSalesIncentives', array($parameters));
    }

    /**
     * @param importDirectOrdersCustom2 $parameters
     * @return importDirectOrdersResponse
     */
    public function importDirectOrders($parameters)
    {
      return $this->__soapCall('importDirectOrders', array($parameters));
    }

    /**
     * @param importOrderStatusCustom2 $parameters
     * @return importOrderStatusResponse
     */
    public function importOrderStatus($parameters)
    {
      return $this->__soapCall('importOrderStatus', array($parameters));
    }

    /**
     * @param importPriceBooksCustom2 $parameters
     * @return importPriceBooksResponse
     */
    public function importPriceBooks($parameters)
    {
      return $this->__soapCall('importPriceBooks', array($parameters));
    }

    /**
     * @param importBuyerCustom2 $parameters
     * @return importBuyerResponse
     */
    public function importBuyer($parameters)
    {
      return $this->__soapCall('importBuyer', array($parameters));
    }

    /**
     * @param importSizeChartCustom2 $parameters
     * @return importSizeChartResponse
     */
    public function importSizeChart($parameters)
    {
      return $this->__soapCall('importSizeChart', array($parameters));
    }

    /**
     * @param exportDealerOrdersCustom2 $parameters
     * @return exportDealerOrdersResponse
     */
    public function exportDealerOrders($parameters)
    {
      return $this->__soapCall('exportDealerOrders', array($parameters));
    }

    /**
     * @param importBulkOrdersCustom2 $parameters
     * @return importBulkOrdersResponse
     */
    public function importBulkOrders($parameters)
    {
      return $this->__soapCall('importBulkOrders', array($parameters));
    }

    /**
     * @param importDirectOrders2Custom2 $parameters
     * @return importDirectOrders2Response
     */
    public function importDirectOrders2($parameters)
    {
      return $this->__soapCall('importDirectOrders2', array($parameters));
    }

    /**
     * @param exportDirectOrdersCustom2 $parameters
     * @return exportDirectOrdersResponse
     */
    public function exportDirectOrders($parameters)
    {
      return $this->__soapCall('exportDirectOrders', array($parameters));
    }

    /**
     * @param importSalesTerritoryCustom2 $parameters
     * @return importSalesTerritoryResponse
     */
    public function importSalesTerritory($parameters)
    {
      return $this->__soapCall('importSalesTerritory', array($parameters));
    }

    /**
     * @param exportInventoryCustom2 $parameters
     * @return exportInventoryResponse
     */
    public function exportInventory($parameters)
    {
      return $this->__soapCall('exportInventory', array($parameters));
    }

    /**
     * @param importStoreCustom2 $parameters
     * @return importStoreResponse
     */
    public function importStore($parameters)
    {
      return $this->__soapCall('importStore', array($parameters));
    }

    /**
     * @param importSourceCustom2 $parameters
     * @return importSourceResponse
     */
    public function importSource($parameters)
    {
      return $this->__soapCall('importSource', array($parameters));
    }

}
