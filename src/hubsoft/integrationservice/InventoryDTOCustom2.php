<?php

namespace mattanger\hubsoft\integrationservice;

class InventoryDTOCustom2
{

    /**
     * @var string $colorCode
     */
    protected $colorCode = null;

    /**
     * @var string $colorName
     */
    protected $colorName = null;

    /**
     * @var string $productName
     */
    protected $productName = null;

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    /**
     * @var string $productReference
     */
    protected $productReference = null;

    /**
     * @var int $quantity
     */
    protected $quantity = null;

    /**
     * @var string $sizeName
     */
    protected $sizeName = null;

    /**
     * @var string $sku
     */
    protected $sku = null;

    /**
     * @var string $upc
     */
    protected $upc = null;

    /**
     * @var string $warehouseCode
     */
    protected $warehouseCode = null;

    /**
     * @var string $wipDate
     */
    protected $wipDate = null;

    /**
     * @param string $colorCode
     * @param string $colorName
     * @param string $productName
     * @param string $productReference
     */
    public function __construct($colorCode, $colorName, $productName, $productReference)
    {
      $this->colorCode = $colorCode;
      $this->colorName = $colorName;
      $this->productName = $productName;
      $this->productReference = $productReference;
    }

    /**
     * @return string
     */
    public function getColorCode()
    {
      return $this->colorCode;
    }

    /**
     * @param string $colorCode
     * @return \mattanger\hubsoft\integrationservice\InventoryDTO
     */
    public function setColorCode($colorCode)
    {
      $this->colorCode = $colorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getColorName()
    {
      return $this->colorName;
    }

    /**
     * @param string $colorName
     * @return \mattanger\hubsoft\integrationservice\InventoryDTO
     */
    public function setColorName($colorName)
    {
      $this->colorName = $colorName;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
      return $this->productName;
    }

    /**
     * @param string $productName
     * @return \mattanger\hubsoft\integrationservice\InventoryDTO
     */
    public function setProductName($productName)
    {
      $this->productName = $productName;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \mattanger\hubsoft\integrationservice\InventoryDTO
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductReference()
    {
      return $this->productReference;
    }

    /**
     * @param string $productReference
     * @return \mattanger\hubsoft\integrationservice\InventoryDTO
     */
    public function setProductReference($productReference)
    {
      $this->productReference = $productReference;
      return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
      return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return \mattanger\hubsoft\integrationservice\InventoryDTO
     */
    public function setQuantity($quantity)
    {
      $this->quantity = $quantity;
      return $this;
    }

    /**
     * @return string
     */
    public function getSizeName()
    {
      return $this->sizeName;
    }

    /**
     * @param string $sizeName
     * @return \mattanger\hubsoft\integrationservice\InventoryDTO
     */
    public function setSizeName($sizeName)
    {
      $this->sizeName = $sizeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
      return $this->sku;
    }

    /**
     * @param string $sku
     * @return \mattanger\hubsoft\integrationservice\InventoryDTO
     */
    public function setSku($sku)
    {
      $this->sku = $sku;
      return $this;
    }

    /**
     * @return string
     */
    public function getUpc()
    {
      return $this->upc;
    }

    /**
     * @param string $upc
     * @return \mattanger\hubsoft\integrationservice\InventoryDTO
     */
    public function setUpc($upc)
    {
      $this->upc = $upc;
      return $this;
    }

    /**
     * @return string
     */
    public function getWarehouseCode()
    {
      return $this->warehouseCode;
    }

    /**
     * @param string $warehouseCode
     * @return \mattanger\hubsoft\integrationservice\InventoryDTO
     */
    public function setWarehouseCode($warehouseCode)
    {
      $this->warehouseCode = $warehouseCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getWipDate()
    {
      return $this->wipDate;
    }

    /**
     * @param string $wipDate
     * @return \mattanger\hubsoft\integrationservice\InventoryDTO
     */
    public function setWipDate($wipDate)
    {
      $this->wipDate = $wipDate;
      return $this;
    }

}
