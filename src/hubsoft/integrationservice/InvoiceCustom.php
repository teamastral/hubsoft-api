<?php

namespace mattanger\hubsoft\integrationservice;

class InvoiceCustom
{

    /**
     * @var string $invoiceDate
     */
    protected $invoiceDate = null;

    /**
     * @var string $invoiceNumber
     */
    protected $invoiceNumber = null;

    /**
     * @var string $invoiceReason
     */
    protected $invoiceReason = null;

    /**
     * @var string $invoiceStatus
     */
    protected $invoiceStatus = null;

    /**
     * @var string $invoiceType
     */
    protected $invoiceType = null;

    /**
     * @var ArrayOfOrderItem $orderItemList
     */
    protected $orderItemList = null;

    /**
     * @var ArrayOfShipment $shipmentList
     */
    protected $shipmentList = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getInvoiceDate()
    {
      return $this->invoiceDate;
    }

    /**
     * @param string $invoiceDate
     * @return \mattanger\hubsoft\integrationservice\Invoice
     */
    public function setInvoiceDate($invoiceDate)
    {
      $this->invoiceDate = $invoiceDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNumber()
    {
      return $this->invoiceNumber;
    }

    /**
     * @param string $invoiceNumber
     * @return \mattanger\hubsoft\integrationservice\Invoice
     */
    public function setInvoiceNumber($invoiceNumber)
    {
      $this->invoiceNumber = $invoiceNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceReason()
    {
      return $this->invoiceReason;
    }

    /**
     * @param string $invoiceReason
     * @return \mattanger\hubsoft\integrationservice\Invoice
     */
    public function setInvoiceReason($invoiceReason)
    {
      $this->invoiceReason = $invoiceReason;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceStatus()
    {
      return $this->invoiceStatus;
    }

    /**
     * @param string $invoiceStatus
     * @return \mattanger\hubsoft\integrationservice\Invoice
     */
    public function setInvoiceStatus($invoiceStatus)
    {
      $this->invoiceStatus = $invoiceStatus;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceType()
    {
      return $this->invoiceType;
    }

    /**
     * @param string $invoiceType
     * @return \mattanger\hubsoft\integrationservice\Invoice
     */
    public function setInvoiceType($invoiceType)
    {
      $this->invoiceType = $invoiceType;
      return $this;
    }

    /**
     * @return ArrayOfOrderItem
     */
    public function getOrderItemList()
    {
      return $this->orderItemList;
    }

    /**
     * @param ArrayOfOrderItem $orderItemList
     * @return \mattanger\hubsoft\integrationservice\Invoice
     */
    public function setOrderItemList($orderItemList)
    {
      $this->orderItemList = $orderItemList;
      return $this;
    }

    /**
     * @return ArrayOfShipment
     */
    public function getShipmentList()
    {
      return $this->shipmentList;
    }

    /**
     * @param ArrayOfShipment $shipmentList
     * @return \mattanger\hubsoft\integrationservice\Invoice
     */
    public function setShipmentList($shipmentList)
    {
      $this->shipmentList = $shipmentList;
      return $this;
    }

}
