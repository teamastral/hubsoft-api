<?php

namespace hubsoft\api\integrationservice;

class MustShipTogether
{

    /**
     * @var ArrayOfGroup $groupList
     */
    protected $groupList = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfGroup
     */
    public function getGroupList()
    {
      return $this->groupList;
    }

    /**
     * @param ArrayOfGroup $groupList
     * @return \hubsoft\api\integrationservice\MustShipTogether
     */
    public function setGroupList($groupList)
    {
      $this->groupList = $groupList;
      return $this;
    }

}
