<?php

namespace mattanger\hubsoft\integrationservice;

class MustShipTogetherCustom
{

    /**
     * @var ArrayOfGroup $groupList
     */
    protected $groupList = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfGroup
     */
    public function getGroupList()
    {
      return $this->groupList;
    }

    /**
     * @param ArrayOfGroup $groupList
     * @return \mattanger\hubsoft\integrationservice\MustShipTogether
     */
    public function setGroupList($groupList)
    {
      $this->groupList = $groupList;
      return $this;
    }

}
