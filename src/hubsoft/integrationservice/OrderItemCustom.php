<?php

namespace mattanger\hubsoft\integrationservice;

class OrderItemCustom
{

    /**
     * @var string $salesTax
     */
    protected $salesTax = null;

    /**
     * @var string $shippedQty
     */
    protected $shippedQty = null;

    /**
     * @var string $sku
     */
    protected $sku = null;

    /**
     * @var string $unitPrice
     */
    protected $unitPrice = null;

    /**
     * @var string $upc
     */
    protected $upc = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSalesTax()
    {
      return $this->salesTax;
    }

    /**
     * @param string $salesTax
     * @return \mattanger\hubsoft\integrationservice\OrderItem
     */
    public function setSalesTax($salesTax)
    {
      $this->salesTax = $salesTax;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippedQty()
    {
      return $this->shippedQty;
    }

    /**
     * @param string $shippedQty
     * @return \mattanger\hubsoft\integrationservice\OrderItem
     */
    public function setShippedQty($shippedQty)
    {
      $this->shippedQty = $shippedQty;
      return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
      return $this->sku;
    }

    /**
     * @param string $sku
     * @return \mattanger\hubsoft\integrationservice\OrderItem
     */
    public function setSku($sku)
    {
      $this->sku = $sku;
      return $this;
    }

    /**
     * @return string
     */
    public function getUnitPrice()
    {
      return $this->unitPrice;
    }

    /**
     * @param string $unitPrice
     * @return \mattanger\hubsoft\integrationservice\OrderItem
     */
    public function setUnitPrice($unitPrice)
    {
      $this->unitPrice = $unitPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getUpc()
    {
      return $this->upc;
    }

    /**
     * @param string $upc
     * @return \mattanger\hubsoft\integrationservice\OrderItem
     */
    public function setUpc($upc)
    {
      $this->upc = $upc;
      return $this;
    }

}
