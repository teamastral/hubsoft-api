<?php

namespace mattanger\hubsoft\integrationservice;

class OrderItemDTOCustom
{

    /**
     * @var int $cancelledQty
     */
    protected $cancelledQty = null;

    /**
     * @var ArrayOfCaseItemDTO $caseItems
     */
    protected $caseItems = null;

    /**
     * @var string $colorCode
     */
    protected $colorCode = null;

    /**
     * @var string $colorName
     */
    protected $colorName = null;

    /**
     * @var string $coordinateCode
     */
    protected $coordinateCode = null;

    /**
     * @var string $distributionCode
     */
    protected $distributionCode = null;

    /**
     * @var float $msrp
     */
    protected $msrp = null;

    /**
     * @var int $orderedQty
     */
    protected $orderedQty = null;

    /**
     * @var string $productName
     */
    protected $productName = null;

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    /**
     * @var string $productReference
     */
    protected $productReference = null;

    /**
     * @var float $salesTax
     */
    protected $salesTax = null;

    /**
     * @var int $shippedQty
     */
    protected $shippedQty = null;

    /**
     * @var string $sizeName
     */
    protected $sizeName = null;

    /**
     * @var string $sku
     */
    protected $sku = null;

    /**
     * @var float $unitPrice
     */
    protected $unitPrice = null;

    /**
     * @var string $upc
     */
    protected $upc = null;

    /**
     * @var float $wholesalePrice
     */
    protected $wholesalePrice = null;

    /**
     * @param ArrayOfCaseItemDTO $caseItems
     * @param string $colorCode
     * @param string $colorName
     * @param string $coordinateCode
     * @param string $distributionCode
     * @param string $productName
     * @param string $productNumber
     * @param string $productReference
     * @param string $sizeName
     */
    public function __construct($caseItems, $colorCode, $colorName, $coordinateCode, $distributionCode, $productName, $productNumber, $productReference, $sizeName)
    {
      $this->caseItems = $caseItems;
      $this->colorCode = $colorCode;
      $this->colorName = $colorName;
      $this->coordinateCode = $coordinateCode;
      $this->distributionCode = $distributionCode;
      $this->productName = $productName;
      $this->productNumber = $productNumber;
      $this->productReference = $productReference;
      $this->sizeName = $sizeName;
    }

    /**
     * @return int
     */
    public function getCancelledQty()
    {
      return $this->cancelledQty;
    }

    /**
     * @param int $cancelledQty
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setCancelledQty($cancelledQty)
    {
      $this->cancelledQty = $cancelledQty;
      return $this;
    }

    /**
     * @return ArrayOfCaseItemDTO
     */
    public function getCaseItems()
    {
      return $this->caseItems;
    }

    /**
     * @param ArrayOfCaseItemDTO $caseItems
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setCaseItems($caseItems)
    {
      $this->caseItems = $caseItems;
      return $this;
    }

    /**
     * @return string
     */
    public function getColorCode()
    {
      return $this->colorCode;
    }

    /**
     * @param string $colorCode
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setColorCode($colorCode)
    {
      $this->colorCode = $colorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getColorName()
    {
      return $this->colorName;
    }

    /**
     * @param string $colorName
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setColorName($colorName)
    {
      $this->colorName = $colorName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCoordinateCode()
    {
      return $this->coordinateCode;
    }

    /**
     * @param string $coordinateCode
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setCoordinateCode($coordinateCode)
    {
      $this->coordinateCode = $coordinateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getDistributionCode()
    {
      return $this->distributionCode;
    }

    /**
     * @param string $distributionCode
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setDistributionCode($distributionCode)
    {
      $this->distributionCode = $distributionCode;
      return $this;
    }

    /**
     * @return float
     */
    public function getMsrp()
    {
      return $this->msrp;
    }

    /**
     * @param float $msrp
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setMsrp($msrp)
    {
      $this->msrp = $msrp;
      return $this;
    }

    /**
     * @return int
     */
    public function getOrderedQty()
    {
      return $this->orderedQty;
    }

    /**
     * @param int $orderedQty
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setOrderedQty($orderedQty)
    {
      $this->orderedQty = $orderedQty;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
      return $this->productName;
    }

    /**
     * @param string $productName
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setProductName($productName)
    {
      $this->productName = $productName;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductReference()
    {
      return $this->productReference;
    }

    /**
     * @param string $productReference
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setProductReference($productReference)
    {
      $this->productReference = $productReference;
      return $this;
    }

    /**
     * @return float
     */
    public function getSalesTax()
    {
      return $this->salesTax;
    }

    /**
     * @param float $salesTax
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setSalesTax($salesTax)
    {
      $this->salesTax = $salesTax;
      return $this;
    }

    /**
     * @return int
     */
    public function getShippedQty()
    {
      return $this->shippedQty;
    }

    /**
     * @param int $shippedQty
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setShippedQty($shippedQty)
    {
      $this->shippedQty = $shippedQty;
      return $this;
    }

    /**
     * @return string
     */
    public function getSizeName()
    {
      return $this->sizeName;
    }

    /**
     * @param string $sizeName
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setSizeName($sizeName)
    {
      $this->sizeName = $sizeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
      return $this->sku;
    }

    /**
     * @param string $sku
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setSku($sku)
    {
      $this->sku = $sku;
      return $this;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
      return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setUnitPrice($unitPrice)
    {
      $this->unitPrice = $unitPrice;
      return $this;
    }

    /**
     * @return string
     */
    public function getUpc()
    {
      return $this->upc;
    }

    /**
     * @param string $upc
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setUpc($upc)
    {
      $this->upc = $upc;
      return $this;
    }

    /**
     * @return float
     */
    public function getWholesalePrice()
    {
      return $this->wholesalePrice;
    }

    /**
     * @param float $wholesalePrice
     * @return \mattanger\hubsoft\integrationservice\OrderItemDTO
     */
    public function setWholesalePrice($wholesalePrice)
    {
      $this->wholesalePrice = $wholesalePrice;
      return $this;
    }

}
