<?php

namespace mattanger\hubsoft\integrationservice;

class OrderStatusDTOCustom2
{

    /**
     * @var string $hubsoftOrderNumber
     */
    protected $hubsoftOrderNumber = null;

    /**
     * @var string $incentiveName
     */
    protected $incentiveName = null;

    /**
     * @var string $invoiceDate
     */
    protected $invoiceDate = null;

    /**
     * @var string $invoiceNumber
     */
    protected $invoiceNumber = null;

    /**
     * @var string $orderStatusCode
     */
    protected $orderStatusCode = null;

    /**
     * @var string $priceBookCode
     */
    protected $priceBookCode = null;

    /**
     * @var string $remoteLastModifiedDt
     */
    protected $remoteLastModifiedDt = null;

    /**
     * @var string $remoteOrderNumber
     */
    protected $remoteOrderNumber = null;

    /**
     * @var string $repCode
     */
    protected $repCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getHubsoftOrderNumber()
    {
      return $this->hubsoftOrderNumber;
    }

    /**
     * @param string $hubsoftOrderNumber
     * @return \mattanger\hubsoft\integrationservice\OrderStatusDTO
     */
    public function setHubsoftOrderNumber($hubsoftOrderNumber)
    {
      $this->hubsoftOrderNumber = $hubsoftOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getIncentiveName()
    {
      return $this->incentiveName;
    }

    /**
     * @param string $incentiveName
     * @return \mattanger\hubsoft\integrationservice\OrderStatusDTO
     */
    public function setIncentiveName($incentiveName)
    {
      $this->incentiveName = $incentiveName;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceDate()
    {
      return $this->invoiceDate;
    }

    /**
     * @param string $invoiceDate
     * @return \mattanger\hubsoft\integrationservice\OrderStatusDTO
     */
    public function setInvoiceDate($invoiceDate)
    {
      $this->invoiceDate = $invoiceDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNumber()
    {
      return $this->invoiceNumber;
    }

    /**
     * @param string $invoiceNumber
     * @return \mattanger\hubsoft\integrationservice\OrderStatusDTO
     */
    public function setInvoiceNumber($invoiceNumber)
    {
      $this->invoiceNumber = $invoiceNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderStatusCode()
    {
      return $this->orderStatusCode;
    }

    /**
     * @param string $orderStatusCode
     * @return \mattanger\hubsoft\integrationservice\OrderStatusDTO
     */
    public function setOrderStatusCode($orderStatusCode)
    {
      $this->orderStatusCode = $orderStatusCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPriceBookCode()
    {
      return $this->priceBookCode;
    }

    /**
     * @param string $priceBookCode
     * @return \mattanger\hubsoft\integrationservice\OrderStatusDTO
     */
    public function setPriceBookCode($priceBookCode)
    {
      $this->priceBookCode = $priceBookCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRemoteLastModifiedDt()
    {
      return $this->remoteLastModifiedDt;
    }

    /**
     * @param string $remoteLastModifiedDt
     * @return \mattanger\hubsoft\integrationservice\OrderStatusDTO
     */
    public function setRemoteLastModifiedDt($remoteLastModifiedDt)
    {
      $this->remoteLastModifiedDt = $remoteLastModifiedDt;
      return $this;
    }

    /**
     * @return string
     */
    public function getRemoteOrderNumber()
    {
      return $this->remoteOrderNumber;
    }

    /**
     * @param string $remoteOrderNumber
     * @return \mattanger\hubsoft\integrationservice\OrderStatusDTO
     */
    public function setRemoteOrderNumber($remoteOrderNumber)
    {
      $this->remoteOrderNumber = $remoteOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCode()
    {
      return $this->repCode;
    }

    /**
     * @param string $repCode
     * @return \mattanger\hubsoft\integrationservice\OrderStatusDTO
     */
    public function setRepCode($repCode)
    {
      $this->repCode = $repCode;
      return $this;
    }

}
