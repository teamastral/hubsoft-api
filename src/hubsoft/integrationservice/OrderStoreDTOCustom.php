<?php

namespace mattanger\hubsoft\integrationservice;

class OrderStoreDTOCustom
{

    /**
     * @var AddressDTO $billingAddress
     */
    protected $billingAddress = null;

    /**
     * @var string $bulkOrderCode
     */
    protected $bulkOrderCode = null;

    /**
     * @var string $customerAccountNumber
     */
    protected $customerAccountNumber = null;

    /**
     * @var string $dropShipAddress
     */
    protected $dropShipAddress = null;

    /**
     * @var string $freightTermCode
     */
    protected $freightTermCode = null;

    /**
     * @var ArrayOfOrderItemDTO $orderItemList
     */
    protected $orderItemList = null;

    /**
     * @var float $salesTax
     */
    protected $salesTax = null;

    /**
     * @var string $shipToFirstName
     */
    protected $shipToFirstName = null;

    /**
     * @var string $shipToLastName
     */
    protected $shipToLastName = null;

    /**
     * @var string $shipToPhone
     */
    protected $shipToPhone = null;

    /**
     * @var ArrayOfShipmentInfo $shipmentList
     */
    protected $shipmentList = null;

    /**
     * @var AddressDTO $shippingAddress
     */
    protected $shippingAddress = null;

    /**
     * @var string $shippingMethodCode
     */
    protected $shippingMethodCode = null;

    /**
     * @var string $storeCode
     */
    protected $storeCode = null;

    /**
     * @var string $storeName
     */
    protected $storeName = null;

    /**
     * @var string $warehouseCode
     */
    protected $warehouseCode = null;

    /**
     * @param string $customerAccountNumber
     * @param string $dropShipAddress
     */
    public function __construct($customerAccountNumber, $dropShipAddress)
    {
      $this->customerAccountNumber = $customerAccountNumber;
      $this->dropShipAddress = $dropShipAddress;
    }

    /**
     * @return AddressDTO
     */
    public function getBillingAddress()
    {
      return $this->billingAddress;
    }

    /**
     * @param AddressDTO $billingAddress
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setBillingAddress($billingAddress)
    {
      $this->billingAddress = $billingAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getBulkOrderCode()
    {
      return $this->bulkOrderCode;
    }

    /**
     * @param string $bulkOrderCode
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setBulkOrderCode($bulkOrderCode)
    {
      $this->bulkOrderCode = $bulkOrderCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerAccountNumber()
    {
      return $this->customerAccountNumber;
    }

    /**
     * @param string $customerAccountNumber
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setCustomerAccountNumber($customerAccountNumber)
    {
      $this->customerAccountNumber = $customerAccountNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getDropShipAddress()
    {
      return $this->dropShipAddress;
    }

    /**
     * @param string $dropShipAddress
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setDropShipAddress($dropShipAddress)
    {
      $this->dropShipAddress = $dropShipAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getFreightTermCode()
    {
      return $this->freightTermCode;
    }

    /**
     * @param string $freightTermCode
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setFreightTermCode($freightTermCode)
    {
      $this->freightTermCode = $freightTermCode;
      return $this;
    }

    /**
     * @return ArrayOfOrderItemDTO
     */
    public function getOrderItemList()
    {
      return $this->orderItemList;
    }

    /**
     * @param ArrayOfOrderItemDTO $orderItemList
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setOrderItemList($orderItemList)
    {
      $this->orderItemList = $orderItemList;
      return $this;
    }

    /**
     * @return float
     */
    public function getSalesTax()
    {
      return $this->salesTax;
    }

    /**
     * @param float $salesTax
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setSalesTax($salesTax)
    {
      $this->salesTax = $salesTax;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToFirstName()
    {
      return $this->shipToFirstName;
    }

    /**
     * @param string $shipToFirstName
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setShipToFirstName($shipToFirstName)
    {
      $this->shipToFirstName = $shipToFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToLastName()
    {
      return $this->shipToLastName;
    }

    /**
     * @param string $shipToLastName
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setShipToLastName($shipToLastName)
    {
      $this->shipToLastName = $shipToLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipToPhone()
    {
      return $this->shipToPhone;
    }

    /**
     * @param string $shipToPhone
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setShipToPhone($shipToPhone)
    {
      $this->shipToPhone = $shipToPhone;
      return $this;
    }

    /**
     * @return ArrayOfShipmentInfo
     */
    public function getShipmentList()
    {
      return $this->shipmentList;
    }

    /**
     * @param ArrayOfShipmentInfo $shipmentList
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setShipmentList($shipmentList)
    {
      $this->shipmentList = $shipmentList;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getShippingAddress()
    {
      return $this->shippingAddress;
    }

    /**
     * @param AddressDTO $shippingAddress
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setShippingAddress($shippingAddress)
    {
      $this->shippingAddress = $shippingAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingMethodCode()
    {
      return $this->shippingMethodCode;
    }

    /**
     * @param string $shippingMethodCode
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setShippingMethodCode($shippingMethodCode)
    {
      $this->shippingMethodCode = $shippingMethodCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
      return $this->storeCode;
    }

    /**
     * @param string $storeCode
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setStoreCode($storeCode)
    {
      $this->storeCode = $storeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreName()
    {
      return $this->storeName;
    }

    /**
     * @param string $storeName
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setStoreName($storeName)
    {
      $this->storeName = $storeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getWarehouseCode()
    {
      return $this->warehouseCode;
    }

    /**
     * @param string $warehouseCode
     * @return \mattanger\hubsoft\integrationservice\OrderStoreDTO
     */
    public function setWarehouseCode($warehouseCode)
    {
      $this->warehouseCode = $warehouseCode;
      return $this;
    }

}
