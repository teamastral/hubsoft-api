<?php

namespace hubsoft\api\integrationservice;

class PriceBookDTO
{

    /**
     * @var string $currencyCode
     */
    protected $currencyCode = null;

    /**
     * @var string $priceBookCode
     */
    protected $priceBookCode = null;

    /**
     * @var string $priceBookName
     */
    protected $priceBookName = null;

    /**
     * @var ArrayOfPriceBookProductDTO $priceBookProductList
     */
    protected $priceBookProductList = null;

    /**
     * @var string $startFrom
     */
    protected $startFrom = null;

    /**
     * @var string $startTo
     */
    protected $startTo = null;

    /**
     * @var string $whslRetailFlag
     */
    protected $whslRetailFlag = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
      return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     * @return \hubsoft\api\integrationservice\PriceBookDTO
     */
    public function setCurrencyCode($currencyCode)
    {
      $this->currencyCode = $currencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPriceBookCode()
    {
      return $this->priceBookCode;
    }

    /**
     * @param string $priceBookCode
     * @return \hubsoft\api\integrationservice\PriceBookDTO
     */
    public function setPriceBookCode($priceBookCode)
    {
      $this->priceBookCode = $priceBookCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPriceBookName()
    {
      return $this->priceBookName;
    }

    /**
     * @param string $priceBookName
     * @return \hubsoft\api\integrationservice\PriceBookDTO
     */
    public function setPriceBookName($priceBookName)
    {
      $this->priceBookName = $priceBookName;
      return $this;
    }

    /**
     * @return ArrayOfPriceBookProductDTO
     */
    public function getPriceBookProductList()
    {
      return $this->priceBookProductList;
    }

    /**
     * @param ArrayOfPriceBookProductDTO $priceBookProductList
     * @return \hubsoft\api\integrationservice\PriceBookDTO
     */
    public function setPriceBookProductList($priceBookProductList)
    {
      $this->priceBookProductList = $priceBookProductList;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartFrom()
    {
      return $this->startFrom;
    }

    /**
     * @param string $startFrom
     * @return \hubsoft\api\integrationservice\PriceBookDTO
     */
    public function setStartFrom($startFrom)
    {
      $this->startFrom = $startFrom;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartTo()
    {
      return $this->startTo;
    }

    /**
     * @param string $startTo
     * @return \hubsoft\api\integrationservice\PriceBookDTO
     */
    public function setStartTo($startTo)
    {
      $this->startTo = $startTo;
      return $this;
    }

    /**
     * @return string
     */
    public function getWhslRetailFlag()
    {
      return $this->whslRetailFlag;
    }

    /**
     * @param string $whslRetailFlag
     * @return \hubsoft\api\integrationservice\PriceBookDTO
     */
    public function setWhslRetailFlag($whslRetailFlag)
    {
      $this->whslRetailFlag = $whslRetailFlag;
      return $this;
    }

}
