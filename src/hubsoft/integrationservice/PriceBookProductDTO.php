<?php

namespace hubsoft\api\integrationservice;

class PriceBookProductDTO
{

    /**
     * @var string $discountPct
     */
    protected $discountPct = null;

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    /**
     * @var string $repCommission
     */
    protected $repCommission = null;

    /**
     * @var string $specifiedMSRP
     */
    protected $specifiedMSRP = null;

    /**
     * @var string $specifiedPrice
     */
    protected $specifiedPrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDiscountPct()
    {
      return $this->discountPct;
    }

    /**
     * @param string $discountPct
     * @return \hubsoft\api\integrationservice\PriceBookProductDTO
     */
    public function setDiscountPct($discountPct)
    {
      $this->discountPct = $discountPct;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \hubsoft\api\integrationservice\PriceBookProductDTO
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCommission()
    {
      return $this->repCommission;
    }

    /**
     * @param string $repCommission
     * @return \hubsoft\api\integrationservice\PriceBookProductDTO
     */
    public function setRepCommission($repCommission)
    {
      $this->repCommission = $repCommission;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecifiedMSRP()
    {
      return $this->specifiedMSRP;
    }

    /**
     * @param string $specifiedMSRP
     * @return \hubsoft\api\integrationservice\PriceBookProductDTO
     */
    public function setSpecifiedMSRP($specifiedMSRP)
    {
      $this->specifiedMSRP = $specifiedMSRP;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecifiedPrice()
    {
      return $this->specifiedPrice;
    }

    /**
     * @param string $specifiedPrice
     * @return \hubsoft\api\integrationservice\PriceBookProductDTO
     */
    public function setSpecifiedPrice($specifiedPrice)
    {
      $this->specifiedPrice = $specifiedPrice;
      return $this;
    }

}
