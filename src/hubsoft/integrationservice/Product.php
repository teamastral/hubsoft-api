<?php

namespace hubsoft\api\integrationservice;

class Product
{

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \hubsoft\api\integrationservice\Product
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

}
