<?php

namespace mattanger\hubsoft\integrationservice;

class ProductColorDTOCustom
{

    /**
     * @var string $colorCode
     */
    protected $colorCode = null;

    /**
     * @var string $colorDesc
     */
    protected $colorDesc = null;

    /**
     * @var string $colorSwatchImageUrl
     */
    protected $colorSwatchImageUrl = null;

    /**
     * @var string $imageThumbUrl
     */
    protected $imageThumbUrl = null;

    /**
     * @var string $imageUrl
     */
    protected $imageUrl = null;

    /**
     * @var float $msrp
     */
    protected $msrp = null;

    /**
     * @var int $orderMultiple
     */
    protected $orderMultiple = null;

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    /**
     * @var string $searchTerm
     */
    protected $searchTerm = null;

    /**
     * @var string $sizeChartCode
     */
    protected $sizeChartCode = null;

    /**
     * @var ArrayOfSizeDTO $sizes
     */
    protected $sizes = null;

    /**
     * @var float $wholesalePrice
     */
    protected $wholesalePrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getColorCode()
    {
      return $this->colorCode;
    }

    /**
     * @param string $colorCode
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setColorCode($colorCode)
    {
      $this->colorCode = $colorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getColorDesc()
    {
      return $this->colorDesc;
    }

    /**
     * @param string $colorDesc
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setColorDesc($colorDesc)
    {
      $this->colorDesc = $colorDesc;
      return $this;
    }

    /**
     * @return string
     */
    public function getColorSwatchImageUrl()
    {
      return $this->colorSwatchImageUrl;
    }

    /**
     * @param string $colorSwatchImageUrl
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setColorSwatchImageUrl($colorSwatchImageUrl)
    {
      $this->colorSwatchImageUrl = $colorSwatchImageUrl;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageThumbUrl()
    {
      return $this->imageThumbUrl;
    }

    /**
     * @param string $imageThumbUrl
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setImageThumbUrl($imageThumbUrl)
    {
      $this->imageThumbUrl = $imageThumbUrl;
      return $this;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
      return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setImageUrl($imageUrl)
    {
      $this->imageUrl = $imageUrl;
      return $this;
    }

    /**
     * @return float
     */
    public function getMsrp()
    {
      return $this->msrp;
    }

    /**
     * @param float $msrp
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setMsrp($msrp)
    {
      $this->msrp = $msrp;
      return $this;
    }

    /**
     * @return int
     */
    public function getOrderMultiple()
    {
      return $this->orderMultiple;
    }

    /**
     * @param int $orderMultiple
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setOrderMultiple($orderMultiple)
    {
      $this->orderMultiple = $orderMultiple;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getSearchTerm()
    {
      return $this->searchTerm;
    }

    /**
     * @param string $searchTerm
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setSearchTerm($searchTerm)
    {
      $this->searchTerm = $searchTerm;
      return $this;
    }

    /**
     * @return string
     */
    public function getSizeChartCode()
    {
      return $this->sizeChartCode;
    }

    /**
     * @param string $sizeChartCode
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setSizeChartCode($sizeChartCode)
    {
      $this->sizeChartCode = $sizeChartCode;
      return $this;
    }

    /**
     * @return ArrayOfSizeDTO
     */
    public function getSizes()
    {
      return $this->sizes;
    }

    /**
     * @param ArrayOfSizeDTO $sizes
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setSizes($sizes)
    {
      $this->sizes = $sizes;
      return $this;
    }

    /**
     * @return float
     */
    public function getWholesalePrice()
    {
      return $this->wholesalePrice;
    }

    /**
     * @param float $wholesalePrice
     * @return \mattanger\hubsoft\integrationservice\ProductColorDTO
     */
    public function setWholesalePrice($wholesalePrice)
    {
      $this->wholesalePrice = $wholesalePrice;
      return $this;
    }

}
