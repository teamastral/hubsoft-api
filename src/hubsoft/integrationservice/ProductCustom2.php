<?php

namespace mattanger\hubsoft\integrationservice;

class ProductCustom2
{

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \mattanger\hubsoft\integrationservice\Product
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

}
