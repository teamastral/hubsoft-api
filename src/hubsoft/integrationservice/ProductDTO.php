<?php

namespace hubsoft\api\integrationservice;

class ProductDTO
{

    /**
     * @var ArrayOfString $classificationCode
     */
    protected $classificationCode = null;

    /**
     * @var ArrayOfString $classificationDesc
     */
    protected $classificationDesc = null;

    /**
     * @var ArrayOfProductColorDTO $productColor
     */
    protected $productColor = null;

    /**
     * @var string $productName
     */
    protected $productName = null;

    /**
     * @var string $productReference
     */
    protected $productReference = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfString
     */
    public function getClassificationCode()
    {
      return $this->classificationCode;
    }

    /**
     * @param ArrayOfString $classificationCode
     * @return \hubsoft\api\integrationservice\ProductDTO
     */
    public function setClassificationCode($classificationCode)
    {
      $this->classificationCode = $classificationCode;
      return $this;
    }

    /**
     * @return ArrayOfString
     */
    public function getClassificationDesc()
    {
      return $this->classificationDesc;
    }

    /**
     * @param ArrayOfString $classificationDesc
     * @return \hubsoft\api\integrationservice\ProductDTO
     */
    public function setClassificationDesc($classificationDesc)
    {
      $this->classificationDesc = $classificationDesc;
      return $this;
    }

    /**
     * @return ArrayOfProductColorDTO
     */
    public function getProductColor()
    {
      return $this->productColor;
    }

    /**
     * @param ArrayOfProductColorDTO $productColor
     * @return \hubsoft\api\integrationservice\ProductDTO
     */
    public function setProductColor($productColor)
    {
      $this->productColor = $productColor;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
      return $this->productName;
    }

    /**
     * @param string $productName
     * @return \hubsoft\api\integrationservice\ProductDTO
     */
    public function setProductName($productName)
    {
      $this->productName = $productName;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductReference()
    {
      return $this->productReference;
    }

    /**
     * @param string $productReference
     * @return \hubsoft\api\integrationservice\ProductDTO
     */
    public function setProductReference($productReference)
    {
      $this->productReference = $productReference;
      return $this;
    }

}
