<?php

namespace mattanger\hubsoft\integrationservice;

class ProductDescDTOCustom
{

    /**
     * @var string $description
     */
    protected $description = null;

    /**
     * @var string $description1
     */
    protected $description1 = null;

    /**
     * @var string $description2
     */
    protected $description2 = null;

    /**
     * @var string $description3
     */
    protected $description3 = null;

    /**
     * @var string $description4
     */
    protected $description4 = null;

    /**
     * @var string $description5
     */
    protected $description5 = null;

    /**
     * @var string $description6
     */
    protected $description6 = null;

    /**
     * @var string $language
     */
    protected $language = null;

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->description;
    }

    /**
     * @param string $description
     * @return \mattanger\hubsoft\integrationservice\ProductDescDTO
     */
    public function setDescription($description)
    {
      $this->description = $description;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription1()
    {
      return $this->description1;
    }

    /**
     * @param string $description1
     * @return \mattanger\hubsoft\integrationservice\ProductDescDTO
     */
    public function setDescription1($description1)
    {
      $this->description1 = $description1;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription2()
    {
      return $this->description2;
    }

    /**
     * @param string $description2
     * @return \mattanger\hubsoft\integrationservice\ProductDescDTO
     */
    public function setDescription2($description2)
    {
      $this->description2 = $description2;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription3()
    {
      return $this->description3;
    }

    /**
     * @param string $description3
     * @return \mattanger\hubsoft\integrationservice\ProductDescDTO
     */
    public function setDescription3($description3)
    {
      $this->description3 = $description3;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription4()
    {
      return $this->description4;
    }

    /**
     * @param string $description4
     * @return \mattanger\hubsoft\integrationservice\ProductDescDTO
     */
    public function setDescription4($description4)
    {
      $this->description4 = $description4;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription5()
    {
      return $this->description5;
    }

    /**
     * @param string $description5
     * @return \mattanger\hubsoft\integrationservice\ProductDescDTO
     */
    public function setDescription5($description5)
    {
      $this->description5 = $description5;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription6()
    {
      return $this->description6;
    }

    /**
     * @param string $description6
     * @return \mattanger\hubsoft\integrationservice\ProductDescDTO
     */
    public function setDescription6($description6)
    {
      $this->description6 = $description6;
      return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
      return $this->language;
    }

    /**
     * @param string $language
     * @return \mattanger\hubsoft\integrationservice\ProductDescDTO
     */
    public function setLanguage($language)
    {
      $this->language = $language;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \mattanger\hubsoft\integrationservice\ProductDescDTO
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

}
