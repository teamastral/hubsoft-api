<?php

namespace mattanger\hubsoft\integrationservice;

class RepAccountDTOCustom2
{

    /**
     * @var string $accountCode
     */
    protected $accountCode = null;

    /**
     * @var ArrayOfRepBuyerDTO $buyerList
     */
    protected $buyerList = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAccountCode()
    {
      return $this->accountCode;
    }

    /**
     * @param string $accountCode
     * @return \mattanger\hubsoft\integrationservice\RepAccountDTO
     */
    public function setAccountCode($accountCode)
    {
      $this->accountCode = $accountCode;
      return $this;
    }

    /**
     * @return ArrayOfRepBuyerDTO
     */
    public function getBuyerList()
    {
      return $this->buyerList;
    }

    /**
     * @param ArrayOfRepBuyerDTO $buyerList
     * @return \mattanger\hubsoft\integrationservice\RepAccountDTO
     */
    public function setBuyerList($buyerList)
    {
      $this->buyerList = $buyerList;
      return $this;
    }

}
