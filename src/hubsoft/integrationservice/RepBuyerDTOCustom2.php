<?php

namespace mattanger\hubsoft\integrationservice;

class RepBuyerDTOCustom2
{

    /**
     * @var string $activeFlag
     */
    protected $activeFlag = null;

    /**
     * @var string $divisionRestrictionCode
     */
    protected $divisionRestrictionCode = null;

    /**
     * @var string $email
     */
    protected $email = null;

    /**
     * @var string $fax
     */
    protected $fax = null;

    /**
     * @var string $firstName
     */
    protected $firstName = null;

    /**
     * @var string $lastName
     */
    protected $lastName = null;

    /**
     * @var string $mobile
     */
    protected $mobile = null;

    /**
     * @var string $phone
     */
    protected $phone = null;

    /**
     * @var string $referenceCode
     */
    protected $referenceCode = null;

    /**
     * @var ArrayOfRepBuyerStoreDTO $storeList
     */
    protected $storeList = null;

    /**
     * @var string $title
     */
    protected $title = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getActiveFlag()
    {
      return $this->activeFlag;
    }

    /**
     * @param string $activeFlag
     * @return \mattanger\hubsoft\integrationservice\RepBuyerDTO
     */
    public function setActiveFlag($activeFlag)
    {
      $this->activeFlag = $activeFlag;
      return $this;
    }

    /**
     * @return string
     */
    public function getDivisionRestrictionCode()
    {
      return $this->divisionRestrictionCode;
    }

    /**
     * @param string $divisionRestrictionCode
     * @return \mattanger\hubsoft\integrationservice\RepBuyerDTO
     */
    public function setDivisionRestrictionCode($divisionRestrictionCode)
    {
      $this->divisionRestrictionCode = $divisionRestrictionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
      return $this->email;
    }

    /**
     * @param string $email
     * @return \mattanger\hubsoft\integrationservice\RepBuyerDTO
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

    /**
     * @return string
     */
    public function getFax()
    {
      return $this->fax;
    }

    /**
     * @param string $fax
     * @return \mattanger\hubsoft\integrationservice\RepBuyerDTO
     */
    public function setFax($fax)
    {
      $this->fax = $fax;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return \mattanger\hubsoft\integrationservice\RepBuyerDTO
     */
    public function setFirstName($firstName)
    {
      $this->firstName = $firstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return \mattanger\hubsoft\integrationservice\RepBuyerDTO
     */
    public function setLastName($lastName)
    {
      $this->lastName = $lastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
      return $this->mobile;
    }

    /**
     * @param string $mobile
     * @return \mattanger\hubsoft\integrationservice\RepBuyerDTO
     */
    public function setMobile($mobile)
    {
      $this->mobile = $mobile;
      return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
      return $this->phone;
    }

    /**
     * @param string $phone
     * @return \mattanger\hubsoft\integrationservice\RepBuyerDTO
     */
    public function setPhone($phone)
    {
      $this->phone = $phone;
      return $this;
    }

    /**
     * @return string
     */
    public function getReferenceCode()
    {
      return $this->referenceCode;
    }

    /**
     * @param string $referenceCode
     * @return \mattanger\hubsoft\integrationservice\RepBuyerDTO
     */
    public function setReferenceCode($referenceCode)
    {
      $this->referenceCode = $referenceCode;
      return $this;
    }

    /**
     * @return ArrayOfRepBuyerStoreDTO
     */
    public function getStoreList()
    {
      return $this->storeList;
    }

    /**
     * @param ArrayOfRepBuyerStoreDTO $storeList
     * @return \mattanger\hubsoft\integrationservice\RepBuyerDTO
     */
    public function setStoreList($storeList)
    {
      $this->storeList = $storeList;
      return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
      return $this->title;
    }

    /**
     * @param string $title
     * @return \mattanger\hubsoft\integrationservice\RepBuyerDTO
     */
    public function setTitle($title)
    {
      $this->title = $title;
      return $this;
    }

}
