<?php

namespace mattanger\hubsoft\integrationservice;

class RepBuyerStoreDTOCustom2
{

    /**
     * @var string $defaultFlag
     */
    protected $defaultFlag = null;

    /**
     * @var string $storeCode
     */
    protected $storeCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDefaultFlag()
    {
      return $this->defaultFlag;
    }

    /**
     * @param string $defaultFlag
     * @return \mattanger\hubsoft\integrationservice\RepBuyerStoreDTO
     */
    public function setDefaultFlag($defaultFlag)
    {
      $this->defaultFlag = $defaultFlag;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
      return $this->storeCode;
    }

    /**
     * @param string $storeCode
     * @return \mattanger\hubsoft\integrationservice\RepBuyerStoreDTO
     */
    public function setStoreCode($storeCode)
    {
      $this->storeCode = $storeCode;
      return $this;
    }

}
