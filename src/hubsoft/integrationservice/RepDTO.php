<?php

namespace hubsoft\api\integrationservice;

class RepDTO
{

    /**
     * @var ArrayOfRepAccountDTO $accountList
     */
    protected $accountList = null;

    /**
     * @var string $repCode
     */
    protected $repCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfRepAccountDTO
     */
    public function getAccountList()
    {
      return $this->accountList;
    }

    /**
     * @param ArrayOfRepAccountDTO $accountList
     * @return \hubsoft\api\integrationservice\RepDTO
     */
    public function setAccountList($accountList)
    {
      $this->accountList = $accountList;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCode()
    {
      return $this->repCode;
    }

    /**
     * @param string $repCode
     * @return \hubsoft\api\integrationservice\RepDTO
     */
    public function setRepCode($repCode)
    {
      $this->repCode = $repCode;
      return $this;
    }

}
