<?php

namespace mattanger\hubsoft\integrationservice;

class RepDTOCustom2
{

    /**
     * @var ArrayOfRepAccountDTO $accountList
     */
    protected $accountList = null;

    /**
     * @var string $repCode
     */
    protected $repCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfRepAccountDTO
     */
    public function getAccountList()
    {
      return $this->accountList;
    }

    /**
     * @param ArrayOfRepAccountDTO $accountList
     * @return \mattanger\hubsoft\integrationservice\RepDTO
     */
    public function setAccountList($accountList)
    {
      $this->accountList = $accountList;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCode()
    {
      return $this->repCode;
    }

    /**
     * @param string $repCode
     * @return \mattanger\hubsoft\integrationservice\RepDTO
     */
    public function setRepCode($repCode)
    {
      $this->repCode = $repCode;
      return $this;
    }

}
