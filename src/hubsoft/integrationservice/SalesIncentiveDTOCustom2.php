<?php

namespace mattanger\hubsoft\integrationservice;

class SalesIncentiveDTOCustom2
{

    /**
     * @var string $active
     */
    protected $active = null;

    /**
     * @var string $billingTermCode
     */
    protected $billingTermCode = null;

    /**
     * @var ArrayOfSalesIncentiveProductDTO $incentiveProductList
     */
    protected $incentiveProductList = null;

    /**
     * @var string $salesIncentiveCode
     */
    protected $salesIncentiveCode = null;

    /**
     * @var string $salesIncentiveName
     */
    protected $salesIncentiveName = null;

    /**
     * @var string $seasonCode
     */
    protected $seasonCode = null;

    /**
     * @var string $startFrom
     */
    protected $startFrom = null;

    /**
     * @var string $startTo
     */
    protected $startTo = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getActive()
    {
      return $this->active;
    }

    /**
     * @param string $active
     * @return \mattanger\hubsoft\integrationservice\SalesIncentiveDTO
     */
    public function setActive($active)
    {
      $this->active = $active;
      return $this;
    }

    /**
     * @return string
     */
    public function getBillingTermCode()
    {
      return $this->billingTermCode;
    }

    /**
     * @param string $billingTermCode
     * @return \mattanger\hubsoft\integrationservice\SalesIncentiveDTO
     */
    public function setBillingTermCode($billingTermCode)
    {
      $this->billingTermCode = $billingTermCode;
      return $this;
    }

    /**
     * @return ArrayOfSalesIncentiveProductDTO
     */
    public function getIncentiveProductList()
    {
      return $this->incentiveProductList;
    }

    /**
     * @param ArrayOfSalesIncentiveProductDTO $incentiveProductList
     * @return \mattanger\hubsoft\integrationservice\SalesIncentiveDTO
     */
    public function setIncentiveProductList($incentiveProductList)
    {
      $this->incentiveProductList = $incentiveProductList;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesIncentiveCode()
    {
      return $this->salesIncentiveCode;
    }

    /**
     * @param string $salesIncentiveCode
     * @return \mattanger\hubsoft\integrationservice\SalesIncentiveDTO
     */
    public function setSalesIncentiveCode($salesIncentiveCode)
    {
      $this->salesIncentiveCode = $salesIncentiveCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesIncentiveName()
    {
      return $this->salesIncentiveName;
    }

    /**
     * @param string $salesIncentiveName
     * @return \mattanger\hubsoft\integrationservice\SalesIncentiveDTO
     */
    public function setSalesIncentiveName($salesIncentiveName)
    {
      $this->salesIncentiveName = $salesIncentiveName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSeasonCode()
    {
      return $this->seasonCode;
    }

    /**
     * @param string $seasonCode
     * @return \mattanger\hubsoft\integrationservice\SalesIncentiveDTO
     */
    public function setSeasonCode($seasonCode)
    {
      $this->seasonCode = $seasonCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartFrom()
    {
      return $this->startFrom;
    }

    /**
     * @param string $startFrom
     * @return \mattanger\hubsoft\integrationservice\SalesIncentiveDTO
     */
    public function setStartFrom($startFrom)
    {
      $this->startFrom = $startFrom;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartTo()
    {
      return $this->startTo;
    }

    /**
     * @param string $startTo
     * @return \mattanger\hubsoft\integrationservice\SalesIncentiveDTO
     */
    public function setStartTo($startTo)
    {
      $this->startTo = $startTo;
      return $this;
    }

}
