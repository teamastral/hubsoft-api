<?php

namespace mattanger\hubsoft\integrationservice;

class SalesTerritoryDTOCustom
{

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var string $parent
     */
    protected $parent = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return \mattanger\hubsoft\integrationservice\SalesTerritoryDTO
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return string
     */
    public function getParent()
    {
      return $this->parent;
    }

    /**
     * @param string $parent
     * @return \mattanger\hubsoft\integrationservice\SalesTerritoryDTO
     */
    public function setParent($parent)
    {
      $this->parent = $parent;
      return $this;
    }

}
