<?php

namespace mattanger\hubsoft\integrationservice;

class ShipmentCustom
{

    /**
     * @var string $numberOfPackages
     */
    protected $numberOfPackages = null;

    /**
     * @var string $receivedBy
     */
    protected $receivedBy = null;

    /**
     * @var string $salesTax
     */
    protected $salesTax = null;

    /**
     * @var string $shipCost
     */
    protected $shipCost = null;

    /**
     * @var string $shipDate
     */
    protected $shipDate = null;

    /**
     * @var string $shippingMethod
     */
    protected $shippingMethod = null;

    /**
     * @var string $trackingNumber
     */
    protected $trackingNumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getNumberOfPackages()
    {
      return $this->numberOfPackages;
    }

    /**
     * @param string $numberOfPackages
     * @return \mattanger\hubsoft\integrationservice\Shipment
     */
    public function setNumberOfPackages($numberOfPackages)
    {
      $this->numberOfPackages = $numberOfPackages;
      return $this;
    }

    /**
     * @return string
     */
    public function getReceivedBy()
    {
      return $this->receivedBy;
    }

    /**
     * @param string $receivedBy
     * @return \mattanger\hubsoft\integrationservice\Shipment
     */
    public function setReceivedBy($receivedBy)
    {
      $this->receivedBy = $receivedBy;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesTax()
    {
      return $this->salesTax;
    }

    /**
     * @param string $salesTax
     * @return \mattanger\hubsoft\integrationservice\Shipment
     */
    public function setSalesTax($salesTax)
    {
      $this->salesTax = $salesTax;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipCost()
    {
      return $this->shipCost;
    }

    /**
     * @param string $shipCost
     * @return \mattanger\hubsoft\integrationservice\Shipment
     */
    public function setShipCost($shipCost)
    {
      $this->shipCost = $shipCost;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipDate()
    {
      return $this->shipDate;
    }

    /**
     * @param string $shipDate
     * @return \mattanger\hubsoft\integrationservice\Shipment
     */
    public function setShipDate($shipDate)
    {
      $this->shipDate = $shipDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingMethod()
    {
      return $this->shippingMethod;
    }

    /**
     * @param string $shippingMethod
     * @return \mattanger\hubsoft\integrationservice\Shipment
     */
    public function setShippingMethod($shippingMethod)
    {
      $this->shippingMethod = $shippingMethod;
      return $this;
    }

    /**
     * @return string
     */
    public function getTrackingNumber()
    {
      return $this->trackingNumber;
    }

    /**
     * @param string $trackingNumber
     * @return \mattanger\hubsoft\integrationservice\Shipment
     */
    public function setTrackingNumber($trackingNumber)
    {
      $this->trackingNumber = $trackingNumber;
      return $this;
    }

}
