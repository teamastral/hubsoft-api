<?php

namespace mattanger\hubsoft\integrationservice;

class ShipmentInfoCustom2
{

    /**
     * @var string $numberOfPackages
     */
    protected $numberOfPackages = null;

    /**
     * @var string $receivedBy
     */
    protected $receivedBy = null;

    /**
     * @var float $salesTaxAmount
     */
    protected $salesTaxAmount = null;

    /**
     * @var float $shipCharged
     */
    protected $shipCharged = null;

    /**
     * @var float $shipCost
     */
    protected $shipCost = null;

    /**
     * @var string $shippingCarrier
     */
    protected $shippingCarrier = null;

    /**
     * @var string $shippingCarrierService
     */
    protected $shippingCarrierService = null;

    /**
     * @var string $shippingMethodCode
     */
    protected $shippingMethodCode = null;

    /**
     * @var string $trackingNumber
     */
    protected $trackingNumber = null;

    /**
     * @param float $shipCharged
     */
    public function __construct($shipCharged)
    {
      $this->shipCharged = $shipCharged;
    }

    /**
     * @return string
     */
    public function getNumberOfPackages()
    {
      return $this->numberOfPackages;
    }

    /**
     * @param string $numberOfPackages
     * @return \mattanger\hubsoft\integrationservice\ShipmentInfo
     */
    public function setNumberOfPackages($numberOfPackages)
    {
      $this->numberOfPackages = $numberOfPackages;
      return $this;
    }

    /**
     * @return string
     */
    public function getReceivedBy()
    {
      return $this->receivedBy;
    }

    /**
     * @param string $receivedBy
     * @return \mattanger\hubsoft\integrationservice\ShipmentInfo
     */
    public function setReceivedBy($receivedBy)
    {
      $this->receivedBy = $receivedBy;
      return $this;
    }

    /**
     * @return float
     */
    public function getSalesTaxAmount()
    {
      return $this->salesTaxAmount;
    }

    /**
     * @param float $salesTaxAmount
     * @return \mattanger\hubsoft\integrationservice\ShipmentInfo
     */
    public function setSalesTaxAmount($salesTaxAmount)
    {
      $this->salesTaxAmount = $salesTaxAmount;
      return $this;
    }

    /**
     * @return float
     */
    public function getShipCharged()
    {
      return $this->shipCharged;
    }

    /**
     * @param float $shipCharged
     * @return \mattanger\hubsoft\integrationservice\ShipmentInfo
     */
    public function setShipCharged($shipCharged)
    {
      $this->shipCharged = $shipCharged;
      return $this;
    }

    /**
     * @return float
     */
    public function getShipCost()
    {
      return $this->shipCost;
    }

    /**
     * @param float $shipCost
     * @return \mattanger\hubsoft\integrationservice\ShipmentInfo
     */
    public function setShipCost($shipCost)
    {
      $this->shipCost = $shipCost;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingCarrier()
    {
      return $this->shippingCarrier;
    }

    /**
     * @param string $shippingCarrier
     * @return \mattanger\hubsoft\integrationservice\ShipmentInfo
     */
    public function setShippingCarrier($shippingCarrier)
    {
      $this->shippingCarrier = $shippingCarrier;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingCarrierService()
    {
      return $this->shippingCarrierService;
    }

    /**
     * @param string $shippingCarrierService
     * @return \mattanger\hubsoft\integrationservice\ShipmentInfo
     */
    public function setShippingCarrierService($shippingCarrierService)
    {
      $this->shippingCarrierService = $shippingCarrierService;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingMethodCode()
    {
      return $this->shippingMethodCode;
    }

    /**
     * @param string $shippingMethodCode
     * @return \mattanger\hubsoft\integrationservice\ShipmentInfo
     */
    public function setShippingMethodCode($shippingMethodCode)
    {
      $this->shippingMethodCode = $shippingMethodCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getTrackingNumber()
    {
      return $this->trackingNumber;
    }

    /**
     * @param string $trackingNumber
     * @return \mattanger\hubsoft\integrationservice\ShipmentInfo
     */
    public function setTrackingNumber($trackingNumber)
    {
      $this->trackingNumber = $trackingNumber;
      return $this;
    }

}
