<?php

namespace hubsoft\api\integrationservice;

class ShippingNoticeDTO
{

    /**
     * @var string $actualShippingAmount
     */
    protected $actualShippingAmount = null;

    /**
     * @var string $hubsoftOrderNumber
     */
    protected $hubsoftOrderNumber = null;

    /**
     * @var string $invoiceNumber
     */
    protected $invoiceNumber = null;

    /**
     * @var ArrayOfOrderItemDTO $itemInfo
     */
    protected $itemInfo = null;

    /**
     * @var string $orderDatetime
     */
    protected $orderDatetime = null;

    /**
     * @var string $shipDate
     */
    protected $shipDate = null;

    /**
     * @var string $shippingMethodCode
     */
    protected $shippingMethodCode = null;

    /**
     * @var string $totalInvoiceAmount
     */
    protected $totalInvoiceAmount = null;

    /**
     * @var string $totalOrderedQty
     */
    protected $totalOrderedQty = null;

    /**
     * @var string $totalShippingAmount
     */
    protected $totalShippingAmount = null;

    /**
     * @var string $trackingNumber
     */
    protected $trackingNumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getActualShippingAmount()
    {
      return $this->actualShippingAmount;
    }

    /**
     * @param string $actualShippingAmount
     * @return \hubsoft\api\integrationservice\ShippingNoticeDTO
     */
    public function setActualShippingAmount($actualShippingAmount)
    {
      $this->actualShippingAmount = $actualShippingAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getHubsoftOrderNumber()
    {
      return $this->hubsoftOrderNumber;
    }

    /**
     * @param string $hubsoftOrderNumber
     * @return \hubsoft\api\integrationservice\ShippingNoticeDTO
     */
    public function setHubsoftOrderNumber($hubsoftOrderNumber)
    {
      $this->hubsoftOrderNumber = $hubsoftOrderNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNumber()
    {
      return $this->invoiceNumber;
    }

    /**
     * @param string $invoiceNumber
     * @return \hubsoft\api\integrationservice\ShippingNoticeDTO
     */
    public function setInvoiceNumber($invoiceNumber)
    {
      $this->invoiceNumber = $invoiceNumber;
      return $this;
    }

    /**
     * @return ArrayOfOrderItemDTO
     */
    public function getItemInfo()
    {
      return $this->itemInfo;
    }

    /**
     * @param ArrayOfOrderItemDTO $itemInfo
     * @return \hubsoft\api\integrationservice\ShippingNoticeDTO
     */
    public function setItemInfo($itemInfo)
    {
      $this->itemInfo = $itemInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrderDatetime()
    {
      return $this->orderDatetime;
    }

    /**
     * @param string $orderDatetime
     * @return \hubsoft\api\integrationservice\ShippingNoticeDTO
     */
    public function setOrderDatetime($orderDatetime)
    {
      $this->orderDatetime = $orderDatetime;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipDate()
    {
      return $this->shipDate;
    }

    /**
     * @param string $shipDate
     * @return \hubsoft\api\integrationservice\ShippingNoticeDTO
     */
    public function setShipDate($shipDate)
    {
      $this->shipDate = $shipDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingMethodCode()
    {
      return $this->shippingMethodCode;
    }

    /**
     * @param string $shippingMethodCode
     * @return \hubsoft\api\integrationservice\ShippingNoticeDTO
     */
    public function setShippingMethodCode($shippingMethodCode)
    {
      $this->shippingMethodCode = $shippingMethodCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getTotalInvoiceAmount()
    {
      return $this->totalInvoiceAmount;
    }

    /**
     * @param string $totalInvoiceAmount
     * @return \hubsoft\api\integrationservice\ShippingNoticeDTO
     */
    public function setTotalInvoiceAmount($totalInvoiceAmount)
    {
      $this->totalInvoiceAmount = $totalInvoiceAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getTotalOrderedQty()
    {
      return $this->totalOrderedQty;
    }

    /**
     * @param string $totalOrderedQty
     * @return \hubsoft\api\integrationservice\ShippingNoticeDTO
     */
    public function setTotalOrderedQty($totalOrderedQty)
    {
      $this->totalOrderedQty = $totalOrderedQty;
      return $this;
    }

    /**
     * @return string
     */
    public function getTotalShippingAmount()
    {
      return $this->totalShippingAmount;
    }

    /**
     * @param string $totalShippingAmount
     * @return \hubsoft\api\integrationservice\ShippingNoticeDTO
     */
    public function setTotalShippingAmount($totalShippingAmount)
    {
      $this->totalShippingAmount = $totalShippingAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getTrackingNumber()
    {
      return $this->trackingNumber;
    }

    /**
     * @param string $trackingNumber
     * @return \hubsoft\api\integrationservice\ShippingNoticeDTO
     */
    public function setTrackingNumber($trackingNumber)
    {
      $this->trackingNumber = $trackingNumber;
      return $this;
    }

}
