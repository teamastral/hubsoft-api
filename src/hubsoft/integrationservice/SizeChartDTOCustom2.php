<?php

namespace mattanger\hubsoft\integrationservice;

class SizeChartDTOCustom2
{

    /**
     * @var string $code
     */
    protected $code = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var ArrayOfSizeChartSizeDTO $sizeList
     */
    protected $sizeList = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->code;
    }

    /**
     * @param string $code
     * @return \mattanger\hubsoft\integrationservice\SizeChartDTO
     */
    public function setCode($code)
    {
      $this->code = $code;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return \mattanger\hubsoft\integrationservice\SizeChartDTO
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return ArrayOfSizeChartSizeDTO
     */
    public function getSizeList()
    {
      return $this->sizeList;
    }

    /**
     * @param ArrayOfSizeChartSizeDTO $sizeList
     * @return \mattanger\hubsoft\integrationservice\SizeChartDTO
     */
    public function setSizeList($sizeList)
    {
      $this->sizeList = $sizeList;
      return $this;
    }

}
