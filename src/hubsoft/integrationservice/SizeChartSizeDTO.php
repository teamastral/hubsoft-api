<?php

namespace hubsoft\api\integrationservice;

class SizeChartSizeDTO
{

    /**
     * @var string $name
     */
    protected $name = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return \hubsoft\api\integrationservice\SizeChartSizeDTO
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

}
