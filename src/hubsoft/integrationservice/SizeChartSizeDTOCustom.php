<?php

namespace mattanger\hubsoft\integrationservice;

class SizeChartSizeDTOCustom
{

    /**
     * @var string $name
     */
    protected $name = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return \mattanger\hubsoft\integrationservice\SizeChartSizeDTO
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

}
