<?php

namespace mattanger\hubsoft\integrationservice;

class SizeDTOCustom2
{

    /**
     * @var string $carryOver
     */
    protected $carryOver = null;

    /**
     * @var float $msrp
     */
    protected $msrp = null;

    /**
     * @var string $sizeCode
     */
    protected $sizeCode = null;

    /**
     * @var string $sku
     */
    protected $sku = null;

    /**
     * @var string $upc
     */
    protected $upc = null;

    /**
     * @var float $wholesalePrice
     */
    protected $wholesalePrice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCarryOver()
    {
      return $this->carryOver;
    }

    /**
     * @param string $carryOver
     * @return \mattanger\hubsoft\integrationservice\SizeDTO
     */
    public function setCarryOver($carryOver)
    {
      $this->carryOver = $carryOver;
      return $this;
    }

    /**
     * @return float
     */
    public function getMsrp()
    {
      return $this->msrp;
    }

    /**
     * @param float $msrp
     * @return \mattanger\hubsoft\integrationservice\SizeDTO
     */
    public function setMsrp($msrp)
    {
      $this->msrp = $msrp;
      return $this;
    }

    /**
     * @return string
     */
    public function getSizeCode()
    {
      return $this->sizeCode;
    }

    /**
     * @param string $sizeCode
     * @return \mattanger\hubsoft\integrationservice\SizeDTO
     */
    public function setSizeCode($sizeCode)
    {
      $this->sizeCode = $sizeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
      return $this->sku;
    }

    /**
     * @param string $sku
     * @return \mattanger\hubsoft\integrationservice\SizeDTO
     */
    public function setSku($sku)
    {
      $this->sku = $sku;
      return $this;
    }

    /**
     * @return string
     */
    public function getUpc()
    {
      return $this->upc;
    }

    /**
     * @param string $upc
     * @return \mattanger\hubsoft\integrationservice\SizeDTO
     */
    public function setUpc($upc)
    {
      $this->upc = $upc;
      return $this;
    }

    /**
     * @return float
     */
    public function getWholesalePrice()
    {
      return $this->wholesalePrice;
    }

    /**
     * @param float $wholesalePrice
     * @return \mattanger\hubsoft\integrationservice\SizeDTO
     */
    public function setWholesalePrice($wholesalePrice)
    {
      $this->wholesalePrice = $wholesalePrice;
      return $this;
    }

}
