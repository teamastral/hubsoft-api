<?php

namespace hubsoft\api\integrationservice;

class SkuSeasonDTO
{

    /**
     * @var string $seasonCode
     */
    protected $seasonCode = null;

    /**
     * @var string $sku
     */
    protected $sku = null;

    /**
     * @var string $startShipDate
     */
    protected $startShipDate = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSeasonCode()
    {
      return $this->seasonCode;
    }

    /**
     * @param string $seasonCode
     * @return \hubsoft\api\integrationservice\SkuSeasonDTO
     */
    public function setSeasonCode($seasonCode)
    {
      $this->seasonCode = $seasonCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
      return $this->sku;
    }

    /**
     * @param string $sku
     * @return \hubsoft\api\integrationservice\SkuSeasonDTO
     */
    public function setSku($sku)
    {
      $this->sku = $sku;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartShipDate()
    {
      return $this->startShipDate;
    }

    /**
     * @param string $startShipDate
     * @return \hubsoft\api\integrationservice\SkuSeasonDTO
     */
    public function setStartShipDate($startShipDate)
    {
      $this->startShipDate = $startShipDate;
      return $this;
    }

}
