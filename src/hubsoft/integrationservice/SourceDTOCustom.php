<?php

namespace mattanger\hubsoft\integrationservice;

class SourceDTOCustom
{

    /**
     * @var ArrayOfAttrDTO $attrList
     */
    protected $attrList = null;

    /**
     * @var string $code
     */
    protected $code = null;

    /**
     * @var string $name
     */
    protected $name = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAttrDTO
     */
    public function getAttrList()
    {
      return $this->attrList;
    }

    /**
     * @param ArrayOfAttrDTO $attrList
     * @return \mattanger\hubsoft\integrationservice\SourceDTO
     */
    public function setAttrList($attrList)
    {
      $this->attrList = $attrList;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->code;
    }

    /**
     * @param string $code
     * @return \mattanger\hubsoft\integrationservice\SourceDTO
     */
    public function setCode($code)
    {
      $this->code = $code;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return \mattanger\hubsoft\integrationservice\SourceDTO
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

}
