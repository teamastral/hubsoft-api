<?php

namespace hubsoft\api\integrationservice;

class StoreDTO
{

    /**
     * @var AddressDTO $billingAddress
     */
    protected $billingAddress = null;

    /**
     * @var string $buyerFirstName
     */
    protected $buyerFirstName = null;

    /**
     * @var string $buyerLastName
     */
    protected $buyerLastName = null;

    /**
     * @var string $code
     */
    protected $code = null;

    /**
     * @var ContactDTO $contact
     */
    protected $contact = null;

    /**
     * @var string $createdDate
     */
    protected $createdDate = null;

    /**
     * @var string $exposeToLocator
     */
    protected $exposeToLocator = null;

    /**
     * @var string $freightTermCode
     */
    protected $freightTermCode = null;

    /**
     * @var AddressDTO $locatorAddress
     */
    protected $locatorAddress = null;

    /**
     * @var string $locatorNotes
     */
    protected $locatorNotes = null;

    /**
     * @var string $locatorPhone
     */
    protected $locatorPhone = null;

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var string $repCode
     */
    protected $repCode = null;

    /**
     * @var string $shipperAccountNumber
     */
    protected $shipperAccountNumber = null;

    /**
     * @var AddressDTO $shippingAddress
     */
    protected $shippingAddress = null;

    /**
     * @var string $shippingMethodCode
     */
    protected $shippingMethodCode = null;

    /**
     * @var string $storeTypeCode
     */
    protected $storeTypeCode = null;

    /**
     * @var string $territoryCode
     */
    protected $territoryCode = null;

    /**
     * @var string $url
     */
    protected $url = null;

    /**
     * @var string $warehouseCode
     */
    protected $warehouseCode = null;

    /**
     * @param string $createdDate
     */
    public function __construct($createdDate)
    {
      $this->createdDate = $createdDate;
    }

    /**
     * @return AddressDTO
     */
    public function getBillingAddress()
    {
      return $this->billingAddress;
    }

    /**
     * @param AddressDTO $billingAddress
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setBillingAddress($billingAddress)
    {
      $this->billingAddress = $billingAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getBuyerFirstName()
    {
      return $this->buyerFirstName;
    }

    /**
     * @param string $buyerFirstName
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setBuyerFirstName($buyerFirstName)
    {
      $this->buyerFirstName = $buyerFirstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getBuyerLastName()
    {
      return $this->buyerLastName;
    }

    /**
     * @param string $buyerLastName
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setBuyerLastName($buyerLastName)
    {
      $this->buyerLastName = $buyerLastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
      return $this->code;
    }

    /**
     * @param string $code
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setCode($code)
    {
      $this->code = $code;
      return $this;
    }

    /**
     * @return ContactDTO
     */
    public function getContact()
    {
      return $this->contact;
    }

    /**
     * @param ContactDTO $contact
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setContact($contact)
    {
      $this->contact = $contact;
      return $this;
    }

    /**
     * @return string
     */
    public function getCreatedDate()
    {
      return $this->createdDate;
    }

    /**
     * @param string $createdDate
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setCreatedDate($createdDate)
    {
      $this->createdDate = $createdDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getExposeToLocator()
    {
      return $this->exposeToLocator;
    }

    /**
     * @param string $exposeToLocator
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setExposeToLocator($exposeToLocator)
    {
      $this->exposeToLocator = $exposeToLocator;
      return $this;
    }

    /**
     * @return string
     */
    public function getFreightTermCode()
    {
      return $this->freightTermCode;
    }

    /**
     * @param string $freightTermCode
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setFreightTermCode($freightTermCode)
    {
      $this->freightTermCode = $freightTermCode;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getLocatorAddress()
    {
      return $this->locatorAddress;
    }

    /**
     * @param AddressDTO $locatorAddress
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setLocatorAddress($locatorAddress)
    {
      $this->locatorAddress = $locatorAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getLocatorNotes()
    {
      return $this->locatorNotes;
    }

    /**
     * @param string $locatorNotes
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setLocatorNotes($locatorNotes)
    {
      $this->locatorNotes = $locatorNotes;
      return $this;
    }

    /**
     * @return string
     */
    public function getLocatorPhone()
    {
      return $this->locatorPhone;
    }

    /**
     * @param string $locatorPhone
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setLocatorPhone($locatorPhone)
    {
      $this->locatorPhone = $locatorPhone;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return string
     */
    public function getRepCode()
    {
      return $this->repCode;
    }

    /**
     * @param string $repCode
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setRepCode($repCode)
    {
      $this->repCode = $repCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getShipperAccountNumber()
    {
      return $this->shipperAccountNumber;
    }

    /**
     * @param string $shipperAccountNumber
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setShipperAccountNumber($shipperAccountNumber)
    {
      $this->shipperAccountNumber = $shipperAccountNumber;
      return $this;
    }

    /**
     * @return AddressDTO
     */
    public function getShippingAddress()
    {
      return $this->shippingAddress;
    }

    /**
     * @param AddressDTO $shippingAddress
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setShippingAddress($shippingAddress)
    {
      $this->shippingAddress = $shippingAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getShippingMethodCode()
    {
      return $this->shippingMethodCode;
    }

    /**
     * @param string $shippingMethodCode
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setShippingMethodCode($shippingMethodCode)
    {
      $this->shippingMethodCode = $shippingMethodCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreTypeCode()
    {
      return $this->storeTypeCode;
    }

    /**
     * @param string $storeTypeCode
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setStoreTypeCode($storeTypeCode)
    {
      $this->storeTypeCode = $storeTypeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getTerritoryCode()
    {
      return $this->territoryCode;
    }

    /**
     * @param string $territoryCode
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setTerritoryCode($territoryCode)
    {
      $this->territoryCode = $territoryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
      return $this->url;
    }

    /**
     * @param string $url
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setUrl($url)
    {
      $this->url = $url;
      return $this;
    }

    /**
     * @return string
     */
    public function getWarehouseCode()
    {
      return $this->warehouseCode;
    }

    /**
     * @param string $warehouseCode
     * @return \hubsoft\api\integrationservice\StoreDTO
     */
    public function setWarehouseCode($warehouseCode)
    {
      $this->warehouseCode = $warehouseCode;
      return $this;
    }

}
