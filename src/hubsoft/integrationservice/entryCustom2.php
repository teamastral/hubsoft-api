<?php

namespace mattanger\hubsoft\integrationservice;

class entryCustom2
{

    /**
     * @var string $key
     */
    protected $key = null;

    /**
     * @var string $value
     */
    protected $value = null;

    /**
     * @param string $key
     * @param string $value
     */
    public function __construct($key, $value)
    {
      $this->key = $key;
      $this->value = $value;
    }

    /**
     * @return string
     */
    public function getKey()
    {
      return $this->key;
    }

    /**
     * @param string $key
     * @return \mattanger\hubsoft\integrationservice\entry
     */
    public function setKey($key)
    {
      $this->key = $key;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param string $value
     * @return \mattanger\hubsoft\integrationservice\entry
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

}
