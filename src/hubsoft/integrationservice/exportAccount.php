<?php

namespace hubsoft\api\integrationservice;

class exportAccount
{

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param string2stringMap $serviceConfig
     */
    public function __construct($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\exportAccount
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
