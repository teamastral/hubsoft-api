<?php

namespace hubsoft\api\integrationservice;

class exportAccountResponse
{

    /**
     * @var ArrayOfAccountDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfAccountDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfAccountDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfAccountDTO $out
     * @return \hubsoft\api\integrationservice\exportAccountResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
