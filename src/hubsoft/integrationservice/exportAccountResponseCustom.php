<?php

namespace mattanger\hubsoft\integrationservice;

class exportAccountResponseCustom
{

    /**
     * @var ArrayOfAccountDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfAccountDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfAccountDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfAccountDTO $out
     * @return \mattanger\hubsoft\integrationservice\exportAccountResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
