<?php

namespace mattanger\hubsoft\integrationservice;

class exportBulkOrdersResponseCustom
{

    /**
     * @var ArrayOfBulkOrderDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfBulkOrderDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfBulkOrderDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfBulkOrderDTO $out
     * @return \mattanger\hubsoft\integrationservice\exportBulkOrdersResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
