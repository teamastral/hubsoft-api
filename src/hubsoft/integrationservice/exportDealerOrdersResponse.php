<?php

namespace hubsoft\api\integrationservice;

class exportDealerOrdersResponse
{

    /**
     * @var ArrayOfDealerOrderDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfDealerOrderDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfDealerOrderDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfDealerOrderDTO $out
     * @return \hubsoft\api\integrationservice\exportDealerOrdersResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
