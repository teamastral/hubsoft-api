<?php

namespace mattanger\hubsoft\integrationservice;

class exportDealerOrdersResponseCustom
{

    /**
     * @var ArrayOfDealerOrderDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfDealerOrderDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfDealerOrderDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfDealerOrderDTO $out
     * @return \mattanger\hubsoft\integrationservice\exportDealerOrdersResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
