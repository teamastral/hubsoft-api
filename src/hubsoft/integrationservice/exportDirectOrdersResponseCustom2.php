<?php

namespace mattanger\hubsoft\integrationservice;

class exportDirectOrdersResponseCustom2
{

    /**
     * @var ArrayOfDirectOrderDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfDirectOrderDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfDirectOrderDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfDirectOrderDTO $out
     * @return \mattanger\hubsoft\integrationservice\exportDirectOrdersResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
