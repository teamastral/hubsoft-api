<?php

namespace mattanger\hubsoft\integrationservice;

class exportInventoryCustom2
{

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param string2stringMap $serviceConfig
     */
    public function __construct($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\exportInventory
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
