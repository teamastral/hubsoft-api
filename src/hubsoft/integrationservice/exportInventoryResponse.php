<?php

namespace hubsoft\api\integrationservice;

class exportInventoryResponse
{

    /**
     * @var ArrayOfInventoryDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfInventoryDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfInventoryDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfInventoryDTO $out
     * @return \hubsoft\api\integrationservice\exportInventoryResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
