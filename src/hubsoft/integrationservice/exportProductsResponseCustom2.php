<?php

namespace mattanger\hubsoft\integrationservice;

class exportProductsResponseCustom2
{

    /**
     * @var ArrayOfProductDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfProductDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfProductDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfProductDTO $out
     * @return \mattanger\hubsoft\integrationservice\exportProductsResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
