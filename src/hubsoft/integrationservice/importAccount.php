<?php

namespace hubsoft\api\integrationservice;

class importAccount
{

    /**
     * @var ArrayOfAccountDTO $accountList
     */
    protected $accountList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfAccountDTO $accountList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($accountList, $serviceConfig)
    {
      $this->accountList = $accountList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfAccountDTO
     */
    public function getAccountList()
    {
      return $this->accountList;
    }

    /**
     * @param ArrayOfAccountDTO $accountList
     * @return \hubsoft\api\integrationservice\importAccount
     */
    public function setAccountList($accountList)
    {
      $this->accountList = $accountList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importAccount
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
