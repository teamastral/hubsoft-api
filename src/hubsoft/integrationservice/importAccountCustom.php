<?php

namespace mattanger\hubsoft\integrationservice;

class importAccountCustom
{

    /**
     * @var ArrayOfAccountDTO $accountList
     */
    protected $accountList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfAccountDTO $accountList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($accountList, $serviceConfig)
    {
      $this->accountList = $accountList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfAccountDTO
     */
    public function getAccountList()
    {
      return $this->accountList;
    }

    /**
     * @param ArrayOfAccountDTO $accountList
     * @return \mattanger\hubsoft\integrationservice\importAccount
     */
    public function setAccountList($accountList)
    {
      $this->accountList = $accountList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\importAccount
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
