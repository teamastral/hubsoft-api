<?php

namespace mattanger\hubsoft\integrationservice;

class importAccountProspectCustom
{

    /**
     * @var ArrayOfAccountProspectDTO $accountProspectList
     */
    protected $accountProspectList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfAccountProspectDTO $accountProspectList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($accountProspectList, $serviceConfig)
    {
      $this->accountProspectList = $accountProspectList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfAccountProspectDTO
     */
    public function getAccountProspectList()
    {
      return $this->accountProspectList;
    }

    /**
     * @param ArrayOfAccountProspectDTO $accountProspectList
     * @return \mattanger\hubsoft\integrationservice\importAccountProspect
     */
    public function setAccountProspectList($accountProspectList)
    {
      $this->accountProspectList = $accountProspectList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\importAccountProspect
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
