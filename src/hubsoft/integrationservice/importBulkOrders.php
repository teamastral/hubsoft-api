<?php

namespace hubsoft\api\integrationservice;

class importBulkOrders
{

    /**
     * @var ArrayOfBulkOrderDTO $orderList
     */
    protected $orderList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfBulkOrderDTO $orderList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($orderList, $serviceConfig)
    {
      $this->orderList = $orderList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfBulkOrderDTO
     */
    public function getOrderList()
    {
      return $this->orderList;
    }

    /**
     * @param ArrayOfBulkOrderDTO $orderList
     * @return \hubsoft\api\integrationservice\importBulkOrders
     */
    public function setOrderList($orderList)
    {
      $this->orderList = $orderList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importBulkOrders
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
