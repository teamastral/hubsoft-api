<?php

namespace hubsoft\api\integrationservice;

class importBuyer
{

    /**
     * @var ArrayOfRepDTO $repList
     */
    protected $repList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfRepDTO $repList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($repList, $serviceConfig)
    {
      $this->repList = $repList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfRepDTO
     */
    public function getRepList()
    {
      return $this->repList;
    }

    /**
     * @param ArrayOfRepDTO $repList
     * @return \hubsoft\api\integrationservice\importBuyer
     */
    public function setRepList($repList)
    {
      $this->repList = $repList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importBuyer
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
