<?php

namespace hubsoft\api\integrationservice;

class importDealerOrders
{

    /**
     * @var ArrayOfDealerOrderDTO $orderList
     */
    protected $orderList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfDealerOrderDTO $orderList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($orderList, $serviceConfig)
    {
      $this->orderList = $orderList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfDealerOrderDTO
     */
    public function getOrderList()
    {
      return $this->orderList;
    }

    /**
     * @param ArrayOfDealerOrderDTO $orderList
     * @return \hubsoft\api\integrationservice\importDealerOrders
     */
    public function setOrderList($orderList)
    {
      $this->orderList = $orderList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importDealerOrders
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
