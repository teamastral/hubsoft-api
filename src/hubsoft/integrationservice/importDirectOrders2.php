<?php

namespace hubsoft\api\integrationservice;

class importDirectOrders2
{

    /**
     * @var ArrayOfDirectOrderUpdate2DTO $orderList
     */
    protected $orderList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfDirectOrderUpdate2DTO $orderList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($orderList, $serviceConfig)
    {
      $this->orderList = $orderList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfDirectOrderUpdate2DTO
     */
    public function getOrderList()
    {
      return $this->orderList;
    }

    /**
     * @param ArrayOfDirectOrderUpdate2DTO $orderList
     * @return \hubsoft\api\integrationservice\importDirectOrders2
     */
    public function setOrderList($orderList)
    {
      $this->orderList = $orderList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importDirectOrders2
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
