<?php

namespace hubsoft\api\integrationservice;

class importDirectOrders2Response
{

    /**
     * @var ImportResponse $out
     */
    protected $out = null;

    /**
     * @param ImportResponse $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ImportResponse
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ImportResponse $out
     * @return \hubsoft\api\integrationservice\importDirectOrders2Response
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
