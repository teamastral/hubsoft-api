<?php

namespace mattanger\hubsoft\integrationservice;

class importDirectOrdersCustom2
{

    /**
     * @var ArrayOfDirectOrderUpdateDTO $orderList
     */
    protected $orderList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfDirectOrderUpdateDTO $orderList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($orderList, $serviceConfig)
    {
      $this->orderList = $orderList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfDirectOrderUpdateDTO
     */
    public function getOrderList()
    {
      return $this->orderList;
    }

    /**
     * @param ArrayOfDirectOrderUpdateDTO $orderList
     * @return \mattanger\hubsoft\integrationservice\importDirectOrders
     */
    public function setOrderList($orderList)
    {
      $this->orderList = $orderList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\importDirectOrders
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
