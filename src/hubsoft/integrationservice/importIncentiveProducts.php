<?php

namespace hubsoft\api\integrationservice;

class importIncentiveProducts
{

    /**
     * @var ArrayOfIncentiveProductDTO $incentiveProductList
     */
    protected $incentiveProductList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfIncentiveProductDTO $incentiveProductList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($incentiveProductList, $serviceConfig)
    {
      $this->incentiveProductList = $incentiveProductList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfIncentiveProductDTO
     */
    public function getIncentiveProductList()
    {
      return $this->incentiveProductList;
    }

    /**
     * @param ArrayOfIncentiveProductDTO $incentiveProductList
     * @return \hubsoft\api\integrationservice\importIncentiveProducts
     */
    public function setIncentiveProductList($incentiveProductList)
    {
      $this->incentiveProductList = $incentiveProductList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importIncentiveProducts
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
