<?php

namespace mattanger\hubsoft\integrationservice;

class importInventoryCustom2
{

    /**
     * @var ArrayOfInventoryDTO $invList
     */
    protected $invList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfInventoryDTO $invList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($invList, $serviceConfig)
    {
      $this->invList = $invList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfInventoryDTO
     */
    public function getInvList()
    {
      return $this->invList;
    }

    /**
     * @param ArrayOfInventoryDTO $invList
     * @return \mattanger\hubsoft\integrationservice\importInventory
     */
    public function setInvList($invList)
    {
      $this->invList = $invList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\importInventory
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
