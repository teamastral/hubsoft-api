<?php

namespace hubsoft\api\integrationservice;

class importOrderStatus
{

    /**
     * @var ArrayOfOrderStatusDTO $orderStatusList
     */
    protected $orderStatusList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfOrderStatusDTO $orderStatusList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($orderStatusList, $serviceConfig)
    {
      $this->orderStatusList = $orderStatusList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfOrderStatusDTO
     */
    public function getOrderStatusList()
    {
      return $this->orderStatusList;
    }

    /**
     * @param ArrayOfOrderStatusDTO $orderStatusList
     * @return \hubsoft\api\integrationservice\importOrderStatus
     */
    public function setOrderStatusList($orderStatusList)
    {
      $this->orderStatusList = $orderStatusList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importOrderStatus
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
