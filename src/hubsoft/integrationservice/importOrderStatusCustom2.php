<?php

namespace mattanger\hubsoft\integrationservice;

class importOrderStatusCustom2
{

    /**
     * @var ArrayOfOrderStatusDTO $orderStatusList
     */
    protected $orderStatusList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfOrderStatusDTO $orderStatusList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($orderStatusList, $serviceConfig)
    {
      $this->orderStatusList = $orderStatusList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfOrderStatusDTO
     */
    public function getOrderStatusList()
    {
      return $this->orderStatusList;
    }

    /**
     * @param ArrayOfOrderStatusDTO $orderStatusList
     * @return \mattanger\hubsoft\integrationservice\importOrderStatus
     */
    public function setOrderStatusList($orderStatusList)
    {
      $this->orderStatusList = $orderStatusList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\importOrderStatus
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
