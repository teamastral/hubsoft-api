<?php

namespace mattanger\hubsoft\integrationservice;

class importPriceBooksCustom2
{

    /**
     * @var PriceBookDTO $priceBook
     */
    protected $priceBook = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param PriceBookDTO $priceBook
     * @param string2stringMap $serviceConfig
     */
    public function __construct($priceBook, $serviceConfig)
    {
      $this->priceBook = $priceBook;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return PriceBookDTO
     */
    public function getPriceBook()
    {
      return $this->priceBook;
    }

    /**
     * @param PriceBookDTO $priceBook
     * @return \mattanger\hubsoft\integrationservice\importPriceBooks
     */
    public function setPriceBook($priceBook)
    {
      $this->priceBook = $priceBook;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\importPriceBooks
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
