<?php

namespace hubsoft\api\integrationservice;

class importProductDescription
{

    /**
     * @var ArrayOfProductDescDTO $productDescList
     */
    protected $productDescList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfProductDescDTO $productDescList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($productDescList, $serviceConfig)
    {
      $this->productDescList = $productDescList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfProductDescDTO
     */
    public function getProductDescList()
    {
      return $this->productDescList;
    }

    /**
     * @param ArrayOfProductDescDTO $productDescList
     * @return \hubsoft\api\integrationservice\importProductDescription
     */
    public function setProductDescList($productDescList)
    {
      $this->productDescList = $productDescList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importProductDescription
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
