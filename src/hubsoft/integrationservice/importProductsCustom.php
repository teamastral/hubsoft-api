<?php

namespace mattanger\hubsoft\integrationservice;

class importProductsCustom
{

    /**
     * @var ArrayOfProductDTO $productList
     */
    protected $productList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfProductDTO $productList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($productList, $serviceConfig)
    {
      $this->productList = $productList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfProductDTO
     */
    public function getProductList()
    {
      return $this->productList;
    }

    /**
     * @param ArrayOfProductDTO $productList
     * @return \mattanger\hubsoft\integrationservice\importProducts
     */
    public function setProductList($productList)
    {
      $this->productList = $productList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\importProducts
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
