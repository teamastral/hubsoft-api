<?php

namespace hubsoft\api\integrationservice;

class importSalesIncentives
{

    /**
     * @var ArrayOfSalesIncentiveDTO $saleIncentiveList
     */
    protected $saleIncentiveList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfSalesIncentiveDTO $saleIncentiveList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($saleIncentiveList, $serviceConfig)
    {
      $this->saleIncentiveList = $saleIncentiveList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfSalesIncentiveDTO
     */
    public function getSaleIncentiveList()
    {
      return $this->saleIncentiveList;
    }

    /**
     * @param ArrayOfSalesIncentiveDTO $saleIncentiveList
     * @return \hubsoft\api\integrationservice\importSalesIncentives
     */
    public function setSaleIncentiveList($saleIncentiveList)
    {
      $this->saleIncentiveList = $saleIncentiveList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importSalesIncentives
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
