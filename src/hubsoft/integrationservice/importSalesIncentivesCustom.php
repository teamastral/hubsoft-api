<?php

namespace mattanger\hubsoft\integrationservice;

class importSalesIncentivesCustom
{

    /**
     * @var ArrayOfSalesIncentiveDTO $saleIncentiveList
     */
    protected $saleIncentiveList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfSalesIncentiveDTO $saleIncentiveList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($saleIncentiveList, $serviceConfig)
    {
      $this->saleIncentiveList = $saleIncentiveList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfSalesIncentiveDTO
     */
    public function getSaleIncentiveList()
    {
      return $this->saleIncentiveList;
    }

    /**
     * @param ArrayOfSalesIncentiveDTO $saleIncentiveList
     * @return \mattanger\hubsoft\integrationservice\importSalesIncentives
     */
    public function setSaleIncentiveList($saleIncentiveList)
    {
      $this->saleIncentiveList = $saleIncentiveList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\importSalesIncentives
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
