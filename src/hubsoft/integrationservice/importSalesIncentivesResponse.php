<?php

namespace hubsoft\api\integrationservice;

class importSalesIncentivesResponse
{

    /**
     * @var ImportResponse $out
     */
    protected $out = null;

    /**
     * @param ImportResponse $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ImportResponse
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ImportResponse $out
     * @return \hubsoft\api\integrationservice\importSalesIncentivesResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
