<?php

namespace hubsoft\api\integrationservice;

class importSalesTerritory
{

    /**
     * @var ArrayOfSalesTerritoryDTO $salesTerritoryList
     */
    protected $salesTerritoryList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfSalesTerritoryDTO $salesTerritoryList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($salesTerritoryList, $serviceConfig)
    {
      $this->salesTerritoryList = $salesTerritoryList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfSalesTerritoryDTO
     */
    public function getSalesTerritoryList()
    {
      return $this->salesTerritoryList;
    }

    /**
     * @param ArrayOfSalesTerritoryDTO $salesTerritoryList
     * @return \hubsoft\api\integrationservice\importSalesTerritory
     */
    public function setSalesTerritoryList($salesTerritoryList)
    {
      $this->salesTerritoryList = $salesTerritoryList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importSalesTerritory
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
