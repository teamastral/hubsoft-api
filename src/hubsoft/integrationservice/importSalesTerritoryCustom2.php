<?php

namespace mattanger\hubsoft\integrationservice;

class importSalesTerritoryCustom2
{

    /**
     * @var ArrayOfSalesTerritoryDTO $salesTerritoryList
     */
    protected $salesTerritoryList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfSalesTerritoryDTO $salesTerritoryList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($salesTerritoryList, $serviceConfig)
    {
      $this->salesTerritoryList = $salesTerritoryList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfSalesTerritoryDTO
     */
    public function getSalesTerritoryList()
    {
      return $this->salesTerritoryList;
    }

    /**
     * @param ArrayOfSalesTerritoryDTO $salesTerritoryList
     * @return \mattanger\hubsoft\integrationservice\importSalesTerritory
     */
    public function setSalesTerritoryList($salesTerritoryList)
    {
      $this->salesTerritoryList = $salesTerritoryList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\importSalesTerritory
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
