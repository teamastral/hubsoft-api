<?php

namespace hubsoft\api\integrationservice;

class importSeasonAssignment
{

    /**
     * @var ArrayOfSkuSeasonDTO $skuSeasonList
     */
    protected $skuSeasonList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfSkuSeasonDTO $skuSeasonList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($skuSeasonList, $serviceConfig)
    {
      $this->skuSeasonList = $skuSeasonList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfSkuSeasonDTO
     */
    public function getSkuSeasonList()
    {
      return $this->skuSeasonList;
    }

    /**
     * @param ArrayOfSkuSeasonDTO $skuSeasonList
     * @return \hubsoft\api\integrationservice\importSeasonAssignment
     */
    public function setSkuSeasonList($skuSeasonList)
    {
      $this->skuSeasonList = $skuSeasonList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importSeasonAssignment
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
