<?php

namespace mattanger\hubsoft\integrationservice;

class importSizeChartCustom
{

    /**
     * @var ArrayOfSizeChartDTO $sizeChartList
     */
    protected $sizeChartList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfSizeChartDTO $sizeChartList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($sizeChartList, $serviceConfig)
    {
      $this->sizeChartList = $sizeChartList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfSizeChartDTO
     */
    public function getSizeChartList()
    {
      return $this->sizeChartList;
    }

    /**
     * @param ArrayOfSizeChartDTO $sizeChartList
     * @return \mattanger\hubsoft\integrationservice\importSizeChart
     */
    public function setSizeChartList($sizeChartList)
    {
      $this->sizeChartList = $sizeChartList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\importSizeChart
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
