<?php

namespace hubsoft\api\integrationservice;

class importSizeChartResponse
{

    /**
     * @var ImportResponse $out
     */
    protected $out = null;

    /**
     * @param ImportResponse $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ImportResponse
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ImportResponse $out
     * @return \hubsoft\api\integrationservice\importSizeChartResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
