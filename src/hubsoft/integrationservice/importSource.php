<?php

namespace hubsoft\api\integrationservice;

class importSource
{

    /**
     * @var string $sourceType
     */
    protected $sourceType = null;

    /**
     * @var ArrayOfSourceDTO $sourceList
     */
    protected $sourceList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param string $sourceType
     * @param ArrayOfSourceDTO $sourceList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($sourceType, $sourceList, $serviceConfig)
    {
      $this->sourceType = $sourceType;
      $this->sourceList = $sourceList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return string
     */
    public function getSourceType()
    {
      return $this->sourceType;
    }

    /**
     * @param string $sourceType
     * @return \hubsoft\api\integrationservice\importSource
     */
    public function setSourceType($sourceType)
    {
      $this->sourceType = $sourceType;
      return $this;
    }

    /**
     * @return ArrayOfSourceDTO
     */
    public function getSourceList()
    {
      return $this->sourceList;
    }

    /**
     * @param ArrayOfSourceDTO $sourceList
     * @return \hubsoft\api\integrationservice\importSource
     */
    public function setSourceList($sourceList)
    {
      $this->sourceList = $sourceList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importSource
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
