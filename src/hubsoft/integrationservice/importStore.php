<?php

namespace hubsoft\api\integrationservice;

class importStore
{

    /**
     * @var ArrayOfAccountStoreDTO $storeList
     */
    protected $storeList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfAccountStoreDTO $storeList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($storeList, $serviceConfig)
    {
      $this->storeList = $storeList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfAccountStoreDTO
     */
    public function getStoreList()
    {
      return $this->storeList;
    }

    /**
     * @param ArrayOfAccountStoreDTO $storeList
     * @return \hubsoft\api\integrationservice\importStore
     */
    public function setStoreList($storeList)
    {
      $this->storeList = $storeList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \hubsoft\api\integrationservice\importStore
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
