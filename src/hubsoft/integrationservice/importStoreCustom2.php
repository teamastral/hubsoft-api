<?php

namespace mattanger\hubsoft\integrationservice;

class importStoreCustom2
{

    /**
     * @var ArrayOfAccountStoreDTO $storeList
     */
    protected $storeList = null;

    /**
     * @var string2stringMap $serviceConfig
     */
    protected $serviceConfig = null;

    /**
     * @param ArrayOfAccountStoreDTO $storeList
     * @param string2stringMap $serviceConfig
     */
    public function __construct($storeList, $serviceConfig)
    {
      $this->storeList = $storeList;
      $this->serviceConfig = $serviceConfig;
    }

    /**
     * @return ArrayOfAccountStoreDTO
     */
    public function getStoreList()
    {
      return $this->storeList;
    }

    /**
     * @param ArrayOfAccountStoreDTO $storeList
     * @return \mattanger\hubsoft\integrationservice\importStore
     */
    public function setStoreList($storeList)
    {
      $this->storeList = $storeList;
      return $this;
    }

    /**
     * @return string2stringMap
     */
    public function getServiceConfig()
    {
      return $this->serviceConfig;
    }

    /**
     * @param string2stringMap $serviceConfig
     * @return \mattanger\hubsoft\integrationservice\importStore
     */
    public function setServiceConfig($serviceConfig)
    {
      $this->serviceConfig = $serviceConfig;
      return $this;
    }

}
