<?php

namespace mattanger\hubsoft\integrationservice;

class importStoreResponseCustom
{

    /**
     * @var ImportResponse $out
     */
    protected $out = null;

    /**
     * @param ImportResponse $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ImportResponse
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ImportResponse $out
     * @return \mattanger\hubsoft\integrationservice\importStoreResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
