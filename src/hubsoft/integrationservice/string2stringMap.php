<?php

namespace hubsoft\api\integrationservice;

class string2stringMap
{

    /**
     * @var entry[] $entry
     */
    protected $entry = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return entry[]
     */
    public function getEntry()
    {
      return $this->entry;
    }

    /**
     * @param entry[] $entry
     * @return \hubsoft\api\integrationservice\string2stringMap
     */
    public function setEntry(array $entry = null)
    {
      $this->entry = $entry;
      return $this;
    }

}
