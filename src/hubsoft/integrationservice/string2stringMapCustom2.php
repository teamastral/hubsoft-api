<?php

namespace mattanger\hubsoft\integrationservice;

class string2stringMapCustom2
{

    /**
     * @var entry[] $entry
     */
    protected $entry = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return entry[]
     */
    public function getEntry()
    {
      return $this->entry;
    }

    /**
     * @param entry[] $entry
     * @return \mattanger\hubsoft\integrationservice\string2stringMap
     */
    public function setEntry(array $entry = null)
    {
      $this->entry = $entry;
      return $this;
    }

}
