<?php

namespace hubsoft\api\site;

class AccountStoreDTO
{

    /**
     * @var string $accountCode
     */
    protected $accountCode = null;

    /**
     * @var boolean $found
     */
    protected $found = null;

    /**
     * @var string $invoiceAmount
     */
    protected $invoiceAmount = null;

    /**
     * @var string $invoiceDate
     */
    protected $invoiceDate = null;

    /**
     * @var string $invoiceNumber
     */
    protected $invoiceNumber = null;

    /**
     * @var string $storeCode
     */
    protected $storeCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAccountCode()
    {
      return $this->accountCode;
    }

    /**
     * @param string $accountCode
     * @return \hubsoft\api\site\AccountStoreDTO
     */
    public function setAccountCode($accountCode)
    {
      $this->accountCode = $accountCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFound()
    {
      return $this->found;
    }

    /**
     * @param boolean $found
     * @return \hubsoft\api\site\AccountStoreDTO
     */
    public function setFound($found)
    {
      $this->found = $found;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceAmount()
    {
      return $this->invoiceAmount;
    }

    /**
     * @param string $invoiceAmount
     * @return \hubsoft\api\site\AccountStoreDTO
     */
    public function setInvoiceAmount($invoiceAmount)
    {
      $this->invoiceAmount = $invoiceAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceDate()
    {
      return $this->invoiceDate;
    }

    /**
     * @param string $invoiceDate
     * @return \hubsoft\api\site\AccountStoreDTO
     */
    public function setInvoiceDate($invoiceDate)
    {
      $this->invoiceDate = $invoiceDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNumber()
    {
      return $this->invoiceNumber;
    }

    /**
     * @param string $invoiceNumber
     * @return \hubsoft\api\site\AccountStoreDTO
     */
    public function setInvoiceNumber($invoiceNumber)
    {
      $this->invoiceNumber = $invoiceNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
      return $this->storeCode;
    }

    /**
     * @param string $storeCode
     * @return \hubsoft\api\site\AccountStoreDTO
     */
    public function setStoreCode($storeCode)
    {
      $this->storeCode = $storeCode;
      return $this;
    }

}
