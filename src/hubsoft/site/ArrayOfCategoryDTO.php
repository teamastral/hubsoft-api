<?php

namespace hubsoft\api\site;

class ArrayOfCategoryDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CategoryDTO[] $CategoryDTO
     */
    protected $CategoryDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CategoryDTO[]
     */
    public function getCategoryDTO()
    {
      return $this->CategoryDTO;
    }

    /**
     * @param CategoryDTO[] $CategoryDTO
     * @return \hubsoft\api\site\ArrayOfCategoryDTO
     */
    public function setCategoryDTO(array $CategoryDTO = null)
    {
      $this->CategoryDTO = $CategoryDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CategoryDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CategoryDTO
     */
    public function offsetGet($offset)
    {
      return $this->CategoryDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CategoryDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->CategoryDTO[] = $value;
      } else {
        $this->CategoryDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CategoryDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CategoryDTO Return the current element
     */
    public function current()
    {
      return current($this->CategoryDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CategoryDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CategoryDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CategoryDTO);
    }

    /**
     * Countable implementation
     *
     * @return CategoryDTO Return count of elements
     */
    public function count()
    {
      return count($this->CategoryDTO);
    }

}
