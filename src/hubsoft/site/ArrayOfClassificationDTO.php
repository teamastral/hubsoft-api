<?php

namespace hubsoft\api\site;

class ArrayOfClassificationDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ClassificationDTO[] $ClassificationDTO
     */
    protected $ClassificationDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ClassificationDTO[]
     */
    public function getClassificationDTO()
    {
      return $this->ClassificationDTO;
    }

    /**
     * @param ClassificationDTO[] $ClassificationDTO
     * @return \hubsoft\api\site\ArrayOfClassificationDTO
     */
    public function setClassificationDTO(array $ClassificationDTO = null)
    {
      $this->ClassificationDTO = $ClassificationDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ClassificationDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ClassificationDTO
     */
    public function offsetGet($offset)
    {
      return $this->ClassificationDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ClassificationDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ClassificationDTO[] = $value;
      } else {
        $this->ClassificationDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ClassificationDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ClassificationDTO Return the current element
     */
    public function current()
    {
      return current($this->ClassificationDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ClassificationDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ClassificationDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ClassificationDTO);
    }

    /**
     * Countable implementation
     *
     * @return ClassificationDTO Return count of elements
     */
    public function count()
    {
      return count($this->ClassificationDTO);
    }

}
