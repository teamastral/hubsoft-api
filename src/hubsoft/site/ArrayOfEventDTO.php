<?php

namespace hubsoft\api\site;

class ArrayOfEventDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var EventDTO[] $EventDTO
     */
    protected $EventDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return EventDTO[]
     */
    public function getEventDTO()
    {
      return $this->EventDTO;
    }

    /**
     * @param EventDTO[] $EventDTO
     * @return \hubsoft\api\site\ArrayOfEventDTO
     */
    public function setEventDTO(array $EventDTO = null)
    {
      $this->EventDTO = $EventDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->EventDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return EventDTO
     */
    public function offsetGet($offset)
    {
      return $this->EventDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param EventDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->EventDTO[] = $value;
      } else {
        $this->EventDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->EventDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return EventDTO Return the current element
     */
    public function current()
    {
      return current($this->EventDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->EventDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->EventDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->EventDTO);
    }

    /**
     * Countable implementation
     *
     * @return EventDTO Return count of elements
     */
    public function count()
    {
      return count($this->EventDTO);
    }

}
