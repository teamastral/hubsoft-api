<?php

namespace hubsoft\api\site;

class ArrayOfNewsDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var NewsDTO[] $NewsDTO
     */
    protected $NewsDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return NewsDTO[]
     */
    public function getNewsDTO()
    {
      return $this->NewsDTO;
    }

    /**
     * @param NewsDTO[] $NewsDTO
     * @return \hubsoft\api\site\ArrayOfNewsDTO
     */
    public function setNewsDTO(array $NewsDTO = null)
    {
      $this->NewsDTO = $NewsDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->NewsDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return NewsDTO
     */
    public function offsetGet($offset)
    {
      return $this->NewsDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param NewsDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->NewsDTO[] = $value;
      } else {
        $this->NewsDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->NewsDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return NewsDTO Return the current element
     */
    public function current()
    {
      return current($this->NewsDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->NewsDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->NewsDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->NewsDTO);
    }

    /**
     * Countable implementation
     *
     * @return NewsDTO Return count of elements
     */
    public function count()
    {
      return count($this->NewsDTO);
    }

}
