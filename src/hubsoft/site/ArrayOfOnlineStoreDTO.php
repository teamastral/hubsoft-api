<?php

namespace hubsoft\api\site;

class ArrayOfOnlineStoreDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var OnlineStoreDTO[] $OnlineStoreDTO
     */
    protected $OnlineStoreDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return OnlineStoreDTO[]
     */
    public function getOnlineStoreDTO()
    {
      return $this->OnlineStoreDTO;
    }

    /**
     * @param OnlineStoreDTO[] $OnlineStoreDTO
     * @return \hubsoft\api\site\ArrayOfOnlineStoreDTO
     */
    public function setOnlineStoreDTO(array $OnlineStoreDTO = null)
    {
      $this->OnlineStoreDTO = $OnlineStoreDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->OnlineStoreDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return OnlineStoreDTO
     */
    public function offsetGet($offset)
    {
      return $this->OnlineStoreDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param OnlineStoreDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->OnlineStoreDTO[] = $value;
      } else {
        $this->OnlineStoreDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->OnlineStoreDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return OnlineStoreDTO Return the current element
     */
    public function current()
    {
      return current($this->OnlineStoreDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->OnlineStoreDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->OnlineStoreDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->OnlineStoreDTO);
    }

    /**
     * Countable implementation
     *
     * @return OnlineStoreDTO Return count of elements
     */
    public function count()
    {
      return count($this->OnlineStoreDTO);
    }

}
