<?php

namespace hubsoft\api\site;

class ArrayOfSeasonDTO implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SeasonDTO[] $SeasonDTO
     */
    protected $SeasonDTO = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SeasonDTO[]
     */
    public function getSeasonDTO()
    {
      return $this->SeasonDTO;
    }

    /**
     * @param SeasonDTO[] $SeasonDTO
     * @return \hubsoft\api\site\ArrayOfSeasonDTO
     */
    public function setSeasonDTO(array $SeasonDTO = null)
    {
      $this->SeasonDTO = $SeasonDTO;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SeasonDTO[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SeasonDTO
     */
    public function offsetGet($offset)
    {
      return $this->SeasonDTO[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SeasonDTO $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->SeasonDTO[] = $value;
      } else {
        $this->SeasonDTO[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SeasonDTO[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SeasonDTO Return the current element
     */
    public function current()
    {
      return current($this->SeasonDTO);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SeasonDTO);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SeasonDTO);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SeasonDTO);
    }

    /**
     * Countable implementation
     *
     * @return SeasonDTO Return count of elements
     */
    public function count()
    {
      return count($this->SeasonDTO);
    }

}
