<?php

namespace hubsoft\api\site;

class CategoryDTO
{

    /**
     * @var string $categoryName
     */
    protected $categoryName = null;

    /**
     * @var int $categoryUID
     */
    protected $categoryUID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCategoryName()
    {
      return $this->categoryName;
    }

    /**
     * @param string $categoryName
     * @return \hubsoft\api\site\CategoryDTO
     */
    public function setCategoryName($categoryName)
    {
      $this->categoryName = $categoryName;
      return $this;
    }

    /**
     * @return int
     */
    public function getCategoryUID()
    {
      return $this->categoryUID;
    }

    /**
     * @param int $categoryUID
     * @return \hubsoft\api\site\CategoryDTO
     */
    public function setCategoryUID($categoryUID)
    {
      $this->categoryUID = $categoryUID;
      return $this;
    }

}
