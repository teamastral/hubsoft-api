<?php

namespace hubsoft\api\site;

class ClassificationDTO
{

    /**
     * @var string $classificationName
     */
    protected $classificationName = null;

    /**
     * @var int $classificationUID
     */
    protected $classificationUID = null;

    /**
     * @var int $parentClassificationUID
     */
    protected $parentClassificationUID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getClassificationName()
    {
      return $this->classificationName;
    }

    /**
     * @param string $classificationName
     * @return \hubsoft\api\site\ClassificationDTO
     */
    public function setClassificationName($classificationName)
    {
      $this->classificationName = $classificationName;
      return $this;
    }

    /**
     * @return int
     */
    public function getClassificationUID()
    {
      return $this->classificationUID;
    }

    /**
     * @param int $classificationUID
     * @return \hubsoft\api\site\ClassificationDTO
     */
    public function setClassificationUID($classificationUID)
    {
      $this->classificationUID = $classificationUID;
      return $this;
    }

    /**
     * @return int
     */
    public function getParentClassificationUID()
    {
      return $this->parentClassificationUID;
    }

    /**
     * @param int $parentClassificationUID
     * @return \hubsoft\api\site\ClassificationDTO
     */
    public function setParentClassificationUID($parentClassificationUID)
    {
      $this->parentClassificationUID = $parentClassificationUID;
      return $this;
    }

}
