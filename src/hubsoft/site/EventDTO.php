<?php

namespace hubsoft\api\site;

class EventDTO
{

    /**
     * @var string $briefDesc
     */
    protected $briefDesc = null;

    /**
     * @var int $categoryUID
     */
    protected $categoryUID = null;

    /**
     * @var string $eventDate
     */
    protected $eventDate = null;

    /**
     * @var string $eventName
     */
    protected $eventName = null;

    /**
     * @var int $eventUID
     */
    protected $eventUID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getBriefDesc()
    {
      return $this->briefDesc;
    }

    /**
     * @param string $briefDesc
     * @return \hubsoft\api\site\EventDTO
     */
    public function setBriefDesc($briefDesc)
    {
      $this->briefDesc = $briefDesc;
      return $this;
    }

    /**
     * @return int
     */
    public function getCategoryUID()
    {
      return $this->categoryUID;
    }

    /**
     * @param int $categoryUID
     * @return \hubsoft\api\site\EventDTO
     */
    public function setCategoryUID($categoryUID)
    {
      $this->categoryUID = $categoryUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getEventDate()
    {
      return $this->eventDate;
    }

    /**
     * @param string $eventDate
     * @return \hubsoft\api\site\EventDTO
     */
    public function setEventDate($eventDate)
    {
      $this->eventDate = $eventDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getEventName()
    {
      return $this->eventName;
    }

    /**
     * @param string $eventName
     * @return \hubsoft\api\site\EventDTO
     */
    public function setEventName($eventName)
    {
      $this->eventName = $eventName;
      return $this;
    }

    /**
     * @return int
     */
    public function getEventUID()
    {
      return $this->eventUID;
    }

    /**
     * @param int $eventUID
     * @return \hubsoft\api\site\EventDTO
     */
    public function setEventUID($eventUID)
    {
      $this->eventUID = $eventUID;
      return $this;
    }

}
