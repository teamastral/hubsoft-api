<?php

namespace hubsoft\api\site;

class NewsFullDTO
{

    /**
     * @var string $briefDesc
     */
    protected $briefDesc = null;

    /**
     * @var int $categoryUID
     */
    protected $categoryUID = null;

    /**
     * @var string $longDesc
     */
    protected $longDesc = null;

    /**
     * @var string $storyDate
     */
    protected $storyDate = null;

    /**
     * @var string $storyName
     */
    protected $storyName = null;

    /**
     * @var int $storyUID
     */
    protected $storyUID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getBriefDesc()
    {
      return $this->briefDesc;
    }

    /**
     * @param string $briefDesc
     * @return \hubsoft\api\site\NewsFullDTO
     */
    public function setBriefDesc($briefDesc)
    {
      $this->briefDesc = $briefDesc;
      return $this;
    }

    /**
     * @return int
     */
    public function getCategoryUID()
    {
      return $this->categoryUID;
    }

    /**
     * @param int $categoryUID
     * @return \hubsoft\api\site\NewsFullDTO
     */
    public function setCategoryUID($categoryUID)
    {
      $this->categoryUID = $categoryUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getLongDesc()
    {
      return $this->longDesc;
    }

    /**
     * @param string $longDesc
     * @return \hubsoft\api\site\NewsFullDTO
     */
    public function setLongDesc($longDesc)
    {
      $this->longDesc = $longDesc;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoryDate()
    {
      return $this->storyDate;
    }

    /**
     * @param string $storyDate
     * @return \hubsoft\api\site\NewsFullDTO
     */
    public function setStoryDate($storyDate)
    {
      $this->storyDate = $storyDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoryName()
    {
      return $this->storyName;
    }

    /**
     * @param string $storyName
     * @return \hubsoft\api\site\NewsFullDTO
     */
    public function setStoryName($storyName)
    {
      $this->storyName = $storyName;
      return $this;
    }

    /**
     * @return int
     */
    public function getStoryUID()
    {
      return $this->storyUID;
    }

    /**
     * @param int $storyUID
     * @return \hubsoft\api\site\NewsFullDTO
     */
    public function setStoryUID($storyUID)
    {
      $this->storyUID = $storyUID;
      return $this;
    }

}
