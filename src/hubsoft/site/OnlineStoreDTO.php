<?php

namespace hubsoft\api\site;

class OnlineStoreDTO
{

    /**
     * @var string $storeName
     */
    protected $storeName = null;

    /**
     * @var string $storeURL
     */
    protected $storeURL = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getStoreName()
    {
      return $this->storeName;
    }

    /**
     * @param string $storeName
     * @return \hubsoft\api\site\OnlineStoreDTO
     */
    public function setStoreName($storeName)
    {
      $this->storeName = $storeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreURL()
    {
      return $this->storeURL;
    }

    /**
     * @param string $storeURL
     * @return \hubsoft\api\site\OnlineStoreDTO
     */
    public function setStoreURL($storeURL)
    {
      $this->storeURL = $storeURL;
      return $this;
    }

}
