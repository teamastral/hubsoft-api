<?php

namespace hubsoft\api\site;

class SeasonDTO
{

    /**
     * @var string $seasonName
     */
    protected $seasonName = null;

    /**
     * @var int $seasonUID
     */
    protected $seasonUID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSeasonName()
    {
      return $this->seasonName;
    }

    /**
     * @param string $seasonName
     * @return \hubsoft\api\site\SeasonDTO
     */
    public function setSeasonName($seasonName)
    {
      $this->seasonName = $seasonName;
      return $this;
    }

    /**
     * @return int
     */
    public function getSeasonUID()
    {
      return $this->seasonUID;
    }

    /**
     * @param int $seasonUID
     * @return \hubsoft\api\site\SeasonDTO
     */
    public function setSeasonUID($seasonUID)
    {
      $this->seasonUID = $seasonUID;
      return $this;
    }

}
