<?php

namespace hubsoft\api\site;

class SiteService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'getEventCategories' => 'hubsoft\\api\\site\\getEventCategories',
      'getEventCategoriesResponse' => 'hubsoft\\api\\site\\getEventCategoriesResponse',
      'getStoreByInvoice' => 'hubsoft\\api\\site\\getStoreByInvoice',
      'getStoreByInvoiceResponse' => 'hubsoft\\api\\site\\getStoreByInvoiceResponse',
      'getProducts' => 'hubsoft\\api\\site\\getProducts',
      'getProductsResponse' => 'hubsoft\\api\\site\\getProductsResponse',
      'changeUserPassword' => 'hubsoft\\api\\site\\changeUserPassword',
      'changeUserPasswordResponse' => 'hubsoft\\api\\site\\changeUserPasswordResponse',
      'getNewsDetail' => 'hubsoft\\api\\site\\getNewsDetail',
      'getNewsDetailResponse' => 'hubsoft\\api\\site\\getNewsDetailResponse',
      'getSeasons' => 'hubsoft\\api\\site\\getSeasons',
      'getSeasonsResponse' => 'hubsoft\\api\\site\\getSeasonsResponse',
      'getClassifications' => 'hubsoft\\api\\site\\getClassifications',
      'getClassificationsResponse' => 'hubsoft\\api\\site\\getClassificationsResponse',
      'getClassification' => 'hubsoft\\api\\site\\getClassification',
      'getClassificationResponse' => 'hubsoft\\api\\site\\getClassificationResponse',
      'getSeason' => 'hubsoft\\api\\site\\getSeason',
      'getSeasonResponse' => 'hubsoft\\api\\site\\getSeasonResponse',
      'getNewsCategories' => 'hubsoft\\api\\site\\getNewsCategories',
      'getNewsCategoriesResponse' => 'hubsoft\\api\\site\\getNewsCategoriesResponse',
      'getEvents' => 'hubsoft\\api\\site\\getEvents',
      'getEventsResponse' => 'hubsoft\\api\\site\\getEventsResponse',
      'createUser' => 'hubsoft\\api\\site\\createUser',
      'createUserResponse' => 'hubsoft\\api\\site\\createUserResponse',
      'getRelatedProducts' => 'hubsoft\\api\\site\\getRelatedProducts',
      'getRelatedProductsResponse' => 'hubsoft\\api\\site\\getRelatedProductsResponse',
      'getProductColors' => 'hubsoft\\api\\site\\getProductColors',
      'getProductColorsResponse' => 'hubsoft\\api\\site\\getProductColorsResponse',
      'authUser' => 'hubsoft\\api\\site\\authUser',
      'authUserResponse' => 'hubsoft\\api\\site\\authUserResponse',
      'getProduct' => 'hubsoft\\api\\site\\getProduct',
      'getProductResponse' => 'hubsoft\\api\\site\\getProductResponse',
      'getStores' => 'hubsoft\\api\\site\\getStores',
      'getStoresResponse' => 'hubsoft\\api\\site\\getStoresResponse',
      'getOnlineStores' => 'hubsoft\\api\\site\\getOnlineStores',
      'getOnlineStoresResponse' => 'hubsoft\\api\\site\\getOnlineStoresResponse',
      'getNews' => 'hubsoft\\api\\site\\getNews',
      'getNewsResponse' => 'hubsoft\\api\\site\\getNewsResponse',
      'resetUserPassword' => 'hubsoft\\api\\site\\resetUserPassword',
      'resetUserPasswordResponse' => 'hubsoft\\api\\site\\resetUserPasswordResponse',
      'getEventDetail' => 'hubsoft\\api\\site\\getEventDetail',
      'getEventDetailResponse' => 'hubsoft\\api\\site\\getEventDetailResponse',
      'ArrayOfCategoryDTO' => 'hubsoft\\api\\site\\ArrayOfCategoryDTO',
      'CategoryDTO' => 'hubsoft\\api\\site\\CategoryDTO',
      'ServiceDisabledException' => 'hubsoft\\api\\site\\ServiceDisabledException',
      'AuthenticationFailureException' => 'hubsoft\\api\\site\\AuthenticationFailureException',
      'AccountStoreDTO' => 'hubsoft\\api\\site\\AccountStoreDTO',
      'ArrayOfProductDTO' => 'hubsoft\\api\\site\\ArrayOfProductDTO',
      'ProductDTO' => 'hubsoft\\api\\site\\ProductDTO',
      'NewsFullDTO' => 'hubsoft\\api\\site\\NewsFullDTO',
      'ArrayOfSeasonDTO' => 'hubsoft\\api\\site\\ArrayOfSeasonDTO',
      'SeasonDTO' => 'hubsoft\\api\\site\\SeasonDTO',
      'ArrayOfClassificationDTO' => 'hubsoft\\api\\site\\ArrayOfClassificationDTO',
      'ClassificationDTO' => 'hubsoft\\api\\site\\ClassificationDTO',
      'ArrayOfEventDTO' => 'hubsoft\\api\\site\\ArrayOfEventDTO',
      'EventDTO' => 'hubsoft\\api\\site\\EventDTO',
      'UserDTO' => 'hubsoft\\api\\site\\UserDTO',
      'ValidationFailureException' => 'hubsoft\\api\\site\\ValidationFailureException',
      'ArrayOfStoreDTO' => 'hubsoft\\api\\site\\ArrayOfStoreDTO',
      'StoreDTO' => 'hubsoft\\api\\site\\StoreDTO',
      'ArrayOfOnlineStoreDTO' => 'hubsoft\\api\\site\\ArrayOfOnlineStoreDTO',
      'OnlineStoreDTO' => 'hubsoft\\api\\site\\OnlineStoreDTO',
      'ArrayOfNewsDTO' => 'hubsoft\\api\\site\\ArrayOfNewsDTO',
      'NewsDTO' => 'hubsoft\\api\\site\\NewsDTO',
      'EventFullDTO' => 'hubsoft\\api\\site\\EventFullDTO',
      'LoginFailureException' => 'hubsoft\\api\\site\\LoginFailureException',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'https://ws.hubsoft.com/services/SiteService?wsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param getEventCategories $parameters
     * @return getEventCategoriesResponse
     */
    public function getEventCategories(getEventCategories $parameters)
    {
      return $this->__soapCall('getEventCategories', array($parameters));
    }

    /**
     * @param getStoreByInvoice $parameters
     * @return getStoreByInvoiceResponse
     */
    public function getStoreByInvoice(getStoreByInvoice $parameters)
    {
      return $this->__soapCall('getStoreByInvoice', array($parameters));
    }

    /**
     * @param getProducts $parameters
     * @return getProductsResponse
     */
    public function getProducts(getProducts $parameters)
    {
      return $this->__soapCall('getProducts', array($parameters));
    }

    /**
     * @param changeUserPassword $parameters
     * @return changeUserPasswordResponse
     */
    public function changeUserPassword(changeUserPassword $parameters)
    {
      return $this->__soapCall('changeUserPassword', array($parameters));
    }

    /**
     * @param getNewsDetail $parameters
     * @return getNewsDetailResponse
     */
    public function getNewsDetail(getNewsDetail $parameters)
    {
      return $this->__soapCall('getNewsDetail', array($parameters));
    }

    /**
     * @param getSeasons $parameters
     * @return getSeasonsResponse
     */
    public function getSeasons(getSeasons $parameters)
    {
      return $this->__soapCall('getSeasons', array($parameters));
    }

    /**
     * @param getClassifications $parameters
     * @return getClassificationsResponse
     */
    public function getClassifications(getClassifications $parameters)
    {
      return $this->__soapCall('getClassifications', array($parameters));
    }

    /**
     * @param getClassification $parameters
     * @return getClassificationResponse
     */
    public function getClassification(getClassification $parameters)
    {
      return $this->__soapCall('getClassification', array($parameters));
    }

    /**
     * @param getSeason $parameters
     * @return getSeasonResponse
     */
    public function getSeason(getSeason $parameters)
    {
      return $this->__soapCall('getSeason', array($parameters));
    }

    /**
     * @param getNewsCategories $parameters
     * @return getNewsCategoriesResponse
     */
    public function getNewsCategories(getNewsCategories $parameters)
    {
      return $this->__soapCall('getNewsCategories', array($parameters));
    }

    /**
     * @param getEvents $parameters
     * @return getEventsResponse
     */
    public function getEvents(getEvents $parameters)
    {
      return $this->__soapCall('getEvents', array($parameters));
    }

    /**
     * @param createUser $parameters
     * @return createUserResponse
     */
    public function createUser(createUser $parameters)
    {
      return $this->__soapCall('createUser', array($parameters));
    }

    /**
     * @param getRelatedProducts $parameters
     * @return getRelatedProductsResponse
     */
    public function getRelatedProducts(getRelatedProducts $parameters)
    {
      return $this->__soapCall('getRelatedProducts', array($parameters));
    }

    /**
     * @param getProductColors $parameters
     * @return getProductColorsResponse
     */
    public function getProductColors(getProductColors $parameters)
    {
      return $this->__soapCall('getProductColors', array($parameters));
    }

    /**
     * @param authUser $parameters
     * @return authUserResponse
     */
    public function authUser(authUser $parameters)
    {
      return $this->__soapCall('authUser', array($parameters));
    }

    /**
     * @param getProduct $parameters
     * @return getProductResponse
     */
    public function getProduct(getProduct $parameters)
    {
      return $this->__soapCall('getProduct', array($parameters));
    }

    /**
     * @param getStores $parameters
     * @return getStoresResponse
     */
    public function getStores(getStores $parameters)
    {
      return $this->__soapCall('getStores', array($parameters));
    }

    /**
     * @param getOnlineStores $parameters
     * @return getOnlineStoresResponse
     */
    public function getOnlineStores(getOnlineStores $parameters)
    {
      return $this->__soapCall('getOnlineStores', array($parameters));
    }

    /**
     * @param getNews $parameters
     * @return getNewsResponse
     */
    public function getNews(getNews $parameters)
    {
      return $this->__soapCall('getNews', array($parameters));
    }

    /**
     * @param resetUserPassword $parameters
     * @return resetUserPasswordResponse
     */
    public function resetUserPassword(resetUserPassword $parameters)
    {
      return $this->__soapCall('resetUserPassword', array($parameters));
    }

    /**
     * @param getEventDetail $parameters
     * @return getEventDetailResponse
     */
    public function getEventDetail(getEventDetail $parameters)
    {
      return $this->__soapCall('getEventDetail', array($parameters));
    }

}
