<?php

namespace hubsoft\api\site;

class StoreDTO
{

    /**
     * @var string $accountCode
     */
    protected $accountCode = null;

    /**
     * @var string $accountName
     */
    protected $accountName = null;

    /**
     * @var string $accountNotes
     */
    protected $accountNotes = null;

    /**
     * @var string $accountTypeCode
     */
    protected $accountTypeCode = null;

    /**
     * @var string $city
     */
    protected $city = null;

    /**
     * @var string $countryCode
     */
    protected $countryCode = null;

    /**
     * @var string $latitude
     */
    protected $latitude = null;

    /**
     * @var string $longitude
     */
    protected $longitude = null;

    /**
     * @var string $notes
     */
    protected $notes = null;

    /**
     * @var string $phoneNumber
     */
    protected $phoneNumber = null;

    /**
     * @var string $stateCode
     */
    protected $stateCode = null;

    /**
     * @var string $storeCode
     */
    protected $storeCode = null;

    /**
     * @var string $storeName
     */
    protected $storeName = null;

    /**
     * @var string $storeTypeName
     */
    protected $storeTypeName = null;

    /**
     * @var string $storeURL
     */
    protected $storeURL = null;

    /**
     * @var string $street
     */
    protected $street = null;

    /**
     * @var string $street2
     */
    protected $street2 = null;

    /**
     * @var string $zip
     */
    protected $zip = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAccountCode()
    {
      return $this->accountCode;
    }

    /**
     * @param string $accountCode
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setAccountCode($accountCode)
    {
      $this->accountCode = $accountCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccountName()
    {
      return $this->accountName;
    }

    /**
     * @param string $accountName
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setAccountName($accountName)
    {
      $this->accountName = $accountName;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccountNotes()
    {
      return $this->accountNotes;
    }

    /**
     * @param string $accountNotes
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setAccountNotes($accountNotes)
    {
      $this->accountNotes = $accountNotes;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccountTypeCode()
    {
      return $this->accountTypeCode;
    }

    /**
     * @param string $accountTypeCode
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setAccountTypeCode($accountTypeCode)
    {
      $this->accountTypeCode = $accountTypeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
      return $this->city;
    }

    /**
     * @param string $city
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setCity($city)
    {
      $this->city = $city;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
      return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setCountryCode($countryCode)
    {
      $this->countryCode = $countryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
      return $this->latitude;
    }

    /**
     * @param string $latitude
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setLatitude($latitude)
    {
      $this->latitude = $latitude;
      return $this;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
      return $this->longitude;
    }

    /**
     * @param string $longitude
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setLongitude($longitude)
    {
      $this->longitude = $longitude;
      return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
      return $this->notes;
    }

    /**
     * @param string $notes
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setNotes($notes)
    {
      $this->notes = $notes;
      return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
      return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setPhoneNumber($phoneNumber)
    {
      $this->phoneNumber = $phoneNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getStateCode()
    {
      return $this->stateCode;
    }

    /**
     * @param string $stateCode
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setStateCode($stateCode)
    {
      $this->stateCode = $stateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
      return $this->storeCode;
    }

    /**
     * @param string $storeCode
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setStoreCode($storeCode)
    {
      $this->storeCode = $storeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreName()
    {
      return $this->storeName;
    }

    /**
     * @param string $storeName
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setStoreName($storeName)
    {
      $this->storeName = $storeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreTypeName()
    {
      return $this->storeTypeName;
    }

    /**
     * @param string $storeTypeName
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setStoreTypeName($storeTypeName)
    {
      $this->storeTypeName = $storeTypeName;
      return $this;
    }

    /**
     * @return string
     */
    public function getStoreURL()
    {
      return $this->storeURL;
    }

    /**
     * @param string $storeURL
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setStoreURL($storeURL)
    {
      $this->storeURL = $storeURL;
      return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
      return $this->street;
    }

    /**
     * @param string $street
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setStreet($street)
    {
      $this->street = $street;
      return $this;
    }

    /**
     * @return string
     */
    public function getStreet2()
    {
      return $this->street2;
    }

    /**
     * @param string $street2
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setStreet2($street2)
    {
      $this->street2 = $street2;
      return $this;
    }

    /**
     * @return string
     */
    public function getZip()
    {
      return $this->zip;
    }

    /**
     * @param string $zip
     * @return \hubsoft\api\site\StoreDTO
     */
    public function setZip($zip)
    {
      $this->zip = $zip;
      return $this;
    }

}
