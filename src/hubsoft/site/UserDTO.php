<?php

namespace hubsoft\api\site;

class UserDTO
{

    /**
     * @var string $agency
     */
    protected $agency = null;

    /**
     * @var string $catalogCode
     */
    protected $catalogCode = null;

    /**
     * @var string $contactPhone
     */
    protected $contactPhone = null;

    /**
     * @var string $countryCode
     */
    protected $countryCode = null;

    /**
     * @var string $currencyCode
     */
    protected $currencyCode = null;

    /**
     * @var string $email
     */
    protected $email = null;

    /**
     * @var string $employeeCode
     */
    protected $employeeCode = null;

    /**
     * @var string $fax
     */
    protected $fax = null;

    /**
     * @var string $firstName
     */
    protected $firstName = null;

    /**
     * @var string $isSalesRep
     */
    protected $isSalesRep = null;

    /**
     * @var string $languageCode
     */
    protected $languageCode = null;

    /**
     * @var string $lastName
     */
    protected $lastName = null;

    /**
     * @var string $mobilePhone
     */
    protected $mobilePhone = null;

    /**
     * @var string $password
     */
    protected $password = null;

    /**
     * @var string $salesRepCode
     */
    protected $salesRepCode = null;

    /**
     * @var string $statusCode
     */
    protected $statusCode = null;

    /**
     * @var string $userGroupCode
     */
    protected $userGroupCode = null;

    /**
     * @var string $username
     */
    protected $username = null;

    /**
     * @var string $warehouseCode
     */
    protected $warehouseCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAgency()
    {
      return $this->agency;
    }

    /**
     * @param string $agency
     * @return \hubsoft\api\site\UserDTO
     */
    public function setAgency($agency)
    {
      $this->agency = $agency;
      return $this;
    }

    /**
     * @return string
     */
    public function getCatalogCode()
    {
      return $this->catalogCode;
    }

    /**
     * @param string $catalogCode
     * @return \hubsoft\api\site\UserDTO
     */
    public function setCatalogCode($catalogCode)
    {
      $this->catalogCode = $catalogCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
      return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     * @return \hubsoft\api\site\UserDTO
     */
    public function setContactPhone($contactPhone)
    {
      $this->contactPhone = $contactPhone;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
      return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return \hubsoft\api\site\UserDTO
     */
    public function setCountryCode($countryCode)
    {
      $this->countryCode = $countryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
      return $this->currencyCode;
    }

    /**
     * @param string $currencyCode
     * @return \hubsoft\api\site\UserDTO
     */
    public function setCurrencyCode($currencyCode)
    {
      $this->currencyCode = $currencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
      return $this->email;
    }

    /**
     * @param string $email
     * @return \hubsoft\api\site\UserDTO
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmployeeCode()
    {
      return $this->employeeCode;
    }

    /**
     * @param string $employeeCode
     * @return \hubsoft\api\site\UserDTO
     */
    public function setEmployeeCode($employeeCode)
    {
      $this->employeeCode = $employeeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getFax()
    {
      return $this->fax;
    }

    /**
     * @param string $fax
     * @return \hubsoft\api\site\UserDTO
     */
    public function setFax($fax)
    {
      $this->fax = $fax;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return \hubsoft\api\site\UserDTO
     */
    public function setFirstName($firstName)
    {
      $this->firstName = $firstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getIsSalesRep()
    {
      return $this->isSalesRep;
    }

    /**
     * @param string $isSalesRep
     * @return \hubsoft\api\site\UserDTO
     */
    public function setIsSalesRep($isSalesRep)
    {
      $this->isSalesRep = $isSalesRep;
      return $this;
    }

    /**
     * @return string
     */
    public function getLanguageCode()
    {
      return $this->languageCode;
    }

    /**
     * @param string $languageCode
     * @return \hubsoft\api\site\UserDTO
     */
    public function setLanguageCode($languageCode)
    {
      $this->languageCode = $languageCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return \hubsoft\api\site\UserDTO
     */
    public function setLastName($lastName)
    {
      $this->lastName = $lastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMobilePhone()
    {
      return $this->mobilePhone;
    }

    /**
     * @param string $mobilePhone
     * @return \hubsoft\api\site\UserDTO
     */
    public function setMobilePhone($mobilePhone)
    {
      $this->mobilePhone = $mobilePhone;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
      return $this->password;
    }

    /**
     * @param string $password
     * @return \hubsoft\api\site\UserDTO
     */
    public function setPassword($password)
    {
      $this->password = $password;
      return $this;
    }

    /**
     * @return string
     */
    public function getSalesRepCode()
    {
      return $this->salesRepCode;
    }

    /**
     * @param string $salesRepCode
     * @return \hubsoft\api\site\UserDTO
     */
    public function setSalesRepCode($salesRepCode)
    {
      $this->salesRepCode = $salesRepCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
      return $this->statusCode;
    }

    /**
     * @param string $statusCode
     * @return \hubsoft\api\site\UserDTO
     */
    public function setStatusCode($statusCode)
    {
      $this->statusCode = $statusCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserGroupCode()
    {
      return $this->userGroupCode;
    }

    /**
     * @param string $userGroupCode
     * @return \hubsoft\api\site\UserDTO
     */
    public function setUserGroupCode($userGroupCode)
    {
      $this->userGroupCode = $userGroupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
      return $this->username;
    }

    /**
     * @param string $username
     * @return \hubsoft\api\site\UserDTO
     */
    public function setUsername($username)
    {
      $this->username = $username;
      return $this;
    }

    /**
     * @return string
     */
    public function getWarehouseCode()
    {
      return $this->warehouseCode;
    }

    /**
     * @param string $warehouseCode
     * @return \hubsoft\api\site\UserDTO
     */
    public function setWarehouseCode($warehouseCode)
    {
      $this->warehouseCode = $warehouseCode;
      return $this;
    }

}
