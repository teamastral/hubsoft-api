<?php

namespace hubsoft\api\site;

class authUser
{

    /**
     * @var string $userName
     */
    protected $userName = null;

    /**
     * @var string $password
     */
    protected $password = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $userName
     * @param string $password
     * @param string $accessKey
     */
    public function __construct($userName, $password, $accessKey)
    {
      $this->userName = $userName;
      $this->password = $password;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->userName;
    }

    /**
     * @param string $userName
     * @return \hubsoft\api\site\authUser
     */
    public function setUserName($userName)
    {
      $this->userName = $userName;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
      return $this->password;
    }

    /**
     * @param string $password
     * @return \hubsoft\api\site\authUser
     */
    public function setPassword($password)
    {
      $this->password = $password;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\authUser
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
