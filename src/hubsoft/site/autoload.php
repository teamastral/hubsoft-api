<?php


 function autoload_f1a97f0266eeff39e2f2c521bd2f1598($class)
{
    $classes = array(
        'hubsoft\api\site\SiteService' => __DIR__ .'/SiteService.php',
        'hubsoft\api\site\getEventCategories' => __DIR__ .'/getEventCategories.php',
        'hubsoft\api\site\getEventCategoriesResponse' => __DIR__ .'/getEventCategoriesResponse.php',
        'hubsoft\api\site\getStoreByInvoice' => __DIR__ .'/getStoreByInvoice.php',
        'hubsoft\api\site\getStoreByInvoiceResponse' => __DIR__ .'/getStoreByInvoiceResponse.php',
        'hubsoft\api\site\getProducts' => __DIR__ .'/getProducts.php',
        'hubsoft\api\site\getProductsResponse' => __DIR__ .'/getProductsResponse.php',
        'hubsoft\api\site\changeUserPassword' => __DIR__ .'/changeUserPassword.php',
        'hubsoft\api\site\changeUserPasswordResponse' => __DIR__ .'/changeUserPasswordResponse.php',
        'hubsoft\api\site\getNewsDetail' => __DIR__ .'/getNewsDetail.php',
        'hubsoft\api\site\getNewsDetailResponse' => __DIR__ .'/getNewsDetailResponse.php',
        'hubsoft\api\site\getSeasons' => __DIR__ .'/getSeasons.php',
        'hubsoft\api\site\getSeasonsResponse' => __DIR__ .'/getSeasonsResponse.php',
        'hubsoft\api\site\getClassifications' => __DIR__ .'/getClassifications.php',
        'hubsoft\api\site\getClassificationsResponse' => __DIR__ .'/getClassificationsResponse.php',
        'hubsoft\api\site\getClassification' => __DIR__ .'/getClassification.php',
        'hubsoft\api\site\getClassificationResponse' => __DIR__ .'/getClassificationResponse.php',
        'hubsoft\api\site\getSeason' => __DIR__ .'/getSeason.php',
        'hubsoft\api\site\getSeasonResponse' => __DIR__ .'/getSeasonResponse.php',
        'hubsoft\api\site\getNewsCategories' => __DIR__ .'/getNewsCategories.php',
        'hubsoft\api\site\getNewsCategoriesResponse' => __DIR__ .'/getNewsCategoriesResponse.php',
        'hubsoft\api\site\getEvents' => __DIR__ .'/getEvents.php',
        'hubsoft\api\site\getEventsResponse' => __DIR__ .'/getEventsResponse.php',
        'hubsoft\api\site\createUser' => __DIR__ .'/createUser.php',
        'hubsoft\api\site\createUserResponse' => __DIR__ .'/createUserResponse.php',
        'hubsoft\api\site\getRelatedProducts' => __DIR__ .'/getRelatedProducts.php',
        'hubsoft\api\site\getRelatedProductsResponse' => __DIR__ .'/getRelatedProductsResponse.php',
        'hubsoft\api\site\getProductColors' => __DIR__ .'/getProductColors.php',
        'hubsoft\api\site\getProductColorsResponse' => __DIR__ .'/getProductColorsResponse.php',
        'hubsoft\api\site\authUser' => __DIR__ .'/authUser.php',
        'hubsoft\api\site\authUserResponse' => __DIR__ .'/authUserResponse.php',
        'hubsoft\api\site\getProduct' => __DIR__ .'/getProduct.php',
        'hubsoft\api\site\getProductResponse' => __DIR__ .'/getProductResponse.php',
        'hubsoft\api\site\getStores' => __DIR__ .'/getStores.php',
        'hubsoft\api\site\getStoresResponse' => __DIR__ .'/getStoresResponse.php',
        'hubsoft\api\site\getOnlineStores' => __DIR__ .'/getOnlineStores.php',
        'hubsoft\api\site\getOnlineStoresResponse' => __DIR__ .'/getOnlineStoresResponse.php',
        'hubsoft\api\site\getNews' => __DIR__ .'/getNews.php',
        'hubsoft\api\site\getNewsResponse' => __DIR__ .'/getNewsResponse.php',
        'hubsoft\api\site\resetUserPassword' => __DIR__ .'/resetUserPassword.php',
        'hubsoft\api\site\resetUserPasswordResponse' => __DIR__ .'/resetUserPasswordResponse.php',
        'hubsoft\api\site\getEventDetail' => __DIR__ .'/getEventDetail.php',
        'hubsoft\api\site\getEventDetailResponse' => __DIR__ .'/getEventDetailResponse.php',
        'hubsoft\api\site\ArrayOfCategoryDTO' => __DIR__ .'/ArrayOfCategoryDTO.php',
        'hubsoft\api\site\CategoryDTO' => __DIR__ .'/CategoryDTO.php',
        'hubsoft\api\site\ServiceDisabledException' => __DIR__ .'/ServiceDisabledException.php',
        'hubsoft\api\site\AuthenticationFailureException' => __DIR__ .'/AuthenticationFailureException.php',
        'hubsoft\api\site\AccountStoreDTO' => __DIR__ .'/AccountStoreDTO.php',
        'hubsoft\api\site\ArrayOfProductDTO' => __DIR__ .'/ArrayOfProductDTO.php',
        'hubsoft\api\site\ProductDTO' => __DIR__ .'/ProductDTO.php',
        'hubsoft\api\site\NewsFullDTO' => __DIR__ .'/NewsFullDTO.php',
        'hubsoft\api\site\ArrayOfSeasonDTO' => __DIR__ .'/ArrayOfSeasonDTO.php',
        'hubsoft\api\site\SeasonDTO' => __DIR__ .'/SeasonDTO.php',
        'hubsoft\api\site\ArrayOfClassificationDTO' => __DIR__ .'/ArrayOfClassificationDTO.php',
        'hubsoft\api\site\ClassificationDTO' => __DIR__ .'/ClassificationDTO.php',
        'hubsoft\api\site\ArrayOfEventDTO' => __DIR__ .'/ArrayOfEventDTO.php',
        'hubsoft\api\site\EventDTO' => __DIR__ .'/EventDTO.php',
        'hubsoft\api\site\UserDTO' => __DIR__ .'/UserDTO.php',
        'hubsoft\api\site\ValidationFailureException' => __DIR__ .'/ValidationFailureException.php',
        'hubsoft\api\site\ArrayOfStoreDTO' => __DIR__ .'/ArrayOfStoreDTO.php',
        'hubsoft\api\site\StoreDTO' => __DIR__ .'/StoreDTO.php',
        'hubsoft\api\site\ArrayOfOnlineStoreDTO' => __DIR__ .'/ArrayOfOnlineStoreDTO.php',
        'hubsoft\api\site\OnlineStoreDTO' => __DIR__ .'/OnlineStoreDTO.php',
        'hubsoft\api\site\ArrayOfNewsDTO' => __DIR__ .'/ArrayOfNewsDTO.php',
        'hubsoft\api\site\NewsDTO' => __DIR__ .'/NewsDTO.php',
        'hubsoft\api\site\EventFullDTO' => __DIR__ .'/EventFullDTO.php',
        'hubsoft\api\site\LoginFailureException' => __DIR__ .'/LoginFailureException.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_f1a97f0266eeff39e2f2c521bd2f1598');

// Do nothing. The rest is just leftovers from the code generation.
{
}
