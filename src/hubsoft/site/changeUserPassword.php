<?php

namespace hubsoft\api\site;

class changeUserPassword
{

    /**
     * @var string $userName
     */
    protected $userName = null;

    /**
     * @var string $oldPassword
     */
    protected $oldPassword = null;

    /**
     * @var string $newPassword
     */
    protected $newPassword = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $userName
     * @param string $oldPassword
     * @param string $newPassword
     * @param string $accessKey
     */
    public function __construct($userName, $oldPassword, $newPassword, $accessKey)
    {
      $this->userName = $userName;
      $this->oldPassword = $oldPassword;
      $this->newPassword = $newPassword;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->userName;
    }

    /**
     * @param string $userName
     * @return \hubsoft\api\site\changeUserPassword
     */
    public function setUserName($userName)
    {
      $this->userName = $userName;
      return $this;
    }

    /**
     * @return string
     */
    public function getOldPassword()
    {
      return $this->oldPassword;
    }

    /**
     * @param string $oldPassword
     * @return \hubsoft\api\site\changeUserPassword
     */
    public function setOldPassword($oldPassword)
    {
      $this->oldPassword = $oldPassword;
      return $this;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
      return $this->newPassword;
    }

    /**
     * @param string $newPassword
     * @return \hubsoft\api\site\changeUserPassword
     */
    public function setNewPassword($newPassword)
    {
      $this->newPassword = $newPassword;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\changeUserPassword
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
