<?php

namespace hubsoft\api\site;

class changeUserPasswordResponse
{

    /**
     * @var boolean $out
     */
    protected $out = null;

    /**
     * @param boolean $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return boolean
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param boolean $out
     * @return \hubsoft\api\site\changeUserPasswordResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
