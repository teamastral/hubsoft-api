<?php

namespace hubsoft\api\site;

class createUser
{

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @var UserDTO $userInfo
     */
    protected $userInfo = null;

    /**
     * @param string $accessKey
     * @param UserDTO $userInfo
     */
    public function __construct($accessKey, $userInfo)
    {
      $this->accessKey = $accessKey;
      $this->userInfo = $userInfo;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\createUser
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

    /**
     * @return UserDTO
     */
    public function getUserInfo()
    {
      return $this->userInfo;
    }

    /**
     * @param UserDTO $userInfo
     * @return \hubsoft\api\site\createUser
     */
    public function setUserInfo($userInfo)
    {
      $this->userInfo = $userInfo;
      return $this;
    }

}
