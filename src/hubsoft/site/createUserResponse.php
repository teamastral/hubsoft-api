<?php

namespace hubsoft\api\site;

class createUserResponse
{

    /**
     * @var boolean $out
     */
    protected $out = null;

    /**
     * @param boolean $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return boolean
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param boolean $out
     * @return \hubsoft\api\site\createUserResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
