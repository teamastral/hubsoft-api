<?php

namespace hubsoft\api\site;

class getClassification
{

    /**
     * @var int $classificationUID
     */
    protected $classificationUID = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param int $classificationUID
     * @param string $accessKey
     */
    public function __construct($classificationUID, $accessKey)
    {
      $this->classificationUID = $classificationUID;
      $this->accessKey = $accessKey;
    }

    /**
     * @return int
     */
    public function getClassificationUID()
    {
      return $this->classificationUID;
    }

    /**
     * @param int $classificationUID
     * @return \hubsoft\api\site\getClassification
     */
    public function setClassificationUID($classificationUID)
    {
      $this->classificationUID = $classificationUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getClassification
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
