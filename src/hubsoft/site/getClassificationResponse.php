<?php

namespace hubsoft\api\site;

class getClassificationResponse
{

    /**
     * @var ClassificationDTO $out
     */
    protected $out = null;

    /**
     * @param ClassificationDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ClassificationDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ClassificationDTO $out
     * @return \hubsoft\api\site\getClassificationResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
