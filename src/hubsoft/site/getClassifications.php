<?php

namespace hubsoft\api\site;

class getClassifications
{

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $accessKey
     */
    public function __construct($accessKey)
    {
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getClassifications
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
