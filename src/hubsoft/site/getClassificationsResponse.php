<?php

namespace hubsoft\api\site;

class getClassificationsResponse
{

    /**
     * @var ArrayOfClassificationDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfClassificationDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfClassificationDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfClassificationDTO $out
     * @return \hubsoft\api\site\getClassificationsResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
