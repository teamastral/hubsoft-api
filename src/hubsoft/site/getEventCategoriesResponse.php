<?php

namespace hubsoft\api\site;

class getEventCategoriesResponse
{

    /**
     * @var ArrayOfCategoryDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfCategoryDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfCategoryDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfCategoryDTO $out
     * @return \hubsoft\api\site\getEventCategoriesResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
