<?php

namespace hubsoft\api\site;

class getEventDetail
{

    /**
     * @var int $eventUID
     */
    protected $eventUID = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param int $eventUID
     * @param string $accessKey
     */
    public function __construct($eventUID, $accessKey)
    {
      $this->eventUID = $eventUID;
      $this->accessKey = $accessKey;
    }

    /**
     * @return int
     */
    public function getEventUID()
    {
      return $this->eventUID;
    }

    /**
     * @param int $eventUID
     * @return \hubsoft\api\site\getEventDetail
     */
    public function setEventUID($eventUID)
    {
      $this->eventUID = $eventUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getEventDetail
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
