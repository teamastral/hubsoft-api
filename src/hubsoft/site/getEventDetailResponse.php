<?php

namespace hubsoft\api\site;

class getEventDetailResponse
{

    /**
     * @var EventFullDTO $out
     */
    protected $out = null;

    /**
     * @param EventFullDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return EventFullDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param EventFullDTO $out
     * @return \hubsoft\api\site\getEventDetailResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
