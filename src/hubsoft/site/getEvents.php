<?php

namespace hubsoft\api\site;

class getEvents
{

    /**
     * @var int $categoryUID
     */
    protected $categoryUID = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param int $categoryUID
     * @param string $accessKey
     */
    public function __construct($categoryUID, $accessKey)
    {
      $this->categoryUID = $categoryUID;
      $this->accessKey = $accessKey;
    }

    /**
     * @return int
     */
    public function getCategoryUID()
    {
      return $this->categoryUID;
    }

    /**
     * @param int $categoryUID
     * @return \hubsoft\api\site\getEvents
     */
    public function setCategoryUID($categoryUID)
    {
      $this->categoryUID = $categoryUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getEvents
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
