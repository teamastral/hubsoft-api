<?php

namespace hubsoft\api\site;

class getEventsResponse
{

    /**
     * @var ArrayOfEventDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfEventDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfEventDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfEventDTO $out
     * @return \hubsoft\api\site\getEventsResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
