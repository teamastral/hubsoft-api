<?php

namespace hubsoft\api\site;

class getNewsCategories
{

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $accessKey
     */
    public function __construct($accessKey)
    {
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getNewsCategories
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
