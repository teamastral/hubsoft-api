<?php

namespace hubsoft\api\site;

class getNewsCategoriesResponse
{

    /**
     * @var ArrayOfCategoryDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfCategoryDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfCategoryDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfCategoryDTO $out
     * @return \hubsoft\api\site\getNewsCategoriesResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
