<?php

namespace hubsoft\api\site;

class getNewsDetail
{

    /**
     * @var int $newsUID
     */
    protected $newsUID = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param int $newsUID
     * @param string $accessKey
     */
    public function __construct($newsUID, $accessKey)
    {
      $this->newsUID = $newsUID;
      $this->accessKey = $accessKey;
    }

    /**
     * @return int
     */
    public function getNewsUID()
    {
      return $this->newsUID;
    }

    /**
     * @param int $newsUID
     * @return \hubsoft\api\site\getNewsDetail
     */
    public function setNewsUID($newsUID)
    {
      $this->newsUID = $newsUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getNewsDetail
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
