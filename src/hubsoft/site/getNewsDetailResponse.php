<?php

namespace hubsoft\api\site;

class getNewsDetailResponse
{

    /**
     * @var NewsFullDTO $out
     */
    protected $out = null;

    /**
     * @param NewsFullDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return NewsFullDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param NewsFullDTO $out
     * @return \hubsoft\api\site\getNewsDetailResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
