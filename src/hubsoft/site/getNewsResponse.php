<?php

namespace hubsoft\api\site;

class getNewsResponse
{

    /**
     * @var ArrayOfNewsDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfNewsDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfNewsDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfNewsDTO $out
     * @return \hubsoft\api\site\getNewsResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
