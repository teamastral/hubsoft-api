<?php

namespace hubsoft\api\site;

class getOnlineStores
{

    /**
     * @var int $howMany
     */
    protected $howMany = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param int $howMany
     * @param string $accessKey
     */
    public function __construct($howMany, $accessKey)
    {
      $this->howMany = $howMany;
      $this->accessKey = $accessKey;
    }

    /**
     * @return int
     */
    public function getHowMany()
    {
      return $this->howMany;
    }

    /**
     * @param int $howMany
     * @return \hubsoft\api\site\getOnlineStores
     */
    public function setHowMany($howMany)
    {
      $this->howMany = $howMany;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getOnlineStores
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
