<?php

namespace hubsoft\api\site;

class getOnlineStoresResponse
{

    /**
     * @var ArrayOfOnlineStoreDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfOnlineStoreDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfOnlineStoreDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfOnlineStoreDTO $out
     * @return \hubsoft\api\site\getOnlineStoresResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
