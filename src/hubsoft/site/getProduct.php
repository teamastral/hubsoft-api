<?php

namespace hubsoft\api\site;

class getProduct
{

    /**
     * @var string $productNumber
     */
    protected $productNumber = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $productNumber
     * @param string $accessKey
     */
    public function __construct($productNumber, $accessKey)
    {
      $this->productNumber = $productNumber;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
      return $this->productNumber;
    }

    /**
     * @param string $productNumber
     * @return \hubsoft\api\site\getProduct
     */
    public function setProductNumber($productNumber)
    {
      $this->productNumber = $productNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getProduct
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
