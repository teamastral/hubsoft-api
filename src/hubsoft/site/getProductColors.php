<?php

namespace hubsoft\api\site;

class getProductColors
{

    /**
     * @var int $productUID
     */
    protected $productUID = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param int $productUID
     * @param string $accessKey
     */
    public function __construct($productUID, $accessKey)
    {
      $this->productUID = $productUID;
      $this->accessKey = $accessKey;
    }

    /**
     * @return int
     */
    public function getProductUID()
    {
      return $this->productUID;
    }

    /**
     * @param int $productUID
     * @return \hubsoft\api\site\getProductColors
     */
    public function setProductUID($productUID)
    {
      $this->productUID = $productUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getProductColors
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
