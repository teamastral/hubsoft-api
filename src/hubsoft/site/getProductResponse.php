<?php

namespace hubsoft\api\site;

class getProductResponse
{

    /**
     * @var ProductDTO $out
     */
    protected $out = null;

    /**
     * @param ProductDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ProductDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ProductDTO $out
     * @return \hubsoft\api\site\getProductResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
