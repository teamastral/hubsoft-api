<?php

namespace hubsoft\api\site;

class getProducts
{

    /**
     * @var int $seasonUID
     */
    protected $seasonUID = null;

    /**
     * @var int $classificationUID
     */
    protected $classificationUID = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param int $seasonUID
     * @param int $classificationUID
     * @param string $accessKey
     */
    public function __construct($seasonUID, $classificationUID, $accessKey)
    {
      $this->seasonUID = $seasonUID;
      $this->classificationUID = $classificationUID;
      $this->accessKey = $accessKey;
    }

    /**
     * @return int
     */
    public function getSeasonUID()
    {
      return $this->seasonUID;
    }

    /**
     * @param int $seasonUID
     * @return \hubsoft\api\site\getProducts
     */
    public function setSeasonUID($seasonUID)
    {
      $this->seasonUID = $seasonUID;
      return $this;
    }

    /**
     * @return int
     */
    public function getClassificationUID()
    {
      return $this->classificationUID;
    }

    /**
     * @param int $classificationUID
     * @return \hubsoft\api\site\getProducts
     */
    public function setClassificationUID($classificationUID)
    {
      $this->classificationUID = $classificationUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getProducts
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
