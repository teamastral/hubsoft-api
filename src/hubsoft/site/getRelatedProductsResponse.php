<?php

namespace hubsoft\api\site;

class getRelatedProductsResponse
{

    /**
     * @var ArrayOfProductDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfProductDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfProductDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfProductDTO $out
     * @return \hubsoft\api\site\getRelatedProductsResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
