<?php

namespace hubsoft\api\site;

class getSeason
{

    /**
     * @var int $seasonUID
     */
    protected $seasonUID = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param int $seasonUID
     * @param string $accessKey
     */
    public function __construct($seasonUID, $accessKey)
    {
      $this->seasonUID = $seasonUID;
      $this->accessKey = $accessKey;
    }

    /**
     * @return int
     */
    public function getSeasonUID()
    {
      return $this->seasonUID;
    }

    /**
     * @param int $seasonUID
     * @return \hubsoft\api\site\getSeason
     */
    public function setSeasonUID($seasonUID)
    {
      $this->seasonUID = $seasonUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getSeason
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
