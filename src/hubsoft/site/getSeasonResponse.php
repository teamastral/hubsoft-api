<?php

namespace hubsoft\api\site;

class getSeasonResponse
{

    /**
     * @var SeasonDTO $out
     */
    protected $out = null;

    /**
     * @param SeasonDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return SeasonDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param SeasonDTO $out
     * @return \hubsoft\api\site\getSeasonResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
