<?php

namespace hubsoft\api\site;

class getSeasonsResponse
{

    /**
     * @var ArrayOfSeasonDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfSeasonDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfSeasonDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfSeasonDTO $out
     * @return \hubsoft\api\site\getSeasonsResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
