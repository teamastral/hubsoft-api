<?php

namespace hubsoft\api\site;

class getStoreByInvoice
{

    /**
     * @var string $invoiceDate
     */
    protected $invoiceDate = null;

    /**
     * @var string $invoiceNumber
     */
    protected $invoiceNumber = null;

    /**
     * @var string $invoiceAmount
     */
    protected $invoiceAmount = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $invoiceDate
     * @param string $invoiceNumber
     * @param string $invoiceAmount
     * @param string $accessKey
     */
    public function __construct($invoiceDate, $invoiceNumber, $invoiceAmount, $accessKey)
    {
      $this->invoiceDate = $invoiceDate;
      $this->invoiceNumber = $invoiceNumber;
      $this->invoiceAmount = $invoiceAmount;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getInvoiceDate()
    {
      return $this->invoiceDate;
    }

    /**
     * @param string $invoiceDate
     * @return \hubsoft\api\site\getStoreByInvoice
     */
    public function setInvoiceDate($invoiceDate)
    {
      $this->invoiceDate = $invoiceDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNumber()
    {
      return $this->invoiceNumber;
    }

    /**
     * @param string $invoiceNumber
     * @return \hubsoft\api\site\getStoreByInvoice
     */
    public function setInvoiceNumber($invoiceNumber)
    {
      $this->invoiceNumber = $invoiceNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceAmount()
    {
      return $this->invoiceAmount;
    }

    /**
     * @param string $invoiceAmount
     * @return \hubsoft\api\site\getStoreByInvoice
     */
    public function setInvoiceAmount($invoiceAmount)
    {
      $this->invoiceAmount = $invoiceAmount;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getStoreByInvoice
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
