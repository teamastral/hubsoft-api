<?php

namespace hubsoft\api\site;

class getStoreByInvoiceResponse
{

    /**
     * @var AccountStoreDTO $out
     */
    protected $out = null;

    /**
     * @param AccountStoreDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return AccountStoreDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param AccountStoreDTO $out
     * @return \hubsoft\api\site\getStoreByInvoiceResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
