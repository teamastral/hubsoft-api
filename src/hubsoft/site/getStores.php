<?php

namespace hubsoft\api\site;

class getStores
{

    /**
     * @var string $countryCode
     */
    protected $countryCode = null;

    /**
     * @var string $stateCode
     */
    protected $stateCode = null;

    /**
     * @var string $zip
     */
    protected $zip = null;

    /**
     * @var string $accessKey
     */
    protected $accessKey = null;

    /**
     * @param string $countryCode
     * @param string $stateCode
     * @param string $zip
     * @param string $accessKey
     */
    public function __construct($countryCode, $stateCode, $zip, $accessKey)
    {
      $this->countryCode = $countryCode;
      $this->stateCode = $stateCode;
      $this->zip = $zip;
      $this->accessKey = $accessKey;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
      return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return \hubsoft\api\site\getStores
     */
    public function setCountryCode($countryCode)
    {
      $this->countryCode = $countryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStateCode()
    {
      return $this->stateCode;
    }

    /**
     * @param string $stateCode
     * @return \hubsoft\api\site\getStores
     */
    public function setStateCode($stateCode)
    {
      $this->stateCode = $stateCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getZip()
    {
      return $this->zip;
    }

    /**
     * @param string $zip
     * @return \hubsoft\api\site\getStores
     */
    public function setZip($zip)
    {
      $this->zip = $zip;
      return $this;
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->accessKey;
    }

    /**
     * @param string $accessKey
     * @return \hubsoft\api\site\getStores
     */
    public function setAccessKey($accessKey)
    {
      $this->accessKey = $accessKey;
      return $this;
    }

}
