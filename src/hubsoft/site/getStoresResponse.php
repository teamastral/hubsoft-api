<?php

namespace hubsoft\api\site;

class getStoresResponse
{

    /**
     * @var ArrayOfStoreDTO $out
     */
    protected $out = null;

    /**
     * @param ArrayOfStoreDTO $out
     */
    public function __construct($out)
    {
      $this->out = $out;
    }

    /**
     * @return ArrayOfStoreDTO
     */
    public function getOut()
    {
      return $this->out;
    }

    /**
     * @param ArrayOfStoreDTO $out
     * @return \hubsoft\api\site\getStoresResponse
     */
    public function setOut($out)
    {
      $this->out = $out;
      return $this;
    }

}
