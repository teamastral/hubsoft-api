<?php
namespace hubsoft\tools;

use hubsoft\api\commerce\CommerceService;
use hubsoft\api\commerce\getActivePromotions;
use hubsoft\api\commerce\getPromotionDetail;
use hubsoft\api\integrationservice\ArrayOfDealerOrderDTO;
use hubsoft\api\integrationservice\ArrayOfDirectOrderUpdate2DTO;
use hubsoft\api\integrationservice\ArrayOfDirectOrderUpdateDTO;
use hubsoft\api\integrationservice\ArrayOfProductColorDTO;
use hubsoft\api\integrationservice\ArrayOfProductDTO;
use hubsoft\api\integrationservice\ArrayOfShippingNoticeDTO;
use hubsoft\api\integrationservice\DealerOrderDTO;
use hubsoft\api\integrationservice\DirectOrderUpdateDTO;
use hubsoft\api\integrationservice\exportDirectOrders;
use hubsoft\api\integrationservice\exportInventory;
use hubsoft\api\integrationservice\importDealerOrders;
use hubsoft\api\integrationservice\importDirectOrders;
use hubsoft\api\integrationservice\importDirectOrders2;
use hubsoft\api\integrationservice\importInventory;
use hubsoft\api\integrationservice\importShippingUpdate;
use hubsoft\api\integrationservice\IntegrationServiceV3;
use hubsoft\api\integrationservice\entry;
use hubsoft\api\integrationservice\exportDealerOrders;
use hubsoft\api\integrationservice\exportProducts;
use hubsoft\api\integrationservice\exportProductsResponse;
use hubsoft\api\integrationservice\string2stringMap;
use hubsoft\api\site\getClassifications;
use hubsoft\api\site\SiteService;

define("INTEGRATION_SERVICE_URL", "https://ws.hubsoft.com/services/IntegrationServiceV3?wsdl");
define("COMMERCE_SERVICE_URL", "https://ws.hubsoft.com/services/CommerceService?wsdl");
define("SITE_SERVICE_URL", "https://ws.hubsoft.com/services/SiteService?wsdl");

define("UAT_INTEGRATION_SERVICE_URL", "https://uat.hubsoft.com/services/IntegrationServiceV3?wsdl");
define("UAT_COMMERCE_SERVICE_URL", "https://uat.hubsoft.com/services/CommerceService?wsdl");
define("UAT_SITE_SERVICE_URL", "https://uat.hubsoft.com/services/SiteService?wsdl");

// TODO: More config options for each service soap clients
/**
 * Class HubsoftAPI
 */
class HubsoftAPI
{

    private $integrationService;
    private $isClient;
    private $apiKey;
    private $integrationServiceURL;
    private $commerceServiceURL;
    private $siteServiceURL;
    private $serviceConfig;
    // debugging stuff
    public $SoapDump = false;
    public $soapDumpPath = "/tmp/";
    private $soapClient;

    /**
     * HubsoftAPI constructor.
     * @param $apiKey
     */
    public function __construct($apiKey, $debug = false) {
        $this->apiKey = $apiKey;
        $this->integrationServiceURL = !$debug ? INTEGRATION_SERVICE_URL : UAT_INTEGRATION_SERVICE_URL;
        $this->siteServiceURL = $debug ? SITE_SERVICE_URL : UAT_SITE_SERVICE_URL;
        $this->commerceServiceURL = $debug ? COMMERCE_SERVICE_URL : UAT_COMMERCE_SERVICE_URL;
        $this->integrationService = new IntegrationServiceV3(array("trace" => 1), $this->integrationServiceURL);
        $this->serviceConfig = [
            new entry("access.key", $this->apiKey)
        ];
    }

    public function setSoapClient(\SoapClient $soapClient) {
        $this->soapClient = $soapClient;
    }

    public function getSoapClient() {
        return $this->soapClient;
    }
    /* ------------------------------------------------------
       Integration Service
    --------------------------------------------------------- */
    public function getIntegrationService() {
        return $this->integrationService;
    }

    /**
     * @param $orderNumber
     * @return DealerOrderDTO|null
     * @throws \SoapFault
     */
    public function getDealerOrder($orderNumber) {
        $sc = $this->serviceConfig;
        $sc[] = new entry("hubsoftOrderNumber", $orderNumber);
        $expDealerOrders = new exportDealerOrders($sc);
        $order = $this->integrationService->exportDealerOrders($expDealerOrders);
        $this->dump($order->getOut()->current());
        return $order->getOut()->current();
    }

    /**
     * @param $dealerOrder
     * @param array $config
     * @return mixed
     * @throws \SoapFault
     */
    public function importDealerOrder($dealerOrder, $config = []) {
        $serviceConfig = $this->serviceConfig;
        $serviceConfig[] = new entry("checkInventory", "N");
        $serviceConfig = array_merge($serviceConfig, $config);
        $client = new \SoapClient($this->integrationServiceURL, ['trace' => 1]);
        $arrayOfDealerOrderDTO = new ArrayOfDealerOrderDTO();
        $arrayOfDealerOrderDTO->setDealerOrderDTO([$dealerOrder]);
        $importDealerOrder = new importDealerOrders($arrayOfDealerOrderDTO, $serviceConfig);
        $result = $client->__soapCall('importDealerOrders', [$importDealerOrder]);
        if ($this->SoapDump) {
            $this->soapDump($client, $this->soapDumpPath);
        }
        return $result;
    }

    /**
     * @param $orderNumber
     * @return hubsoft\integrationservice\DirectOrderDTO|null
     * @throws \SoapFault
     */
    public function getDirectOrder($orderNumber) {
        $serviceConfig = $this->serviceConfig;
        $serviceConfig[] = new entry("hubsoftOrderNumber", $orderNumber);
        $directOrder = new exportDirectOrders($serviceConfig);
        $order = $this->integrationService->exportDirectOrders($directOrder);
        return $order->getOut()->current();
    }

    /**
     * @param $directOrder
     * @param array $config
     * @return mixed
     * @throws \SoapFault
     */
    public function importDirectOrder($directOrder, $config = []) {

        $serviceConfig = $this->serviceConfig;
        $serviceConfig[] = new entry("checkInventory", "N");
        $serviceConfig = array_merge($serviceConfig, $config);
        $client = new \SoapClient($this->integrationServiceURL, ['trace' => 1]);

        $arrayOfDirectOrderUpdate = new ArrayOfDirectOrderUpdateDTO();
        $arrayOfDirectOrderUpdate->setDirectOrderUpdateDTO([$directOrder]);
        $importDirectOrder = new importDirectOrders($arrayOfDirectOrderUpdate, $serviceConfig);
        $result = $client->__soapCall('importDirectOrders', [$importDirectOrder]);
        if ($this->SoapDump) {
            $this->soapDump($client, $this->soapDumpPath);
        }
        return $result;
    }

    /**
     * @param $directOrderUpdate
     * @param $config
     * @return mixed
     */
    public function importDirectOrderUpdate2($directOrderUpdate, $config = []) {
        $serviceConfig = $this->serviceConfig;
        $serviceConfig[] = new entry("checkInventory", "N");
        $serviceConfig = array_merge($serviceConfig, $config);
        $client = new \SoapClient($this->integrationServiceURL, ['trace' => 1]);
        $aod = new ArrayOfDirectOrderUpdate2DTO();
        $aod->setDirectOrderUpdate2DTO([$directOrderUpdate]);
        $importDirectOrderUpdate2 = new importDirectOrders2($aod, $serviceConfig);
        $result = $client->__soapCall('importDirectOrders2', [$importDirectOrderUpdate2]);
        if ($this->SoapDump) {
            $this->soapDump($client, $this->soapDumpPath);
        }
        return $result;
    }

    /**
     * @param $status
     * @return \hubsoft\api\integrationservice\ArrayOfDealerOrderDTO
     */
    public function getDealerOrdersByStatus($status) {
        $serviceConfig = $this->serviceConfig;
        $serviceConfig[] = new entry("orderStatus", $status);
        $expDealerOrders = new exportDealerOrders($serviceConfig);
        $orders = $this->integrationService->exportDealerOrders($expDealerOrders);
        return $orders->getOut();
    }

    /**
     * @param $status
     * @return \hubsoft\api\integrationservice\ArrayOfDirectOrderDTO
     */
    public function getDirectOrdersByStatus($status, $dateRange = []) {
        if (isset($dateRange['from'])) {

        }
        if (isset($dateRange['to'])) {

        }

        $serviceConfig = $this->serviceConfig;
        $serviceConfig[] = new entry("orderStatus", $status);
        ob_start();
        var_dump($serviceConfig);
        error_log(ob_get_clean());
        $expDirectOrders = new exportDirectOrders($serviceConfig);
        $orders = $this->integrationService->exportDirectOrders($expDirectOrders);
        if ($this->SoapDump) {
            $this->soapDump($this->integrationService, $this->soapDumpPath);
        }
        return $orders->getOut();
    }

    /**
     * @param $orderNo
     * @param $status
     * @return mixed
     * @throws \SoapFault
     */
    public function updateDealerOrderStatus($orderNo, $status) {
        try {
            $order = $this->getDealerOrder($orderNo);
            $order->setOrderStatusCode($status);
            $result = $this->importDealerOrder($order);
            return $result;
        } catch (\SoapFault $sf) {
            throw $sf;
        }
    }

    /**
     * @param $orderNo
     * @param $status
     * @return mixed
     * @throws \SoapFault
     */
    public function updateDirectOrderStatus($orderNo, $status) {
        $order = $this->getDirectOrder($orderNo);
        $order->DirectOrderDTO->orderStatusCode = $status;
        $result = $this->importDirectOrder($order);
        return $result;
    }

    /**
     * @param bool $sortByClass
     * @return array|ArrayOfProductDTO
     * @throws \Exception
     * @throws \SoapFault
     */
    public function getProducts($sortByClass = false) {
        $exProd = new exportProducts($this->serviceConfig);
        $products = $this->integrationService->exportProducts($exProd);
        return $sortByClass ? $this->sortProductsByClass($products->getOut()) : $products->getOut()->getProductDTO();
    }

    /**
     * @param ArrayOfShippingNoticeDTO $shippingUpdateList
     * @param array $config
     * @return mixed
     */
    public function importShippingUpdate(ArrayOfShippingNoticeDTO $shippingUpdateList, $config = []) {
        $client = new \SoapClient($this->integrationServiceURL, ['trace' => 1]);
        $importShippingUpdate = new importShippingUpdate($shippingUpdateList, $this->serviceConfig);
        $result = $client->__soapCall('importShippingUpdate', [$importShippingUpdate]);
        if ($this->SoapDump) {
            $this->soapDump($client, $this->soapDumpPath);
        }
        return $result;
    }

    /* ------------------------------------------------------
       Commerce Service
    --------------------------------------------------------- */

    /**
     * @return hubsoft\commerce\NameValueBean[]
     */
    public function getActivePromotions() {
        $commerceService = new CommerceService(['trace' => 1]);
        $getActivePromo = new getActivePromotions($this->apiKey);
        return $commerceService->getActivePromotions($getActivePromo)
            ->getOut()->getNameValueBean();
    }

    /**
     * @param $promotionCode
     * @return \hubsoft\api\commerce\PromotionDTO
     */
    public function getPromotionDetails($promotionCode) {
        $commerceService = new CommerceService(['trace' => 1]);
        $getPromoDetail = new getPromotionDetail($promotionCode, 0, $this->apiKey);
        $resp = $commerceService->getPromotionDetail($getPromoDetail);
        if ($this->SoapDump) {
            $this->soapDump($commerceService, $this->soapDumpPath);
        }
        return $resp->getOut();
    }

    /* ------------------------------------------------------
       Site Service
    --------------------------------------------------------- */

    public function getClassifications() {
        $siteService = new SiteService(['trace' => 1]);
        $getClassifications = new getClassifications($this->apiKey);
        return $siteService->getClassifications($getClassifications)
            ->getOut()
            ->getClassificationDTO();
    }

    /* ------------------------------------------------------
       Inventory
    --------------------------------------------------------- */

    /**
     * @return \hubsoft\api\integrationservice\ArrayOfInventoryDTO
     */
    public function getInventory() {
        $expInventory = new exportInventory($this->serviceConfig);
        $inventory = $this->integrationService->exportInventory($expInventory);
        if ($this->SoapDump) {
            $this->soapDump($this->integrationService, $this->soapDumpPath);
        }
        return $inventory->getOut();
    }

    /**
     * @param $inventory
     * @param array $config
     * @return \hubsoft\api\integrationservice\importInventoryResponse
     */
    public function importInventory($inventory, $config = []) {
        $sc = array_merge($this->serviceConfig, $config);
        $importInventory = new importInventory($inventory, $sc);
        if (!$this->soapClient)
            $this->soapClient = new \SoapClient($this->integrationServiceURL, ['trace' => 1]);

        $resp = $this->soapClient->__soapCall('importInventory', [$importInventory]);
        return $resp;
//        return $this->integrationService->importInventory($importInventory);
    }

    /* ------------------------------------------------------
       Helper methods Service
    --------------------------------------------------------- */
    public function enableSoapDump() {
        $this->SoapDump = true;
    }

    public function setSoapDumpPath($path) {
        $this->soapDumpPath = $path;
    }

    private function sortProductsByClass(ArrayOfProductDTO $products) {
        $sortedProducts = [];
        foreach ($products->getProductDTO() as $p) {
            $sortedProducts[implode("/", $p->getClassificationDesc()->getString())] = $p;
        }
        return $sortedProducts;
    }

    private function soapDump(\SoapClient $client, $path) {
        echo  $client->__getLastRequest();
        file_put_contents($path . "response.xml", $client->__getLastResponse());
        file_put_contents($path . "request.xml", $client->__getLastRequest());
    }
}