<?php
/**
 * Created by IntelliJ IDEA.
 * User: anger
 * Date: 1/11/17
 * Time: 7:15 AM
 */

namespace hubsoft\tools;


use hubsoft\api\integrationservice\DealerOrderDTO;
use hubsoft\api\integrationservice\DirectOrderDTO;
use hubsoft\api\integrationservice\DirectOrderUpdateDTO;

class OrderHelper
{
    /**
     * @param $orderItemList
     * @return array
     */
    public function sortOrderItems($orderItemList, $sortBy = []) {
        $sortedOrderItems = [];
        foreach ($orderItemList as $orderItem) {
            $sortedOrderItems[$orderItem->getProductNumber()][] = $orderItem;
        }
        return $sortedOrderItems;
    }

    /**
     * @param $orderData
     * @param $orderItemList
     */
    public function updateOrderItemShipQty($orderData, &$orderItemList) {
        foreach ($orderItemList as $orderItem) {
            $od = $orderData[$orderItem->sku];
            $orderItem->shippedQty = $od;
        }
    }

    /**
     *
     */
    public function createDirectOrder(){

    }

    /**
     * @param $order
     * @param $nPackages
     */
    public function updateDealerOrderNPackages(&$order, $nPackages) {
        $os = is_array($order->orderStoreList->OrderStoreDTO)
            ? $order->orderStoreList->OrderStoreDTO[0]
            : $order->orderStoreList->OrderStoreDTO;
        $shipInfo = is_array($os->shipmentList->ShipmentInfo)
            ? $os->shipmentList->ShipmentInfo[0]
            : $os->shipmentList->ShipmentInfo;
        $shipInfo->numberOfPackages = $nPackages;
    }

    /**
     * @param DealerOrderDTO $dealerOrder
     * @param $orderData
     * @return DealerOrderDTO
     */
    public function setupDealerOrderShip(DealerOrderDTO $dealerOrder, $orderData) {
        $orderItems = $dealerOrder->getOrderStoreList()
            ->current()
            ->getOrderItemList()
            ->getOrderItemDTO();
        foreach ($orderItems as $oi) {
            error_log($oi->getSku());
            error_log($orderData[$oi->getSku()]);
            $oi->setShippedQty($orderData[$oi->getSku()]);
        }
        $dealerOrder->getOrderStoreList()
            ->current()->getShipmentList()
            ->current()->setNumberOfPackages($orderData["num-packages"]);
        return $dealerOrder;
    }

    /**
     * @param DirectOrderDTO $directOrder
     * @param $orderData
     * @return DirectOrderUpdateDTO
     */
    public function setupDirectOrderShip(DirectOrderDTO $directOrder, $orderData) {
        $directOrderUpdate = new DirectOrderUpdateDTO();
        $directOrderUpdate->getShipmentList()
            ->setShipmentInfo($directOrder->getShipmentList());
        $directOrderUpdate->getShipmentList()
            ->current()
            ->setNumberOfPackages($orderData["num-packages"]);
        $directOrderUpdate->setHubsoftOrderNumber($directOrder->getHubsoftOrderNumber());
        $directOrderUpdate->setOrderStatusCode("Picked");
        return $directOrderUpdate;

    }
}