<?php

namespace hubsoft\tools;

define("PRODUCT_CACHE_TABLE_FILE", "hubsoft_product_cache_table");
define("PRODUCT_CACHE_FILE", "hubsoft_product_cache");

class ProductCache
{

    private $serviceConfig;
    private $productCacheTable;
    private $productCache;
    private $hubsoftAPI;

    /**
     * ProductCache constructor.
     * @param $hubsoftAPI
     */
    public function __construct(HubsoftAPI $hubsoftAPI)
    {
        $this->hubsoftAPI = $hubsoftAPI;
        $this->loadProductCache();
    }


    /**
     *
     */
    public function updateProductCache() {
        $this->productCacheTable = [];
        //do file save etc12
        $products = $this->hubsoftAPI->getProducts();

        while($product = $products->current()) {
            $productColor = $product->getProductColor()->getProductColorDTO();
            foreach($productColor as $pc) {
                $this->productCacheTable[$pc->getProductNumber()] = $pc;
            }
            $products->next();
        }

        //here we write the product cache file
        $this->writeProductCacheFile(serialize($this->productCacheTable));
        $this->writeProductCacheFile(serialize($products->getProductDTO()));
    }



    /**
     * @param $productCacheString
     * @param string $fileName
     * @return int
     */
    private function writeProductCacheFile($productCacheString, $fileName = PRODUCT_CACHE_TABLE_FILE) {
        $cacheFile = sys_get_temp_dir() . "/" . $fileName;
        $f = file_put_contents($cacheFile, $productCacheString);
        return $f;
    }

    /**
     *
     */
    public function loadProductCache() {
        $cacheTableFile = sys_get_temp_dir() . "/" . PRODUCT_CACHE_TABLE_FILE;
        $cacheFile = sys_get_temp_dir() . "/" . PRODUCT_CACHE_FILE;
        if (file_exists($cacheTableFile) && file_exists($cacheFile)) {
            $this->productCacheTable = unserialize(file_get_contents($cacheTableFile));
            $this->productCache = unserialize(file_get_contents($cacheFile));
        } else {
            error_log("Updating cache file!");
            $this->updateProductCache();
        }
    }

    /**
     *
     */
    public function getProductImage($productNumber) {
        if (array_key_exists($productNumber, $this->productCacheTable)) {
            $imgUrl = ($this->productCacheTable[$productNumber]->getImageThumbUrl() != "")
                ?  $this->productCacheTable[$productNumber]->getImageThumbUrl()
                : $this->productCacheTable[$productNumber]->getImageUrl();

            return $imgUrl;
        }
        return ""; // todo: find placeholder product image
    }

    /**
     *
     */
    public function getProduct($productNumber) {
        if (array_key_exists($productNumber, $this->productCacheTable)) {
            return $this->productCacheTable[$productNumber];
        }
        return null;
    }

}